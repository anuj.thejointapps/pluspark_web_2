$(document).ready(function(){

  $('.datatable').DataTable({
    "aLengthMenu": [
      [5, 10, 15, -1],
      [5, 10, 15, "All"]
    ],
    "iDisplayLength": 10,
    "language": {
      search: ""
    }
  });
  $("#start_date").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', startDate);
  });
  
  $("#end_date").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', startDate);
  });
})