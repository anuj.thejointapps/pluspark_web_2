$(document).ready(function(){

  $('.datatable').DataTable({
    "aLengthMenu": [
      [5, 10, 15, -1],
      [5, 10, 15, "All"]
    ],
    "iDisplayLength": 10,
    "language": {
      search: ""
    }
  });

  // Date
  $("#start_date").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', startDate);
  });
  
  $("#end_date").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', startDate);
  });

  // Date 1
  $("#start_date1").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#end_date1').datepicker('setStartDate', startDate);
  });
  
  $("#end_date1").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#start_date1').datepicker('setEndDate', startDate);
  });

  // Date 2
  $("#start_date2").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#end_date2').datepicker('setStartDate', startDate);
  });
  
  $("#end_date2").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#start_date2').datepicker('setEndDate', startDate);
  });
  $(".datepicker").datepicker({
    orientation: 'bottom',
    autoclose: true
  });

  $('.timepicker').timepicker({
    timeFormat: 'HH:mm p'
  });

  $(document).on("change",".gal-file",function(e2) {
    var img = e2.target.files[0];
    if(!iEdit.open(img, true, function(res){
      console.log(res)
      // $(e2.target).closest('div').find(".gal-result").attr("src", res1);
      $(e2.target).closest(".gal-result").css("background-image", "url(" + res + ")");
    })){
      alert("Whoops! That is not an image!");
    }
  });
  $(document).on("change",".gal-file1",function(e2) {
    var img1 = e2.target.files[0];
    if(!iEdit.open(img1, true, function(res1){
      console.log(res1)
      $(e2.target).closest('label').find(".gal-result1").attr("src", res1);
      // $(e2.target).closest(".gal-result1").css("background-image", "url(" + res1 + ")");
    })){
      alert("Whoops! That is not an image!");
    }
  });

  $('.logo-file').change(function(){
    var file = this.files[0];
    console.log(file);
    if (file){
      var reader = new FileReader();
      reader.onload = function(event){
        console.log(event.target.result);
        $('.logo-result').attr('src', event.target.result);
      }
      reader.readAsDataURL(file);
    }
  });
  $('.multiselect').multiselect({
    placeholder: "Choose"
  });
  
})