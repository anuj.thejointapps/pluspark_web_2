<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_auto_id')->index()->nullable();
            $table->string('name')->index()->nullable();
            $table->string('email')->index()->nullable();
            $table->string('password')->nullable();
            $table->string('country')->index()->nullable();
            $table->string('city')->index()->nullable();
            $table->string('contact_person_name')->index()->nullable();
            $table->bigInteger('contact_person_mobile')->index()->nullable();
            $table->date('date_of_join')->index()->nullable();
            $table->text('address')->nullable();
            $table->text('notes')->nullable();
            $table->integer('number_of_parking')->index()->default(0);
            $table->boolean('status')->index()->default(1)->comment('1 for active 0 for inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
