<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToBookingPassesQrcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_passes_qrcode', function (Blueprint $table) {
            $table->string('status', 50)->index()->default(1)->after('booking_id')->comment('1=pending, 2=confirmed, 3=cancelled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_passes_qrcode', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
