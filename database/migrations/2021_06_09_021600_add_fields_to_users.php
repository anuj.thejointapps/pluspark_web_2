<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('mobile',50)->index()->nullable()->after('email');
            $table->string('account_type')->index()->nullable()->after('mobile')->comment('1=signup, 2=facebook, 3=google, 4=twitter');
            $table->string('facebook_id')->index()->nullable()->after('account_type');
            $table->string('google_id')->index()->nullable()->after('facebook_id');
            $table->string('twitter_id')->index()->nullable()->after('google_id');
            $table->unsignedInteger('country_id')->index()->nullable()->after('twitter_id');
            $table->string('country_name', 50)->index()->nullable()->after('country_id');
            $table->unsignedInteger('state_id')->index()->nullable()->after('country_name');
            $table->string('state_name', 50)->index()->nullable()->after('state_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
