<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookingTypeToWasherBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('washer_bookings', function (Blueprint $table) {
            $table->string('booking_type')->index()->after('washer_id')->default('qr_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('washer_bookings', function (Blueprint $table) {
            $table->dropColumn('booking_type');
        });
    }
}
