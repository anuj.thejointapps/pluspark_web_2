<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrivateCodeToParkingSetupSpaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_setup_spaces', function (Blueprint $table) {
            $table->string('private_code')->index()->nullable()->after('scan_me_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_setup_spaces', function (Blueprint $table) {
            $table->dropColumn(['private_code']);
        });
    }
}
