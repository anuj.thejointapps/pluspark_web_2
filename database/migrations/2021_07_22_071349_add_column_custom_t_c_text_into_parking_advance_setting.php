<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCustomTCTextIntoParkingAdvanceSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_advance_settings', function (Blueprint $table) {
            $table->text('custom_t_and_c_text')->nullable()->after('customer_t_and_c');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_advance_settings', function (Blueprint $table) {
            $table->dropColumn(['custom_t_and_c_text']);
        });
    }
}
