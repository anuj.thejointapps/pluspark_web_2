<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranRefToPaymnes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('tran_ref')->index()->nullable()->after('payment_type');
            $table->string('tran_type')->index()->nullable()->after('tran_ref');
            $table->string('cart_id')->index()->nullable()->after('tran_type');
            $table->string('cart_description')->index()->nullable()->after('cart_id');
            $table->string('cart_currency')->index()->nullable()->after('cart_description');
            $table->string('cart_amount')->index()->nullable()->after('cart_currency');
            $table->string('callback')->index()->nullable()->after('cart_amount');
            $table->text('customer_details')->nullable()->after('callback');
            $table->text('payment_result')->nullable()->after('customer_details');
            $table->text('payment_info')->nullable()->after('payment_result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            //
        });
    }
}
