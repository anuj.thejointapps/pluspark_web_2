<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParkedDateTimeToSupervisorBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supervisor_bookings', function (Blueprint $table) {
            $table->dateTime('parked_date_time')->index()->after('requested_date_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supervisor_bookings', function (Blueprint $table) {
            $table->dropColumn('parked_date_time');
        });
    }
}
