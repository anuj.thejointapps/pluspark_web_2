<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnValetPriceIntoEventPopularSpace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_popular_places', function (Blueprint $table) {
            $table->decimal('valet_price')->index()->nullable()->after('parking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_popular_places', function (Blueprint $table) {
            $table->dropColumn(['valet_price']);
        });
    }
}
