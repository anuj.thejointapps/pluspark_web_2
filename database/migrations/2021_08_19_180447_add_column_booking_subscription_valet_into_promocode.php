<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBookingSubscriptionValetIntoPromocode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promocodes', function (Blueprint $table) {
            $table->boolean('on_booking')->index()->default(false)->after('status');
            $table->boolean('on_subscription')->index()->default(false)->after('on_booking');
            $table->boolean('on_wallet')->index()->default(false)->after('on_subscription');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promocodes', function (Blueprint $table) {
            $table->dropColumn(['on_booking', 'on_subscription', 'on_wallet']);
        });
    }
}
