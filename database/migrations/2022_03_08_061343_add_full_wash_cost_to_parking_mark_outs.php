<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFullWashCostToParkingMarkOuts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_mark_outs', function (Blueprint $table) {
            $table->string('full_wash_cost', 10)->index()->nullable()->after('set_valet_price');
            $table->string('interior_wash_cost', 10)->index()->nullable()->after('full_wash_cost');
            $table->string('exterior_wash_cost', 10)->index()->nullable()->after('interior_wash_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_mark_outs', function (Blueprint $table) {
            $table->dropColumn(['full_wash_cost', 'interior_wash_cost', 'exterior_wash_cost']);
        });
    }
}
