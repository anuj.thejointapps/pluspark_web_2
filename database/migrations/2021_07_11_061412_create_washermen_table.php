<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWashermenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('washermen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('washer_auto_id')->index();
            $table->string('name')->index();
            $table->string('city')->nullable()->index();
            $table->string('country')->nullable()->index();
            $table->bigInteger('mobile')->nullable()->index();
            $table->string('email')->nullable()->index();
            $table->string('gender')->nullable()->index();
            $table->date('date_of_join')->index()->nullable();
            $table->text('address')->nullable();
            $table->string('image_path')->nullable();
            $table->boolean('status')->index()->default(1)->comment('1 for active 0 for inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('washermen');
    }
}
