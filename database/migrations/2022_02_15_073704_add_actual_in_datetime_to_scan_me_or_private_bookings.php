<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualInDatetimeToScanMeOrPrivateBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
            $table->dateTime('actual_in_dateTime')->nullable()->after('exit_date_time')->index();
            $table->dateTime('actual_out_dateTime')->nullable()->after('actual_in_dateTime')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
            $table->dropColumn(['actual_in_dateTime', 'actual_out_dateTime']);
        });
    }
}
