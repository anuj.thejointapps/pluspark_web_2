<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPassNumberToBookingPassesQrcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_passes_qrcode', function (Blueprint $table) {
            $table->string('pass_number')->index()->after('booking_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_passes_qrcode', function (Blueprint $table) {
            $table->dropColumn('pass_number');
        });
    }
}
