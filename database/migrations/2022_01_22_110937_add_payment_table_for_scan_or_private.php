<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentTableForScanOrPrivate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_for_scan_private', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('booking_id')->index();
            $table->string('transaction_id')->index()->nullable();
            $table->string('tran_ref')->index()->nullable();
            $table->string('tran_type')->index()->nullable();
            $table->string('cart_id')->index()->nullable();
            $table->string('cart_description')->index()->nullable();
            $table->string('cart_currency')->index()->nullable();
            $table->string('cart_amount')->index()->nullable();
            $table->string('callback')->index()->nullable();
            $table->string('payment_type')->index()->nullable();
            $table->text('customer_details')->nullable();
            $table->text('payment_result')->nullable();
            $table->text('payment_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_for_scan_private');
    }
}
