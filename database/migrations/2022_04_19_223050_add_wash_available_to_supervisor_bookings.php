<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWashAvailableToSupervisorBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
             $table->boolean('wash_available')->after('valet')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
             $table->dropColumn('wash_available');
        });
    }
}
