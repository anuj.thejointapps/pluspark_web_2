<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->unsignedInteger('parking_id')->index();
            $table->unsignedInteger('booking_id')->index();
            $table->unsignedInteger('driver_id')->index();
            $table->string('key_no')->index();
            $table->string('park_no')->index();
            $table->string('pick_up_lat')->index()->nullable();
            $table->string('pick_up_long')->index()->nullable();
            $table->dateTime('arrived_date_time')->index()->nullable();
            $table->dateTime('exit_date_time')->index()->nullable();
            $table->dateTime('requested_date_time')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_bookings');
    }
}
