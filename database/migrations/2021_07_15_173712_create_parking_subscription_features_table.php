<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingSubscriptionFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_subscription_features', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parking_id')->nullable()->index();
            $table->bigInteger('parking_feature_id')->nullable()->index();
            $table->decimal('cash')->nullable();
            $table->string('visa_master')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_subscription_features');
    }
}
