<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToWasherBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('washer_bookings', function (Blueprint $table) {
             $table->string('washing_status')->after('park_no')->nullable();
             $table->dateTime('washing_start_date')->after('washing_status')->nullable();
             $table->dateTime('washing_end_date')->after('washing_start_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('washer_bookings', function (Blueprint $table) {
             $table->dropColumn(['booking_status' ,'washing_start_date', 'washing_end_date']);
        });
    }
}
