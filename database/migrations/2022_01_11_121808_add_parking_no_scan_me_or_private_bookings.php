<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParkingNoScanMeOrPrivateBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
            $table->string('parking_no')->index()->after('parking_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
            $table->dropColumn('parking_no');
        });
    }
}
