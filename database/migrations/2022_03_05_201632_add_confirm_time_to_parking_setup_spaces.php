<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmTimeToParkingSetupSpaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_setup_spaces', function (Blueprint $table) {
            $table->string('booking_confirm_time')->nullable()->index()->after('total_slot');
            $table->string('booking_cancel_edit_time')->nullable()->index()->after('booking_confirm_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_setup_spaces', function (Blueprint $table) {
            $table->dropColumn(['booking_confirm_time', 'booking_cancel_edit_time']);
        });
    }
}
