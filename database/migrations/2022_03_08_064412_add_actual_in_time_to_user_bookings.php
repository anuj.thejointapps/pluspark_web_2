<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualInTimeToUserBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_bookings', function (Blueprint $table) {
            $table->dateTime('actual_in_dateTime')->index()->nullable()->after('exit_date_time');
            $table->dateTime('actual_out_dateTime')->index()->nullable()->after('actual_in_dateTime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_bookings', function (Blueprint $table) {
            $table->dropColumn(['actual_in_dateTime', 'actual_out_dateTime']);
        });
    }
}
