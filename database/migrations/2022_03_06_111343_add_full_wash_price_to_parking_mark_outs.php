<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFullWashPriceToParkingMarkOuts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_mark_outs', function (Blueprint $table) {
            $table->string('full_wash_price', 20)->index()->nullable()->after('set_valet_price');
            $table->string('interior_wash_price', 20)->index()->nullable()->after('full_wash_price');
            $table->string('exterior_wash_price', 20)->index()->nullable()->after('interior_wash_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_mark_outs', function (Blueprint $table) {
            //
        });
    }
}
