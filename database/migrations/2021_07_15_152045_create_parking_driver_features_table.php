<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingDriverFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_driver_features', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parking_id')->nullable()->index();
            $table->bigInteger('parking_feature_id')->nullable()->index();
            $table->decimal('cash')->nullable();
            $table->string('visa_master')->nullable();
            $table->decimal('cost_of_valet')->nullable();
            $table->enum('type', ['company', 'plus_park', 'third_party']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_driver_features');
    }
}
