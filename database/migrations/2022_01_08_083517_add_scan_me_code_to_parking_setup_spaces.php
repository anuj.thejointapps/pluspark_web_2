<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScanMeCodeToParkingSetupSpaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_setup_spaces', function (Blueprint $table) {
            $table->string('scan_me_image')->index()->nullable()->after('pass_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_setup_spaces', function (Blueprint $table) {
            $table->dropColumn(['scan_me_image']);
        });
    }
}
