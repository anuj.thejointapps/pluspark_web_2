<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingForScanMeOrPrivate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scan_me_or_private_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number', 100)->index()->nullable();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('pass_id')->index()->default(0);
            $table->unsignedInteger('vehicle_id')->index()->default(0);
            $table->unsignedInteger('parking_id')->index();
            $table->unsignedInteger('promocode_id')->index()->default(0);
            $table->string('promocode', 100)->index()->nullable();
            $table->string('booking_price', 100)->index()->nullable();
            $table->string('wash_price', 100)->index()->default(0);
            $table->string('sub_total', 100)->index()->default(0);
            $table->string('paid_money', 100)->index()->default(0);
            $table->boolean('is_paid')->index()->default(false);
            $table->string('location', 200)->index()->nullable();
            $table->string('latitude', 200)->index()->nullable();
            $table->string('longitude', 200)->index()->nullable();
            $table->boolean('valet')->index()->default(false);
            $table->boolean('valet_and_washing')->index()->default(false);
            $table->string('wash_type', 20)->index()->nullable();
            $table->boolean('is_pre_book')->index()->default(false);
            $table->string('payment_method', 120)->index()->nullable();
            $table->string('booking_status', 120)->index()->nullable();
            $table->dateTime('scan_timestamp')->index()->nullable();
            $table->dateTime('arrival_date_time')->index()->nullable();
            $table->dateTime('exit_date_time')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_me_or_private_bookings');
    }
}
