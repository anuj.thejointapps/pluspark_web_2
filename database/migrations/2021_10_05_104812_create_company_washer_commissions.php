<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyWasherCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('washer_commissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('company_id')->index();
            $table->string('company_cash_comm', 2)->index()->default(0);
            $table->string('company_card_comm', 2)->index()->default(0);
            $table->string('pls_park_cash_comm', 2)->index()->default(0);
            $table->string('pls_park_card_comm', 2)->index()->default(0);
            $table->string('thrd_party_cash_comm', 2)->index()->default(0);
            $table->string('thrd_party_card_comm', 2)->index()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_washer_commissiona');
    }
}
