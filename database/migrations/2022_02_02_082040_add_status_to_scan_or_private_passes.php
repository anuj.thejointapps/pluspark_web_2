<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToScanOrPrivatePasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scan_or_private_passes', function (Blueprint $table) {
            $table->string('status', 50)->index()->default(1)->after('pass_number')->comment('1=pending, 2=confirmed, 3=cancelled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scan_or_private_passes', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
