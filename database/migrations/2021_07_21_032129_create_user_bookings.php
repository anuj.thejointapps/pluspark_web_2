<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('parking_id')->index();
            $table->string('booking_price')->index()->nullable();
            $table->string('paid_money')->index()->nullable();
            $table->string('location')->index()->nullable();
            $table->string('latitude')->index()->nullable();
            $table->string('longitude')->index()->nullable();
            $table->boolean('valet')->index()->default(false);
            $table->boolean('valet_and_washing')->index()->default(false);
            $table->string('payment_method')->index()->nullable();
            $table->dateTime('arrival_date_time')->index()->nullable();
            $table->dateTime('exit_date_time')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_bookings');
    }
}
