<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeyNoToScanMeOrPrivateBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
            $table->string('key_no')->index()->after('order_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scan_me_or_private_bookings', function (Blueprint $table) {
            $table->dropColumn('key_no');
        });
    }
}
