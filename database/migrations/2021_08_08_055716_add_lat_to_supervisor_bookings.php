<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatToSupervisorBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supervisor_bookings', function (Blueprint $table) {
            $table->string('pick_up_lat')->index()->nullable()->after('park_no');
            $table->string('pick_up_long')->index()->nullable()->after('pick_up_lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supervisor_bookings', function (Blueprint $table) {
            $table->dropColumn(['pick_up_lat', 'pick_up_long']);
        });
    }
}
