<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_parkings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parking_auto_id')->nullable()->index();
            $table->integer('company_id')->nullable()->index();
            $table->integer('parking_type')->nullable()->index();
            $table->string('name')->nullable()->index();
            $table->string('city')->nullable()->index();
            $table->string('country')->nullable()->index();
            $table->text('location')->nullable();
            $table->string('image_path')->nullable();
            $table->boolean('status')->index()->default(1)->comment('1 for active 0 for inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_parkings');
    }
}
