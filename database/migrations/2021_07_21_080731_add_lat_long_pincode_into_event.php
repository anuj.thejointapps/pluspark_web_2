<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatLongPincodeIntoEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('latitude')->index()->nullable()->after('location');
            $table->string('longitude')->index()->nullable()->after('latitude');
            $table->string('pincode')->index()->nullable()->after('longitude');
            $table->string('end_time')->index()->nullable()->after('time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn(['latitude', 'longitude', 'pincode', 'end_time']);
        });
    }
}
