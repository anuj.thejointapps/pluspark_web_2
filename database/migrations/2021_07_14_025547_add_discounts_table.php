<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('company_id')->index();
            $table->string('title')->index()->nullable();
            $table->text('description')->nullable();
            $table->string('discount_id')->index();
            $table->string('percent_discount')->index();
            $table->boolean('on_booking')->index()->default(false);
            $table->boolean('on_subscription')->index()->default(false);
            $table->boolean('on_wallet')->index()->default(false);
            $table->dateTime('start_date')->index();
            $table->dateTime('end_date')->index();
            $table->boolean('status')->index()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
