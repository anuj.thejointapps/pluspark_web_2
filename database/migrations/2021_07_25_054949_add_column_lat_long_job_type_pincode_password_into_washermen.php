<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLatLongJobTypePincodePasswordIntoWashermen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->string('latitude')->nullable()->after('address');
            $table->string('longitude')->nullable()->after('latitude');
            $table->integer('pincode')->nullable()->after('longitude');
            $table->string('password')->nullable()->after('email');
            $table->integer('job_type_id')->nullable()->after('washer_auto_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->dropColumn(['custom_t_and_c_text']);
        });
    }
}
