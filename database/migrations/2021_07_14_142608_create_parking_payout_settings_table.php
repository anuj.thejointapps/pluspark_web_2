<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingPayoutSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_payout_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parking_id')->index()->nullable();
            $table->text('address')->nullable();
            $table->string('country')->nullable()->index();
            $table->integer('pincode')->nullable()->index();
            $table->string('first_name')->nullable()->index();
            $table->string('last_name')->nullable()->index();
            $table->date('date_of_birth')->nullable()->index();
            $table->string('identification')->nullable()->index();
            $table->string('identification_photo')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_payout_settings');
    }
}
