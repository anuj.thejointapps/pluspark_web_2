<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->string('name', 100)->index();
            $table->string('code', 100)->index();
            $table->string('usage_allowed',7)->index();
            $table->string('offer_type')->default('percent_discount')->index();
            $table->string('discount')->index();
            $table->dateTime('start_date')->index();
            $table->dateTime('end_date')->index();
            $table->boolean('status')->default(true)->index();
            $table->string('added_by')->default('company')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocodes');
    }
}
