<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingMarkOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_mark_outs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parking_id')->index()->nullable();
            $table->string('parking_space_label')->nullable()->index();
            $table->text('special_access_instructions')->nullable();
            $table->string('set_available_time')->nullable()->index();
            $table->string('set_available_time_am_pm')->nullable()->index();
            $table->string('start_time')->nullable()->index();
            $table->string('start_time_am_pm')->nullable()->index();
            $table->string('end_time')->nullable()->index();
            $table->string('end_time_am_pm')->nullable()->index();
            $table->text('about_extended_time')->nullable();
            $table->string('set_book_price')->nullable()->index();
            $table->string('set_valet_price')->nullable()->index();
            $table->string('set_valet_and_washing_price')->nullable()->index();
            $table->string('method_of_payment')->nullable()->index();
            $table->string('days_space_available')->nullable()->index();
            $table->date('days_mark_space_unavailable')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_mark_outs');
    }
}
