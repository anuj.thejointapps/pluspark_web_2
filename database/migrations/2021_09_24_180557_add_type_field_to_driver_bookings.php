<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFieldToDriverBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_bookings', function (Blueprint $table) {
            $table->string('set_or_get', 10)->index()->after('driver_id')->default('set');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_bookings', function (Blueprint $table) {
            $table->dropColumn('set_or_get');
        });
    }
}
