<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentBookingIdToWashermen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->unsignedInteger('current_booking_id')->index()->after('washer_auto_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->dropColumn('current_booking_id');
        });
    }
}
