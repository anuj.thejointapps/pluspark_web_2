<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsDeviceIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('device_type')->index()->nullable()->after('password')->comment('1=android, 2=ios');
            $table->string('device_id')->index()->nullable()->after('device_type');
            $table->string('mobile_code')->index()->nullable()->before('mobile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['device_type', 'device_id', 'mobile_code']);
        });
    }
}
