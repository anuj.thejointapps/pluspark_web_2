<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->string('plate_number')->index();
            $table->unsignedInteger('brand_id')->index();
            $table->string('brand_name')->index();
            $table->unsignedInteger('modal_id')->index();
            $table->string('modal_name')->index();
            $table->unsignedInteger('color_id')->index();
            $table->string('color_name')->index();
            $table->string('image')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_vehicles');
    }
}
