<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingAdvanceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_advance_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parking_id')->index()->nullable();
            $table->boolean('discounts')->index()->default(1)->comment('1 for on 0 for off');
            $table->string('private_space_link')->index()->nullable();
            $table->boolean('private_space')->index()->default(1)->comment('1 for on 0 for off');
            $table->string('hidden_space_link')->index()->nullable();
            $table->boolean('hidden_space')->index()->default(1)->comment('1 for on 0 for off');
            $table->boolean('customer_t_and_c')->index()->default(1)->comment('1 for on 0 for off');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_advance_settings');
    }
}
