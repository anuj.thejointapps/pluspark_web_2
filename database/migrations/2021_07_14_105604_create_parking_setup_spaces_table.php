<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingSetupSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_setup_spaces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parking_id')->index()->nullable();
            $table->string('space_headline')->index()->nullable();
            $table->integer('space_type')->index()->nullable();
            $table->string('pass_type')->nullable();
            $table->text('location')->nullable();
            $table->string('parker_book_space')->index()->nullable();
            $table->boolean('valet_service_available')->index()->default(1)->comment('1 for on 0 for off');
            $table->boolean('washer_service_available')->index()->default(1)->comment('1 for on 0 for off');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_setup_spaces');
    }
}
