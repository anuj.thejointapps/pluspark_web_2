<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentParkingId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->unsignedInteger('current_parking_id')->after('company_id')->index()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->dropColumn(['current_parking_id']);
        });
    }
}
