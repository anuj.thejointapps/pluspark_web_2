<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFieldToSupervisorBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supervisor_bookings', function (Blueprint $table) {
            $table->string('set_or_get', 10)->index()->after('supervisor_id')->default('set');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supervisor_bookings', function (Blueprint $table) {
            $table->dropColumn('set_or_get');
        });
    }
}
