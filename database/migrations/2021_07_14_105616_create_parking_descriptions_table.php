<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_descriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parking_id')->index()->nullable();
            $table->text('description')->nullable();
            $table->text('idle_page_for_people')->nullable();
            $table->string('select_vehicle_type')->nullable()->index();
            $table->text('select_space_features')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_descriptions');
    }
}
