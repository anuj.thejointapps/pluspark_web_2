<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatLongToUserSearchPlace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_search_places', function (Blueprint $table) {
            $table->string('latitude')->index()->after('search_place')->nullable();
            $table->string('longitude')->index()->after('latitude')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_search_places', function (Blueprint $table) {
            $table->dropColumn(['latitude', 'longitude']);
        });
    }
}
