<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCompanyIdIntoWasher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->integer('company_id')->index()->nullable()->after('washer_auto_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('washermen', function (Blueprint $table) {
            $table->dropColumn(['company_id']);
        });
    }
}
