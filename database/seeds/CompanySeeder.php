<?php

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Address Types
        $credentials =  [
            'name'              => 'Company',
            'email'             => 'admin@pluspark.com',
            'password'          => Hash::make('admin@pluspark'),
        ];

        $companies = Company::all();
        if($companies->isEmpty()) {
            Company::create($credentials);
        } else {
            $companies->first()->update($credentials);
        }
    }
}
