<?php

use App\Models\DiscountPercent;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DiscountPercentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize discount Percent
        $percents =  [1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,45,50];

        # Store Data to model
        foreach ($percents as $key => $percent) {
            $discountpercent = DiscountPercent::where('discount', $percent)->get();
            if($discountpercent->isEmpty()) {
                DiscountPercent::create(['discount' => $percent]);
            } else {
                $discountpercent->first()->update(['discount' => $percent]);
            }
        }
    }
}
