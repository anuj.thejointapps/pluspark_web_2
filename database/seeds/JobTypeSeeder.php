<?php

use Illuminate\Database\Seeder;
use App\Models\JobType;

class JobTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobTypes = [
            'Supervisor',
            'Driver',
            'Washer',
        ];

        # Store Data to model
        foreach ($jobTypes as $key => $jobType) {
            $jobTypeAvail = JobType::where('name', $jobType)->get();
            if($jobTypeAvail->isEmpty()) {
                JobType::create(['name' => $jobType]);
            }
        }
    }
}
