<?php

use App\Models\SpaceType;
use Illuminate\Database\Seeder;

class SpaceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $spaceTypes = [
            'Driverway(1-5 spaces)', 
            'Courtyard(1-5 spaces)', 
            'Private Residential Garage', 
            'Small, Private Car Park', 
            'Visitor Parking Space', 
            'Apartment, Condominium Car Park', 
            'Private Residential Car Space', 
            'Storage Facility(truck, buses, shipping containers)', 
            'Sports venue or stadium car park', 
            'Restaurant/bar/hotel/casino car park', 
            'Airport Car Park', 
            'Vacant land', 
            'Goverment or concil car park', 
            'Shopping mall or retail car park', 
            'Mosque/church/synogogue car park', 
            'Hospital/medical center park', 
            'School/College/University park', 
            'Private/Residential Car Spaces', 
            'Single boot morning or jetty', 
            'Helipad' 
        ];

        # Store Data to model
        foreach ($spaceTypes as $key => $spaceType) {
            $spaceTypeAvail = SpaceType::where('name', $spaceType)->get();
            if($spaceTypeAvail->isEmpty()) {
                SpaceType::create(['name' => $spaceType]);
            }
        }
    }
}
