<?php

use App\Models\Color;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = [
           '#000000' => 'Black ',
            '#0C090A' => 'Night',
            '#34282C' => 'Charcoal',
            '#3B3131' => 'Oil',
            '#3A3B3C' => 'Dark Gray',
            '#413839' => 'Black Cat',
            '#3D3C3A' => 'Iridium',
            '#463E3F' => 'Black Eel',
            '#4C4646' => 'Black Cow',
            '#504A4B' => 'Gray Wolf',
            '#565051' => 'Vampire Gray',
            '#52595D' => 'Iron Gray',
            '#5C5858' => 'Gray Dolphin',
            '#625D5D' => 'Carbon Gray',
            '#666362' => 'Ash Gray',
            '#6D6968' => 'Cloudy Gray',
            '#696969' => 'DimGray or DimGrey ',
            '#726E6D' => 'Smokey Gray',
            '#736F6E' => 'Alien Gray',
            '#757575' => 'Sonic Silver',
            '#797979' => 'Platinum Gray',
            '#837E7C' => 'Granite',
            '#808080' => 'Gray or Grey ',
            '#848482' => 'Battleship Gray',
            '#A9A9A9' => 'DarkGray or DarkGrey ',
            '#B6B6B4' => 'Gray Cloud',
            '#C0C0C0' => 'Silver ',
            '#C9C0BB' => 'Pale Silver',
            '#D1D0CE' => 'Gray Goose',
            '#CECECE' => 'Platinum Silver',
            '#D3D3D3' => 'LightGray or LightGrey ',
            '#DCDCDC' => 'Gainsboro ',
            '#E5E4E2' => 'Platinum',
            '#BCC6CC' => 'Metallic Silver',
            '#98AFC7' => 'Blue Gray',
            '#838996' => 'Roman Silver',
            '#778899' => 'LightSlateGray or LightSlateGrey ',
            '#708090' => 'SlateGray or SlateGrey ',
            '#6D7B8D' => 'Rat Gray',
            '#657383' => 'Slate Granite Gray',
            '#616D7E' => 'Jet Gray',
            '#646D7E' => 'Mist Blue',
            '#566D7E' => 'Marble Blue',
            '#737CA1' => 'Slate Blue Grey',
            '#728FCE' => 'Light Purple Blue',
            '#4863A0' => 'Azure Blue',
            '#2B547E' => 'Blue Jay',
            '#29465B' => 'Dark Blue Grey',
            '#2B3856' => 'Dark Slate',
            '#123456' => 'Deep Sea Blue',
            '#151B54' => 'Night Blue',
            '#191970' => 'MidnightBlue ',
            '#000080' => 'Navy ',
            '#151B8D' => 'Denim Dark Blue',
            '#00008B' => 'DarkBlue ',
            '#15317E' => 'Lapis Blue',
            '#0000A0' => 'New Midnight Blue',
            '#0000A5' => 'Earth Blue',
            '#0020C2' => 'Cobalt Blue',
            '#0000CD' => 'MediumBlue ',
            '#0041C2' => 'Blueberry Blue',
            '#2916F5' => 'Canary Blue',
            '#0000FF' => 'Blue ',
            '#0909FF' => 'Bright Blue',
            '#1F45FC' => 'Blue Orchid',
            '#2554C7' => 'Sapphire Blue',
            '#1569C7' => 'Blue Eyes',
            '#1974D2' => 'Bright Navy Blue',
            '#2B60DE' => 'Balloon Blue',
            '#4169E1' => 'RoyalBlue ',
            '#2B65EC' => 'Ocean Blue',
            '#306EFF' => 'Blue Ribbon',
            '#157DEC' => 'Blue Dress',
            '#1589FF' => 'Neon Blue',
            '#1E90FF' => 'DodgerBlue ',
            '#368BC1' => 'Glacial Blue Ice',
            '#4682B4' => 'SteelBlue ',
            '#488AC7' => 'Silk Blue',
            '#357EC7' => 'Windows Blue',
            '#3090C7' => 'Blue Ivy',
            '#659EC7' => 'Blue Koi',
            '#87AFC7' => 'Columbia Blue',
            '#95B9C7' => 'Baby Blue',
            '#6495ED' => 'CornflowerBlue ',
            '#6698FF' => 'Sky Blue Dress',
            '#56A5EC' => 'Iceberg',
            '#38ACEC' => 'Butterfly Blue',
            '#00BFFF' => 'DeepSkyBlue ',
            '#3BB9FF' => 'Midday Blue',
            '#5CB3FF' => 'Crystal Blue',
            '#79BAEC' => 'Denim Blue',
            '#82CAFF' => 'Day Sky Blue',
            '#87CEFA' => 'LightSkyBlue ',
            '#87CEEB' => 'SkyBlue ',
            '#A0CFEC' => 'Jeans Blue',
            '#B7CEEC' => 'Blue Angel',
            '#B4CFEC' => 'Pastel Blue',
            '#ADDFFF' => 'Light Day Blue',
            '#C2DFFF' => 'Sea Blue',
            '#C6DEFF' => 'Heavenly Blue',
            '#BDEDFF' => 'Robin Egg Blue',
            '#B0E0E6' => 'PowderBlue ',
            '#AFDCEC' => 'Coral Blue',
            '#ADD8E6' => 'LightBlue ',
            '#B0CFDE' => 'LightSteelBlue ',
            '#C9DFEC' => 'Gulf Blue',
            '#E3E4FA' => 'Lavender Blue',
            '#E6E6FA' => 'Lavender ',
            '#EBF4FA' => 'Water',
            '#F0F8FF' => 'AliceBlue ',
            '#F8F8FF' => 'GhostWhite ',
            '#F0FFFF' => 'Azure ',
            '#E0FFFF' => 'LightCyan ',
            '#CCFFFF' => 'Light Slate',
            '#9AFEFF' => 'Electric Blue',
            '#7DFDFE' => 'Tron Blue',
            '#57FEFF' => 'Blue Zircon',
            '#00FFFF' => 'Aqua or Cyan ',
            '#0AFFFF' => 'Bright Cyan',
            '#50EBEC' => 'Celeste',
            '#4EE2EC' => 'Blue Diamond',
            '#16E2F5' => 'Bright Turquoise',
            '#8EEBEC' => 'Blue Lagoon',
            '#AFEEEE' => 'PaleTurquoise ',
            '#CFECEC' => 'Pale Blue Lily',
            '#81D8D0' => 'Tiffany Blue',
            '#77BFC7' => 'Blue Hosta',
            '#92C7C7' => 'Cyan Opaque',
            '#78C7C7' => 'Northern Lights Blue',
            '#7BCCB5' => 'Blue Green',
            '#66CDAA' => 'MediumAquaMarine ',
            '#7FFFD4' => 'Aquamarine ',
            '#93FFE8' => 'Light Aquamarine',
            '#40E0D0' => 'Turquoise ',
            '#48D1CC' => 'MediumTurquoise ',
            '#48CCCD' => 'Deep Turquoise',
            '#46C7C7' => 'Jellyfish',
            '#43C6DB' => 'Blue Turquoise',
            '#00CED1' => 'DarkTurquoise ',
            '#43BFC7' => 'Macaw Blue Green',
            '#20B2AA' => 'LightSeaGreen ',
            '#3EA99F' => 'Seafoam Green',
            '#5F9EA0' => 'CadetBlue ',
            '#3B9C9C' => 'Deep Sea',
            '#008B8B' => 'DarkCyan ',
            '#008080' => 'Teal ',
            '#045F5F' => 'Medium Teal',
            '#033E3E' => 'Deep Teal',
            '#25383C' => 'DarkSlateGray or DarkSlateGrey ',
            '#2C3539' => 'Gunmetal',
            '#3C565B' => 'Blue Moss Green',
            '#4C787E' => 'Beetle Green',
            '#5E7D7E' => 'Grayish Turquoise',
            '#307D7E' => 'Greenish Blue',
            '#348781' => 'Aquamarine Stone',
            '#438D80' => 'Sea Turtle Green',
            '#4E8975' => 'Dull Sea Green',
            '#306754' => 'Deep Sea Green',
            '#2E8B57' => 'SeaGreen ',
            '#31906E' => 'Dark Mint',
            '#34A56F' => 'Earth Green',
            '#50C878' => 'Emerald',
            '#3EB489' => 'Mint',
            '#3CB371' => 'MediumSeaGreen ',
            '#78866B' => 'Camouflage Green',
            '#848B79' => 'Sage Green',
            '#617C58' => 'Hazel Green',
            '#728C00' => 'Venom Green',
            '#6B8E23' => 'OliveDrab ',
            '#808000' => 'Olive ',
            '#556B2F' => 'DarkOliveGreen ',
            '#4B5320' => 'Army Green',
            '#667C26' => 'Fern Green',
            '#4E9258' => 'Fall Forest Green',
            '#387C44' => 'Pine Green',
            '#347235' => 'Medium Forest Green',
            '#347C2C' => 'Jungle Green',
            '#228B22' => 'ForestGreen ',
            '#008000' => 'Green ',
            '#006400' => 'DarkGreen ',
            '#046307' => 'Deep Emerald Green',
            '#254117' => 'Dark Forest Green',
            '#437C17' => 'Seaweed Green',
            '#347C17' => 'Shamrock Green',
            '#6AA121' => 'Green Onion',
            '#4AA02C' => 'Green Pepper',
            '#41A317' => 'Dark Lime Green',
            '#12AD2B' => 'Parrot Green',
            '#3EA055' => 'Clover Green',
            '#73A16C' => 'Dinosaur Green',
            '#6CBB3C' => 'Green Snake',
            '#6CC417' => 'Alien Green',
            '#4CC417' => 'Green Apple',
            '#32CD32' => 'LimeGreen ',
            '#52D017' => 'Pea Green',
            '#4CC552' => 'Kelly Green',
            '#54C571' => 'Zombie Green',
            '#99C68E' => 'Frog Green',
            '#8FBC8F' => 'DarkSeaGreen ',
            '#89C35C' => 'Green Peas',
            '#85BB65' => 'Dollar Bill Green',
            '#9CB071' => 'Iguana Green',
            '#B0BF1A' => 'Acid Green',
            '#B2C248' => 'Avocado Green',
            '#9DC209' => 'Pistachio Green',
            '#A1C935' => 'Salad Green',
            '#9ACD32' => 'YellowGreen ',
            '#77DD77' => 'Pastel Green',
            '#7FE817' => 'Hummingbird Green',
            '#59E817' => 'Nebula Green',
            '#57E964' => 'Stoplight Go Green',
            '#16F529' => 'Neon Green',
            '#5EFB6E' => 'Jade Green',
            '#36F57F' => 'Lime Mint Green',
            '#00FF7F' => 'SpringGreen ',
            '#00FA9A' => 'MediumSpringGreen ',
            '#5FFB17' => 'Emerald Green',
            '#00FF00' => 'Lime ',
            '#7CFC00' => 'LawnGreen ',
            '#66FF00' => 'Bright Green',
            '#7FFF00' => 'Chartreuse ',
            '#87F717' => 'Yellow Lawn Green',
            '#98F516' => 'Aloe Vera Green',
            '#B1FB17' => 'Dull Green Yellow',
            '#ADFF2F' => 'GreenYellow ',
            '#BDF516' => 'Chameleon Green',
            '#E2F516' => 'Yellow Green Grosbeak',
            '#CCFB5D' => 'Tea Green',
            '#BCE954' => 'Slime Green',
            '#64E986' => 'Algae Green',
            '#90EE90' => 'LightGreen ',
            '#6AFB92' => 'Dragon Green',
            '#98FB98' => 'PaleGreen ',
            '#98FF98' => 'Mint Green',
            '#B5EAAA' => 'Green Thumb',
            '#E3F9A6' => 'Organic Brown',
            '#C3FDB8' => 'Light Jade',
            '#DBF9DB' => 'Light Rose Green',
            '#F0FFF0' => 'HoneyDew ',
            '#F5FFFA' => 'MintCream ',
            '#FFFACD' => 'LemonChiffon ',
            '#FFFFC2' => 'Parchment',
            '#FFFFCC' => 'Cream',
            '#FAFAD2' => 'LightGoldenRodYellow ',
            '#FFFFE0' => 'LightYellow ',
            '#F5F5DC' => 'Beige ',
            '#FFF8DC' => 'Cornsilk ',
            '#FBF6D9' => 'Blonde',
            '#F7E7CE' => 'Champagne',
            '#FAEBD7' => 'AntiqueWhite ',
            '#FFEFD5' => 'PapayaWhip ',
            '#FFEBCD' => 'BlanchedAlmond ',
            '#FFE4C4' => 'Bisque ',
            '#F5DEB3' => 'Wheat ',
            '#FFE4B5' => 'Moccasin ',
            '#FFE5B4' => 'Peach',
            '#FED8B1' => 'Light Orange',
            '#FFDAB9' => 'PeachPuff ',
            '#FFDEAD' => 'NavajoWhite ',
            '#F3E3C3' => 'Golden Silk',
            '#F3E5AB' => 'Vanilla',
            '#ECE5B6' => 'Tan Brown',
            '#EEE8AA' => 'PaleGoldenRod ',
            '#F0E68C' => 'Khaki ',
            '#EDDA74' => 'Cardboard Brown',
            '#EDE275' => 'Harvest Gold',
            '#FFE87C' => 'Sun Yellow',
            '#FFF380' => 'Corn Yellow',
            '#FFFF00' => 'Yellow ',
            '#FFEF00' => 'Canary Yellow',
            '#F5E216' => 'Banana Yellow',
            '#FFDB58' => 'Mustard',
            '#FFDF00' => 'Golden Yellow',
            '#F9DB24' => 'Bold Yellow',
            '#FFD801' => 'Rubber Ducky Yellow',
            '#FFD700' => 'Gold ',
            '#FDD017' => 'Bright Gold',
            '#EAC117' => 'Golden Brown',
            '#F6BE00' => 'Deep Yellow',
            '#F2BB66' => 'Macaroni and Cheese',
            '#FBB917' => 'Saffron',
            '#FBB117' => 'Beer',
            '#FFAE42' => 'Yellow Orange or Orange Yellow',
            '#FFA62F' => 'Cantaloupe',
            '#FFA500' => 'Orange ',
            '#EE9A4D' => 'Brown Sand',
            '#F4A460' => 'SandyBrown ',
            '#E2A76F' => 'Brown Sugar',
            '#C19A6B' => 'Camel Brown',
            '#E6BF83' => 'Deer Brown',
            '#DEB887' => 'BurlyWood ',
            '#D2B48C' => 'Tan ',
            '#C8AD7F' => 'Light French Beige',
            '#C2B280' => 'Sand',
            '#BCB88A' => 'Sage',
            '#C8B560' => 'Fall Leaf Brown',
            '#C9BE62' => 'Ginger Brown',
            '#BDB76B' => 'DarkKhaki ',
            '#BAB86C' => 'Olive Green',
            '#B5A642' => 'Brass',
            '#C7A317' => 'Cookie Brown',
            '#D4AF37' => 'Metallic Gold',
            '#E9AB17' => 'Bee Yellow',
            '#E8A317' => 'School Bus Yellow',
            '#DAA520' => 'GoldenRod ',
            '#D4A017' => 'Orange Gold',
            '#C68E17' => 'Caramel',
            '#B8860B' => 'DarkGoldenRod ',
            '#C58917' => 'Cinnamon',
            '#CD853F' => 'Peru ',
            '#CD7F32' => 'Bronze',
            '#C88141' => 'Tiger Orange',
            '#B87333' => 'Copper',
            '#966F33' => 'Wood',
            '#806517' => 'Oak Brown',
            '#665D1E' => 'Antique Bronze',
            '#8E7618' => 'Hazel',
            '#8B8000' => 'Dark Yellow',
            '#827839' => 'Dark Moccasin',
            '#AF9B60' => 'Bullet Shell',
            '#827B60' => 'Army Brown',
            '#786D5F' => 'Sandstone',
            '#483C32' => 'Taupe',
            '#493D26' => 'Mocha',
            '#513B1C' => 'Milk Chocolate',
            '#3D3635' => 'Gray Brown',
            '#3B2F2F' => 'Dark Coffee',
            '#43302E' => 'Old Burgundy',
            '#5C3317' => 'Bakers Brown',
            '#654321' => 'Dark Brown',
            '#6F4E37' => 'Coffee',
            '#835C3B' => 'Brown Bear',
            '#7F5217' => 'Red Dirt',
            '#7F462C' => 'Sepia',
            '#A0522D' => 'Sienna ',
            '#8B4513' => 'SaddleBrown ',
            '#8A4117' => 'Dark Sienna',
            '#7E3817' => 'Sangria',
            '#7E3517' => 'Blood Red',
            '#954535' => 'Chestnut',
            '#C34A2C' => 'Chestnut Red',
            '#C04000' => 'Mahogany',
            '#C35817' => 'Red Fox',
            '#B86500' => 'Dark Bisque',
            '#B5651D' => 'Light Brown',
            '#C36241' => 'Rust',
            '#CB6D51' => 'Copper Red',
            '#C47451' => 'Orange Salmon',
            '#D2691E' => 'Chocolate ',
            '#CC6600' => 'Sedona',
            '#E56717' => 'Papaya Orange',
            '#E66C2C' => 'Halloween Orange',
            '#FF6700' => 'Neon Orange',
            '#FF5F1F' => 'Bright Orange',
            '#F87217' => 'Pumpkin Orange',
            '#F88017' => 'Carrot Orange',
            '#FF8C00' => 'DarkOrange ',
            '#F87431' => 'Construction Cone Orange',
            '#E67451' => 'Sunrise Orange',
            '#FF8040' => 'Mango Orange',
            '#FF7F50' => 'Coral ',
            '#F88158' => 'Basket Ball Orange',
            '#F9966B' => 'Light Salmon Rose',
            '#FFA07A' => 'LightSalmon ',
            '#E9967A' => 'DarkSalmon ',
            '#E78A61' => 'Tangerine',
            '#DA8A67' => 'Light Copper',
            '#FA8072' => 'Salmon ',
            '#F08080' => 'LightCoral ',
            '#E77471' => 'Pink Coral',
            '#F75D59' => 'Bean Red',
            '#E55451' => 'Valentine Red',
            '#CD5C5C' => 'IndianRed ',
            '#FF6347' => 'Tomato ',
            '#E55B3C' => 'Shocking Orange',
            '#FF4500' => 'OrangeRed ',
            '#FF0000' => 'Red ',
            '#FF2400' => 'Scarlet',
            '#F62217' => 'Ruby Red',
            '#F70D1A' => 'Ferrari Red',
            '#F62817' => 'Fire Engine Red',
            '#E42217' => 'Lava Red',
            '#E41B17' => 'Love Red',
            '#DC381F' => 'Grapefruit',
            '#C24641' => 'Cherry Red',
            '#C11B17' => 'Chilli Pepper',
            '#B22222' => 'FireBrick ',
            '#A52A2A' => 'Brown ',
            '#A70D2A' => 'Carbon Red',
            '#9F000F' => 'Cranberry',
            '#990012' => 'Red Wine or Wine Red',
            '#8B0000' => 'DarkRed ',
            '#800000' => 'Maroon ',
            '#8C001A' => 'Burgundy',
            '#800517' => 'Deep Red',
            '#660000' => 'Red Blood',
            '#551606' => 'Blood Night',
            '#3D0C02' => 'Black Bean',
            '#3F000F' => 'Chocolate Brown',
            '#2B1B17' => 'Midnight',
            '#550A35' => 'Purple Lily',
            '#810541' => 'Purple Maroon',
            '#7D0541' => 'Plum Pie',
            '#7D0552' => 'Plum Velvet',
            '#7E354D' => 'Velvet Maroon',
            '#7F4E52' => 'Rosy Finch',
            '#7F525D' => 'Dull Purple',
            '#7F5A58' => 'Puce',
            '#997070' => 'Rose Dust',
            '#B38481' => 'Rosy Pink',
            '#BC8F8F' => 'RosyBrown ',
            '#C5908E' => 'Khaki Rose',
            '#C48189' => 'Pink Brown',
            '#C48793' => 'Lipstick Pink',
            '#E8ADAA' => 'Rose',
            '#C4AEAD' => 'Silver Pink',
            '#ECC5C0' => 'Rose Gold',
            '#FFCBA4' => 'Deep Peach',
            '#EDC9AF' => 'Desert Sand',
            '#FFDDCA' => 'Unbleached Silk',
            '#FDD7E4' => 'Pig Pink',
            '#FFDFDD' => 'Pink Bubble Gum',
            '#FFE4E1' => 'MistyRose ',
            '#FFCCCB' => 'Light Red',
            '#FBCFCD' => 'Light Rose',
            '#FBBBB9' => 'Deep Rose',
            '#FFC0CB' => 'Pink ',
            '#FFB6C1' => 'LightPink ',
            '#FAAFBE' => 'Donut Pink',
            '#FAAFBA' => 'Baby Pink',
            '#F9A7B0' => 'Flamingo Pink',
            '#E7A1B0' => 'Pink Rose',
            '#E799A3' => 'Pink Daisy',
            '#E38AAE' => 'Cadillac Pink',
            '#F778A1' => 'Carnation Pink',
            '#E56E94' => 'Blush Red',
            '#DB7093' => 'PaleVioletRed ',
            '#D16587' => 'Purple Pink',
            '#C25A7C' => 'Tulip Pink',
            '#C25283' => 'Bashful Pink',
            '#E75480' => 'Dark Pink',
            '#F660AB' => 'Dark Hot Pink',
            '#FF69B4' => 'HotPink ',
            '#FC6C85' => 'Watermelon Pink',
            '#F6358A' => 'Violet Red',
            '#F52887' => 'Hot Deep Pink',
            '#FF1493' => 'DeepPink ',
            '#F535AA' => 'Neon Pink',
            '#E45E9D' => 'Pink Cupcake',
            '#E3319D' => 'Dimorphotheca Magenta',
            '#E4287C' => 'Pink Lemonade',
            '#DC143C' => 'Crimson ',
            '#C32148' => 'Bright Maroon',
            '#C21E56' => 'Rose Red',
            '#C12869' => 'Rogue Pink',
            '#C12267' => 'Burnt Pink',
            '#CA226B' => 'Pink Violet',
            '#C71585' => 'MediumVioletRed ',
            '#C12283' => 'Dark Carnation Pink',
            '#B93B8F' => 'Pink Plum',
            '#DA70D6' => 'Orchid ',
            '#DF73D4' => 'Deep Mauve',
            '#EE82EE' => 'Violet ',
            '#F433FF' => 'Bright Neon Pink',
            '#FF00FF' => 'Fuchsia or Magenta ',
            '#E238EC' => 'Crimson Purple',
            '#D462FF' => 'Heliotrope Purple',
            '#C45AEC' => 'Tyrian Purple',
            '#BA55D3' => 'MediumOrchid ',
            '#A74AC7' => 'Purple Flower',
            '#B048B5' => 'Orchid Purple',
            '#915F6D' => 'Mauve Taupe',
            '#7E587E' => 'Viola Purple',
            '#614051' => 'Eggplant',
            '#583759' => 'Plum Purple',
            '#5E5A80' => 'Grape',
            '#4E5180' => 'Purple Navy',
            '#6A5ACD' => 'SlateBlue ',
            '#6960EC' => 'Blue Lotus',
            '#736AFF' => 'Light Slate Blue',
            '#7B68EE' => 'MediumSlateBlue ',
            '#6C2DC7' => 'Purple Amethyst',
            '#6A0DAD' => 'Bright Purple',
            '#483D8B' => 'DarkSlateBlue ',
            '#4E387E' => 'Purple Haze',
            '#571B7E' => 'Purple Iris',
            '#4B0150' => 'Dark Purple',
            '#36013F' => 'Deep Purple',
            '#461B7E' => 'Purple Monster',
            '#4B0082' => 'Indigo ',
            '#342D7E' => 'Blue Whale',
            '#663399' => 'RebeccaPurple ',
            '#6A287E' => 'Purple Jam',
            '#8B008B' => 'DarkMagenta ',
            '#800080' => 'Purple ',
            '#86608E' => 'French Lilac',
            '#9932CC' => 'DarkOrchid ',
            '#9400D3' => 'DarkViolet ',
            '#8D38C9' => 'Purple Violet',
            '#A23BEC' => 'Jasmine Purple',
            '#B041FF' => 'Purple Daffodil',
            '#842DCE' => 'Clemantis Violet',
            '#8A2BE2' => 'BlueViolet ',
            '#7A5DC7' => 'Purple Sage Bush',
            '#7F38EC' => 'Lovely Purple',
            '#8E35EF' => 'Purple Plum',
            '#893BFF' => 'Aztech Purple',
            '#967BB6' => 'Lavender Purple',
            '#9370DB' => 'MediumPurple ',
            '#8467D7' => 'Light Purple',
            '#9172EC' => 'Crocus Purple',
            '#9E7BFF' => 'Purple Mimosa',
            '#DCD0FF' => 'Pale Lilac',
            '#E0B0FF' => 'Mauve',
            '#D891EF' => 'Bright Lilac',
            '#B666D2' => 'Rich Lilac',
            '#C38EC7' => 'Purple Dragon',
            '#C8A2C8' => 'Lilac',
            '#DDA0DD' => 'Plum ',
            '#E6A9EC' => 'Blush Pink',
            '#F9B7FF' => 'Blossom Pink',
            '#C6AEC7' => 'Wisteria Purple',
            '#D2B9D3' => 'Purple Thistle',
            '#D8BFD8' => 'Thistle ',
            '#E9CFEC' => 'Periwinkle',
            '#FCDFFF' => 'Cotton Candy',
            '#EBDDE2' => 'Lavender Pinocchio',
            '#E9E4D4' => 'Ash White',
            '#EDE6D6' => 'White Chocolate',
            '#FAF0DD' => 'Soft Ivory',
            '#F8F0E3' => 'Off White',
            '#FFF0F5' => 'LavenderBlush ',
            '#FDEEF4' => 'Pearl',
            '#FFF9E3' => 'Egg Shell',
            '#FDF5E6' => 'OldLace ',
            '#FAF0E6' => 'Linen ',
            '#FFF5EE' => 'SeaShell ',
            '#FAF5EF' => 'Rice',
            '#FFFAF0' => 'FloralWhite ',
            '#FFFFF0' => 'Ivory ',
            '#FFFFF7' => 'Light White',
            '#F5F5F5' => 'WhiteSmoke ',
            '#FBFBF9' => 'Cotton',
            '#FFFAFA' => 'Snow ',
            '#FEFCFF' => 'Milk White',
            '#FFFFFF' => 'White ',
        ];

        # Store Data to model
        foreach ($colors as $key => $color) {
            $colorAvail = Color::where('name', $color)->get();
            if($colorAvail->isEmpty()) {
                Color::create(['name' => $color, 'code' => $key, 'status' => true]);
            } else {
                $colorAvail->first()->update(['name' => $color, 'code' => $key, 'status' => true]);
            }
        }
    }
}
