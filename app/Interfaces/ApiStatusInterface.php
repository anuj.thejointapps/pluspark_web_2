<?php

namespace App\Interfaces;

interface ApiStatusInterface {
	const OK 			= 200;
	const NOT_FOUND 	= 404;
	const BAD_REQUEST  	= 400;
	const UNAUTHORIZED 	= 401;
}