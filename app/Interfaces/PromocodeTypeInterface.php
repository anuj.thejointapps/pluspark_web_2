<?php

namespace App\Interfaces;

interface PromocodeTypeInterface {
	const FLAT 		= 'flat';
	const PERCENT 	= 'percent';
}