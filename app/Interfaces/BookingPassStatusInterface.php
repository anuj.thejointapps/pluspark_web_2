<?php

namespace App\Interfaces;

interface BookingPassStatusInterface {
	const PENDING = 1;
	const CONFIRMED  = 2;
	const CANCELLED  = 3;
	const EXPIRED = 4;
}