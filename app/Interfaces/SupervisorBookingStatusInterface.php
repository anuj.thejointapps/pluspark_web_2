<?php

namespace App\Interfaces;

interface SupervisorBookingStatusInterface {
	const INPROGRESS = 1;
	const COMPLETED  = 2;
	const ON_THE_WAY_TO_PARK  = 3;
	const PARKED  = 4;
	const GET_CAR_REQUEST  = 5;
	const ON_THE_WAY_TO_DROP = 6;
   	const ARRIVED = 7;
   	const BILL_GENERATED = 8;
}