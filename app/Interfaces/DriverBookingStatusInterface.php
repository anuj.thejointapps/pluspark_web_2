<?php

namespace App\Interfaces;

interface DriverBookingStatusInterface {
	const INPROGRESS = 1;
	const COMPLETED  = 2;
	const ON_THE_WAY_TO_PARK  = 3;
	const PARKED  = 4;
	const GET_CAR_REQUESTD  = 5;
	const ON_THE_WAY_TO_DROPD = 6;
   	const ARRIVEDD = 7;
   	const BILL_GENERATEDD = 8;
}