<?php

namespace App\Interfaces;

interface UserBookingStatusInterface {
	const INITIAL = 1;
	const ON_THE_WAY_PARK = 2;
	const AVAILABLE_TO_PICK = 3;
	const CONFIRMED = 4;
	const CANCELLED = 5;
	const EXPIRED = 6;
	const BOOKING_COMPLETED = 7;
	const VEHICLE_PARKED = 8;
}