<?php

namespace App\Interfaces;

interface UserTypeInterface {
	const SUPERVISOR 	= 1;
	const DRIVER 		= 2;
	const WASHER  		= 3;
}