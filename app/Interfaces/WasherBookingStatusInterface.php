<?php

namespace App\Interfaces;

interface WasherBookingStatusInterface {
	const INPROGRESS = 1;
	const COMPLETED  = 2;
	const PENDING  = 3;
	const WASH_INITIAL  = 4;
	const NO_WASH  = 5;
}