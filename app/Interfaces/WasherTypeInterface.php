<?php

namespace App\Interfaces;

interface WasherTypeInterface {
	const FULL_WASH = 1;
	const INTERIOR  = 2;
	const EXTERIOR  = 3;
}