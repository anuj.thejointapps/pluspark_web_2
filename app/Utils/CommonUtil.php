<?php

namespace App\Utils;

use Auth;
use App\Models\ReferenceCount;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class CommonUtil
{
	# Bind the Reference count model
	protected $referenceCount;

	/*
	* constructor of util
	*
	* @param ReferenceCount
	*/
	public function __construct(ReferenceCount $referenceCount)
	{
		$this->referenceCount = $referenceCount;
	}

	/*
	* reference count corresponding specific type
	*
	* @param
	*/	
	public function lastReferenceCount($type)
	{
		# get last count of specific type.
        $refCount = $this->referenceCount->onlyReferenceType($type)->first();
           
        # check type count is empty or not.
        if (!empty($refCount)) {

            # return ref.
            return $refCount->ref_count;

        } else {
        	# create new refrence count corresponsing his type
        	$newReferenceCount = [
        		'ref_type' 	=> $type,
        		'ref_count' => 1
        	];

        	# create new ref. count of type
        	$referenceCount = $this->referenceCount->create($newReferenceCount);

        	# return current reference count
            return $referenceCount->ref_count;
        }
	}

	/*
	* reference count corresponding specific type
	*
	* @param
	*/	
	public function referenceCount($type)
	{
		# get last count of specific type.
        $refCount = $this->referenceCount->onlyReferenceType($type)->first();
                               
        # check type count is empty or not.
        if (!empty($refCount)) {
        	
            $refCount = $this->referenceCount->findOrFail($refCount->id);

            $refCount->ref_count  +=  1;
            $refCount->ref_type   =   $type;

            $refCount->save();

            # return ref.
            return $refCount->ref_count;

        } else {
        	# create new refrence count corresponsing his type
        	$newReferenceCount = [
        		'ref_type' 	=> $type,
        		'ref_count' => 1
        	];

        	# create new ref. count of type
        	$referenceCount = $this->referenceCount->create($newReferenceCount);

        	# return current reference count
            return $referenceCount->ref_count;
        
        }
	}

	/*
	* Generate sequence of module according his type
	*
	* @param
	*/	
	public static function genrateSequences($ref_count, $type)
    {
        $ref_digits =  str_pad($ref_count, 4, 0, STR_PAD_LEFT);
        
        if (!empty($type)) {
        	return $type.$ref_digits;
        }

        return "dummy".$ref_digits;
    }

    /**
     * @method to fetch the Lat, Long From Address String
     * @param Address String 
     * @return lat, long 
     */
    public function geocode($address)
    {
        # url encode the address
        $address = urlencode($address);

        # Define the Api Key for Google Api
        $apiKey = 'AIzaSyDD4LaNErtgQJDO9lyPwWlfEOhPCMmJrmE';

        # google map geocode api url
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=$apiKey ";

        # get the json response
        $resp_json = file_get_contents($url);

        # decode the json
        $resp = json_decode($resp_json, true);

        # response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK') {
            # get the important data
            $lati = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : "";
            $longi = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : "";
            // $formatted_address = isset($resp['results'][0]['formatted_address']) ? $resp['results'][0]['formatted_address'] : "";
            $pincode = '';
            if (isset($resp['results'][0]['address_components'])) {
                foreach ($resp['results'][0]['address_components'] as $addressComponent) {
                    if (isset($addressComponent['types'])) {
                        foreach ($addressComponent['types'] as $type) {
                            if ($type == 'postal_code') {
                                $pincode = $addressComponent['long_name'];
                            }
                        }
                    }
                }
            }

            # verify if data is complete
            if(($lati && $longi) || $pincode) {
                # put the data in the array
                $data_arr = array();

                # push Data in empty array
                array_push(
	                $data_arr,
	                $lati,
	                $longi,
	                $pincode
	                // $formatted_address
                );

                # return the data Array
                return $data_arr;
            } else {
                return false;
            }
        } else {
            // echo "<strong>ERROR: {$resp['status']}</strong>";
            return false;
        }
    }

    /**
     * @method to fetch Auth comapny guard
     * @param Address String 
     * @return lat, long 
     */
    public function getLoginCompanyId()
    {
        return Auth::guard('company')->user()->id ?? 0;
    }

    /**
    * Gera a paginação dos itens de um array ou collection.
    *
    * @param array|Collection      $items
    * @param int   $perPage
    * @param int  $page
    * @param array $options
    *
    * @return LengthAwarePaginator
    */
    public function paginate($items, $perPage = 15, $baseUrl = null, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        if ($baseUrl) {
            $lap->setPath($baseUrl);
        }

        return $lap;
    }

     /**
    * Gera a paginação dos itens de um array ou collection.
    *
    * @param array|Collection      $items
    * @param int   $perPage
    * @param int  $page
    * @param array $options
    *
    * @return LengthAwarePaginator
    */
    public function customPaginate($items, $perPage = 15, $baseUrl = null, $page = null, $options = [])
    {
        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($items);

        //Define how many items we want to be visible in each page
        #$per_page = (int) per_page();

        //Slice the collection to get the items to display in current page
        $currentPageResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->values();

        //Create our paginator and add it to the items array
        $items['results'] = new LengthAwarePaginator($currentPageResults, count($collection), $perPage);

        //Set base url for pagination links to follow e.g custom/url?page=6
        return $items['results']->setPath(request()->url());
    }

}