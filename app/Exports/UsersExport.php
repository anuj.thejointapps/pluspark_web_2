<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
	protected $data;
	public function __construct($data)
	{
		$this->data = $data;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	// dd($this->data);
        // return User::all();
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'Id',
            'User name',
            'Email',
            'Mobile Number',
            'Join Date',
            'Country Name',
            'State Name',
        ];
    }
}
