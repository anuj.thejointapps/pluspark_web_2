<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WasherAllotCompany extends Model
{
    use SoftDeletes;
  
    # define table
    protected $table = 'washer_allot_companies';
  
    # define fillable fildes
    protected $fillable =   [
                                'washer_id',
                                'company_id',
                            ];



    /**
     * @method to fetch relation with washer
     * @return state collection
     * @param
    */
    public function washer()
    {
        return $this->belongsTo(Washerman::class, 'washer_id', 'id');
    }

     /**
     * @method to fetch relation with washer
     * @return state collection
     * @param
    */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    /**
     * @method to scope get alloted washer by company id
     * @return 
     * @param
    */
    public function scopeonlyCompany($query, $companyId)
    {
        return $query->where('company_id', $companyId);
    }
}
