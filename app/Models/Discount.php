<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
  	# define table.
  	protected $table = 'discounts';

  	# define fillable fildes
  	protected $fillable = 	[
  		'company_id',
		'title',
		'description',
		'discount_id',
		'percent_discount',
		'on_booking',
		'on_subscription',
		'on_wallet',
		'start_date',
		'end_date',
		'status',
		'added_by',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    		=> 'datetime',
	  'start_date'    		=> 'datetime',
	  'end_date'    		=> 'datetime',
	  'status'        		=> 'boolean',
	  'on_booking'        	=> 'boolean',
	  'on_subscription'     => 'boolean',
	  'on_wallet'        	=> 'boolean',
	];

	/**
	 * @method to define relation b/w model and company
	 * @return relation
	 * @param
	 */
	public function company()
	{
		return $this->belongsTo(Company::class);
	}
	
  	/**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeActive($query)
	{
	  return $query->where('status', 1);
	}

  	/**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeonlyCompany($query, $companyId)
	{
	  return $query->where('company_id', $companyId);
	}

	/**
	 * @method to define atrribute to set formatted Start Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getStartDateFormattedAttribute()
	{
		return $this->start_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getEndDateFormattedAttribute()
	{
		return $this->end_date->format('Y-m-d');
	}
	
    /**
     * @method to user by filters
     * @return query builder
     * @param
    */
    public function scopeSearchFilter($query, $fromDate, $toDate, $month)
    {

        if (!empty($fromDate) || !empty($toDate)) {
            $fromDate   = date('Y-m-d', strtotime($fromDate));
            $toDate     = date('Y-m-d', strtotime($toDate));
            $query->where(function($query) use ($fromDate, $toDate) {
                $query->whereDate('start_date', '>', $fromDate)->whereDate('end_date', '<', $toDate);
            });
        }

        if (!empty($month)) {
            if ($month == '1') {
                $startDate = Carbon::now()->subDays(30);
            } else if ($month == '3') {
                $startDate = Carbon::now()->subDays(90);
            } else if ($month == '6') {
                $startDate = Carbon::now()->subDays(180);
            }
            
            // dd($startDate, $carbonToDayDate, date('Y-m-d'), date('Y-m-d', strtotime("-30 days")) );
            $query->where(function($query) use ($startDate) {
                # Get carbon now date
                $carbonToDayDate = Carbon::now();
                $query->whereDate('created_at', '>', $startDate)->whereDate('created_at', '<', $carbonToDayDate);
            });
        }
    }
}
