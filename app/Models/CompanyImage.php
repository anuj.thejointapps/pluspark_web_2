<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyImage extends Model
{
    use SoftDeletes, Notifiable;

    # define table
    protected $table ='company_images';

    # define fillable fields
    protected $fillable =   [
                                'company_id',
                                'image_path'
                            ];
}
