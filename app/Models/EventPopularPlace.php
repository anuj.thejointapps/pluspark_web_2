<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventPopularPlace extends Model
{
    use SoftDeletes;
  
    # define table
    protected $table ='event_popular_places';
  
    # define fillable fildes
    protected $fillable =   [
                                'event_id',
                                'parking_id',
                                'valet_price'
                            ];


    /**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeonlyEvent($query, $eventId)
    {
      return $query->where('event_id', $eventId);
    }

    /**
     * @method to define relation b/w model and Parking
     * @return relations
     * @param 
     */
    public function parking()
    {
        return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
    }
}
