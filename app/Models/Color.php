<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model
{
	# user softdeletes.
    use SoftDeletes;

  	# define table.
  	protected $table 	= 	'colors';

  	# define fillable fildes
  	protected $fillable = 	[
  		'name',
  		'code',
  		'status'
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    => 'datetime',
	  'status'        => 'boolean',
	];

  	/**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeActive($query)
	{
	  return $query->where('status', 1);
	}
}
