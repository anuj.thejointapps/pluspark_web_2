<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

#Interfaces
use App\Interfaces\BookingPassStatusInterface;

class PassesForScanOrPrivateBooking extends Model implements BookingPassStatusInterface
{
	#use SoftDeletes
	use SoftDeletes;

  	# define table.
  	protected $table = 'scan_or_private_passes';

  	# define fillable fildes
  	protected $fillable = 	[
  		'booking_id',
  		'pass_number',
  		'status',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    		=> 'datetime',
	];

	/**
	 * @method to fetch the pass status
	 * @return String
	 * @param
	 */
	public function getPassStatusAttribute()
	{
		#FEtch Booking Status
		$status = $this->status;
		$statusString = '';

		#validate and set Status
		if($status == BookingPassStatusInterface::PENDING) {
			$statusString = 'Pending';
		} elseif ($status == BookingPassStatusInterface::CONFIRMED) {
			$statusString = 'Confirmed';
		} elseif ($status == BookingPassStatusInterface::CANCELLED) {
			$statusString = 'Cancelled';
		} elseif ($status == BookingPassStatusInterface::EXPIRED) {
			$statusString = 'Expired';
		}

		#return 
		return $statusString;
	}
}
