<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingDescription extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_descriptions';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'description',
                                'idle_page_for_people',
                                'select_vehicle_type',
                                'select_space_features',
                            ];

    /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function parking()
    {
        return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
    }
    
    /**
     * @method to scope 
     * @return 
     * @param
    */
    public function scopeonlyParking($query, $parkingId)
    {
        return $query->where('parking_id', $parkingId);
    }

    /**
     * @method to fetch amenities of Parking
     * @return string
     * @param
     */
    public function getAllAmenitiesAttribute()
    {
        return json_decode($this->select_space_features);
    }
    
}
