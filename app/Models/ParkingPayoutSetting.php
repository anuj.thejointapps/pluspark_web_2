<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingPayoutSetting extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_payout_settings';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'first_name',
                                'last_name',
                                'address',
                                'country',
                                'pincode',
                                'date_of_birth',
                                'identification',
                                'identification_photo',
                            ];

    /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function parking()
    {
        return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
    }

    /**
     * @method to scope 
     * @return 
     * @param
    */
    public function scopeonlyParking($query, $parkingId)
    {
        return $query->where('parking_id', $parkingId);
    }
}
