<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobType extends Model
{
    # define table.
    protected $table = 'job_types';

    # define fillable fildes
    protected $fillable =   [
        'name',
    ];
}
