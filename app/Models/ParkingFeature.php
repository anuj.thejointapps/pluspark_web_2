<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingFeature extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_features';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'selected_feature'
                            ];
}
