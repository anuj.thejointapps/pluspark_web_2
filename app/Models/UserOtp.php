<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserOtp extends Model
{
  
  # define table
  protected $table ='user_otps';
  
  # define fillable fields
  protected $fillable = [
    'email_or_phone',
    'mobile_code',
    'otp',
  ];

  /**
    * The attributes that should be cast to native types.
    *
    * @var array
  */
  protected $casts = [
    'created_at'        => 'datetime',
  ];
}
