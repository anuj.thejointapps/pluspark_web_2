<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocode extends Model
{
  	# define table.
  	protected $table = 'promocodes';

  	# define fillable fildes
  	protected $fillable = 	[
  		'unique_id',
		'company_id',
		'name',
		'code',
		'usage_allowed',
		'offer_type',
		'discount',
		'start_date',
		'end_date',
		'on_booking',
		'on_subscription',
		'on_wallet',
		'status',
		'added_by',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    		=> 'datetime',
	  'start_date'    		=> 'datetime',
	  'end_date'    		=> 'datetime',
	  'status'        		=> 'boolean',
	];

	/**
	 * @method to define relation b/w model and company
	 * @return relation
	 * @param
	 */
	public function company()
	{
		return $this->belongsTo(Company::class);
	}
	
  	/**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeActive($query)
	{
	  return $query->where('status', 1);
	}

  	/**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeonlyCompany($query, $companyId)
	{
	  return $query->where('company_id', $companyId);
	}

	/**
	 * @method to define atrribute to set formatted Start Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getStartDateFormattedAttribute()
	{
		return $this->start_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getEndDateFormattedAttribute()
	{
		return $this->end_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to set formatted valid till date
	 * @return string
	 * @param
	 */
	public function getValidTillDateStringAttribute()
	{
		return 'this offer is valid till date '.$this->end_date->format('d F');
	}

	/**
	 * @method to define atrribute to fetch month or days between dates
	 * @return string
	 * @param
	 */
	public function getValidityInMonthAttribute()
	{
		#Fetch month or Days
		if($this->end_date->diff($this->start_date)->m != 0) {
			return $this->end_date->diff($this->start_date)->m .' Month '.$this->end_date->diff($this->start_date)->d .' Days';
		} else {
			return $this->end_date->diff($this->start_date)->d .' Days';
		}
	}

	/**
	 * @method to define is Valid
	 * @return string
	 * @param
	 */
	public function getIsValidAttribute()
	{
		if(Carbon::now()->between($this->start_date, $this->end_date)) {
			return true;
		}

		return false;
	}

	/**
	 * @method to define is Active
	 * @return string
	 * @param
	 */
	public function getIsActiveAttribute()
	{
		if($this->status) {
			return true;
		}

		return false;
	}
	
    /**
     * @method to user by filters
     * @return query builder
     * @param
    */
    public function scopeSearchFilter($query, $fromDate, $toDate, $month)
    {

        if (!empty($fromDate) || !empty($toDate)) {
            $fromDate   = date('Y-m-d', strtotime($fromDate));
            $toDate     = date('Y-m-d', strtotime($toDate));
            $query->where(function($query) use ($fromDate, $toDate) {
                $query->whereDate('start_date', '>', $fromDate)->whereDate('end_date', '<', $toDate);
            });
        }

        if (!empty($month)) {
            if ($month == '1') {
                $startDate = Carbon::now()->subDays(30);
            } else if ($month == '3') {
                $startDate = Carbon::now()->subDays(90);
            } else if ($month == '6') {
                $startDate = Carbon::now()->subDays(180);
            }
            
            // dd($startDate, $carbonToDayDate, date('Y-m-d'), date('Y-m-d', strtotime("-30 days")) );
            $query->where(function($query) use ($startDate) {
                # Get carbon now date
                $carbonToDayDate = Carbon::now();
                $query->whereDate('created_at', '>', $startDate)->whereDate('created_at', '<', $carbonToDayDate);
            });
        }
    }
}
