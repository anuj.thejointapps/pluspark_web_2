<?php

namespace App\Models;

use App\Models\Color;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserVehicle extends Model
{
	# user softdeletes.
    use SoftDeletes;

  	# define table.
  	protected $table = 'user_vehicles';

  	# define fillable fildes
  	protected $fillable = 	[
  		'user_id',
		'plate_number',
		'brand_id',
		'brand_name',
		'modal_id',
		'modal_name',
		'color_id',
		'color_name',
		'vehicle_type',
		'image',
		'qr_code_image',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    => 'datetime',
	  'status'        => 'boolean',
	];

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @method to define relation b/w model and bookngs
	 * @return relation
	 * @param
	 */
	public function bookings()
	{
		return $this->hasMany(Booking::class, 'vehicle_id');
	}
	

  	/**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeActive($query)
	{
	  return $query->where('status', 1);
	}

	/**
	 * @method to fetch image with full path
	 * @return string|''
	 * @param
	*/
	public function getImageFullPathAttribute()
	{
	  return $this->image != '' ? Url('/').'/'.$this->image : '';
	}

	/**
	 * @method to fetch Qr Code image with full path
	 * @return string|''
	 * @param
	*/
	public function getQrCodeImageFullPathAttribute()
	{
	  return $this->qr_code_image != '' ? Url('/').'/'.$this->qr_code_image : '';
	}

	/**
	 * @method to fetch full name
	 * @return string|''
	 * @param
	*/
	public function getFullNameAttribute()
	{
	  return ($this->brand_name);
	}

	/**
	 * @method to fetch hex code from color name
	 * @return string|''
	 * @param
	*/
	public function getHexCodeAttribute()
	{
	  	#fethc color name
		$colorName = strtolower($this->color_name);

		#validate
		if($colorName != ''){
			$color = Color::whereRaw('lower(name) = ?', $colorName)->get();
			
			#Validate color
			if($color->isNotEmpty()) {
				$hexCode = $color->first()->code;

				#return 
				return $hexCode;
			} else {
				return '';
			}
		}

		#return 
		return '';
	}
}
