<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;
    
    # define table
    protected $table ='states';
    
    # define fillable fildes
    protected $fillable = [
    	                   		'name',
    	                   		'country_id',
                                'status'
    	               		];
    
    /**
    * @method to define relation to country
    * @param
    * @return relation
    */
    public function country()
    {
    	 return $this->belongsTo(Country::class);
    }

    /**
    * @method scope to get states by country
    * @param
    * @return relation
    */
    public function scopeIsCountry($query, $countryId)
    {
      return $query->where('country_id', $countryId);
    }
}
