<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSearchPlace extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='user_search_places';

    # define fillable fildes
    protected $fillable =   [
        'user_id',
        'search_place',
        'latitude',
        'longitude',
    ];

    /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
