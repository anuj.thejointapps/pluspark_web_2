<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingBookingFeature extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_booking_features';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'parking_feature_id',
                                'cash',
                                'visa_master',
                            ];
}
