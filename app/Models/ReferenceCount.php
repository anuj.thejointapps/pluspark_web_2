<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferenceCount extends Model
{
    use SoftDeletes;
  
    # define table
    protected $table ='reference_counts';
  
    # define fillable fildes
    protected $fillable =   [
                                'ref_type',
                                'ref_count'
                            ];

    /*
    * Define scope
    *
    * @param
    */
    public function scopeonlyReferenceType($query, $type)
    {
        return $query->where('ref_type', $type);
    }
}
