<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingWasherFeature extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_washer_features';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'parking_feature_id',
                                'cash',
                                'visa_master',
                                'cost_of_full_wash',
                                'cost_of_exterior_wash',
                                'cost_of_interior_wash',
                                'type',
                            ];
}
