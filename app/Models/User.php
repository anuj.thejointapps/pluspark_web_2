<?php

namespace App\Models;

use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'name', 
        'email', 
        'mobile_code', 
        'mobile', 
        'account_type', 
        'facebook_id', 
        'google_id', 
        'twitter_id', 
        'apple_id', 
        'country_id', 
        'country_name', 
        'state_id', 
        'state_name',
        'address',
        'gender',
        'status',
        'image', 
        'image_url', 
        'password',
        'current_booking_id',
        'current_booking_type',
        'switch_screen',
        'device_type',
        'device_token',
        'device_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'switch_screen'     => 'boolean',
    ];

    /**
     * @method to fetch country using relation
     * @return country collection
     * @param
    */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * @method to fetch state using relation
     * @return state collection
     * @param
    */
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    /**
     * @method to fetch relation with vehicles
     * @return state collection
     * @param
    */
    public function vehicles()
    {
        return $this->hasMany(UserVehicle::class);
    }

    /**
     * @method to fetch relation with search Places
     * @return state collection
     * @param
    */
    public function searchPlaces()
    {
        return $this->hasMany(UserSearchPlace::class);
    }

    /**
     * @method to fetch relation with bookings
     * @return state collection
     * @param
    */
    public function bookings()
    {
        return $this->hasMany(Booking::class, 'user_id', 'id');
    }

    /**
     * @method to fetch relation with scan or private bookings
     * @return state collection
     * @param
    */
    public function scanPrivateBookings()
    {
        return $this->hasMany(ScanOrPrivateBooking::class, 'user_id', 'id');
    }

    /**
     * @method to fetch relation with bookings
     * @return state collection
     * @param
    */
    public function currentBooking()
    {
        return $this->belongsTo(Booking::class, 'current_booking_id', 'id');
    }

    /**
     * @method to fetch relation with Current Scan Or Private Booking
     * @return state collection
     * @param
    */
    public function currentScanOrPrivateBooking()
    {
        return $this->belongsTo(ScanOrPrivateBooking::class, 'current_booking_id', 'id');
    }


    /**
     * @method to user by filters
     * @return query builder
     * @param
    */
    public function scopeSearchFilter($query, $countryName, $stateName, $fromDate, $toDate, $month)
    {
        if (!empty($countryName)) {
            $query->where(function($query) use ($countryName) {
                $query->where('country_name', 'LIKE', '%' . $countryName . '%');
            });
        }

        if (!empty($stateName)) {
            $query->where(function($query) use ($stateName) {
                $query->where('state_name', 'LIKE', '%' . $stateName . '%');
            });
        }

        if (!empty($fromDate) || !empty($toDate)) {
            $fromDate   = date('Y-m-d', strtotime($fromDate));
            $toDate     = date('Y-m-d', strtotime($toDate));
            $query->where(function($query) use ($fromDate, $toDate) {
                $query->whereDate('created_at', '>', $fromDate)->whereDate('created_at', '<', $toDate);
            });
        }

        if (!empty($month)) {
            if ($month == '1') {
                $startDate = Carbon::now()->subDays(30);
            } else if ($month == '3') {
                $startDate = Carbon::now()->subDays(90);
            } else if ($month == '6') {
                $startDate = Carbon::now()->subDays(180);
            }
            
            // dd($startDate, $carbonToDayDate, date('Y-m-d'), date('Y-m-d', strtotime("-30 days")) );
            $query->where(function($query) use ($startDate) {
                # Get carbon now date
                $carbonToDayDate = Carbon::now();
                $query->whereDate('created_at', '>', $startDate)->whereDate('created_at', '<', $carbonToDayDate);
            });
        }
    }

    /**
     * @method to fetch image with full path
     * @return string|''
     * @param
    */
    public function getImageFullPathAttribute()
    {
        return $this->image != '' ? Url('/').'/'.$this->image : '';
    }
}
