<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='countries';
  
  # define fillable fildes
  protected $fillable = [
  	                   		'name',
                          'country_code',
                          'status'
  	               		];

  /**
  * @method to define relation to states
  * @param
  * @return relation
  */
  public function states()
  {
    return $this->hasMany(State::class);
  }
}
