<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

#Interface
use App\Interfaces\UserTypeInterface;

class Washerman extends Model implements UserTypeInterface
{
    use SoftDeletes;
  
    # define table
    protected $table ='washermen';
  
    # define fillable fildes
    protected $fillable =   [
        'washer_auto_id',
        'current_booking_id',
        'name',
        'email',
        'mobile',
        'city',
        'country',
        'gender',
        'date_of_join',
        'address',
        'image_path',
        'status',
        'device_type',
        'device_token',
        'latitude',
        'longitude',
        'pincode',
        'job_type_id',
        'company_id',
        'current_parking_id',
        'country_code',
        'password',
        'token',
    ];

    /**
     * @method to fetch relation with company
     * @return state collection
     * @param
    */
    public function washermanCompanies()
    {
        return $this->hasMany(WasherAllotCompany::class, 'washer_id', 'id');
    }

     /**
     * @method to fetch relation with company
     * @return state collection
     * @param
    */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    /**
     * @method to fetch relation with company
     * @return state collection
     * @param
    */
    public function supervisorBookings()
    {
        return $this->hasMany(SupervisorBooking::class, 'supervisor_id', 'id');
    }

    /**
     * @method to fetch relation with driverBookings
     * @return state collection
     * @param
    */
    public function driverBookings()
    {
        return $this->hasMany(DriverBooking::class, 'driver_id', 'id');
    }

    /**
     * @method to fetch relation with company
     * @return state collection
     * @param
    */
    public function washerBookings()
    {
        return $this->hasMany(WasherBooking::class, 'washer_id', 'id');
    }

    /**
     * @method to fetch relation with current parking
     * @return relation
     * @param
    */
    public function currentParking()
    {
        return $this->belongsTo(CompanyParking::class, 'current_parking_id', 'id');
    }

    /**
     * @method to fetch relation with current Booking
     * @return relation
     * @param
    */
    public function currentBookingOnSupervisor()
    {
        return $this->belongsTo(SupervisorBooking::class, 'current_booking_id', 'booking_id')
                    ->where('set_or_get', 'get');
    }

    /**
     * @method to fetch relation with current Booking
     * @return relation
     * @param
    */
    public function currentBookingOnDriver()
    {
        return $this->belongsTo(DriverBooking::class, 'current_booking_id', 'booking_id')
                    ->where('set_or_get', 'get');
    }
    
    /**
     * @method to user by filters
     * @return query builder
     * @param
    */
    public function scopeSearchFilter($query, $countryName, $stateName, $fromDate, $toDate, $month)
    {
        if (!empty($countryName)) {
            $query->where(function($query) use ($countryName) {
                $query->where('country', 'LIKE', '%' . $countryName . '%');
            });
        }

        if (!empty($stateName)) {
            $query->where(function($query) use ($stateName) {
                $query->where('city', 'LIKE', '%' . $stateName . '%');
            });
        }

        if (!empty($fromDate) || !empty($toDate)) {
            $fromDate   = date('Y-m-d', strtotime($fromDate));
            $toDate     = date('Y-m-d', strtotime($toDate));
            $query->where(function($query) use ($fromDate, $toDate) {
                // $query->whereDate('created_at', '>', $fromDate)->whereDate('created_at', '<', $toDate);
                $query->whereDate('date_of_join', '>', $fromDate)->whereDate('date_of_join', '<', $toDate);
            });
        }

        if (!empty($month)) {
            if ($month == '1') {
                $startDate = Carbon::now()->subDays(30);
            } else if ($month == '3') {
                $startDate = Carbon::now()->subDays(90);
            } else if ($month == '6') {
                $startDate = Carbon::now()->subDays(180);
            }
            
            $query->where(function($query) use ($startDate) {
                # Get carbon now date
                $carbonToDayDate = Carbon::now();
                // $query->whereDate('created_at', '>', $startDate)->whereDate('created_at', '<', $carbonToDayDate);
                $query->whereDate('date_of_join', '>', $startDate)->whereDate('date_of_join', '<', $carbonToDayDate);
            });
        }
    }

    /**
     * @method 
     * @return query builder
     * @param
    */
    public function scopeIsNotWasher($query)
    {
        return $query->where('job_type_id', '!=', 3);
    }

    /**
     * Scope a query to only Supervsior users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeSupervisor($query)
    {
        #Query
        return $query->where('job_type_id', UserTypeInterface::SUPERVISOR);
    }

    /**
     * Scope a query to only Driver users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeDriver($query)
    {
        #Query
        return $query->where('job_type_id', UserTypeInterface::DRIVER);
    }

    /**
     * Scope a query to only Washer users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeWasher($query)
    {
        #Query
        return $query->where('job_type_id', UserTypeInterface::WASHER);
    }

    /**
     * @method 
     * @return query builder
     * @param
    */
    public function scopeonlyWasher($query)
    {
        return $query->where('job_type_id', 3);
    }

    /**
     * @method 
     * @return query builder
     * @param
    */
    public function scopeonlyCompany($query, $companyId)
    {
        return $query->where('company_id', $companyId);
    }

    /**
     * @method
     * @return query builder
     * @param
    */
    public function getJobTypeAttribute()
    {
        if ($this->job_type_id == 1) {
            return "Supervisor";
        } else if ($this->job_type_id == 2) {
            return "Driver";
        } else {
             return "Washer";
        }
    }

    /**
     * @method to fetch image with full path
     * @return string|''
     * @param
    */
    public function getImageFullPathAttribute()
    {
      return $this->image_path != '' ? Url('/').'/'.$this->image_path : '';
    }
}
