<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PopularPlace extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='popular_places';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'company_id',
                                'selected_popular_place',
                            ];
                            
    /**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeonlyCompany($query, $companyId)
    {
      return $query->where('company_id', $companyId);
    }
}
