<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Company extends Authenticatable
{
    use SoftDeletes, Notifiable;

    # define table
    protected $table ='companies';

    # define fillable fields
    protected $fillable =   [
                                'company_auto_id',
                                'name',
                                'email',
                                'password',
                                'country',
                                'city',
                                'contact_person_name',
                                'contact_person_mobile',
                                'date_of_join',
                                'address',
                                'notes',
                                'number_of_parking',
                                'country_code',
                                'currency_id',
                                'status'
                            ];

    /**
     * @method to fetch relation with company images
     * @return state collection
     * @param
    */
    public function companyImages()
    {
        return $this->hasMany(CompanyImage::class, 'company_id', 'id');
    }

    /**
     * @method to fetch relation with popular places
     * @return state collection
     * @param
    */
    public function popularPlaces()
    {
        return $this->hasMany(PopularPlace::class, 'company_id', 'id');
    }

    /**
     * @method to fetch relation with washer
     * @return state collection
     * @param
    */
    public function allotWasherOfCompany()
    {
        return $this->hasMany(WasherAllotCompany::class, 'company_id', 'id');
    }

    /**
     * @method to fetch relation with promocodes
     * @return state collection
     * @param
    */
    public function promocodes()
    {
        return $this->hasMany(Promocode::class);
    }

    /**
     * @method to fetch relation with company parking
     * @return state collection
     * @param
    */
    public function companyParking()
    {
        return $this->hasMany(CompanyParking::class, 'company_id', 'id');
    }

    /**
     * @method to fetch relation with  parking
     * @return state collection
     * @param
    */
    public function parkings()
    {
        return $this->hasMany(CompanyParking::class, 'company_id', 'id');
    }

    /**
     * @method to fetch relation with  driver Commision
     * @return state collection
     * @param
    */
    public function driverCommission()
    {
        return $this->hasOne(DriverCommission::class, 'company_id', 'id');
    }

    /**
     * @method to fetch relation with  driver Commision
     * @return state collection
     * @param
    */
    public function washerCommission()
    {
        return $this->hasOne(WasherCommission::class, 'company_id', 'id');
    }

    
    /**
    * @method to define relation to currency
    * @param
    * @return relation
    */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @method to user by filters
     * @return query builder
     * @param
    */
    public function scopeSearchFilter($query, $countryName, $stateName, $fromDate, $toDate)
    {
        if (!empty($countryName)) {
            $query->where(function($query) use ($countryName) {
                $query->where('country', 'LIKE', '%' . $countryName . '%');
            });
        }

        if (!empty($stateName)) {
            $query->where(function($query) use ($stateName) {
                $query->where('city', 'LIKE', '%' . $stateName . '%');
            });
        }

        if (!empty($fromDate) || !empty($toDate)) {
            $fromDate   = date('Y-m-d', strtotime($fromDate));
            $toDate     = date('Y-m-d', strtotime($toDate));
            $query->where(function($query) use ($fromDate, $toDate) {
                // $query->whereDate('created_at', '>', $fromDate)->whereDate('created_at', '<', $toDate);
                $query->whereDate('date_of_join', '>', $fromDate)->whereDate('date_of_join', '<', $toDate);
            });
        }
    }
}
