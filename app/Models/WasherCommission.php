<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WasherCommission extends Model
{
  	# define table.
  	protected $table = 'washer_commissions';

  	# define fillable fildes
  	protected $fillable = 	[
  		'company_id',
		'company_cash_comm',
		'company_card_comm',
		'pls_park_cash_comm',
		'pls_park_card_comm',
		'thrd_party_cash_comm',
		'thrd_party_card_comm',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    => 'datetime',
	];
}
