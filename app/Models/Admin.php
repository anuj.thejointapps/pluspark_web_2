<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
  use Notifiable;

  use SoftDeletes;
  
  # define table
  protected $table ='admin';
  
  # define fillable fildes
  protected $fillable = [
  	                   'name',
  	                   'email',
                       'mobile',
                       'region',
                       'address',
  	                   'password',
                       'country_id',
                       'city_id',
  	                   'remember_token'
  ];
}
