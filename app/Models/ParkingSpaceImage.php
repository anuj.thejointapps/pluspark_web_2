<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingSpaceImage extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_space_images';

    # define fillable fildes
    protected $fillable =   [
                                'image_path',
                                'parking_space_id'
                            ];

    /**
     * @method to fetch image with full path
     * @return string|''
     * @param
    */
    public function getImageFullPathAttribute()
    {
      return $this->image_path != '' ? Url('/').'/'.$this->image_path : '';
    }
}
