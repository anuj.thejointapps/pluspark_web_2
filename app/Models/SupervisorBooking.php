<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Interfaces\SupervisorBookingStatusInterface;

class SupervisorBooking extends Model implements SupervisorBookingStatusInterface
{
  	# define table.
  	protected $table = 'supervisor_bookings';

  	# define fillable fildes
  	protected $fillable = 	[
  		'parking_id',
		'booking_id',
		'supervisor_id',
		'set_or_get',
		'key_no',
		'park_no',
		'pick_up_lat',
		'pick_up_long',
		'booking_status',
		'arrived_date_time',
		'exit_date_time',
		'requested_date_time',
		'parked_date_time'
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    			=> 'datetime',
	  'arrived_date_time'  		=> 'datetime',
	  'exit_date_time'    		=> 'datetime',
	  'requested_date_time'    	=> 'datetime',
	  'parked_date_time'    	=> 'datetime',
	];

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function parking()
	{
		return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and vehicle
	 * @return relation
	 * @param
	 */
	public function vehicle()
	{
		return $this->belongsTo(UserVehicle::class, 'vehicle_id', 'id');
	}

	/**
	 * @method to define relation b/w model and Supervisor
	 * @return relation
	 * @param
	 */
	public function supervisor()
	{
		return $this->belongsTo(Washerman::class, 'supervisor_id', 'id');
	}

	/**
	 * @method to define relation b/w model and driver
	 * @return relation
	 * @param
	 */
	public function driver()
	{
		return $this->belongsTo(Washerman::class, 'supervisor_id', 'id');
	}

	/**
	 * @method to define relation b/w model and booking
	 * @return relation
	 * @param
	 */
	public function booking()
	{
		return $this->belongsTo(Booking::class, 'booking_id', 'id');
	}
	
	/**
	 * @method to define atrribute to set formatted Start Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getStartDateFormattedAttribute()
	{
		return $this->start_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getEndDateFormattedAttribute()
	{
		return $this->end_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedFormattedAttribute()
	{
		return ($this->arrived_date_time->format('l'). ' '.$this->arrived_date_time->format('H:i a'));
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedDateTimeFormattedAttribute()
	{
		return ($this->arrived_date_time->format('Y-m-d'). ' '.$this->arrived_date_time->format('H:i:s'));
	}

	/**
	 * @method to define atrribute to parked_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getParkedDateTimeFormattedAttribute()
	{
		return $this->parked_date_time != '' ? ($this->parked_date_time->format('h:i a')) : '';
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getExitFormattedAttribute()
	{
		return ($this->exit_date_time->format('l'). ' '.$this->exit_date_time->format('H:i a'));
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedTimeAttribute()
	{
		return $this->arrived_date_time->format('H:i a');
	}

	/**
	 * @method to define atrribute to fetch Exit time formatyted in 12 hours
	 * @return string
	 * @param
	 */
	public function getExitTimeAttribute()
	{
		return $this->exit_date_time != '' ? $this->exit_date_time->format('H:i a') : '';
	}

	/**
	 * @method to define atrribute to fetch requested date time timestamp
	 * @return string
	 * @param
	 */
	public function getRequestTimestampAttribute()
	{
		return $this->requested_date_time != '' ? $this->requested_date_time->timestamp : 0;
	}

	/**
	 * @method to define atrribute to set requested formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getRequestedDateFormattedAttribute()
	{
		return $this->requested_date_time != '' ? $this->requested_date_time->format('Y-m-d') : '';
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getRequestedFormattedTimeAttribute()
	{
		if($this->requested_date_time != '') {
			return ($this->requested_date_time != '' ? $this->requested_date_time->format('H:i a') : '');
		}

		#return
		return '';
	}

	/**
	 * @method to define atrribute to fetch booking Status
	 * @return string
	 * @param
	 */
	public function getFetchBookingStatusAttribute()
	{
		if($this->booking_status == SupervisorBookingStatusInterface::INPROGRESS) {
			return 'In Progress';
		} elseif ($this->booking_status == SupervisorBookingStatusInterface::COMPLETED) {
			return 'Completed';
		} elseif ($this->booking_status == SupervisorBookingStatusInterface::ON_THE_WAY_TO_PARK) {
			return 'ON_THE_WAY_TO_PARK';
		} elseif ($this->booking_status == SupervisorBookingStatusInterface::PARKED) {
			return 'PARKED';
		} elseif ($this->booking_status == SupervisorBookingStatusInterface::GET_CAR_REQUEST) {
			return 'GET_CAR_REQUEST';
		} elseif ($this->booking_status == SupervisorBookingStatusInterface::ON_THE_WAY_TO_DROP) {
			return 'ON_THE_WAY_TO_DROP';
		} elseif ($this->booking_status == SupervisorBookingStatusInterface::ARRIVED) {
			return 'ARRIVED';
		} elseif ($this->booking_status == SupervisorBookingStatusInterface::BILL_GENERATED) {
			return 'BILL_GENERATED';
		} else{
			return '';
		}
	}

	/**
	 * @method to define scope only for GET_CAR_REQUEST
	 * @return Builder
	 * @param
	 */
	public function scopeGetCarRequest($query)
	{
		return $query->where('booking_status', SupervisorBookingStatusInterface::GET_CAR_REQUEST);
	}

	/**
	 * @method to define attribute for completed status
	 * @return Builder
	 * @param
	 */
	public function getIsCompletedAttribute()
	{
		return ($this->booking_status == SupervisorBookingStatusInterface::COMPLETED ? true : false);
	}

	/**
	 * @method to define attribute for BILL_GENERATED status
	 * @return Builder
	 * @param
	 */
	public function getIsBillGeneratedAttribute()
	{
		return ($this->booking_status == SupervisorBookingStatusInterface::BILL_GENERATED ? true : false);
	}

	/**
	 * @method to define scope only for set type booking
	 * @return Builder
	 * @param
	 */
	public function scopeSetType($query)
	{
		return $query->where('set_or_get', 'set');
	}

	/**
	 * @method to define scope only for get type bookings
	 * @return Builder
	 * @param
	 */
	public function scopegetType($query)
	{
		return $query->where('set_or_get', 'get');
	}
}
