<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modal extends Model
{	
	# user softdeletes.
  use SoftDeletes;

  # define table.
  protected $table 	= 	'modals';

  # define fillable fildes
  protected $fillable = 	[
    'brand_id',
    'name',
    'status'
  ];

  /**
  * @method to define relation to brand
  * @param
  * @return relation
  */
  public function brand()
  {
    return $this->belongsTo(Brand::class, 'brand_id', 'id');
  }
}
