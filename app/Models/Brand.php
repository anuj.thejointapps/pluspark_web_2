<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
	# user softdeletes.
    use SoftDeletes;

  	# define table.
  	protected $table = 'brands';

  	# define fillable fildes
  	protected $fillable = 	[
  	    'name',
  	    'status'
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    => 'datetime',
	  'status'        => 'boolean',
	];

	/**
     * @method Scope a query to only include active brands.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeActive($query)
	{
	  return $query->where('status', 1);
	}

	/**
	 * @method to define relation B/W brand and modals
	 * @param
	 * @return relation
	 */
	public function modals()
	{
		return $this->hasMany(Modal::class)
					->where('status', 1);
	}
}
