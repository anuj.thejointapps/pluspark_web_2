<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Interfaces\WasherBookingStatusInterface;

class WasherBooking extends Model implements WasherBookingStatusInterface
{
  	# define table.
  	protected $table = 'washer_bookings';

  	# define fillable fildes
  	protected $fillable = 	[
  		'parking_id',
		'booking_id',
		'washer_id',
		'booking_type',
		'key_no',
		'park_no',
		'washing_status',
		'washing_start_date',
		'washing_end_date',
		'arrived_date_time',
		'exit_date_time',
		'requested_date_time'
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    			=> 'datetime',
	  'washing_start_date'    	=> 'datetime',
	  'washing_end_date'    	=> 'datetime',
	  'arrived_date_time'  		=> 'datetime',
	  'exit_date_time'    		=> 'datetime',
	  'requested_date_time'    	=> 'datetime',
	];

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function parking()
	{
		return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and booking
	 * @return relation
	 * @param
	 */
	public function booking()
	{
		return $this->belongsTo(Booking::class, 'booking_id', 'id');
	}


	/**
	 * @method to define relation b/w model and booking
	 * @return relation
	 * @param
	 */
	public function scanPrivateBooking()
	{
		return $this->belongsTo(ScanOrPrivateBooking::class, 'booking_id', 'id');
	}

	/**
     * Scope a query to only include qr booking.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeQrBookings($query)
    {
        return $query->where('booking_type', 'qr_code');
    }

	/**
     * Scope a query to only include scan or private booking.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNonQrBookings($query)
    {
        return $query->where('booking_type', '<>', 'qr_code');
    }
	
	/**
	 * @method to define atrribute to set formatted Start Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getStartDateFormattedAttribute()
	{
		return $this->start_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to set formatted Start Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getWashStartDateTimestampAttribute()
	{
		return $this->washing_start_date != '' ? $this->washing_start_date->timestamp : '';
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getEndDateFormattedAttribute()
	{
		return $this->end_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedFormattedAttribute()
	{
		return ($this->arrived_date_time->format('l'). ' '.$this->arrived_date_time->format('H:i a'));
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getExitFormattedAttribute()
	{
		return ($this->exit_date_time->format('l'). ' '.$this->exit_date_time->format('H:i a'));
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedTimeAttribute()
	{
		return $this->arrived_date_time->format('H:i a');
	}

	/**
	 * @method to define atrribute to diffrenece in Hour and Minute in wash start and end Time
	 * @return string
	 * @param
	 */
	public function getDiffStartEndWashAttribute()
	{
		#Diff in minutes
		$diffInMin = $this->washing_start_date->diffInMinutes($this->washing_end_date);

		#Hours
		$hours = floor($diffInMin / 60);
    	$minutes = ($diffInMin % 60);

    	#return
    	return $hours .' Hour'.' '.$minutes.' minutes';
	}

	/**
	 * @method to define atrribute to fetch booking Status
	 * @return string
	 * @param
	 */
	public function getFetchWashingStatusAttribute()
	{
		if($this->washing_status == WasherBookingStatusInterface::INPROGRESS) {
			return 'WASH_IN_PROGRESS';
		} elseif ($this->washing_status == WasherBookingStatusInterface::COMPLETED) {
			return 'WASH_COMPLETED';
		} elseif ($this->washing_status == WasherBookingStatusInterface::PENDING) {
			return 'WASH_PENDING';
		} elseif ($this->washing_status == WasherBookingStatusInterface::WASH_INITIAL) {
			return 'WASH_INITIALNO_WASH';
		} elseif ($this->washing_status == WasherBookingStatusInterface::NO_WASH) {
			return 'NO_WASH';
		} 
	}

	/**
	 * @method to define atrribute to Set booking Status
	 * @return string
	 * @param
	 */
	public function getStatusStringAttribute($status)
	{
		if($status == WasherBookingStatusInterface::INPROGRESS) {
			return 'WASH_IN_PROGRESS';
		} elseif ($status == WasherBookingStatusInterface::COMPLETED) {
			return 'WASH_COMPLETED';
		} elseif ($status == WasherBookingStatusInterface::PENDING) {
			return 'WASH_PENDING';
		} elseif ($status == WasherBookingStatusInterface::WASH_INITIAL) {
			return 'WASH_INITIALNO_WASH';
		} elseif ($status == WasherBookingStatusInterface::NO_WASH) {
			return 'NO_WASH';
		} else {
			return 'NO_STATUS';
		}
	}
}
