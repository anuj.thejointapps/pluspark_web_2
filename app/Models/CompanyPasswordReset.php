<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyPasswordReset extends Model
{
    # define table
    protected $table ='company_password_resets';

    # define fillable fields
    protected $fillable = [
        'email',
        'token',
        'created_at',
    ];

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
    'created_at'        => 'datetime',
    ];
}
