<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentForScanPrivateBooking extends Model
{
  	# define table.
  	protected $table = 'payment_for_scan_private';

  	# define fillable fildes
  	protected $fillable = [
  		'booking_id',
		'transaction_id',
		'tran_ref',
		'tran_type',
		'cart_id',
		'cart_description',
		'cart_currency',
		'cart_amount',
		'callback',
		'customer_details',
		'payment_result',
		'payment_info',
		'payment_type',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    => 'datetime',
	];
}
