<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

#Interfaces
use App\Interfaces\WasherTypeInterface;
use App\Interfaces\UserBookingStatusInterface;
use App\Interfaces\WasherBookingStatusInterface;

class Booking extends Model implements WasherBookingStatusInterface, UserBookingStatusInterface, WasherTypeInterface
{
  	# define table.
  	protected $table = 'user_bookings';

  	# define fillable fildes
  	protected $fillable = 	[
  		'order_number',
  		'key_no',
  		'pass_id',
  		'user_id',
  		'vehicle_id',
  		'park_no',
		'parking_id',
		'parking_type',
		'promocode_id',
		'promocode',
		'booking_price',
		'wash_price',
		'is_pre_book',
		'sub_total',
		'paid_money',
		'is_paid',
		'location',
		'latitude',
		'longitude',
		'valet',
		'wash_available',
		'valet_and_washing',
		'wash_type',
		'payment_method',
		'booking_status',
		'is_checked_in',
		'scan_timestamp',
		'arrival_date_time',
		'exit_date_time',
		'actual_in_dateTime',
		'actual_out_dateTime',
		'is_deleted',
		'is_payment_requested',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    		=> 'datetime',
	  'arrival_date_time'  	=> 'datetime',
	  'scan_timestamp'  	=> 'datetime',
	  'exit_date_time'    	=> 'datetime',
	  'actual_in_dateTime'  => 'datetime',
	  'actual_out_dateTime' => 'datetime',
	  'valet'        		=> 'boolean',
	  'wash_available'      => 'boolean',
	  'valet_and_washing'   => 'boolean',
	  'is_paid'   			=> 'boolean',
	  'is_pre_book'   		=> 'boolean',
	  'is_deleted'   		=> 'boolean',
	  'is_checked_in'  		=> 'boolean',
	  'is_payment_requested'=> 'boolean',
	];

	#Append the attibutes
	protected $appends = ['bill','arrived_timestamp'];

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function parking()
	{
		return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and vehicle
	 * @return relation
	 * @param
	 */
	public function vehicle()
	{
		return $this->belongsTo(UserVehicle::class, 'vehicle_id', 'id');
	}

	/**
	 * @method to define relation b/w model and Supervisor
	 * @return relation
	 * @param
	 */
	public function supervisor()
	{
		return $this->hasOne(SupervisorBooking::class, 'booking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and driver
	 * @return relation
	 * @param
	 */
	public function driver()
	{
		return $this->hasOne(DriverBooking::class, 'booking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and Supervisor
	 * @return relation
	 * @param
	 */
	public function supervisors()
	{
		return $this->hasMany(SupervisorBooking::class, 'booking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and driver
	 * @return relation
	 * @param
	 */
	public function drivers()
	{
		return $this->hasMany(DriverBooking::class, 'booking_id', 'id');
	}


	/**
	 * @method to define relation b/w model and washer
	 * @return relation
	 * @param
	 */
	public function washer()
	{
		return $this->hasOne(WasherBooking::class, 'booking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and payment
	 * @return relation
	 * @param
	 */
	public function payment()
	{
		return $this->hasMany(Payment::class, 'booking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and Promocode
	 * @return relation
	 * @param
	 */
	public function promoCode()
	{
		return $this->belongsTo(Promocode::class, 'promocode_id', 'id');
	}

	/**
	 * @method to define relation b/w model and pass
	 * @return relation
	 * @param
	 */
	public function pass()
	{
		return $this->hasOne(PassesForQrBooking::class, 'id', 'pass_id');
	}

	/**
     * @method to user by filters
     * @return query builder
     * @param $query Builder
    */
    public function scopeWashingAvail($query)
    {
    	$query->where('wash_available', true);
    }

    /**
     * @method filter models which have pass
     * @return query builder
     * @param $query Builder
    */
    public function scopePassAvailable($query)
    {
    	$query->where('pass_id', '<>', 0);
    }

    /**
     * @method to user by filters
     * @return query builder
     * @param $query Builder
    */
    public function scopeOnInitialStatus($query)
    {
    	$query->where('booking_status', UserBookingStatusInterface::INITIAL);
    }
	
	/**
	 * @method to define atrribute to set formatted Start Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getArrivedDateFormattedAttribute()
	{
		return $this->arrival_date_time->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getExitdateFormattedAttribute()
	{
		return $this->exit_date_time != '' ? $this->exit_date_time->format('Y-m-d') : '';
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getFullArrivedDateAttribute()
	{
		return ($this->arrival_date_time->format('Y-m-d'). ' '.$this->arrival_date_time->format('H:i:s'));
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getFullExitdateAttribute()
	{
		if($this->exit_date_time != '') {
			return ($this->exit_date_time->format('Y-m-d'). ' '.$this->exit_date_time->format('H:i:s'));
		}

		#return
		return '';
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedFormattedAttribute()
	{
		return ($this->arrival_date_time->format('l'). ' '.$this->arrival_date_time->format('h:i A'));
	}

	/**
	 * @method to define atrribute to arrived_after
	 * @return string
	 * @param
	 */
	public function getArrivedAfterDateAttribute()
	{
		if($this->actual_in_dateTime != '') {
			return ($this->actual_in_dateTime->format('l M d'));
		} elseif ($this->arrival_date_time != '') {
			return ($this->arrival_date_time->format('l M d'));
		} 

		#return 
		return '';
	}


	/**
	 * @method to define atrribute to arrived_after
	 * @return string
	 * @param
	 */
	public function getArrivedAfterTimeAttribute()
	{
		if($this->actual_in_dateTime != '') {
			return ($this->actual_in_dateTime->format('h:i A'));
		} elseif ($this->arrival_date_time != '') {
			return ($this->arrival_date_time->format('h:i A'));
		} 

		#return 
		return '';
	}

	/**
	 * @method to define atrribute to exit_after
	 * @return string
	 * @param
	 */
	public function getExitAfterDateAttribute()
	{
		if($this->actual_out_dateTime != '') {
			return ($this->actual_out_dateTime->format('l M d'));
		} elseif ($this->exit_date_time != '') {
			return ($this->exit_date_time->format('l M d'));
		} 

		#return 
		return '';
	}

	/**
	 * @method to define atrribute to exit_after
	 * @return string
	 * @param
	 */
	public function getExitAfterTimeAttribute()
	{
		if($this->actual_out_dateTime != '') {
			return ($this->actual_out_dateTime->format('h:i A'));
		} elseif ($this->exit_date_time != '') {
			return ($this->exit_date_time->format('h:i A'));
		} 

		#return 
		return '';
	}


	/**
	 * @method to define atrribute to Created Date
	 * @return string
	 * @param
	 */
	public function getCreatedDateAttribute()
	{
		return ($this->created_at->format('Y-m-d'));
	}

	/**
	 * @method to define atrribute to Created Time
	 * @return string
	 * @param
	 */
	public function getCreatedTimeAttribute()
	{
		return ($this->created_at->format('h:i A'));
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getExitFormattedAttribute()
	{
		if($this->exit_date_time != '') {
			return ($this->exit_date_time->format('l'). ' '.$this->exit_date_time->format('h:i A'));
		}

		#return
		return '';
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedFormattedTimeAttribute()
	{
		return ($this->arrival_date_time->format('h:i a'));
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedTimestampAttribute()
	{
		return ($this->arrival_date_time->timestamp);
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedTimeAttribute()
	{
		return $this->arrival_date_time->format('H:i a');
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getExitFormattedTimeAttribute()
	{
		if($this->exit_date_time != '') {
			return ($this->exit_date_time != '' ? $this->exit_date_time->format('H:i a') : '');
		}

		#return
		return '';
	}

	/**
	 * @method to define atrribute to fetch minute b/w created and arrival
	 * @return string
	 * @param
	 */
	public function getMinBwCreatedArrivalAttribute()
	{
		return $this->created_at->diffInMinutes($this->arrival_date_time). ' min';
	}

	/**
	 * @method to define atrribute to fetch arrival remaining time
	 * @return string
	 * @param
	 */
	public function getRemainingArrivalTimeAttribute()
	{
		#FEtch minutes
		$secondTime = $this->arrival_date_time->diffInSeconds(Carbon::now());
		$convertedTime = '';
		$format = '%02d hours %02d minutes %02d seconds';

		#Validate time 
		if ($secondTime < 1) {
	       $convertedTime = '';
	    } else {
	    	$hours = floor($secondTime / 3600);
		    $minutes = floor(($secondTime / 60) % 60);
		    $seconds = ($secondTime % 60);
		    $convertedTime = sprintf($format, $hours, $minutes, $seconds);
	    }
	 	
	 	#return 
	 	return $convertedTime;
	}

	/**
	 * @method to define atrribute to fetch Promocode Discount money
	 * @return string
	 * @param
	 */
	public function getPromocodeDiscountAttribute()
	{
		#Fetch total to pay
		$subTotal = $this->sub_total;

		#Fethc promocode
		$promocode = $this->promoCode;

		#validate Promocode
		if($promocode != '') {
			if($promocode->offer_type == 'percent') {
				$discount = ($subTotal * $promocode->discount)/100;

				#return 
				return $discount;
			}

			#Fetch Discount
			return $promocode->discount;
		}

		#return 
		return 0;
	}

	/**
	 * @method to define atrribute to set washing status 
	 * @return string
	 * @param
	 */
	public function getWashingStatusAttribute()
	{
		#FEtch washer
		$washer = $this->washer;

		#Validate washer
		if($washer != '' AND $washer->washing_status != '') {
			if($washer->washing_status == WasherBookingStatusInterface::INPROGRESS) {
				return 'WASH IN PROGRESS';
			} elseif ($washer->washing_status == WasherBookingStatusInterface::COMPLETED) {
				return 'WASH COMPLETED';
			} elseif ($washer->washing_status == WasherBookingStatusInterface::PENDING) {
				return 'WASH PENDING';
			}
		}
	}

	/**
	 * @method to define atrribute to set booking status 
	 * @return string
	 * @param
	 */
	public function getFetchBookingStatusAttribute()
	{
		if($this->booking_status == UserBookingStatusInterface::INITIAL) {
			return 'INITIAL';
		} elseif ($this->booking_status == UserBookingStatusInterface::ON_THE_WAY_PARK) {
			return 'ON_THE_WAY_PARK';
		} elseif ($this->booking_status == UserBookingStatusInterface::AVAILABLE_TO_PICK) {
			return 'AVAILABLE_TO_PICK';
		} elseif ($this->booking_status == UserBookingStatusInterface::CONFIRMED) {
			return 'CONFIRMED';
		} elseif ($this->booking_status == UserBookingStatusInterface::CANCELLED) {
			return 'CANCELLED';
		} elseif ($this->booking_status == UserBookingStatusInterface::EXPIRED) {
			return 'EXPIRED';
		} elseif ($this->booking_status == UserBookingStatusInterface::BOOKING_COMPLETED) {
			return 'Completed';
		} elseif ($this->booking_status == UserBookingStatusInterface::VEHICLE_PARKED) {
			return 'VEHICLE_PARKED';
		}
	}


	/**
	 * @method to define atrribute to check if booking is parked
	 * @return string
	 * @param
	 */
	public function getIsParkedAttribute()
	{
		if($this->booking_status == UserBookingStatusInterface::VEHICLE_PARKED) {
			return true;
		} 

		#return
		return false;
	}

	
	/**
	 * @method to define atrribute to parked_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getParkedDateTimeFormattedAttribute()
	{
		return $this->actual_in_dateTime != '' ? ($this->actual_in_dateTime->format('h:i a')) : '';
	}

	/**
	 * @method to define atrribute to booking status is in available to Pick
	 * @return string
	 * @param
	 */
	public function getIsInAvailToPickAttribute()
	{
		if($this->booking_status == UserBookingStatusInterface::AVAILABLE_TO_PICK) {
			return true;
		} 

		#retrun 
		return false;
	}

	/**
	 * @method to define atrribute to booking status is in Cancelled
	 * @return string
	 * @param
	 */
	public function getIsCancelledAttribute()
	{
		if($this->booking_status == UserBookingStatusInterface::CANCELLED) {
			return true;
		} 

		#retrun 
		return false;
	}

	/**
	 * @method to define atrribute to booking status is Expired
	 * @return string
	 * @param
	 */
	public function getIsExpiredAttribute()
	{
		if($this->booking_status == UserBookingStatusInterface::EXPIRED) {
			return true;
		} 

		#retrun 
		return false;
	}

	/**
	 * @method to define atrribute to booking status is Completed
	 * @return string
	 * @param
	 */
	public function getIsCompletedAttribute()
	{
		if($this->booking_status == UserBookingStatusInterface::BOOKING_COMPLETED) {
			return true;
		} 

		#retrun 
		return false;
	}


	/**
	 * @method to define atrribute to booking status is Completed
	 * @return string
	 * @param
	 */
	public function getIsOnTheWayOrAvialableToPickAttribute()
	{
		#allowed Status
		$allowedStatus = [
			UserBookingStatusInterface::INITIAL, 
			#UserBookingStatusInterface::AVAILABLE_TO_PICK, 
			#UserBookingStatusInterface::ON_THE_WAY_PARK, 
			UserBookingStatusInterface::CONFIRMED
		];

		#VAlidate and return
		if(in_array($this->booking_status, $allowedStatus)) {
			return true;
		} 

		#retrun 
		return false;
	}

	/**
	 * @method to define atrribute to set washing type 
	 * @return string
	 * @param
	 */
	public function getFetchWashTypeAttribute()
	{
		#FEtch washType
		$washType = $this->wash_type;

		#Validate washType
		if($washType == 1) {
			return 'Full Wash';
		} elseif ($washType == 2) {
			return 'Interior Wash';
		} elseif ($washType == 3) {
			return 'Exterior Wash';
		} else {
			return 'Not found';
		}
	}

	
	/**
	 * @method to fetch bill 
	 * @return array
	 * @param
	 */
	public function getBillAttribute()
	{
		#Fetch BookingPayment method
		$paymentMethod = $this->payment_method;

		#Fethc washer and driver Commision on Company of Parking on which booking occur
		$washerCommission = $this->parking->company->washerCommission;
		$driverCommission =  $this->parking->company->driverCommission;

		// #Validate washer and driver commission
		// if($washerCommission == '' OR $driverCommission == '') {
		// 	$data = ['status'=> false, 'message'=> 'Driver or Washer commission is not set by admin on company of parking'];

		// 	#return 
		// 	return $data;
		// }

		#Fetch Booking Price,Valet Price and valetOrWashPrice
		$bookingPrice = $this->parking->parkingMarkOut->set_book_price ?? 0;
		$valetPrice = $this->parking->parkingMarkOut->set_valet_price ?? 0;
		$fullWashPrice = $this->parking->parkingMarkOut->full_wash_cost ?? 0;
		$interiorWashPrice = $this->parking->parkingMarkOut->interior_wash_cost ?? 0;
		$exteriorWahPrice = $this->parking->parkingMarkOut->exterior_wash_cost ?? 0;
		$valetWashPrice = $this->parking->parkingMarkOut->set_valet_and_washing_price ?? 0;

		#Fetch Parking Driver fetaures
		$parkingDriverFeature = $this->parking->parkingDriveFeatures;

		#Validate
		if($parkingDriverFeature->isEmpty() OR $parkingDriverFeature->last()->type == '') {
			$data = ['status'=> false, 'message'=> 'Driver fetaures Or Driver type is not set on Parking of Booking'];

			#return 
			return $data;
		}

		#Fetch Parking Washer 
		$parkingWasherFeature = $this->parking->parkingWasherFeatures;

		#Validate
		if($parkingWasherFeature->isEmpty() OR $parkingWasherFeature->last()->type == '') {
			$data = ['status'=> false, 'message'=> 'Washer fetaures Or Washer type is not set on Parking of Booking'];

			#return 
			return $data;
		}

		#Fetch Driver and Washer provided by (Company, PlusPark or Third Party)
		$driverProvider = $parkingDriverFeature->last()->type;
		$washerProvider = $parkingWasherFeature->last()->type;

		#set total commission percent on valet
		$totalDriverCommissionPercentOnCash = 0;
		$totalDriverCommissionPercentOnCard = 0;
		if($this->valet AND $driverCommission != '') {
			if($driverProvider == 'company') {
				$totalDriverCommissionPercentOnCash = $driverCommission->company_cash_comm;
				$totalDriverCommissionPercentOnCard = $driverCommission->company_card_comm;
			} elseif ($driverProvider == 'plus_park') {
				$totalDriverCommissionPercentOnCash = $driverCommission->company_cash_comm + 
														$driverCommission->pls_park_cash_comm;
				$totalDriverCommissionPercentOnCard = $driverCommission->company_card_comm + 
														$driverCommission->pls_park_card_comm;
			} elseif ($driverProvider == 'third_party') {
				$totalDriverCommissionPercentOnCash = $driverCommission->company_cash_comm + 
														$driverCommission->pls_park_cash_comm + 
														$driverCommission->thrd_party_cash_comm;
				$totalDriverCommissionPercentOnCard = $driverCommission->company_card_comm + 
														$driverCommission->pls_park_card_comm +
														$driverCommission->thrd_party_card_comm;;
			}

		}
	
		#set total commission percent on washer
		$totalWasherCommissionPercentOnCash = 0;
		$totalWasherCommissionPercentOnCard = 0;
		if($this->wash_available AND $washerCommission != '') {
			if($washerProvider == 'company') {
				$totalWasherCommissionPercentOnCash = $washerCommission->company_cash_comm;
				$totalWasherCommissionPercentOnCard = $washerCommission->company_card_comm;
			} elseif ($driverProvider == 'plus_park') {
				$totalWasherCommissionPercentOnCash = $washerCommission->company_cash_comm + 
														$washerCommission->pls_park_cash_comm;
				$totalWasherCommissionPercentOnCard = $washerCommission->company_card_comm + 
														$washerCommission->pls_park_card_comm;
			} elseif ($driverProvider == 'third_party') {
				$totalWasherCommissionPercentOnCash = $washerCommission->company_cash_comm + 
														$washerCommission->pls_park_cash_comm + 
														$washerCommission->thrd_party_cash_comm;
				$totalWasherCommissionPercentOnCard = $washerCommission->company_card_comm + 
														$washerCommission->pls_park_card_comm +
														$washerCommission->thrd_party_card_comm;;
			}
		}
	

		#Fetch booking Price 
		$bookingPrice = $this->parking->parkingMarkOut->set_book_price ?? 0;
		#$valetPrice = $parkingDriverFeature->last()->cost_of_valet ?? 0;
		
		#Set Wash Price
		$washPrice = 0;
		if($this->wash_available) {
			if($this->wash_type == WasherTypeInterface::FULL_WASH) {
				#$washPrice = $parkingWasherFeature->last()->cost_of_full_wash ?? 0;
				$washPrice = $fullWashPrice ?? 0;
			} elseif ($this->wash_type == WasherTypeInterface::INTERIOR) {
				#$washPrice = $parkingWasherFeature->last()->cost_of_interior_wash ?? 0;
				$washPrice = $interiorWashPrice ?? 0;
			} elseif ($this->wash_type == WasherTypeInterface::EXTERIOR) {
				#$washPrice = $parkingWasherFeature->last()->cost_of_exterior_wash ?? 0;
				$washPrice = $exteriorWahPrice ?? 0;
			}
		}

		#Set valet Commision Percent
		$valetCommissionPercent = 0;
		if($this->payment_method == 'cash') {
			$valetCommissionPercent = $totalDriverCommissionPercentOnCash;
		} elseif ($this->payment_method == 'card') {
			$valetCommissionPercent = $totalDriverCommissionPercentOnCard;
		}

		#Set washer Commision Percent
		$washerCommissionPercent = 0;
		if($this->payment_method == 'cash') {
			$washerCommissionPercent = $totalWasherCommissionPercentOnCash;
		} elseif ($this->payment_method == 'card') {
			$washerCommissionPercent = $totalWasherCommissionPercentOnCard;
		}

		#Set total Valet Price
		$totalValetPrice = ($this->valet_and_washing OR $this->valet) ? 
							(($valetPrice * (100 + $valetCommissionPercent))/100) : 0;
		$totalWasherPrice = ($washPrice * (100 + $washerCommissionPercent))/100;

		#Set bookng
		$booking 		= $this;
		$user          	= $booking->user;
        $vehicle        = $booking->vehicle;
        $parking        = $booking->parking;
        $company        = $booking->parking->company;
        $parkingSpace   = $booking->parking->parkingSetupSpace;

        #Set total Commising
        $totalCommission = 0;
        if($this->valet AND $this->wash_available) {
        	$totalCommission = (($totalValetPrice - $valetPrice) + ($totalWasherPrice - $washPrice));
        } elseif ($this->valet) {
        	$totalCommission = (($totalValetPrice - $valetPrice));
        } elseif($this->wash_available) {
        	$totalCommission =  ($totalWasherPrice - $washPrice);
        }

        #FEtch payment on Boooking
        $paymentsOnBooking = $this->payment->isNotEmpty() ? $this->payment->sum('cart_amount') : 0;

		#Set Booking Bill Data
		$data = [
			'parking_id'        => $booking->parking_id ?? '',
            'payment_method'    => $booking->payment_method ?? '',
            'company_name'      => $company->name ?? '',
            'user_address'      => $user->address ?? '',
            'user_id'      		=> $user->id ?? '',
            'booking_id'        => $booking->id ?? '',
            #'supervisor_id'     => $supervisor->id ?? '',
             'vehicle_id'  		=> $vehicle->id ?? '',
            'vehicle_plate_no'  => $vehicle->plate_number ?? '',
            'vehicle_name'      => $vehicle->full_name ?? '',
            'vehicle_type'      => $vehicle->vehicle_type ?? '',
			'booking_price' 	=> (string)$bookingPrice,
			'valet_base_price'	=> $this->is_valet_available ? (string)$valetPrice : '0',
			'valet_commission'	=> ($totalValetPrice != 0) ? ($totalValetPrice - $valetPrice) : 0,
			'wash_base_price'	=> (string)$washPrice,
			'wash_commission'	=> ($totalWasherPrice - $washPrice),
			'total_commission'	=> $totalCommission,
			'payable_money'		=> (string)($bookingPrice + ( $this->is_valet_available ? $totalValetPrice : 0) + $totalWasherPrice),
			'paid_money'		=> $paymentsOnBooking,
			'due_amount'		=> $this->due_amount
		];
		#returj data
		$data = [
			'status'	=> true, 
			'message'	=> 'Bill data found',
			'data' 		=> $data
		];

		#return 
		return $data;
	}

	/**
	 * @method to fetch paid Amount on Booking
	 * @return 
	 * @param
	 */
	public function getPaidAmountAttribute()
	{
		#FEtch payment on Boooking
        $paidAmount = $this->payment->isNotEmpty() ? $this->payment->sum('cart_amount') : 0;

        #return paid
        return $paidAmount;
	}

	/**
	 * @method to fetch due Amounton Booking
	 * @return 
	 * @param
	 */
	public function getDueAmountAttribute()
	{
		#Fetch BookingPayment method
		$paymentMethod = $this->payment_method;

		#Fethc washer and driver Commision on Company of Parking on which booking occur
		$washerCommission = $this->parking->company->washerCommission;
		$driverCommission =  $this->parking->company->driverCommission;

		// #Validate washer and driver commission
		// if($washerCommission == '' OR $driverCommission == '') {
		// 	$data = ['status'=> false, 'message'=> 'Driver or Washer commission is not set by admin on company of parking'];

		// 	#return 
		// 	return $data;
		// }

		#Fetch Booking Price,Valet Price and valetOrWashPrice
		$bookingPrice = $this->parking->parkingMarkOut->set_book_price ?? 0;
		$valetPrice = $this->parking->parkingMarkOut->set_valet_price ?? 0;
		$fullWashPrice = $this->parking->parkingMarkOut->full_wash_cost ?? 0;
		$interiorWashPrice = $this->parking->parkingMarkOut->interior_wash_cost ?? 0;
		$exteriorWahPrice = $this->parking->parkingMarkOut->exterior_wash_cost ?? 0;
		$valetWashPrice = $this->parking->parkingMarkOut->set_valet_and_washing_price ?? 0;

		#Fetch Parking Driver fetaures
		$parkingDriverFeature = $this->parking->parkingDriveFeatures;

		#Validate
		if($parkingDriverFeature->isEmpty() OR $parkingDriverFeature->last()->type == '') {

			#return 
			return 0;
		}

		#Fetch Parking Washer 
		$parkingWasherFeature = $this->parking->parkingWasherFeatures;

		#Validate
		if($parkingWasherFeature->isEmpty() OR $parkingWasherFeature->last()->type == '') {

			#return 
			return 0;
		}

		#Fetch Driver and Washer provided by (Company, PlusPark or Third Party)
		$driverProvider = $parkingDriverFeature->last()->type;
		$washerProvider = $parkingWasherFeature->last()->type;

		#set total commission percent on valet
		$totalDriverCommissionPercentOnCash = 0;
		$totalDriverCommissionPercentOnCard = 0;
		if($this->valet AND $driverCommission != '') {
			if($driverProvider == 'company') {
				$totalDriverCommissionPercentOnCash = $driverCommission->company_cash_comm;
				$totalDriverCommissionPercentOnCard = $driverCommission->company_card_comm;
			} elseif ($driverProvider == 'plus_park') {
				$totalDriverCommissionPercentOnCash = $driverCommission->company_cash_comm + 
														$driverCommission->pls_park_cash_comm;
				$totalDriverCommissionPercentOnCard = $driverCommission->company_card_comm + 
														$driverCommission->pls_park_card_comm;
			} elseif ($driverProvider == 'third_party') {
				$totalDriverCommissionPercentOnCash = $driverCommission->company_cash_comm + 
														$driverCommission->pls_park_cash_comm + 
														$driverCommission->thrd_party_cash_comm;
				$totalDriverCommissionPercentOnCard = $driverCommission->company_card_comm + 
														$driverCommission->pls_park_card_comm +
														$driverCommission->thrd_party_card_comm;;
			}

		}
	
		#set total commission percent on washer
		$totalWasherCommissionPercentOnCash = 0;
		$totalWasherCommissionPercentOnCard = 0;
		if($this->wash_available AND $washerCommission != '') {
			if($washerProvider == 'company') {
				$totalWasherCommissionPercentOnCash = $washerCommission->company_cash_comm;
				$totalWasherCommissionPercentOnCard = $washerCommission->company_card_comm;
			} elseif ($driverProvider == 'plus_park') {
				$totalWasherCommissionPercentOnCash = $washerCommission->company_cash_comm + 
														$washerCommission->pls_park_cash_comm;
				$totalWasherCommissionPercentOnCard = $washerCommission->company_card_comm + 
														$washerCommission->pls_park_card_comm;
			} elseif ($driverProvider == 'third_party') {
				$totalWasherCommissionPercentOnCash = $washerCommission->company_cash_comm + 
														$washerCommission->pls_park_cash_comm + 
														$washerCommission->thrd_party_cash_comm;
				$totalWasherCommissionPercentOnCard = $washerCommission->company_card_comm + 
														$washerCommission->pls_park_card_comm +
														$washerCommission->thrd_party_card_comm;;
			}
		}
	

		#Fetch booking Price 
		$bookingPrice = $this->parking->parkingMarkOut->set_book_price ?? 0;
		#$valetPrice = $parkingDriverFeature->last()->cost_of_valet ?? 0;
		
		#Set Wash Price
		$washPrice = 0;
		if($this->wash_available) {
			if($this->wash_type == WasherTypeInterface::FULL_WASH) {
				#$washPrice = $parkingWasherFeature->last()->cost_of_full_wash ?? 0;
				$washPrice = $fullWashPrice ?? 0;
			} elseif ($this->wash_type == WasherTypeInterface::INTERIOR) {
				#$washPrice = $parkingWasherFeature->last()->cost_of_interior_wash ?? 0;
				$washPrice = $interiorWashPrice ?? 0;
			} elseif ($this->wash_type == WasherTypeInterface::EXTERIOR) {
				#$washPrice = $parkingWasherFeature->last()->cost_of_exterior_wash ?? 0;
				$washPrice = $exteriorWahPrice ?? 0;
			}
		}

		#Set valet Commision Percent
		$valetCommissionPercent = 0;
		if($this->payment_method == 'cash') {
			$valetCommissionPercent = $totalDriverCommissionPercentOnCash;
		} elseif ($this->payment_method == 'card') {
			$valetCommissionPercent = $totalDriverCommissionPercentOnCard;
		}

		#Set washer Commision Percent
		$washerCommissionPercent = 0;
		if($this->payment_method == 'cash') {
			$washerCommissionPercent = $totalWasherCommissionPercentOnCash;
		} elseif ($this->payment_method == 'card') {
			$washerCommissionPercent = $totalWasherCommissionPercentOnCard;
		}

		#Set total Valet Price
		$totalValetPrice = ($this->valet_and_washing OR $this->valet) ? 
							(($valetPrice * (100 + $valetCommissionPercent))/100) : 0;
		$totalWasherPrice = ($washPrice * (100 + $washerCommissionPercent))/100;

		#Set bookng
		$booking 		= $this;
		$user          	= $booking->user;
        $vehicle        = $booking->vehicle;
        $parking        = $booking->parking;
        $company        = $booking->parking->company;
        $parkingSpace   = $booking->parking->parkingSetupSpace;

       	#Set total Commising
        $totalCommission = 0;
        if($this->valet AND $this->wash_available) {
        	$totalCommission = (($totalValetPrice - $valetPrice) + ($totalWasherPrice - $washPrice));
        } elseif ($this->valet) {
        	$totalCommission = (($totalValetPrice - $valetPrice));
        } elseif($this->wash_available) {
        	$totalCommission =  ($totalWasherPrice - $washPrice);
        }

        #FEtch payment on Boooking
        $paymentsOnBooking = $this->payment->isNotEmpty() ? $this->payment->sum('cart_amount') : 0;

        #validate bill Status
        $payableAmount = $bookingPrice + ( $this->is_valet_available ? $totalValetPrice : 0) + $totalWasherPrice;

        #FEtch payment on Boooking
        $paidAmount = $this->payment->isNotEmpty() ? $this->payment->sum('cart_amount') : 0;

        #Set Due Amount
        $dueAmount = $payableAmount - $paymentsOnBooking;

        #return paid
        return $dueAmount;
	}
	

	/**
	 * @method to fetch booking Pass Confirm time left in seconds 
	 * @return array
	 * @param
	 */
	public function getConfirmTimeLeftAttribute()
	{
		#Fetch Pass
		$pass = $this->pass;

		#Validate
		if($pass == '') {
			return 0;
		}

		#FEtch PArking Setupspace
		$parkingSpace = $this->parking->parkingSetupSpace;

		#Fetch minutes to Confirm
		$minutesToConfirm = ($parkingSpace != '' AND $parkingSpace->booking_confirm_time != '') ? 
							$parkingSpace->booking_confirm_time : 0;

		#Calculate Time left in seconds
		#$timeInSeconds = 900 - (Carbon::now()->diffInSeconds($pass->created_at));
		$timeInSeconds = $minutesToConfirm * 60;

		#return time in seconds
		return $timeInSeconds;
	}


	/**
	 * @method to fetch booking Pass Confirm time left in seconds 
	 * @return array
	 * @param
	 */
	public function getEditCancelTimeLeftAttribute()
	{
		#Fetch Pass
		$pass = $this->pass;

		#Validate
		if($pass == '') {
			return 0;
		}

		#FEtch PArking Setupspace
		$parkingSpace = $this->parking->parkingSetupSpace;

		#Fetch minutes to Confirm
		$minutesToEditOrCancel = ($parkingSpace != '' AND $parkingSpace->booking_cancel_edit_time != '') ? 
							$parkingSpace->booking_cancel_edit_time : 0;

		#Calculate Time left in seconds
		#$timeInSeconds = 900 - (Carbon::now()->diffInSeconds($pass->created_at));
		$timeInSeconds = $minutesToEditOrCancel * 60;

		#return time in seconds
		return $timeInSeconds;
	}

	/**
	 * @method to fetch booking supervisor name
	 * @return array
	 * @param
	 */
	public function getSupervisorNameAttribute()
	{
		#Fetch Supervsior
		$supervisor = $this->supervisors->isNotEmpty() ? $this->supervisors->last()->supervisor : '';

		#Fetch Supervisor on Parking
		$supervisorsOnParking = $this->parking->supervisors->isNotEmpty() ? 
								$this->parking->supervisors->first() : '';
		
		#Validate
		if($supervisor != '') {
			return $supervisor->name;
		} elseif ($supervisorsOnParking != '') {
			return $supervisorsOnParking->name;
		} else {
			return '';
		}
	}


	/**
	 * @method to fetch booking supervisor name
	 * @return array
	 * @param
	 */
	public function getEmployeeNameAttribute()
	{
		#Fetch Supervsior
		$driver = $this->drivers->isNotEmpty() ? $this->drivers->last()->driver : '';

		#Fetch Supervisor on Parking
		$supervisorsOnParking = $this->parking->supervisors->isNotEmpty() ? 
								$this->parking->supervisors->first() : '';
		
		#Validate
		if($driver != '') {
			return $driver->name;
		} elseif ($supervisorsOnParking != '') {
			return $supervisorsOnParking->name;
		} else {
			return '';
		}
	}


	/**
	 * @method to fetch booking processing time
	 * @return array
	 * @param
	 */
	public function getProcessingTimeAttribute()
	{
		#FEtch minutes
		$secondTime = ($this->actual_in_dateTime != '' AND $this->scan_timestamp != '') ? 
						$this->actual_in_dateTime->diffInSeconds($this->scan_timestamp) : 0;
		$convertedTime = '';
		$format = '%02d h %02d m';

		#Validate time 
		if ($secondTime < 1) {
	       $convertedTime = '';
	    } else {
	    	$hours = floor($secondTime / 3600);
		    $minutes = floor(($secondTime / 60) % 60);
		    $seconds = ($secondTime % 60);
		    $convertedTime = sprintf($format, $hours, $minutes);
	    }
	 	
	 	#return 
	 	return $convertedTime;
	}

	/**
	 * @method to fetch booking duration
	 * @return array
	 * @param
	 */
	public function getDurationAttribute()
	{
		#FEtch minutes
		$secondTime = $this->actual_in_dateTime != '' ? $this->actual_in_dateTime->diffInSeconds(Carbon::now()) : 0;
		$convertedTime = '';
		$format = '%02d h %02d m';

		#Validate time 
		if ($secondTime < 1) {
	       $convertedTime = '';
	    } else {
	    	$hours = floor($secondTime / 3600);
		    $minutes = floor(($secondTime / 60) % 60);
		    $seconds = ($secondTime % 60);
		    $convertedTime = sprintf($format, $hours, $minutes);
	    }
	 	
	 	#return 
	 	return $convertedTime;
	}

	/**
	 * @method to fetch booking key no
	 * @return array
	 * @param
	 */
	public function getKeyNumberAttribute()
	{
		#Fetch Supervsior
		$supervisor = $this->supervisors->isNotEmpty() ? $this->supervisors->last()->supervisor : '';
		$driver = $this->drivers->isNotEmpty() ? $this->drivers->last()->driver : '';
		
		#Validate
		if($supervisor != '' OR $driver != '') {
			if($supervisor != '') {
				return $supervisor->key_no;
			} else {
				return $driver->key_no;
			}
		}

		#return 
		return '';
	}

	/**
	 * @method to check valet is chosen or not
	 * @return boolean
	 * @param
	 */
	public function getIsValetAvailableAttribute()
	{
		if($this->valet) {
			return true;
		}

		#return 
		return false;
	}

}
