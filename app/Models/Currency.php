<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{    
    use SoftDeletes, Notifiable;

    # define table
    protected $table ='currencies';

    # define fillable fields
    protected $fillable =   [
                                'name',
                                'currency_code',
                                'symbol',
                                'country_id',
                                'status'
                            ];

    
    /**
    * @method to define relation to country
    * @param
    * @return relation
    */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
