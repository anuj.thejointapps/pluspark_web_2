<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Interfaces\DriverBookingStatusInterface;

class DriverBooking extends Model implements DriverBookingStatusInterface
{
  	# define table.
  	protected $table = 'driver_bookings';

  	# define fillable fildes
  	protected $fillable = 	[
  		'parking_id',
		'booking_id',
		'driver_id',
		'set_or_get',
		'key_no',
		'park_no',
		'pick_up_lat',
		'pick_up_long',
		'booking_status',
		'arrived_date_time',
		'exit_date_time',
		'requested_date_time',
		'parked_date_time',
  	];

  	/**
    * The attributes that should be cast to native types.
    *
    * @var array
	*/
	protected $casts = [
	  'created_at'    			=> 'datetime',
	  'arrived_date_time'  		=> 'datetime',
	  'exit_date_time'    		=> 'datetime',
	  'requested_date_time'    	=> 'datetime',
	  'parked_date_time'    	=> 'datetime',
	];

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @method to define relation b/w model and user
	 * @return relation
	 * @param
	 */
	public function parking()
	{
		return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
	}

	/**
	 * @method to define relation b/w model and driver
	 * @return relation
	 * @param
	 */
	public function driver()
	{
		return $this->belongsTo(Washerman::class, 'driver_id', 'id');
	}


	/**
	 * @method to define relation b/w model and vehicle
	 * @return relation
	 * @param
	 */
	public function vehicle()
	{
		return $this->belongsTo(UserVehicle::class, 'vehicle_id', 'id');
	}

	/**
	 * @method to define relation b/w model and booking
	 * @return relation
	 * @param
	 */
	public function booking()
	{
		return $this->belongsTo(Booking::class, 'booking_id', 'id');
	}
	
	/**
	 * @method to define atrribute to set formatted Start Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getStartDateFormattedAttribute()
	{
		return $this->start_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getEndDateFormattedAttribute()
	{
		return $this->end_date->format('Y-m-d');
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedFormattedAttribute()
	{
		return ($this->arrived_date_time->format('l'). ' '.$this->arrived_date_time->format('H:i a'));
	}

	/**
	 * @method to define atrribute to set formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getExitFormattedAttribute()
	{
		return ($this->exit_date_time->format('l'). ' '.$this->exit_date_time->format('H:i a'));
	}

	/**
	 * @method to define atrribute to arrived_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getArrivedTimeAttribute()
	{
		return $this->arrived_date_time->format('H:i a');
	}

	/**
	 * @method to define atrribute to fetch Exit time formatyted in 12 hours
	 * @return string
	 * @param
	 */
	public function getExitTimeAttribute()
	{
		return $this->exit_date_time != '' ? $this->exit_date_time->format('H:i a') : '';
	}

	/**
	 * @method to define atrribute to fetch requested date time timestamp
	 * @return string
	 * @param
	 */
	public function getRequestTimestampAttribute()
	{
		return $this->requested_date_time != '' ? $this->requested_date_time->timestamp : 0;
	}

	/**
	 * @method to define atrribute to set requested formatted End Date(YYYY-MM-DD)
	 * @return string
	 * @param
	 */
	public function getRequestedDateFormattedAttribute()
	{
		return $this->requested_date_time != '' ? $this->requested_date_time->format('Y-m-d') : '';
	}

	/**
	 * @method to define atrribute to parked_date_time fomatted formatted Start Date(Day Time)
	 * @return string
	 * @param
	 */
	public function getParkedDateTimeFormattedAttribute()
	{
		return $this->parked_date_time != '' ? ($this->parked_date_time->format('h:i a')) : '';
	}

	/**
	 * @method to define atrribute to set requested time formatted End Date(H:i a)
	 * @return string
	 * @param
	 */
	public function getRequestedFormattedTimeAttribute()
	{
		if($this->requested_date_time != '') {
			return ($this->requested_date_time != '' ? $this->requested_date_time->format('H:i a') : '');
		}

		#return
		return '';
	}

	/**
	 * @method to define atrribute to fetch booking Status
	 * @return string
	 * @param
	 */
	public function getFetchBookingStatusAttribute()
	{
		if($this->booking_status == DriverBookingStatusInterface::INPROGRESS) {
			return 'In Progress';
		} elseif ($this->booking_status == DriverBookingStatusInterface::COMPLETED) {
			return 'Completed';
		} elseif ($this->booking_status == DriverBookingStatusInterface::ON_THE_WAY_TO_PARK) {
			return 'ON_THE_WAY_TO_PARK';
		} elseif ($this->booking_status == DriverBookingStatusInterface::PARKED) {
			return 'PARKED';
		} elseif ($this->booking_status == DriverBookingStatusInterface::GET_CAR_REQUESTD) {
			return 'GET_CAR_REQUEST';
		} elseif ($this->booking_status == DriverBookingStatusInterface::ON_THE_WAY_TO_DROPD) {
			return 'ON_THE_WAY_TO_DROP';
		} elseif ($this->booking_status == DriverBookingStatusInterface::ARRIVEDD) {
			return 'ARRIVED';
		} elseif ($this->booking_status == DriverBookingStatusInterface::BILL_GENERATEDD) {
			return 'BILL_GENERATED';
		} else {
			return '';
		}
	}

	/**
	 * @method to define attribute for completed status
	 * @return Builder
	 * @param
	 */
	public function getIsCompletedAttribute()
	{
		return ($this->booking_status == DriverBookingStatusInterface::COMPLETED ? true : false);
	}

	/**
	 * @method to define attribute for BILL_GENERATED status
	 * @return Builder
	 * @param
	 */
	public function getIsBillGeneratedAttribute()
	{
		return ($this->booking_status == DriverBookingStatusInterface::BILL_GENERATEDD ? true : false);
	}

	/**
	 * @method to define scope only for GET_CAR_REQUEST
	 * @return Builder
	 * @param
	 */
	public function scopeGetCarRequest($query)
	{
		return $query->where('booking_status', DriverBookingStatusInterface::GET_CAR_REQUESTD);
	}

	/**
	 * @method to define scope only for set type booking
	 * @return Builder
	 * @param
	 */
	public function scopeSetType($query)
	{
		return $query->where('set_or_get', 'set');
	}

	/**
	 * @method to define scope only for get type bookings
	 * @return Builder
	 * @param
	 */
	public function scopegetType($query)
	{
		return $query->where('set_or_get', 'get');
	}
}
