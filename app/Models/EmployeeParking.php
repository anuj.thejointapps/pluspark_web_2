<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeParking extends Model
{
    use SoftDeletes;
  
    # define table
    protected $table ='employee_parkings';
  
    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'employee_id'
                            ];


    /**
     * @method Scope a query to only include active color.
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeonlyEmployee($query, $employeeId)
    {
      return $query->where('employee_id', $employeeId);
    }
}
