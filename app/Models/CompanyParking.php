<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyParking extends Model 
{
    use SoftDeletes;
  
    # define table
    protected $table ='company_parkings';

    #Define append
    protected $appends = ['image_full_path'];
  
    # define fillable fildes
    protected $fillable =   [
                                'parking_auto_id',
                                'name',
                                'company_id',
                                'parking_type',
                                'city',
                                'country',
                                'location',
                                'image_path',
                                'status'
                            ];

    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
      'created_at'          => 'datetime',
      'status'              => 'boolean',
    ];

    /**
     * @method to scope get alloted washer by company id
     * @return 
     * @param
    */
    public function scopeonlyCompany($query, $companyId)
    {
        return $query->where('company_id', $companyId);
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingDriveFeatures()
    {
        return $this->hasMany(ParkingDriverFeature::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingWasherFeatures()
    {
        return $this->hasMany(ParkingWasherFeature::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingBookingFeature()
    {
        return $this->hasOne(ParkingBookingFeature::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingSubscriptionFeature()
    {
        return $this->hasOne(ParkingSubscriptionFeature::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingSetupSpace()
    {
        return $this->hasOne(ParkingSetupSpace::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingDescription()
    {
        return $this->hasOne(ParkingDescription::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingFeature()
    {
        return $this->hasOne(ParkingFeature::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingMarkOut()
    {
        return $this->hasOne(ParkingMarkOut::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingPayoutSetting()
    {
        return $this->hasOne(ParkingPayoutSetting::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function parkingAdvanceSetting()
    {
        return $this->hasOne(ParkingAdvanceSetting::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function supervisors()
    {
        return $this->hasMany(Washerman::class, 'current_parking_id', 'id')->where('job_type_id', 1);
    }


    /**
     * @method to relation
     * @return 
     * @param
    */
    public function drivers()
    {
        return $this->hasMany(Washerman::class, 'current_parking_id', 'id')->where('job_type_id', 2);
    }



    /**
     * @method to fetch image with full path
     * @return string|''
     * @param
    */
    public function getImageFullPathAttribute()
    {
      return $this->image_path != '' ? Url('/').'/'.$this->image_path : '';
    }

    /**
     * @method to fetch active 
     * @return string|''
     * @param
    */
    public function getisActiveAttribute()
    {
        return ($this->status ? true : false);
    }

    /**
     * @method to fetch active or disabled 
     * @return string|''
     * @param
    */
    public function getActiveOrDisabledAttribute()
    {
        if($this->status) {
            #Fetch day Today
            $todayDay = strtolower(Carbon::now()->format('l'));

            #Fetch all days when Pparking is open
            $parkingOpenDays = $this->parkingMarkOut != '' ? 
                                json_decode($this->parkingMarkOut->days_space_available) : [];
            
            #Validate
            if(in_array($todayDay, $parkingOpenDays)) {
                return 'Active';
            } else {
                return 'Disabled';
            }
        } else {
            return 'Disabled';
        }
    }

    /**
     * @method to fetch Washing Cost Data
     * @return string|''
     * @param
    */
    public function getWashingCostAttribute()
    {
        #Fetch company of Parking
        $company = $this->company;

        #Fetch washerCommision on Company
        $washerCommission = $company->washerCommission;

        #Set 
        #Set Data
        $data = [
            'commission_percent_on_cash'    =>  '0.00',
            'commission_percent_on_card'    =>  '0.00',
            'cost_of_full_wash'             =>  '0.00',
            'cost_of_exterior_wash'         =>  '0.00',
            'cost_of_interior_wash'         =>  '0.00',
        ];

        #Fetcn washing All costs
        $washingCosts = $this->parkingMarkOut;
        
        #Set Commisiion percent on cash and card
        $commisionPercentOnCard = 0;
        $commisionPercentOnCash = 0;
        if($washingCosts != '' AND $washerCommission != '') {
            $washingCost  = $washingCosts;

            #fetch what type(company, plus_park, third_party) washer avail
            $washerType = $washingCost->type;

            #validate and set Commision
            if($washerType == 'company') {
                $commisionPercentOnCard = $washerCommission->company_card_comm;
                $commisionPercentOnCash = $washerCommission->company_cash_comm;
            } elseif ($washerType == 'plus_park') {
                $commisionPercentOnCard = ($washerCommission->pls_park_card_comm + $washerCommission->company_card_comm);
                $commisionPercentOnCash = ($washerCommission->pls_park_cash_comm + $washerCommission->company_cash_comm);
            } elseif ($washerType == 'third_party') {
                $commisionPercentOnCard = ($washerCommission->thrd_party_card_comm + 
                                          $washerCommission->company_card_comm + 
                                          $washerCommission->pls_park_card_comm);
                $commisionPercentOnCash = ($washerCommission->thrd_party_cash_comm + 
                                            $washerCommission->company_cash_comm + 
                                            $washerCommission->pls_park_cash_comm);
            }
        }

        #validate
        if($washingCosts != '') {
            #Validate
                $washingCost = $washingCosts;
               
                #Set Data
                $data = [
                    'commission_percent_on_cash'    => (string)$commisionPercentOnCash,
                    'commission_percent_on_card'    => (string)$commisionPercentOnCard,
                    'cost_of_full_wash'             => $washingCost->full_wash_cost != '' ? (string)$washingCost->full_wash_cost : '0',
                    'cost_of_interior_wash'         => $washingCost->interior_wash_cost != '' ? (string)$washingCost->interior_wash_cost : '0',
                    'cost_of_exterior_wash'         => $washingCost->exterior_wash_cost != '' ? (string)$washingCost->exterior_wash_cost : '0',
                ];

                #return
                return $data;
        }

        #return 
        return $data;
    }

    /**
     * @method to fetch all vehicle types which can be parked in Parking
     * @return string|''
     * @param
    */
    public function getVehiclesCanParkedAttribute()
    {
        #Fetch
        return json_decode($this->parkingDescription->select_vehicle_type);
    }

    /**
     * @method to fetch all vehicle types which can be parked in Parking
     * @return string|''
     * @param
    */
    public function getAllPaymentMethodsAttribute()
    {
        #Fetch markout
        $markout = $this->parkingMarkOut;

        #Validate
        if($markout != '') {
            #Fetch all payment methods availabale on Parking
            $paymentMethods = $markout->method_of_payment;
            
            #Validate
            if(!empty(json_decode($paymentMethods))) {
                return json_decode($paymentMethods);
            }
        }

        #return
        return [];
    }

    /**
     * @method to fetch private code of parking
     * @return string|''
     * @param
    */
    public function getFetchPrivateCodeAttribute()
    {
        #FEtch parking type
        $parkingPassType = $this->parkingSetupSpace->pass_type;

        #validate
        if($parkingPassType == 'private') {
            return ($this->parkingSetupSpace->private_code);
        }

        #retrn
        return '';
    }


}
