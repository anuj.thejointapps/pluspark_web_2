<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingAdvanceSetting extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_advance_settings';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'discounts',
                                'private_space_link',
                                'private_space',
                                'hidden_space_link',
                                'hidden_space',
                                'customer_t_and_c',
                                'custom_t_and_c_text'
                            ];

    /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function parking()
    {
        return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
    }
    
    /**
     * @method to scope 
     * @return 
     * @param
    */
    public function scopeonlyParking($query, $parkingId)
    {
        return $query->where('parking_id', $parkingId);
    }
}
