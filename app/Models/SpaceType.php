<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpaceType extends Model
{
    use SoftDeletes;
    
    # define table
    protected $table ='space_types';
    
    # define fillable fildes
    protected $fillable = [
                                'name',
                            ];
}
