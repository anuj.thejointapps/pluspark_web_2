<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WSDOtp extends Model
{
  
  # define table
  protected $table ='w_s_d_otps';
  
  # define fillable fields
  protected $fillable = [
    'mobile',
    'mobile_code',
    'otp',
  ];

  /**
    * The attributes that should be cast to native types.
    *
    * @var array
  */
  protected $casts = [
    'created_at'        => 'datetime',
  ];
}
