<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingMarkOut extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_mark_outs';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'parking_space_label',
                                'special_access_instructions',
                                'set_available_time',
                                'set_available_time_am_pm',
                                'start_time',
                                'start_time_am_pm',
                                'end_time',
                                'end_time_am_pm',
                                'about_extended_time',
                                'set_book_price',
                                'set_valet_price',
                                'full_wash_cost',
                                'interior_wash_cost',
                                'exterior_wash_cost',
                                'set_valet_and_washing_price',
                                'method_of_payment',
                                'days_space_available',
                                'days_mark_space_unavailable',
                            ];

    /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function parking()
    {
        return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
    }
    
    /**
     * @method to scope 
     * @return 
     * @param
    */
    public function scopeonlyParking($query, $parkingId)
    {
        return $query->where('parking_id', $parkingId);
    }

    /**
     * @method to fetch full start_time
     * @return time
     * @param 
     */
    public function getFullStartTimeAttribute()
    {
        return (strtotime($this->start_time. ' '. $this->start_time_am_pm));
    }

    /**
     * @method to fetch full end_time
     * @return time
     * @param 
     */
    public function getFullEndTimeAttribute()
    {
        return (strtotime($this->end_time. ' '. $this->end_time_am_pm));
    }

    /**
     * @method to fetch all payment methods
     * @return time
     * @param 
     */
    public function getAllPaymentMethodsAttribute()
    {
        return json_decode($this->method_of_payment);
    }

    /**
     * @method to fetch full wash cost
     * @return array
     * @param
     */
    public function getFetchFullWashCostAttribute()
    {
        #Validate
        if($this->full_wash_cost != '') {
            return $this->full_wash_cost;
        }

        #return 
        return '';
    }

    /**
     * @method to fetch exterior wash cost
     * @return array
     * @param
     */
    public function getFetchExteriorWashCostAttribute()
    {
        #Validate
        if($this->exterior_wash_cost != '') {
            return $this->exterior_wash_cost;
        }

        #return 
        return '';
    }

    /**
     * @method to fetch interior wash cost
     * @return array
     * @param
     */
    public function getFetchInteriorWashCostAttribute()
    {
        #Validate
        if($this->interior_wash_cost != '') {
            return $this->interior_wash_cost;
        }

        #return 
        return '';
    }
    
}
