<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingSetupSpace extends Model
{
    use SoftDeletes;

    # define table
    protected $table ='parking_setup_spaces';

    # define fillable fildes
    protected $fillable =   [
                                'parking_id',
                                'space_headline',
                                'space_type',
                                'pass_type',
                                'scan_me_image',
                                'private_code',
                                'country_id',
                                'location',
                                'longitude',
                                'latitude',
                                'pincode',
                                'total_slot',
                                'booking_confirm_time',
                                'booking_cancel_edit_time',
                                'parker_book_space',
                                'valet_service_available',
                                'washer_service_available',
                            ];

    /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function parking()
    {
        return $this->belongsTo(CompanyParking::class, 'parking_id', 'id');
    }

    /**
     * @method to relation
     * @return 
     * @param
    */
    public function spaceType()
    {
        return $this->belongsTo(SpaceType::class, 'space_type', 'id');
    }

    /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function parkingSpaceImages()
    {
        return $this->hasMany(ParkingSpaceImage::class, 'parking_space_id', 'id');
    }

     /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function images()
    {
        return $this->hasMany(ParkingSpaceImage::class, 'parking_space_id', 'id');
    }

     /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function getPassTypesAttribute()
    {
        if($this->pass_type == 'private') {
            return "Private";
        } else if ($this->pass_type == 'qr_code') {
            return "QR Code";
        } else if ($this->pass_type == 'scan_me') {
            return "Scan Me";
        }
    }

     /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function getValetServicesAvailableAttribute()
    {
        if($this->valet_service_available) {
            return "Yes";
        } else {
            return "NO";
        }
    }

     /**
    * @method to define relation to states
    * @param
    * @return relation
    */
    public function getWasherServicesAvailableAttribute()
    {
        if($this->washer_service_available) {
            return "Yes";
        } else {
            return "NO";
        }
    }

    /**
     * @method to scope 
     * @return 
     * @param
    */
    public function scopeonlyParking($query, $parkingId)
    {
        return $query->where('parking_id', $parkingId);
    }
}
