<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AdminPasswordReset extends Model
{
  # define table
  protected $table ='admin_password_resets';
  
  # define fillable fields
  protected $fillable = [
    'email',
    'token',
    'created_at',
  ];

  /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

  /**
    * The attributes that should be cast to native types.
    *
    * @var array
  */
  protected $casts = [
    'created_at'        => 'datetime',
  ];
}
