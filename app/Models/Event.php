<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
  
    # define table
    protected $table ='events';
  
    # define fillable fildes
    protected $fillable =   [
                                'company_id',
                                'event_auto_id',
                                'name',
                                'location',
                                'latitude',
                                'longitude',
                                'pincode',
                                'start_date',
                                'end_date',
                                'start_time',
                                'end_time',
                                'status'
                            ];


    /**
    * @method to define relation to event popular place
    * @param
    * @return relation
    */
    public function eventPopularPlaces($value='')
    {
        return $this->hasMany(EventPopularPlace::class, 'event_id', 'id');
    }

    /**
    * @method to define relation to event popular place
    * @param
    * @return relation
    */
    public function nearParkings()
    {
        return $this->hasMany(EventPopularPlace::class, 'event_id', 'id');
    }

    /**
     * @method to set formatted start Time
     * @param 
     * @return string
     */
    public function getStartTimeFormattedAttribute($value='')
    {
        return date('h:i a', strtotime($this->start_time));
    }

    /**
     * @method to set formatted start Time
     * @param 
     * @return string
     */
    public function getEndTimeFormattedAttribute($value='')
    {
        return date('h:i a', strtotime($this->end_time));
    }

    
    /**
     * @method to user by filters
     * @return query builder
     * @param
    */
    public function scopeSearchFilter($query, $fromDate, $toDate)
    {
        // if (!empty($countryName)) {
        //     $query->where(function($query) use ($countryName) {
        //         $query->where('country', 'LIKE', '%' . $countryName . '%');
        //     });
        // }

        // if (!empty($stateName)) {
        //     $query->where(function($query) use ($stateName) {
        //         $query->where('city', 'LIKE', '%' . $stateName . '%');
        //     });
        // }

        if (!empty($fromDate) || !empty($toDate)) {
            $fromDate   = date('Y-m-d', strtotime($fromDate));
            $toDate     = date('Y-m-d', strtotime($toDate));
            $query->where(function($query) use ($fromDate, $toDate) {
                // $query->whereDate('created_at', '>', $fromDate)->whereDate('created_at', '<', $toDate);
                $query->whereDate('start_date', '>', $fromDate)->whereDate('end_date', '<', $toDate);
            });
        }

        // if (!empty($month)) {
        //     if ($month == '1') {
        //         $startDate = Carbon::now()->subDays(30);
        //     } else if ($month == '3') {
        //         $startDate = Carbon::now()->subDays(90);
        //     } else if ($month == '6') {
        //         $startDate = Carbon::now()->subDays(180);
        //     }
            
        //     $query->where(function($query) use ($startDate) {
        //         # Get carbon now date
        //         $carbonToDayDate = Carbon::now();
        //         // $query->whereDate('created_at', '>', $startDate)->whereDate('created_at', '<', $carbonToDayDate);
        //         $query->whereDate('date_of_join', '>', $startDate)->whereDate('date_of_join', '<', $carbonToDayDate);
        //     });
        // }
    }
}
