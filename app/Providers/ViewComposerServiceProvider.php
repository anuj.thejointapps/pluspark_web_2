<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use App\Providers\AbstractViewComposerServiceProvider as ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Shared Namespace for the Composer.
     *
     * @var string
     */
    protected $namespace = 'App\Http\View\Composers';

    /**
     * List Composers to register.
     *
     * @var Array
     */
    protected $composers = [
        'CountryComposer'   =>  [
                                    'admin.user_management.index', 
                                    'admin.state.create', 
                                    'admin.state.edit', 
                                    'admin.company.index', 
                                    'admin.washerman.index',
                                    'admin.washer_company_allot.index',
                                    'admin.washer_company_allot.washer_allot_to_company',
                                    'admin.popular_places.*',
                                    'admin.discount.*',
                                    'admin.currency.*',
                                    'company.discount.*',
                                    'company.company_parking.partials.*',
                                    'company.promocode.*',
                                    'company.employee.*',
                                    'company.event.*',
                                    'admin.promocode.*',
                                    'admin.my_profile'
                                ],
        'StateComposer'     =>  [
                                    'admin.user_management.index',
                                    'admin.company.index',
                                    'admin.washerman.index',
                                    'admin.washer_company_allot.index',
                                    'admin.washer_company_allot.washer_allot_to_company',
                                    'admin.popular_places.*',
                                    'company.event.*',
                                    'company.employee.*',
                                    'company.promocode.*',
                                    'admin.my_profile'
                                ],
        'BrandComposer'     =>  ['admin.modals.create', 'admin.modals.edit'],
        'SpaceTypeComposer' =>  ['company.company_parking.partials.setup_your_space'],

        # DiscountPercent Composer
        'DiscountPercentComposer'    =>      ['admin.discount.*'],

        #Companies Composer
        'CompanyComposer'    =>     [
                                        'admin.discount.*', 
                                        'company.discount.*',
                                        'admin.promocode.*',
                                        'company.promocode.*'
                                    ],

        #JobType Composer
        'JobTypeComposer'    =>     [
                                        'company.employee.*'
                                    ],
        # currency Composer
        'CurrencyComposer'    =>     [
                                        'admin.company.*'
                                    ],
    ];
}
