<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    * @method to send Push Notification
    * @param $fcmToken, $title, $message, $id
    * @return
    */
    function sendPushNotificationAndroid($deviceTokens = [], $title, $message, $id = null) 
    {  
        # Set the Push Notification Url
        $url = "https://fcm.googleapis.com/fcm/send"; 
    
        # Set the Key
        $key = config('app.fcm_server_key');     

        # Set the Header
        $headers = array(
            'Authorization:key = '. $key,
            'Content-Type: application/json'
        );   
    
        #set Data
        $postData = [
            "registration_ids" => $deviceTokens,
            "notification" => [
                "title" => $title,
                "body"  => $message,
                "sound" => "default"
            ],
            "data" => [
                #'click_action'  => "FLUTTER_NOTIFICATION_CLICK",
                'title'         => $title,
                'body'          => $message,
                'is_read'       => 0,
            ]
        ];

        /*$fields = [
            "registartion_ids" => $fcm_token,
            "data" => $message,
        ];*/

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData, true));

        $result = curl_exec($ch);    
        curl_close($ch);
        $result = json_decode($result, true);
        
        if($result['success'] == 1) {
            return true;
        } 
    }


    /**
    * @method to send Push Notification for ios
    * @param $fcmToken, $title, $message, $id
    * @return
    */
    function sendPushNotificationIos($data) 
    {  
        # Set the Push Notification Url
        $url = "https://fcm.googleapis.com/fcm/send"; 
    
        # Set the Key
        $key = config('app.fcm_server_key');     

        # Set the Header
        $headers = array(
            'Authorization:key = '. $key,
            'Content-Type: application/json'
        );   

        #set Data
        // $postData = [
        //     "registration_ids" => $deviceTokens,
        //     "category" => $request->category,   //category will be type of Notification like(payement, etc)
        //     "mutable_content" => $request->mutable_content,
        //     "notification" => $request->notification,
        //     "data" => $request->data
        // ];
        #dd($request->except('to'));
        
        $postData = $data;
        /*$fields = [
            "registartion_ids" => $fcm_token,
            "data" => $message,
        ];*/

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData, true));

        $result = curl_exec($ch);    
        curl_close($ch);
        $result = json_decode($result, true);
    
        if($result['success'] == 1) {
            return true;
        } 

        return false;
    }
}
