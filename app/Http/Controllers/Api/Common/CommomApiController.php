<?php

namespace App\Http\Controllers\Api\Common;

use DB;
use Log;
use Auth;
use Validator;
use Carbon\Carbon;

# Models
use App\Models\Country;
use App\Models\Payment;
use App\Models\Booking;
use App\Models\Washerman;
use App\Models\UserVehicle;
use App\Models\DriverBooking;
use App\Models\WasherBooking;
use App\Models\SupervisorBooking;
use App\Models\ScanOrPrivateBooking;
use App\Models\PaymentForScanPrivateBooking;

#Interface
use App\Interfaces\ApiStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class CommomApiController extends Controller
{
    # Variable $country
    protected $country;

    # Variable $wsd(washer, supervisor or Driver)
    protected $wsd;

    # Variable $booking
    protected $booking, $payment;

    # Variable $userVehicle
    protected $userVehicle;

    # Variable $washerBooking
    protected $washerBooking;

    # Variable $driverBooking
    protected $driverBooking;

    # Variable $supervisorBooking
    protected $supervisorBooking;

    # Variable $scanMeOrPrivateBooking
    protected $scanMeOrPrivateBooking, $paymentForScanorPrivate;
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(Country $country, Payment $payment, Washerman $wsd, Booking $booking, UserVehicle $userVehicle,
        DriverBooking $driverBooking,  WasherBooking $washerBooking, SupervisorBooking $supervisorBooking,
        ScanOrPrivateBooking $scanMeOrPrivateBooking, PaymentForScanPrivateBooking $paymentForScanorPrivate)
	{
        $this->wsd                  = $wsd;
        $this->payment              = $payment;
		$this->country              = $country;
        $this->booking              = $booking;
        $this->userVehicle          = $userVehicle;
        $this->driverBooking        = $driverBooking;
        $this->washerBooking        = $washerBooking;
        $this->supervisorBooking    = $supervisorBooking;
        $this->scanMeOrPrivateBooking    = $scanMeOrPrivateBooking;
        $this->paymentForScanorPrivate    = $paymentForScanorPrivate;
	}

	/**
	 * @method to fetch Countries
	 * @param Request $request
	 * @return json
	 */
	public function fetchCountries(Request $request)
	{ 
        $countries = $this->country->get();

        #Validate Country
        $countryArray = [];
        if($countries->isEmpty()) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Countries not found.',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        } else {
            foreach ($countries as $key => $country) {
                $data = [
                    'id' => (string)$country->id,
                    'name' => (string)$country->name,
                ];

                #push to Array
                array_push($countryArray, $data);
            }
        }

        #return resoponse
        $response = [
            'statusCode' => ApiStatusInterface::OK,
            'message'    => 'Countries found.',
            'success'    => true,
            'data'       => $countryArray
        ];

        #return response
        return $response;
    }

    /**
     * @method to Fetch State on Country
     * @param Request $request
     * @return json
     */
    public function fetchStates(Request $request)
    { 
        #Fetch data form request
        $countryId = $request->country_id;

        #Validate country_id
        if($countryId == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'country_id is required.',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        }

        #Fetch all the Countries
        $country = $this->country
                          ->with(['states']) 
                          ->find($countryId);

        #Validate Country
        if($country == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Country not found on id.',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        }

        #FEtch states on Country
        $states = $country->states;

        #Validate States
        $statesArray = [];
        if($states->isEmpty()) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'States not found.',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        } else {
            foreach ($states as $key => $state) {
                $data = [
                    'id' => (string)$state->id,
                    'name' => (string)$state->name,
                ];

                #push to Array
                array_push($statesArray, $data);
            }
        }

        #return resoponse
        $response = [
            'statusCode' => ApiStatusInterface::OK,
            'message'    => 'States found.',
            'success'    => true,
            'data'       => $statesArray
        ];

        #return response
        return $response;
    }

    /**
     * @method to delete booking
     * @param Request $request
     * @retrun json
     */
    public function deleteBooking(Request $request)
    {
        #EFtch booking_id
        $bookingId = $request->booking_id;
        $parkingType = $request->parking_type ?? '';

        #iValidate bookingId
        if($bookingId == '' OR $parkingType == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'booking_id and parking_type is required',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        }

        #Validate parking Type
        if(!in_array($parkingType, ['qr_code', 'scan_me', 'private'])) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'parking type is not valid',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        }

        #Fetch booking of User
        if($parkingType == 'qr_code') {
            $booking = $this->booking->with(['payment', 'pass'])->find($bookingId);
        } elseif (($parkingType == 'scan_me') OR ($parkingType == 'private')) {
            $booking = $this->scanMeOrPrivateBooking->with(['payment', 'pass'])->find($bookingId);
        }
        
        #VAlidate booking
        if($booking == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'booking not found or already deleted',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        }
        
        #Fetch booking on User
        if($parkingType == 'qr_code') {
            $userBookingIds = $this->booking->where('id', $bookingId)->pluck('id')->toArray();
            $supervisorBookingIds = $this->supervisorBooking->where('booking_id', $bookingId)->pluck('id')->toArray();
            $driverBookingIds = $this->driverBooking->where('booking_id', $bookingId)->pluck('id')->toArray();
            $washerBookingIds = $this->washerBooking->where('booking_id', $bookingId)->pluck('id')->toArray();

            #Delete
            if($booking->payment != '')  {
                $this->payment->whereIn('id', $booking->payment->pluck('id')->toArray())->delete();
            }

            #Fetch the pass of booking
            if($booking->pass != '') {
                $booking->pass->delete();
            }
            $this->booking->whereIn('id', $userBookingIds)->delete();        
            $this->supervisorBooking->whereIn('id', $supervisorBookingIds)->delete();        
            $this->driverBooking->whereIn('id', $driverBookingIds)->delete();        
            $this->washerBooking->whereIn('id', $washerBookingIds)->delete(); 
        } else {
            #Fetch the pass of booking
            if($booking->pass != '') {
                $booking->pass->delete();
            }

            #Delete
            if($booking->payment != '')  {
                $this->paymentForScanorPrivate->whereIn('id', $booking->payment->pluck('id')->toArray())->delete();
            }

            $userBookingIds = $this->scanMeOrPrivateBooking->where('id', $bookingId)->pluck('id')->toArray();
            $this->scanMeOrPrivateBooking->whereIn('id', $userBookingIds)->delete();  
        }
              
        #return
        $response = [
            'statusCode' => ApiStatusInterface::OK,
            'message'    => 'booking delete',
            'success'    => true,
            'data'       => []
        ];

        return $response;
    }

    public function sendNotification(Request $request)
    {
        #set data
        $token = [$request->to];
        $category = $request->category ?? '';
        $title = 'Welcome to Plus park';
        $message = 'Heelo How are you';
        
        #validate Data
        if($token == '' OR $category == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'token, category is required',
                'success'    => false,
                'data'       => []
            ];

            return $response;
        }
        
        #call Ios Notification
                $postData = [
                    "registration_ids" => $token,
                    "notification" => $request->notification,
                    "mutable_content" => true,
                    "content_available" => true,
                    "category" => $request->category,
                    "data" => $request->data
                ];
        
        #send notification
        $status = $this->sendPushNotificationIos($postData);

        #return
        if($status) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Notification send',
                'success'    => true,
                'data'       => []
            ];

            return $response;
        } else {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Have some issue',
                'success'    => false,
                'data'       => []
            ];

             return $response;
        }
    }
}