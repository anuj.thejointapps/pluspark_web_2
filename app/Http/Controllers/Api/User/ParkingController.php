<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Log;
use Auth;
use Config;
use Validator;
use Carbon\Carbon;
use \Milon\Barcode\DNS2D;

# Models
use App\Models\User;
use App\Models\Booking;
use App\Models\Payment;
use App\Models\UserVehicle;
use App\Models\DriverBooking;
use App\Models\WasherBooking;
use App\Models\CompanyParking;
use App\Models\UserSearchPlace;
use App\Models\ParkingSetupSpace;
use App\Models\SupervisorBooking;
use App\Models\PassesForQrBooking;
use App\Models\ScanOrPrivateBooking;
use App\Models\PaymentForScanPrivateBooking;
use App\Models\PassesForScanOrPrivateBooking;

# Utils
use App\Utils\CommonUtil;

# Interface
use App\Interfaces\ApiStatusInterface;
use App\Interfaces\WasherTypeInterface;
use App\Interfaces\UserBookingStatusInterface;
use App\Interfaces\BookingPassStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ParkingController extends Controller implements ApiStatusInterface, UserBookingStatusInterface, WasherTypeInterface
{
    # Variable $user
    protected $user;

    # Variable $commonUtil
    protected $commonUtil;

    # Variable $booking
    protected $booking;

    # Variable $payment
    protected $payment, $paymentForScanorPrivate;

    # Variable $driverBooking
    protected $driverBooking;

    # Variable $userVehicle
    protected $userVehicle;

    # Variable $companyParking
    protected $companyParking;

    # Variable $userSearchPlace
    protected $userSearchPlace;

    # Variable $parkingSpace
    protected $parkingSpace;

    # Variable $supervisorBooking
    protected $supervisorBooking;

    # Variable $passesForQrBooking
    protected $passesForQrBooking;

    # Variable $scanOrPrivateBooking
    protected $scanOrPrivateBooking;

    # Variable $passesForScanOrPrivateBooking
    protected $passesForScanOrPrivateBooking; 
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(
        User $user, Booking $booking, Payment $payment, CommonUtil $commonUtil,
        UserVehicle $userVehicle, DriverBooking $driverBooking, CompanyParking $companyParking,
        ParkingSetupSpace $parkingSpace, UserSearchPlace $userSearchPlace, 
        SupervisorBooking $supervisorBooking, PassesForQrBooking $passesForQrBooking, 
        ScanOrPrivateBooking $scanOrPrivateBooking, PassesForScanOrPrivateBooking $passesForScanOrPrivateBooking,
        PaymentForScanPrivateBooking $paymentForScanorPrivate)
	{
		$this->user                             = $user;
        $this->booking                          = $booking;
        $this->payment                          = $payment;
        $this->commonUtil                       = $commonUtil;
        $this->parkingSpace                     = $parkingSpace;
        $this->userVehicle                      = $userVehicle;
        $this->driverBooking                    = $driverBooking;
        $this->companyParking                   = $companyParking;
        $this->userSearchPlace                  = $userSearchPlace;
        $this->supervisorBooking                = $supervisorBooking;
        $this->passesForQrBooking               = $passesForQrBooking;
        $this->scanOrPrivateBooking             = $scanOrPrivateBooking;
        $this->paymentForScanorPrivate          = $paymentForScanorPrivate;
        $this->passesForScanOrPrivateBooking    = $passesForScanOrPrivateBooking;
	}

    /**
     * @method to save search Place of User
     * @return json
     * @param Request $request
     */
    public function saveSearchPlace(Request $request)
    {
        #FEtch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch searchSTring
        $searchString   = $request->search_string ?? '';
        $latitude       = $request->latitude ?? '';
        $longitude      = $request->longitude ?? '';

        #validate searchString
        if($searchString == '' OR $latitude == '' OR $longitude == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'search_string, latitude, longitude is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Data
        $data = [
            'user_id'       => $user->id,
            'search_place'  => $searchString,
            'latitude'      => $latitude,
            'longitude'     => $longitude,
        ];

        #Save Data
        $this->userSearchPlace->create($data);

        #Fetch user
        $user = $this->user
                     ->with(['searchPlaces'])
                     ->find($user->id);

        #Fetch all searchPlace of user
        $userSearchPlaces = $user->searchPlaces;
        
        #FEtch those which have to delete
        $deleteCount = ($userSearchPlaces->count() > 20) ?  ($userSearchPlaces->count() - 20) : 0;

        #Fetch is which have to delete
        $deleteIds = ($deleteCount > 0) ? $userSearchPlaces->take($deleteCount)->pluck('id')->toArray() : [];
        
        #Delete Extara SearchPlaces
        $this->userSearchPlace
             ->whereIn('id', $deleteIds)
             ->forceDelete(); 

        #rewturn response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Search Place Saved.',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to  fetch saved Place of User
     * @return json
     * @param Request $request
     */
    public function fetchSearchPlace(Request $request)
    {
        #FEtch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch user
        $user = $user->with(['searchPlaces'])->find($user->id);

        #Fetch Search Places
        $searchPlaces = $this->userSearchPlace
                             ->orderBy('id', 'DESC')
                             ->select('id', 'search_place', 'latitude', 'longitude')
                             ->where('user_id', $user->id)
                             ->paginate(5);

        #Validate Search Place
        if($searchPlaces->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #rewturn response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Search Place Saved.',
            'success'       => true,
            'data'          => $searchPlaces
        ]);
    }

    /**
     * @method to fetch parking space regarding User requirement
     * @return json
     * @param Request $request
     */
    public function fetchParkingSpace(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'arrived_date_time' => 'required|date_format:Y-m-d H:i:s',
            'exit_date_time'    => 'required|date_format:Y-m-d H:i:s',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'arrived_date_time or exit_date_time format should be Y-m-d H:i:s',
                'success'       => false,
                'data'          => null
            ]);
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        #Fetch Data
        $userLat            = $request->latitude ?? '';
        $userLong           = $request->longitude ?? '';
        $arrivedDateTime    = $request->arrived_date_time ?? '';
        $exitDateTime       = $request->exit_date_time ?? '';

        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate Data
        if($userLat == '' OR $userLong == '' OR $arrivedDateTime == '' OR $exitDateTime == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'latitude, longitude, arrived_date_time, exit_date_time are reuired',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Parking Space
        $allParkingSpaces = $this->parkingSpace
                                 ->with(['parking.parkingMarkOut'])
                                 ->get();

        #Fetch nearBySpaces
        $nearBySpaces = $allParkingSpaces->filter(function($parkingSpace) use($userLat, $userLong) {
            if($parkingSpace->latitude != '' AND $parkingSpace->longitude != '' AND $parkingSpace->parking->is_active) {
                $distanceInMiles = $this->distanceInMiles($userLat, $userLong, $parkingSpace->latitude, $parkingSpace->longitude );
                if($distanceInMiles < Config::get('app.search_miles')) {
                    return $parkingSpace;
                }
            }
        });

        #Validate Spaces
        if($nearBySpaces->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No nearby parking Found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Parse Date to carbon
        $arrivedDateTime = Carbon::parse($arrivedDateTime);
        $exitDateTime = Carbon::parse($exitDateTime);

        #Fetch arrived Day and exit Day
        $arrivedDay = strtolower($arrivedDateTime->format('l'));
        $exitDay    = strtolower($exitDateTime->format('l'));
        
        #Fetch parkingspace avail on that Days or not
        $parkingSpaceAvail = $nearBySpaces->filter(function($parkingSpace) use($arrivedDay, $exitDay) {
            $parkingMarkOut = $parkingSpace->parking->parkingMarkOut;
            $openDaysForParking = $parkingMarkOut != '' ? json_decode($parkingMarkOut->days_space_available) : [];
            if(in_array($arrivedDay, $openDaysForParking) AND in_array($exitDay, $openDaysForParking)) {
                return $parkingSpace;
            }
        });

        #Validate Availa Parking
        if($parkingSpaceAvail->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No nearby parking Found for arrived or exit Day',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Check for Timing
        $arrivedTime   = strtotime($arrivedDateTime->format('H:i a'));
        $exitTime      = strtotime($exitDateTime->format('H:i a'));

        #Fetch Parkings which are avail on requesting time
        $parkingSpaceAvail = $parkingSpaceAvail->filter(function($parkingSpace) use($arrivedTime, $exitTime) {
            $parkingMarkingOut = $parkingSpace->parking->parkingMarkOut;
            if($parkingMarkingOut != ''  AND $parkingMarkingOut->set_available_time == 24) {
                return $parkingSpace;
            } else {
                if($parkingMarkingOut != '' AND ($arrivedTime >= $parkingMarkingOut->full_start_time) AND 
                    ($exitTime <= $parkingMarkingOut->full_end_time)) {
                    return $parkingSpace;
                }
            }
        });
        
        #validate Parking Space Avail
        if($parkingSpaceAvail->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No nearby parking Found for arrived or exit Time',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Data
        $parkingData = [];
        $parkingIds = [];
        foreach ($parkingSpaceAvail as $key => $parkingSpace) {
            $parking = $parkingSpace->parking;

            #Validate
            if(!in_array($parking->id, $parkingIds)) {
                $data = [
                    'id'            => $parking->id ?? '',
                    'parking_name'  => $parking->name ?? '',
                    'image'         => $parking->image_full_path ?? '',
                    'latitude'      => $parkingSpace->latitude ?? '',
                    'longitude'     => $parkingSpace->longitude ?? '',
                    'location'      => $parkingSpace->location ?? '',
                    'book_price'    => $parking->parkingMarkOut->set_book_price ?? '',
                ];

                #push Data
                array_push($parkingData, $data);
                array_push($parkingIds, $parking->id);
            }
        }

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Parking Space Found',
            'success'       => true,
            'data'          => $parkingData
        ]);
    }

    /**
     * @method to fetch parking Details
     * @return json
     * @param Request $request
     */
    public function fetchParkingDetails(Request $request)
    {
        #Fethc Data
        $parkingId = $request->parking_id ?? '';
        $arrivalDateTime = $request->arrival_date_time ?? '';
        $exitDateTime = $request->exit_date_time ?? '';

        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'arrival_date_time' => 'required|date_format:Y-m-d H:i:s',
            'exit_date_time'    => 'required|date_format:Y-m-d H:i:s',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'arrival_date_time or exit_date_time format should be Y-m-d H:i:s',
                'success'       => false,
                'data'          => null
            ]);
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate User
        if($parkingId == '' OR $arrivalDateTime == '' OR $exitDateTime == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'parking_id, arrival and exit DateTime is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set relations
        $relations = [
            'parkingMarkOut',
            'parkingSetupSpace.images',
            'parkingDescription',
            'parkingWasherFeatures',
            'company.driverCommission',
            'company.washerCommission',
        ];

        #Fetch parking
        $parking = $this->companyParking
                        ->with($relations)
                        ->find($parkingId);

        #validate parking
        if($parking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Parking not Found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fethcuser
        $user = $this->user->with(['bookings'])->find($user->id);

        #Fethc all bookings on same parking
        $userBookings = $user->bookings->where('parking_id', $parkingId);
        
        #Validate Booking
        $userBookings = $userBookings->filter(function($userBooking) use($arrivalDateTime, $exitDateTime) {
            if($userBooking->exit_date_time != '' AND $userBooking->arrival_date_time != '' AND !$userBooking->is_completed) {
                $arrivalDateTimeOfBooking = Carbon::parse($arrivalDateTime);
                $exitDateTimeOfBooking = Carbon::parse($exitDateTime);
                $userBookingExitDateTimePlusOneHour = $userBooking->exit_date_time->copy()->addHour(1);
                if($arrivalDateTimeOfBooking->between($userBooking->exit_date_time, $userBooking->arrival_date_time) OR 
                    $exitDateTimeOfBooking->between($userBooking->exit_date_time, $userBooking->arrival_date_time)) {
                    return $userBooking;
                }
            }
        });
       
        #Validate
        if($userBookings->isNotEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Cannot book this parking for this time slot.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Parse Date to carbon
        $arrivedDateTime = Carbon::parse($arrivalDateTime);
        $exitDateTime = Carbon::parse($exitDateTime);

        #Fetch arrived Day and exit Day
        $arrivedDay = strtolower($arrivedDateTime->format('l'));
        $exitDay    = strtolower($exitDateTime->format('l'));
        
        #Validate parking
        $parkingMarkout = $parking->parkingMarkOut;
        $openDaysForParking = $parkingMarkout != '' ? json_decode($parkingMarkout->days_space_available) : [];

        #Validate for that Day
        if(!in_array($arrivedDay, $openDaysForParking) OR !in_array($exitDay, $openDaysForParking)) {
            #return data
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Parking not avail for arrival or exit date time Found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #parking Data
        $parkingMarkout     = $parking->parkingMarkOut;
        $parkingSpace       = $parking->parkingSetupSpace;
        $parkingDescription = $parking->parkingDescription;
        
        #Set parking Space Images
        $parkingSpaceImagesArray = [];
        if($parking->parkingSetupSpace != '' AND $parking->parkingSetupSpace->images->isNotEmpty()) {
            $parkingSpaceImages = $parking->parkingSetupSpace->images;
            
            foreach ($parkingSpaceImages as $key => $image) {
                $data = [
                    'id'            => $image->id ?? '',
                    'image_path'    => $image->image_full_path ?? '',
                ];

                #Push Array
                array_push($parkingSpaceImagesArray, $data);
            }
        }
        
        #Set parking Detail data
        $data = [
            'id'                        => $parking->id ?? '',
            'parking_type'              => $parking->parkingSetupSpace->pass_type ?? '',
            'name'                      => $parking->name ?? '',
            'image'                     => $parking->image_full_path ?? '',
            'booking_price'             => $parkingMarkout ? $parkingMarkout->set_book_price : '',
            'before_you_go'             => $parkingMarkout ? $parkingMarkout->special_access_instructions : '',
            'about_extended_time'       => $parkingMarkout ? $parkingMarkout->about_extended_time : '',
            'location'                  => $parkingSpace ? $parkingSpace->location  :'',
            'latitude'                  => $parkingSpace ? $parkingSpace->latitude  :'',
            'longitude'                 => $parkingSpace ? $parkingSpace->longitude : '',
            'parking_space_images'      => $parkingSpaceImagesArray,
            'about_parking'             => $parkingDescription ? $parkingDescription->description : '',
            'amenities'                 => $parkingDescription ? $parkingDescription->all_amenities : '',
            'book_price'                => $parkingMarkout ? $parkingMarkout->set_book_price : '0',
            'valet_price'               => $parkingMarkout ? $parkingMarkout->set_valet_price : '0',
            'full_wash_price'           => $parkingMarkout->fetch_full_wash_cost,
            'exterior_wash_price'       => $parkingMarkout->fetch_exterior_wash_cost,
            'interior_wash_price'       => $parkingMarkout->fetch_interior_wash_cost,
            #'valet_and_wash_price'      => $parkingMarkout ? $parkingMarkout->set_valet_and_washing_price : '0',
            'payment_methods_avail'     => $parkingMarkout ? $parkingMarkout->all_payment_methods : [],
            'washing_cost'              => $parking->washing_cost,
            'vehicle_types'             => $parking->vehicles_can_parked,
        ];
        
        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Parking Details Found.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to book parking
     * @return json
     * @param Request $request
     */
    public function bookParking(Request $request)
    {
       # Validate request data
        $validator = Validator::make($request->all(), [ 
            'arrival_date_time' => 'required|date_format:Y-m-d H:i:s',
            'exit_date_time'    => 'required|date_format:Y-m-d H:i:s',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'arrival_date_time or exit_date_time format should be Y-m-d H:i:s',
                'success'       => false,
                'data'          => null
            ]);
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Data
        $parkingId          = $request->parking_id ?? '';
        $promoCodeId        = $request->promo_code_id ?? 0;
        $promoCode          = $request->promo_code ?? '';
        $vehicleId          = $request->vehicle_id ?? '';
        $bookingPrice       = $request->booking_price ?? '';
        $subTotal           = $request->sub_total ?? '';
        $paidMoney          = $request->paid_money ?? '';
        $location           = $request->location ?? '';
        $latitude           = $request->latitude ?? '';
        $longitude          = $request->longitude ?? '';
        $valet              = $request->valet ?? '';
        $washing            = $request->washing ?? '';
        $washType           = $request->wash_type ?? '';
        $paymentMethod      = $request->payment_method ?? '';
        $arrivalDateTime    = $request->arrival_date_time ?? '';
        $exitDateTime       = $request->exit_date_time ?? '';

        #Validate Data
        if($parkingId == '' OR $vehicleId == '' OR $bookingPrice == '' OR $subTotal == '' OR $paidMoney == '' OR $location == '' OR $latitude == '' OR $longitude == '' OR $valet == '' OR $washing == '' OR $paymentMethod == '' OR $arrivalDateTime == '' OR $exitDateTime == '' OR $washType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_price, sub_total, vehicle_id, paid_money, location, latitude, longitude, valet, valet_and_washing, payment_method,wash_type, arrival_date_time, exit_date_time is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch parking
        $parking = $this->companyParking
                        ->with(['parkingDescription', 'parkingSetupSpace'])
                        ->find($parkingId);
        
        #Fetch parking space
        $parkingSpace = $parking->parkingSetupSpace;
        
        #Validate parking Space
        if($parkingSpace == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Basic Information not found. Please contact to admin.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch parking pass type
        $parkingPassType = $parkingSpace->pass_type;
        
        #validate pass type
        if($parkingPassType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No pass type found on Parking. Please contact to admin.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate WashType
        $validWashTypes = ['full_wash', 'interior_wash', 'exterior_wash', 'no_wash'];
        if(!in_array($washType, $validWashTypes)) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Wash Status is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Wash Type
        $setWashType = 0;
        if($washType == 'full_wash') {
            $setWashType = WasherTypeInterface::FULL_WASH;
        } elseif ($washType == 'interior_wash') {
            $setWashType = WasherTypeInterface::INTERIOR;
        } elseif ($washType == 'exterior_wash') {
            $setWashType = WasherTypeInterface::EXTERIOR;
        }

        /*#Fetch vehicle type availon parking
        $vhecleTypesCanParkOnParking = $parking->parkingDescription->select_vehicle_type != '' ?
                                        json_decode($parking->parkingDescription->select_vehicle_type) : [];
        
        #Fetch vehicle
        $vehicle = $this->userVehicle->find($vehicleId);

        #Fetch vehicle Type
        $vehicleType = strtolower($vehicle->vehicle_type);

        #Validate vehcileTypeavail on Parking or not
        if(!in_array($vehicleType, $vhecleTypesCanParkOnParking)) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Vehicle type not avail on Parking',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fethcuser
        $user = $this->user->with(['bookings'])->find($user->id);

        #Fethc all bookings on same parking
        $userBookings = $user->bookings->where('parking_id', $parkingId);
        
        #Validate Booking
        $userBookings = $userBookings->filter(function($userBooking) use($arrivalDateTime) {
            if($userBooking->exit_date_time != '') {
                $arrivalDateTimeOfBooking = Carbon::parse($arrivalDateTime);
                $userBookingExitDateTimePlusOneHour = $userBooking->exit_date_time->copy()->addHour(1);
                if($arrivalDateTimeOfBooking->between($userBooking->exit_date_time, $userBookingExitDateTimePlusOneHour)) {
                    return $userBooking;
                }
            }
        });

        #Validate
        if($userBookings->isNotEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Cannot book this parking for this time slot.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        #Set relations
        $relations = [
            'user', 
            'vehicle', 
            'promocode', 
            'parking.parkingSetupSpace', 
            'parking.parkingMarkOut'
        ];

        #Cerate Unique Order Number
        $today = time();
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        $unique = 'PP'.$today . $rand;

        #set Unique key Number
        $bookingCount = ($this->booking->get()->count() > 0) ? 
                            $this->booking->get()->count() + 1 : 1;

        $uniquekeyNumber = 'SP'.'0000'.$bookingCount;

        
        #Set data
        $data = [
            'order_number'      => $unique,
            'key_no'      => $uniquekeyNumber,
            'user_id'           => Auth::user()->id,
            'parking_id'        => $parkingId,
            'parking_type'      => $parkingPassType,
            'promocode_id'      => $promoCodeId,
            'promocode'         => $promoCode,
            'vehicle_id'        => $vehicleId,
            'booking_price'     => $bookingPrice,
            'sub_total'         => $subTotal,
            'paid_money'        => $paidMoney,
            'is_pre_book'       => true,
            'location'          => $location,
            'latitude'          => $latitude,
            'longitude'         => $longitude,
            'valet'             => $valet == 'true' ? true : false ,
            'wash_available'    => $washing == 'true' ? true : false,
            'wash_type'         => $setWashType,
            'payment_method'    => $paymentMethod,
            'booking_status'    => UserBookingStatusInterface::INITIAL,
            'arrival_date_time' => $arrivalDateTime,
            'exit_date_time'    => $exitDateTime,
        ];
        
        DB::beginTransaction();
            if($parkingPassType == 'qr_code') {
                #Create booking
                $booking = $this->booking->create($data);

                #Set Name of Qr Code Image
                $bookingPassImageName = date('d-m-Y'). '_'.rand(1111, 9999).'_pass_'.$booking->id.'.png';

                #Set json Data
                $data = ['booking_id' => $booking->id];

                #set Data
                $data = json_encode($data);

                #Upload Image for Qr Code
                \Storage::disk('public_uploads')->put($bookingPassImageName, base64_decode(DNS2D::getBarcodePNG($data,'QRCODE')));

                #Set Data for Pass
                $data = [
                    'booking_id' => $booking->id,
                    'pass_number' => '#'.str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT),
                    'pass_qr_code_image' => 'images/qrCodeImages/'.$bookingPassImageName
                ];

                #Crate pass for Booking
                $pass = $this->passesForQrBooking->create($data);

                #Update pass on Booking
                $booking->update(['pass_id' => $pass->id]);

                #Fetych bookiong
                $booking = $this->booking
                                ->with($relations)
                                ->find($booking->id);

                #Fetch modals
                $parkingMarkout = $booking->parking->parkingMarkOut;

                #Set Booking Type
                $bookingType = 'qr_code';
            } else {
                #Create Booking
                $booking = $this->scanOrPrivateBooking->create($data);

                #Set Data for Pass
                $data = ['booking_id' => $booking->id,'pass_number' => '#'.str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT)];

                #Crate pass for Booking
                $pass = $this->passesForScanOrPrivateBooking->create($data);

                #Update pass on Booking
                $booking->update(['pass_id' => $pass->id]);

                #Fetych bookiong
                $booking = $this->scanOrPrivateBooking
                                ->with($relations)
                                ->find($booking->id);

                #Fetch modals
                $parkingMarkout = $booking->parking->parkingMarkOut;

                #Set Booking Type
                $bookingType = 'scan_or_private';
            }
            
        DB::commit();
        $parkingMarkout = $booking->parking->parkingMarkOut;
        
        #Set data for response
        $data = [
            'booking_id'                => $booking->id ?? '',
            'parking_type'              => $booking->parking_type ?? '',
            'pass_number'               => $pass->pass_number ?? '',
            'bookingType'               => $bookingType ?? '',
            'promocode_id'              => $booking->promocode_id,
            'promocode'                 => $booking->promocode,
            'promocode_discount'        => $booking->promocode_discount,
            'email'                     => $booking->user->email ?? '',
            'parking_image'             => $booking->parking->image_full_path ?? '',
            'parking_name'              => $booking->parking->name ?? '',
            'parking_location'          => $booking->parking->parkingSetupSpace->location ?? '',
            'parking_latitude'          => $booking->parking->parkingSetupSpace->latitude ?? '',
            'parking_longitude'         => $booking->parking->parkingSetupSpace->longitude ?? '',
            'parking_image'             => $booking->parking->image_full_path ?? '',
            'remaining_time'            => $booking->arrival_date_time->diffInHours(Carbon::now()). ' hours'?? '',
            'in_time'                   => $booking->arrived_formatted ?? '',
            'out_time'                  => $booking->exit_formatted ?? '',
            'vehicle'                   => $booking->vehicle->full_name ?? '',
            'valet'                     => $booking->valet ? true : false,
            'washing'                   => $booking->wash_available ? true : false,
            'valet_price'               => $parkingMarkout->set_valet_price ?? '',
            'full_wash_price'           => $parkingMarkout->fetch_full_wash_cost,
            'exterior_wash_price'       => $parkingMarkout->fetch_exterior_wash_cost,
            'interior_wash_price'       => $parkingMarkout->fetch_interior_wash_cost,
            'valet_and_car_wash_price'  => $parkingMarkout->set_valet_and_washing_price ?? '',
            'subtotal'                  => $booking->sub_total ?? '',
            'total'                     => $booking->paid_money ?? '',
            'payment_way'               => $booking->payment_method ?? '',
            'createdTimestamp'          => $booking->created_at->timestamp ?? '',
        ];

        #retunr response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Parking booked successfully',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to allot parking no to scan and Private Bookings
     * @return json
     * @param Request $request
     */
    public function allotParkingNo(Request $request)
    {
        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #User
        $user = $this->user
                     ->with(['scanPrivateBookings'])
                     ->find($user->id);

        #Fetch Data
        $bookingId = $request->booking_id;
        $parkingNo = $request->parking_no;

        #Validate data
        if($bookingId == '' OR $parkingNo == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_id and parking_id is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch user bookings
        $userBookings = $user->scanPrivateBookings;

        #FEtch Booking
        $booking = $userBookings->where('id', $bookingId);

        #Validate Booking
        if($booking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate parking_no
        if($booking->last()->parking_no != '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Parking no already assigned.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Update Booking
        $booking->last()->update(['parking_no' => $parkingNo]);

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Parking no assigned.',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to update Status of Booking by User
     * @return json
     * @param Request $request
     */
    public function updateBookingStatus(Request $request)
    {
        #FEtch Data
        $bookingId = $request->booking_id ?? '';
        $bookingType = strtolower($request->booking_type) ?? '';
        $bookingStatus = strtolower($request->status) ?? '';

        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '' OR $bookingType == '' OR $bookingStatus == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User,booking_id, booking_type, booking_status not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate BookingTpe
        if(!in_array($bookingType, ['qrcode', 'scan_or_private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid(qrcode, scan_or_private).',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate BookingStatus
        if(!in_array($bookingStatus, ['cancelled', 'confirmed'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid(qrcode, scan_or_private).',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #User
        $user = $this->user
                     ->with(['bookings.pass', 'scanPrivateBookings.pass'])
                    ->find($user->id);

        #FEtch Booking
        if($bookingType == 'qrcode') {
            $booking = $user->bookings->where('id', $bookingId)->first();
        } else {
            $booking = $user->scanPrivateBookings->where('id', $bookingId)->first();
        }

        #Validate Booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking not found on User.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Booking Pass
        $bookingPass = $booking->pass;

        #Set Status Id
        $passStatusId = ($bookingStatus == 'confirmed') ? 2 : 3;
        $bookingStatusId = ($bookingStatus == 'confirmed') ? 4 : 5;

        #Update pass Status if pass avaialabel
        if($bookingStatus == 'confirmed') {
            if($bookingPass != '') {
                $bookingPass->update(['status' => 2]);
            }

            #update Booking
            $booking->update(['booking_status' => 4]);
        } else {
            #FEtch bookingStatus
            $bookingStatus = $booking->booking_status;

            #Validate if booking status not Ready to pick or Parked than cancle booking and pass
            if(in_array($bookingStatus, [UserBookingStatusInterface::INITIAL, UserBookingStatusInterface::CONFIRMED, UserBookingStatusInterface::AVAILABLE_TO_PICK])) {
                if($bookingPass != '') {
                    $bookingPass->update(['status' => 3]);
                }

                #update Booking
                $booking->update(['booking_status' => 5]);
            }
        }
     

        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking has been '.$bookingStatus,
            'success'       => true,
            'data'          => null
        ]);
    }
    
    /**
     * @method to fetch all passes related to user bookings
     * @return json
     * @param
     */
    public function fetchPasses()
    {
        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #User
        $user = $this->user
                     ->with(['bookings.pass', 'bookings.vehicle', 'scanPrivateBookings.pass', 'scanPrivateBookings.vehicle'])
                    ->find($user->id);
                                                  
        #Fetch user bookings
        $qrBookingsWithPass = $user->bookings()->passAvailable()->get();
        $scanPrivatebookingsWithPass = $user->scanPrivateBookings;
                                                  
        #validate
        if($qrBookingsWithPass->isEmpty() AND $scanPrivatebookingsWithPass->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Passes not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #remove deleted bookings
        $qrBookingsWithPass = $qrBookingsWithPass->filter(function($booking) {
            if(!$booking->is_deleted AND $booking->is_on_the_way_or_avialable_to_pick) {
                return $booking;
            }
        });

        #remove deleted bookings
        $scanPrivatebookingsWithPass = $scanPrivatebookingsWithPass->filter(function($booking) {
            if(!$booking->is_deleted AND !$booking->is_checked_in AND !$booking->is_completed) {
                return $booking;
            }
        });

        #Fetch qrbookingsWithPass
        $qrBookingsWithPass = $qrBookingsWithPass->isNotEmpty() ? $qrBookingsWithPass->sortBy('arrived_timestamp') : collect();
        $scanPrivatebookingsWithPass = $scanPrivatebookingsWithPass->isNotEmpty() ? $scanPrivatebookingsWithPass->sortBy('arrived_timestamp') : collect();

        #validate Pass
        if($qrBookingsWithPass->isEmpty() AND $scanPrivatebookingsWithPass->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Passes not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Create Data
        $bookingsData = [];
        foreach ($qrBookingsWithPass as $key => $booking) {
            $data = [
                'booking_id'                => $booking->id ?? '',
                'vehicle_id'                => $booking->vehicle->id ?? '',
                'booking_type'              => 'qrcode',
                'booking_status'            => $booking->fetch_booking_status,
                'parking_type'              => $booking->parking->parkingSetupSpace->pass_type ?? '',
                'bookingWithPassType'       => $bookingType ?? '',
                'promocode_id'              => $booking->promocode_id,
                'promocode'                 => $booking->promocode,
                'promocode_discount'        => $booking->promocode_discount,
                'email'                     => $booking->user->email ?? '',
                'in_time'                   => $booking->arrival_date_time->timestamp ?? '',
                'out_time'                  => $booking->exit_date_time->timestamp ?? '',
                'valet'                     => $booking->valet ? true : false,
                'valet_and_car_wash'        => $booking->valet_and_washing ? true : false,
                'valet_price'               => $parkingMarkout->set_valet_price ?? '',
                'valet_and_car_wash_price'  => $parkingMarkout->set_valet_and_washing_price ?? '',
                'subtotal'                  => $booking->sub_total ?? '',
                'total'                     => $booking->paid_money ?? '',
                'payment_way'               => $booking->payment_method ?? '',
                'created_at'                => $booking->created_at->format('Y-m-d H:i:s') ?? '',
                'pass_id'                   => $booking->pass->id ?? '',
                'pass_image'                => $booking->pass->image_full_path ?? '',
                'pass_number'               => $booking->pass->pass_number ?? '',
                'pass_state'                => $booking->pass->pass_status ?? '',
                'parking_id'                => $booking->parking->id ?? '',
                'private_code'              => $booking->parking->fetch_private_code ?? '',
                'parking_image'             => $booking->parking->image_full_path ?? '',
                'parking_location'          => $booking->parking->parkingSetupSpace->location ?? '',
                'parking_latitude'          => $booking->parking->parkingSetupSpace->latitude ?? '',
                'parking_longitude'         => $booking->parking->parkingSetupSpace->longitude ?? '',
                'parking_image'             => $booking->parking->image_full_path ?? '',
                'remaining_time'            => $booking->remaining_arrival_time ?? '',
                'createdTimestamp'          => $booking->created_at->timestamp ?? '',
                'arrivedTimestamp'          => $booking->arrived_timestamp ?? '',
                'time_duration_key'         => $booking->confirm_time_left ?? '',
                'edit_delete_time_duration' => $booking->edit_cancel_time_left ?? '',
            ];
            
            #push data
            array_push($bookingsData, $data);
        }

        #Create Scan And privateBookings data
        foreach ($scanPrivatebookingsWithPass as $key => $booking) {
            $data = [
                'booking_id'                => $booking->id ?? '',
                'vehicle_id'                => $booking->vehicle->id ?? '',
                'booking_type'              => 'scan_or_private',
                'booking_status'            => $booking->fetch_booking_status,
                'parking_type'              => $booking->parking->parkingSetupSpace->pass_type ?? '',
                'bookingWithPassType'       => $bookingType ?? '',
                'promocode_id'              => $booking->promocode_id,
                'promocode'                 => $booking->promocode,
                'promocode_discount'        => $booking->promocode_discount,
                'email'                     => $booking->user->email ?? '',
                'in_time'                   => $booking->arrival_date_time->timestamp ?? '',
                'out_time'                  => $booking->exit_date_time->timestamp ?? '',
                'valet'                     => $booking->valet ? true : false,
                'valet_and_car_wash'        => $booking->valet_and_washing ? true : false,
                'valet_price'               => $parkingMarkout->set_valet_price ?? '',
                'valet_and_car_wash_price'  => $parkingMarkout->set_valet_and_washing_price ?? '',
                'subtotal'                  => $booking->sub_total ?? '',
                'total'                     => $booking->paid_money ?? '',
                'payment_way'               => $booking->payment_method ?? '',
                'created_at'                => $booking->created_at->format('Y-m-d H:i:s') ?? '',
                'pass_id'                   => $booking->pass->id ?? '',
                'pass_image'                => $booking->pass->image_full_path ?? '',
                'pass_number'               => $booking->pass->pass_number ?? '',
                'pass_state'                => $booking->pass->pass_status ?? '',
                'parking_id'                => $booking->parking->id ?? '',
                'private_code'              => $booking->parking->fetch_private_code ?? '',
                'parking_image'             => $booking->parking->image_full_path ?? '',
                'parking_location'          => $booking->parking->parkingSetupSpace->location ?? '',
                'parking_latitude'          => $booking->parking->parkingSetupSpace->latitude ?? '',
                'parking_longitude'         => $booking->parking->parkingSetupSpace->longitude ?? '',
                'parking_image'             => $booking->parking->image_full_path ?? '',
                'remaining_time'            => $booking->remaining_arrival_time ?? '',
                'createdTimestamp'          => $booking->created_at->timestamp ?? '',
                'arrivedTimestamp'          => $booking->arrived_timestamp ?? '',
                'time_duration_key'         => $booking->confirm_time_left ?? '',
                'edit_delete_time_duration' => $booking->edit_cancel_time_left ?? '',
            ];
            
            #push data
            array_push($bookingsData, $data);
        }

        #Set BookingsData
        $bookingsData = collect($bookingsData)->sortBy('arrivedTimestamp')->reverse()->toArray();
        $pendingConfrimedBookings = collect($bookingsData)->whereIn('booking_status', ['Pending', 'Confirmed'])->reverse()->toArray();
        $restBookings = collect($bookingsData)->whereNotIn('booking_status', ['Pending', 'Confirmed'])->reverse()->toArray();


        #Final BookingsData
        $finalBookingData = array_merge($pendingConfrimedBookings, $restBookings);
        
        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Passes found.',
            'success'       => true,
            'data'          => $finalBookingData
        ]);
    }

     /**
     * @method to delete Pass
     * @return json
     * @param
     */
    public function deletePass(Request $request)
    {
        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #User
        $user = $this->user
                     ->with(['bookings.pass', 'scanPrivateBookings.pass'])
                    ->find($user->id);

        #Fetch Data
        $passId = $request->pass_id ?? '';
        $bookingId = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';
        
        #Validate Data
        if($passId == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'pass_id, booking_id, booking_type is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate BookingType
        $bookingTypes = ['qrcode', 'scan_or_private'];
        if(!in_array($bookingType, $bookingTypes)) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch booking
        if($bookingType == 'qrcode') {
            $bookings = $user->bookings->where('id', $bookingId);
        } else {
            $bookings = $user->scanPrivateBookings->where('id', $bookingId);
        }
        
        #Validate Booking
        if($bookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch Booking
        $booking = $bookings->first();
        $bookingPass = $booking->pass;

        #Validate Booking pass
        if($bookingPass == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Pass not found on booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate BookingPas
        if($bookingPass->id != $passId) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Pass not found on booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Delete Pass And Booking
        $booking->update(['is_deleted' => true]);
        $bookingPass->delete();

        #return
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Pass deleted successfully.',
            'success'       => true,
            'data'          => null
        ]);
    }


     /**
     * @method to fetchPassVehicle
     * @return json
     * @param
     */
    public function fetchPassVehicle(Request $request)
    {
        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #User
        $user = $this->user
                     ->with(['bookings.pass', 'bookings.vehicle', 'scanPrivateBookings.pass', 'scanPrivateBookings.vehicle'])
                    ->find($user->id);

        #Fetch Data
        $passId = $request->pass_id ?? '';
        $bookingId = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';
        
        #Validate Data
        if($passId == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'pass_id, booking_id, booking_type is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate BookingType
        $bookingTypes = ['qrcode', 'scan_or_private'];
        if(!in_array($bookingType, $bookingTypes)) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch booking
        if($bookingType == 'qrcode') {
            $bookings = $user->bookings->where('id', $bookingId);
        } else {
            $bookings = $user->scanPrivateBookings->where('id', $bookingId);
        }
        
        #Validate Booking
        if($bookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch Booking
        $booking = $bookings->first();
        $bookingPass = $booking->pass;

        #Validate Booking pass
        if($bookingPass == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Pass not found on booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate BookingPas
        if($bookingPass->id != $passId) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Pass not found on booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #fetch booking vehcile
        $vehicle = $booking->vehicle;

        #VAlidate vehicle
        if($vehicle == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Vehicle not found on booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        $data = [
            'id'                => (string)$vehicle->id ?? '',
            'booking_id'        => $bookingId,
            'plate_number'      => (string)$vehicle->plate_number ?? '',
            'vehicle_type'      => (string)$vehicle->vehicle_type ?? '',
            'brand_id'          => (string)$vehicle->brand_id ?? '',
            'brand_name'        => (string)$vehicle->brand_name ?? '',
            'modal_id'          => (string)$vehicle->modal_id ?? '',
            'modal_name'        => (string)$vehicle->modal_name ?? '',
            'color_id'          => (string)$vehicle->color_id ?? '',
            'color_name'        => (string)$vehicle->color_name ?? '',
            'color_code'        => (string)$vehicle->hex_code ?? '',
            'image'             => (string)$vehicle->image_full_path ?? '',
            'qr_code_image'     => (string)$vehicle->qr_code_image_full_path ?? '',
        ];  

        #return
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Vehicle Found.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to scan scan me type Booking
     * @return json
     * @param
     */
    public function scanMe(Request $request)
    {
        #FEtchData
        $bookingId = $request->booking_id;

        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #User
        $user = $this->user
                     ->with(['scanPrivateBookings.pass'])
                    ->find($user->id);

        #Validate Data
        if($bookingId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_id is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch User Bookings
        $scanPrivateBookings = $user->scanPrivateBookings;
        
        #fetch requested Booking
        $booking = $scanPrivateBookings->where('id', $bookingId);

        #Validate booking
        if($booking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch booking
        $booking = $booking->first();

        #FEtch Booking Type
        $bookingType = $booking->parking_type;

        #VAlidate BookingType
        if($bookingType == '' OR ($bookingType != 'scan_me')) { 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Either Booking Type is not found or not scan type.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Update actual_in_dateTime
        $booking->update(['actual_in_dateTime' => Carbon::now()->format('Y-m-d H:i:s')]);

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking updated successfully',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to assignSlot Number
     * @return json
     * @param
     */
    public function assignSlotNumber(Request $request)
    {
        #FEtchData
        $bookingId      = $request->booking_id;
        $parkingNumber  = $request->slot_number;

        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #User
        $user = $this->user
                     ->with(['scanPrivateBookings.pass'])
                    ->find($user->id);

        #Validate Data
        if($bookingId == '' OR $parkingNumber == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_id and slot_number is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch User Bookings
        $scanPrivateBookings = $user->scanPrivateBookings;
        
        #fetch requested Booking
        $booking = $scanPrivateBookings->where('id', $bookingId);

        #Validate booking
        if($booking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch booking
        $booking = $booking->first();

        #FEtch Booking Type
        $bookingType = $booking->parking_type;

        #VAlidate BookingType
        /*if($bookingType == '' OR ($bookingType != 'scan_me')) { 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Either Booking Type is not found or not scan type.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        #set Unique key Number
        $bookingCount = ($this->scanOrPrivateBooking->get()->count() > 0) ? 
                            $this->scanOrPrivateBooking->get()->count() + 1 : 1;

        $uniquekeyNumber = 'SP'.'0000'.$bookingCount;

        #Update actual_in_dateTime
        $booking->update([
            'parking_no' => $parkingNumber, 
            'is_checked_in' => true, 
            'key_no' => $uniquekeyNumber, 
            'booking_status' => UserBookingStatusInterface::VEHICLE_PARKED,
            'actual_in_dateTime' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        #Update User
        $user->update(['current_booking_id' => $booking->id, 'current_booking_type' => 'scan_or_private']);

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Slot assigned successfully',
            'success'       => true,
            'data'          => null
        ]);
    }
    

    /**
     * @method to fetch user recent parking
     * @return json
     * @param recentParkingList
     */
    public function recentParkingList(Request $request)
    {
        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #User
        $user = $this->user
                     ->with(['bookings.supervisors', 'bookings.drivers'])
                     ->find($user->id);

        #Fetch user bookings
        $userBookings = $user->bookings;

        #validate bookings
        if($userBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No recent parking Found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch valid User Bookings which are Completed from Supervsior or Driver End
        $completedBookings = $userBookings->filter(function($booking) {
            $bookingSupervisor = $booking->supervisors->isNotEmpty() ? $booking->supervisors->last() : '';
            $bookingDriver = $booking->drivers->isNotEmpty() ? $booking->drivers->last() : '';

            #Fetch Status of Driver and Supervisor
            if($bookingSupervisor != '' OR $bookingDriver != '') {
                if($bookingSupervisor != '') {
                    if($bookingSupervisor->is_completed) {
                        return $booking;
                    }
                } elseif ($bookingDriver != '') {
                    if($bookingDriver->is_completed) {
                        return $booking;
                    }
                }
            }
        });
        
        #validate bookings
        if($completedBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No recent parking Found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch all parkings Ids Unique
        $parkingIds = array_unique($completedBookings->pluck('parking_id')->toArray());
        
        #FEtch all parkings
        $parkings = $this->companyParking
                         ->whereIn('id', $parkingIds)
                         ->with(['parkingSetupSpace'])
                         ->get();

        #Set data
        $parkingsData = [];
        foreach ($parkings as $key => $parking) {
            $data = [
                'id'            => $parking->id ?? '',
                'name'          => $parking->name ?? '',
                'image_path'    => $parking->image_path,
                'latitude'      => $parking->parkingSetupSpace->latitude ?? '',
                'longitude'     => $parking->parkingSetupSpace->longitude ?? '',
                'status'        => $parking->active_or_disabled,
                'address'       => $parking->location ?? '',
            ];

            #push to sarray
            array_push($parkingsData, $data);
        }

        #paginate
        $parkings = $this->commonUtil->customPaginate($parkingsData, 5);

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Recent parking Found.',
            'success'       => true,
            'data'          => $parkings
        ]);

    }

    /**
     * @method to update wash Status on booking
     * @return json
     * @param Request $request
     */
    public function bookingWashStatusUpdate(Request $request)
    {
        #Fetch User
        $user = Auth::user();

        #Fetch Data
        $bookingId = $request->booking_id ?? '';
        $status = $request->status ?? '';
        $washType = $request->wash_type ?? '';
        $bookingType = strtolower($request->booking_type) ?? '';

        #validate User
        if($user == '' OR $bookingId == '' OR $status == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id, booking_type or status not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate Status
        if($status != 0 AND $status != 1) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Status is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate washType
        if($status == 1) {
            if(!in_array($washType, [1,2,3])) {
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Wash Status is not valid.',
                    'success'       => false,
                    'data'          => null
                ]);
            }
        }

         #validate BookingTpe
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid(qrcode, scan_me, private).',
                'success'       => false,
                'data'          => null
            ]);
        }

        #User
        $user = $this->user
                     ->with(['bookings.parking', 'scanPrivateBookings'])
                     ->find($user->id);

         #FEtch Booking
        if($bookingType == 'qr_code') {
            $userBooking = $user->bookings->where('id', $bookingId);
        } else {
            $userBooking = $user->scanPrivateBookings->where('id', $bookingId);
        }

        #validate bookings
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #User Booking
        $userBooking = $userBooking->first();
        
        #update status for eashing
        if($status == 1) {
            $userBooking->update(['wash_available' => true, 'wash_type' => $washType]);
        } else {
            $userBooking->update(['wash_available' => false]);
        }

        #return
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking status Updated.',
            'success'       => true,
            'data'          => null
        ]);
    }
    

    /**
     * @method to fetch Status of Switch Screen
     * @return json
     * @param 
     */
    public function switchScreenStatus(Request $request)
    {
        #Fetch User
        $user = Auth::user();
        
        #validate User
        if($user == '' OR $request->is_first_time == '' OR !in_array($request->is_first_time, [0, 1])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User or is_first_time not found or is_firt_time is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #User
        $user = $this->user
                     ->with(['currentBooking.parking.parkingMarkOut'])
                     ->find($user->id);

        #if switch Screen Status is false
        if($request->is_first_time == 1) {
            #Update Screen Status to False
            $user->update(['switch_screen' => false]);

            #return response
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Switch Screen is disable.',
                'success'       => false,
                'data'          => ['switch_screen_status' => false]
            ]);
        } else {
            if($user->currentBooking == '') {
               #return response
                 return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Switch Screen is disable.',
                    'success'       => false,
                    'data'          => ['switch_screen_status' => false]
                ]);
            }

            #fetch user booking
            $booking = $user->currentBooking;

            #Set Data
            $data = [
                #'booking_id'            => $booking->id,
                #'booking_price'         => $booking->parking->parkingMarkOut->set_book_price ?? 0,
                'switch_screen_status'   => true,
                #'wash_prices'           => $booking->parking->washing_cost
            ];

            #Update Screen Status to False
            $user->update(['switch_screen' => false]);

            #return json
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Switch Screen in enable.',
                'success'       => true,
                'data'          => $data
            ]);
        }
    }


    /**
     * @method to fetch Status of Switch Screen
     * @return json
     * @param 
     */
    public function scanBookingPassQr(Request $request)
    {
        #Fetch User
        $user = Auth::user();
        $bookingId = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';
        
        #validate User
        if($user == '' OR $bookingId == '' OR $request->is_first_time == '' OR !in_array($request->is_first_time, [0, 1])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id, is_first_time not found or is_firt_time is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate BookingTyepe
        if($bookingType == '' OR !in_array($bookingType, ['qr_code', 'scan_or_private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type not found or not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Update User bookingId
        $user->update(['current_booking_id' => $bookingId, 'current_booking_type' => $bookingType]);

        #User
        $user = $this->user
                     ->with(['currentBooking.parking.parkingMarkOut'])
                     ->find($user->id);

        #if switch Screen Status is false
        if($request->is_first_time == 1) {
            #Update Screen Status to False
            $user->update(['switch_screen' => false]);

            #return response
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Switch Screen is disable.',
                'success'       => false,
                'data'          => ['switch_screen_status' => false]
            ]);
        } else {
            if($user->currentBooking == '') {
               #return response
                 return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Switch Screen is disable.',
                    'success'       => false,
                    'data'          => ['switch_screen_status' => false]
                ]);
            }

            #fetch user booking
            $booking = $user->currentBooking;

            #Set Data
            $data = [
                #'booking_id'            => $booking->id,
                #'booking_price'         => $booking->parking->parkingMarkOut->set_book_price ?? 0,
                'switch_screen_status'   => true,
                #'wash_prices'           => $booking->parking->washing_cost
            ];

            #Update Screen Status to False
            $user->update(['switch_screen' => false]);

            #return json
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Switch Screen in enable.',
                'success'       => true,
                'data'          => $data
            ]);
        }
    }


    /**
     * @method to fetch wash cost of current booking of user
     * @return json
     * @param 
     */
    public function fetchWashCost(Request $request)
    {
        #FEtch booking id
        $bookingId = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';

        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id , booking_type not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate booking ztype
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking Type is not valid',
                'success'       => false,
                'data'          => null
            ]);
        }

        #User
        $user = $this->user
                     ->with(['currentBooking.parking.parkingMarkOut'])
                     ->find($user->id);

        // if($user->currentBooking == '') {
        //     return response()->json([
        //         'statusCode'    =>  ApiStatusInterface::OK, 
        //         'message'       => 'No current booking found on User.',
        //         'success'       => false,
        //         'data'          => null
        //     ]);
        // }

        #Fetch booking
        if($bookingType == 'qr_code') {
            #Set Relations
            $relations = [
                'bookings.supervisors',
                'bookings.drivers',
                'bookings.user',
                'bookings.vehicle',
                'bookings.parking.company.driverCommission',
                'bookings.parking.company.washerCommission',
                'bookings.parking.parkingMarkOut',
                'bookings.parking.parkingDriveFeatures',
                'bookings.parking.parkingWasherFeatures',
            ];       

            #Fetch user
            $user = $this->user->with($relations)->find($user->id);   

            #User booking
            $userBooking = $user->bookings->where('id', $bookingId);      
        } else {
            #Set Relations
            $relations = [
                'scanPrivateBookings.supervisors',
                'scanPrivateBookings.drivers',
                'scanPrivateBookings.user',
                'scanPrivateBookings.vehicle',
                'scanPrivateBookings.parking.company.driverCommission',
                'scanPrivateBookings.parking.company.washerCommission',
                'scanPrivateBookings.parking.parkingMarkOut',
                'scanPrivateBookings.parking.parkingDriveFeatures',
                'scanPrivateBookings.parking.parkingWasherFeatures',
            ];

            #Fetch user
            $user = $this->user->with($relations)->find($user->id);  

            #User booking
            $userBooking = $user->scanPrivateBookings->where('id', $bookingId);
        }

        #Validate Booking
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch Bookiong
        $booking = $userBooking->last();

        #Set Data
        $data = [
            'booking_id'    => $booking->id,
            'order_number'  => $booking->order_number,
            'booking_price' => $booking->parking->parkingMarkOut->set_book_price ?? '0.00',
            'wash_prices'   => $booking->parking->washing_cost,
            'booking_type'   => $bookingType
        ];

        #return json
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Wash Cost for current booking.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to fetch all the status regarding Supervisor, Driver and Washer of Booking
     * @param Request $request
     * @return json
     */
    public function bookingStatus(Request $request)
    {
        #fetch bookingId
        $bookingId = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';

         #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id or booking_type not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Relations
        $relations = [
            'bookings.supervisors',
            'bookings.drivers', 
            'bookings.washer',
            'bookings.parking.parkingMarkOut'
        ];

         #validate booking ztype
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking Type is not valid',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch bookinf
        if($bookingType == 'qr_code') {
            #Set Relations
            $relations = [
                'bookings.supervisors',
                'bookings.drivers',
                'bookings.user',
                'bookings.vehicle',
                'bookings.parking.company.driverCommission',
                'bookings.parking.company.washerCommission',
                'bookings.parking.parkingMarkOut',
                'bookings.parking.parkingDriveFeatures',
                'bookings.parking.parkingWasherFeatures',
            ];       

            #Fetch user
            $user = $this->user->with($relations)->find($user->id);   

            #User booking
            $userBooking = $user->bookings->where('id', $bookingId);      
        } else {
            #Set Relations
            $relations = [
                'scanPrivateBookings.supervisors',
                'scanPrivateBookings.drivers',
                'scanPrivateBookings.user',
                'scanPrivateBookings.vehicle',
                'scanPrivateBookings.parking.company.driverCommission',
                'scanPrivateBookings.parking.company.washerCommission',
                'scanPrivateBookings.parking.parkingMarkOut',
                'scanPrivateBookings.parking.parkingDriveFeatures',
                'scanPrivateBookings.parking.parkingWasherFeatures',
            ];

            #Fetch user
            $user = $this->user->with($relations)->find($user->id);  

            #User booking
            $userBooking = $user->scanPrivateBookings->where('id', $bookingId);
        }

        #Validate Booking
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $userBooking->first();
        
        #Set Washer Status
        $washerStatus = '';
        if($booking->wash_available AND $booking->wash_type == '') {
            $washerStatus = 'WASH_INITIAL';
        } elseif ($booking->wash_available AND $booking->wash_type != '') {
            $washerStatus = 'WASH_TYPE_SELECTED';
        } else {
            $washerStatus = 'NO_WASH';
        }

        #FEtch washer of Booking
        $washer = WasherBooking::where('booking_type', $bookingType)->where('booking_id', $booking->id)->get();
        $washerOnBooking = $washer->isNotEmpty() ? $washer->first() : '';
       
        #validate for get
        if($bookingType == 'qr_code') {
            if( $booking->supervisors->where('set_or_get', 'get')->isNotEmpty()) {
                $data = [
                    'supervisor_status' => $booking->supervisors->isNotEmpty() ? $booking->supervisors->last()->fetch_booking_status : '',
                    'driver_status'     => '',
                    'washer_status'     => $washerOnBooking != '' ? $washerOnBooking->fetch_washing_status : $washerStatus,
                ];
            } elseif ($booking->drivers->where('set_or_get', 'get')->isNotEmpty()) {
                  $data = [
                    'supervisor_status' =>  '',
                    'driver_status'     => $booking->drivers->isNotEmpty() ? $booking->drivers->last()->fetch_booking_status : '',
                    'washer_status'     => $washerOnBooking != '' ? $washerOnBooking->fetch_washing_status : $washerStatus,
                ];
            } else {
                $data = [
                    'supervisor_status' => $booking->supervisors->isNotEmpty() ? $booking->supervisors->last()->fetch_booking_status : '',
                    'driver_status'     => $booking->drivers->isNotEmpty() ? $booking->drivers->last()->fetch_booking_status : '',
                    'washer_status'     => $washerOnBooking != '' ? $washerOnBooking->fetch_washing_status : $washerStatus,
                ];
            }
        } else {
             if( $booking->supervisors->isNotEmpty()) {
                $data = [
                    'supervisor_status' => '',
                    'driver_status'     => '',
                    'washer_status'     => $washerOnBooking != '' ? $washerOnBooking->fetch_washing_status : $washerStatus,
                ];
            } elseif ($booking->drivers->isNotEmpty()) {
                  $data = [
                    'supervisor_status' => '',
                    'driver_status'     => '',
                    'washer_status'     => $washerOnBooking != '' ? $washerOnBooking->fetch_washing_status : $washerStatus,
                ];
            } else {
                $data = [
                    'supervisor_status' =>  '',
                    'driver_status'     =>  '',
                    'washer_status'     => $washerOnBooking != '' ? $washerOnBooking->fetch_washing_status : $washerStatus,
                ];
            }
        }
     

        #Set some data
        $data['isPaymentModeSelected'] = $booking->payment_method != '' ? true : false;
        $data['is_paid'] = $booking->is_paid ? true : false;
        $data['payment_methods_avail'] = $booking->parking->all_payment_methods;
        $data['has_valet'] = $booking->is_valet_available;
        $data['parking_type'] = $booking->parking_type;
        $data['booking_status'] = $booking->fetch_booking_status;

        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'All Booking Statues.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to save payment Mode on Booking
     * @param Request $request
     * @return json
     */
    public function savePaymentMode(Request $request)
    {
        #fetch bookingId
        $bookingId      = $request->booking_id ?? '';
        $paymentMethod  = $request->payment_method ?? '';

         #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '' OR $paymentMethod == '' OR !in_array($paymentMethod, ['cash', 'card'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id, payment_method not found or payment_method is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Relations
        $relations = [
            'bookings'
        ];

        #User
        $user = $this->user->find($user->id);
        
        #User booking
        $userBooking = $user->bookings->where('id', $bookingId);

        #Validate Booking
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $userBooking->first();
        
        #Validate if Payment method already on booking
        if($booking->payment_method != '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment method is already exist on booking.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Set Payment method on Booking
        $booking->update(['payment_method' => $paymentMethod]);

        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Payment Method set on Booking.',
            'success'       => true,
            'data'          => []
        ]);
    }

    /**
     * @method to Fetch bill regarding Booking
     * @param Request $request
     * @return json
     */
    public function fetchBookingBill(Request $request)
    {
        #fetch bookingId
        $bookingId      = $request->booking_id ?? '';
        $bookingType    = $request->booking_type ?? '';

         #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id or booking_type not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate booking ztype
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking Type is not valid',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch bookinf
        if($bookingType == 'qr_code') {
            #Set Relations
            $relations = [
                'bookings.supervisors',
                'bookings.drivers',
                'bookings.user',
                'bookings.vehicle',
                'bookings.parking.company.driverCommission',
                'bookings.parking.company.washerCommission',
                'bookings.parking.parkingMarkOut',
                'bookings.parking.parkingDriveFeatures',
                'bookings.parking.parkingWasherFeatures',
            ];       

            #Fetch user
            $user = $this->user->with($relations)->find($user->id);   

            #User booking
            $userBooking = $user->bookings->where('id', $bookingId);      
        } else {
            #Set Relations
            $relations = [
                'scanPrivateBookings.supervisors',
                'scanPrivateBookings.drivers',
                'scanPrivateBookings.user',
                'scanPrivateBookings.vehicle',
                'scanPrivateBookings.parking.company.driverCommission',
                'scanPrivateBookings.parking.company.washerCommission',
                'scanPrivateBookings.parking.parkingMarkOut',
                'scanPrivateBookings.parking.parkingDriveFeatures',
                'scanPrivateBookings.parking.parkingWasherFeatures',
            ];

            #Fetch user
            $user = $this->user->with($relations)->find($user->id);  

            #User booking
            $userBooking = $user->scanPrivateBookings->where('id', $bookingId);
        }

        #Validate Booking
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $userBooking->first();

        #Validate booking Payment method is set or not
        if($booking->payment_method == '') {
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment method is not available on Booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate booking type is scan 
        if(in_array($bookingType, ['scan_me', 'private'])) {
            #Validate 
            if(!$booking->bill['status']) {
                #return 
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => $booking->bill['message'],
                    'success'       => false,
                    'data'          => null
                ]);
            }

            #return 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment Method set on Booking.',
                'success'       => true,
                'data'          => $booking->bill['data']
            ]);
        }
        
        #Fetch booking is in whose get (Supervisor or Driver)
        $supervisors = $booking->supervisors->where('set_or_get','get');
        $drivers = $booking->drivers->where('set_or_get','get');
        
        #validate if Booking is on Supervisor or Driver Get and valet is selected on booking
        if(($supervisors->isNotEmpty() OR $drivers->isNotEmpty()) AND $booking->valet) {
            #Fetch status of Booking
            $bookingStatus = '';
            if($supervisors->isNotEmpty()) {
                $bookingStatus = $supervisors->last()->booking_status;
            } elseif ($drivers->isNotEmpty()) {
                $bookingStatus = $drivers->last()->booking_status;
            }
            
            #Validate if booking is in Bill Generate Stage(GET_CAR_REQUEST,ON_THE_WAY_TO_DROP,ARRIVED,BILL_GENERATED)
            if(!in_array($bookingStatus, [5,6,7,8])) {
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Booking not in Bill Generate Stage yet.',
                    'success'       => false,
                    'data'          => null
                ]);
            }
            
            #Validate 
            if(!$booking->bill['status']) {
                #return 
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => $booking->bill['message'],
                    'success'       => false,
                    'data'          => null
                ]);
            }

            #FEtch data
            $data =  $booking->bill['data'];
            $data['booking_type'] =  $booking->parking_type;

            #Set arrived time of Booking
           $data['arrived_time'] = $booking->arrived_formatted_time ?? '';

            #Set arrived Date of Booking
           $data['arrived_date'] = $booking->arrived_date_formatted ?? '';

            #Set requested time of Booking
           $data['requested_time'] = $supervisors->isNotEmpty() ?  
                                       $supervisors->last()->requested_formatted_time : 
                                       $drivers->last()->requested_formatted_time;

            #Set requested date of Booking
           $data['requested_date'] = $supervisors->isNotEmpty() ?  
                                       $supervisors->last()->requested_date_formatted : 
                                       $drivers->last()->requested_date_formatted;
            
            #return 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment Method set on Booking.',
                'success'       => true,
                'data'          => $data
            ]);
        }
       
        #Fetch status of Booking
        $bookingStatus = '';
        if($supervisors->isNotEmpty()) {
            $bookingStatus = $supervisors->last()->booking_status;
        } elseif ($drivers->isNotEmpty()) {
            $bookingStatus = $drivers->last()->booking_status;
        }
        
        #Validate if booking is in Bill Generate Stage(GET_CAR_REQUEST,ON_THE_WAY_TO_DROP,ARRIVED,BILL_GENERATED)
        /*if(!in_array($bookingStatus, [5,6,7,8])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not in Bill Generate Stage yet.',
                'success'       => false,
                'data'          => null
            ]);
        }*/
        
        #Validate 
        if(!$booking->bill['status']) {
            #return 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => $booking->bill['message'],
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch data
        $data =  $booking->bill['data'];

        #Set arrived time of Booking
        $data['arrived_time'] = $booking->arrived_formatted_time ?? '';

        #Set arrived Date of Booking
       $data['arrived_date'] = $booking->arrived_date_formatted ?? '';

        #Set requested time of Booking
       $data['requested_time'] = '';

        #Set requested date of Booking
       $data['requested_date'] = '';
       $data['parking_type'] = $booking->parking_type;
       $data['vehicle_id'] = $booking->vehicle_id;
       $data['booking_type'] = $booking->parking_type;
        
        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Payment Method set on Booking.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to do payment of Booking
     * @param Request $request
     * @return json
     */
    public function payment(Request $request)
    {
        #fetch bookingId
        $bookingId          = $request->booking_id ?? '';
        $paymentType        = $request->payment_type ?? '';
        $paidMoney          = $request->paid_money ?? '';
        $transactionId      = $request->transaction_id ?? '';
        $tranRef            = $request->tran_ref ?? '';
        $tranType           = $request->tran_type ?? '';
        $cartId             = $request->cart_id ?? '';
        $cartDescription    = $request->cart_description ?? '';
        $cartCurrency       = $request->cart_currency ?? '';
        $cartAmount         = $request->cart_amount ?? '';
        $callback           = $request->callback ?? '';
        $customerDetails    = $request->customer_details ?? '';
        $paymentResult      = $request->payment_result ?? '';
        $paymentInfo        = $request->payment_info ?? '';

         #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '' OR $paidMoney == '' OR $paymentType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id, paid_money, payment_type not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate Payment Type
        if(!in_array($paymentType, ['initial', 'due'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'payment_type is not valid',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Relations
        $relations = [
            'bookings.supervisors.supervisor',
            'bookings.drivers.driver',
            'bookings.payment'
        ];

        #User
        $user = $this->user
                     ->with($relations)
                     ->find($user->id);
        
        #User booking
        $userBooking = $user->bookings->where('id', $bookingId);

        #Validate Booking
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $userBooking->first();

        #Fetch booking is in whose get (Supervisor or Driver)
        $supervisors = $booking->supervisors->where('set_or_get','get');
        $drivers = $booking->drivers->where('set_or_get','get');
        
        #Validate booking if niether picked by Supervisor nor driver
        /*if($supervisors->isEmpty() AND $drivers->isEmpty()) {
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking niether picked by supervisor nor any driver.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        /*#Validate payment
        if($booking->payment != '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment already done regarding this Booking.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        #Make Payment
        $data = [
            'booking_id'        => $booking->id,
            'transaction_id'    => $transactionId,
            'paid_money'        => $paidMoney,
            'tran_ref'          => $tranRef ?? '',
            'tran_type'         => $tranType ?? '',
            'cart_id'           => $cartId ?? '',
            'cart_description'  => $cartDescription ?? '',
            'cart_currency'     => $cartCurrency ?? '',
            'cart_amount'       => $cartAmount ?? '',
            'callback'          => $callback ?? '',
            'customer_details'  => serialize($customerDetails) ?? '',
            'payment_result'    => serialize($paymentResult) ?? '',
            'payment_info'      => serialize($paymentInfo) ?? '',
            'payment_type'      => 'card',
        ];

        DB::beginTransaction();
            #Cerate Payment
            $payment = $this->payment->create($data);
            
            #Update all Status
            if($supervisors->isNotEmpty()) {
                #Update Supervisor
                $supervisors->last()
                            ->supervisor
                            ->update(['current_booking_id' => 0, '']);

                #Update Supervsior Booking to Complete
                $supervisors->last()->update(['booking_status' => 2]);
            }

            #Update all Status
            if($drivers->isNotEmpty()) {
                $drivers->last()->driver->update(['current_booking_id' => 0]);

                #Update Driver Booking to Complete
                $drivers->last()->update(['booking_status' => 2]);
            }

            if($paymentType != 'initial') {
                #Update booking
                $booking->update(['is_paid' => true, 'booking_status' => UserBookingStatusInterface::BOOKING_COMPLETED]);

                #Update user
                $user->update(['current_booking_id' => 0]);
            }
         
        DB::commit();

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type == 0, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $data = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Payment Done",
                        "body" => "Payment Done"
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "payment_status_noti",
                        "status" => true
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($data);
            }
        }

        #return
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Payment successfully',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to do payment of Booking
     * @param Request $request
     * @return json
     */
    public function paymentForScanorPrivate(Request $request)
    {
        #fetch bookingId
        $bookingId          = $request->booking_id ?? '';
        $paymentType        = $request->payment_type ?? '';
        $paidMoney          = $request->paid_money ?? '';
        $transactionId      = $request->transaction_id ?? '';
        $tranRef            = $request->tran_ref ?? '';
        $tranType           = $request->tran_type ?? '';
        $cartId             = $request->cart_id ?? '';
        $cartDescription    = $request->cart_description ?? '';
        $cartCurrency       = $request->cart_currency ?? '';
        $cartAmount         = $request->cart_amount ?? '';
        $callback           = $request->callback ?? '';
        $customerDetails    = $request->customer_details ?? '';
        $paymentResult      = $request->payment_result ?? '';
        $paymentInfo        = $request->payment_info ?? '';

         #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '' OR $paidMoney == '' OR $paymentType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id, paid_money, payment_type not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate PaymentType
        if(!in_array($paymentType, ['initial', 'due'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'payment_type is not valid',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Relations
        $relations = [
            #'scanPrivateBookings.supervisors.supervisor',
            #'scanPrivateBookings.drivers.driver',
            'scanPrivateBookings.payment'
        ];

        #User
        $user = $this->user
                     ->with($relations)
                     ->find($user->id);
        
        #User booking
        $userBooking = $user->scanPrivateBookings->where('id', $bookingId);

        #Validate Booking
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $userBooking->first();

        #Fetch booking is in whose get (Supervisor or Driver)
        $supervisors = $booking->supervisors;
        $drivers = $booking->drivers;
        
        // #Validate payment
        // if($booking->payment != '') {
        //     return response()->json([
        //         'statusCode'    =>  ApiStatusInterface::OK, 
        //         'message'       => 'Payment already done regarding this Booking.',
        //         'success'       => false,
        //         'data'          => null
        //     ]);
        // }

        #Make Payment
        $data = [
            'booking_id'        => $booking->id,
            'transaction_id'    => $transactionId,
            'paid_money'        => $paidMoney,
            'tran_ref'          => $tranRef ?? '',
            'tran_type'         => $tranType ?? '',
            'cart_id'           => $cartId ?? '',
            'cart_description'  => $cartDescription ?? '',
            'cart_currency'     => $cartCurrency ?? '',
            'cart_amount'       => $cartAmount ?? '',
            'callback'          => $callback ?? '',
            'customer_details'  => serialize($customerDetails) ?? '',
            'payment_result'    => serialize($paymentResult) ?? '',
            'payment_info'      => serialize($paymentInfo) ?? '',
            'payment_type'      => 'card',
        ];

        DB::beginTransaction();
            #Cerate Payment
            $payment = $this->paymentForScanorPrivate->create($data);

            #Update all Status
            if($supervisors->isNotEmpty()) {
                #Update Supervisor
                $supervisors->last()
                            ->supervisor
                            ->update(['current_booking_id' => 0, '']);

                #Update Supervsior Booking to Complete
                $supervisors->last()->update(['booking_status' => 2]);
            }

            #Update all Status
            if($drivers->isNotEmpty()) {
                $drivers->last()->driver->update(['current_booking_id' => 0]);

                #Update Driver Booking to Complete
                $drivers->last()->update(['booking_status' => 2]);
            }

            
            if($paymentType != 'initial') {
                #Update booking
                $booking->update(['is_paid' => true, 'booking_status' => UserBookingStatusInterface::BOOKING_COMPLETED]);

                #Update user
                $user->update(['current_booking_id' => 0]);
            }
        DB::commit();

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type == 0, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $data = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Payment Done",
                        "body" => "Payment Done"
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "payment_status_noti",
                        "status" => true
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($data);
            }
        }

        #return
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Payment successfully',
            'success'       => true,
            'data'          => null
        ]);

    }

    /**
     * @method to update Supervisor or Driver Status to Get My Car
     * @param Request $request
     * @return json
     */
    public function getMyCar(Request $request)
    {
        #fetch bookingId
        $bookingId = $request->booking_id ?? '';

         #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '' OR $bookingId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User or booking_id not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #User
        $user = $this->user
                     ->with(['bookings.supervisor', 'bookings.driver', 'bookings.washer'])
                     ->find($user->id);
        
        #User booking
        $userBooking = $user->bookings->where('id', $bookingId);

        #Validate Booking
        if($userBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $userBooking->first();
        
        #Superviswor or Driver
        $driverBooking     = $booking->driver;
        $supervisorBooking = $booking->supervisor;

        #Update Superv isor or Driver Booking Status to Get Car Request
        if($supervisorBooking != '') {
            $supervisorBooking->update([
                'booking_status' => 5,
                'exit_date_time' => Carbon::now()->format('Y-m-d H:i:s'),
                'requested_date_time' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);


            #validate User token
            if($user->device_token != '' AND in_array($user->device_type == 0, [1,2])) {
                #validate deviceType
                if($user->device_type == 0) {
                    #Call Android Notification
                } else {
                    #call Ios Notification
                    $postData = [
                        "registration_ids" => [$user->device_token],
                        "notification" => [
                            "title" => "Status changed.",
                            "body" =>"Vehicle Requested.",
                        ],
                        "mutable_content" => true,
                        "content_available" => true,
                        "data" => [
                            "type"=> "booking_status_changed_noti"
                        ]
                    ];

                    #Call method and Fetch Status
                    $status = $this->sendPushNotificationIos($postData);
                }
            }
        } elseif ($driverBooking != '') {
            $driverBooking->update([
                'booking_status' => 5,
                'exit_date_time' => Carbon::now()->format('Y-m-d H:i:s'),
                'requested_date_time' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

             #validate User token
            if($user->device_token != '' AND in_array($user->device_type == 0, [1,2])) {
                #validate deviceType
                if($user->device_type == 0) {
                    #Call Android Notification
                } else {
                    #call Ios Notification
                    $postData = [
                        "registration_ids" => [$user->device_token],
                        "notification" => [
                            "title" => "Status changed.",
                            "body" =>"Vehicle Requested.",
                        ],
                        "mutable_content" => true,
                        "content_available" => true,
                        "data" => [
                            "type"=> "booking_status_changed_noti"
                        ]
                    ];

                    #Call method and Fetch Status
                    $status = $this->sendPushNotificationIos($postData);
                }
            }
        }

        #Update Booking Exit Date time
        $booking->update(['exit_date_time' => Carbon::now()->format('Y-m-d H:i:s')]);

        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       =>  'Booking Statues Updated.',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to fetch checkForPaymentRequest on Current Booking
     * @param Request $request
     * @return json
     */
    public function checkForPaymentRequest(Request $request)
    {
        #Fetch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #relations
        $relations = [
            'currentBooking',
            'bookings.vehicle',
            'scanPrivateBookings.vehicle',
            'currentScanOrPrivateBooking',
            'bookings.supervisors.supervisor',
        ];

        #USer
        $user = $this->user->with($relations)->find($user->id);

        #Validate if any cuurent Booking available
        if($user->current_booking_id == 0) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User dont have any booking',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate BookingType on current Booking
        if($user->current_booking_type == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking type not found on current Booking',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch booking based on booking type
        if($user->current_booking_type == 'qr_code') {
            $booking = $user->currentBooking;
        } else {
            $booking = $user->currentScanOrPrivateBooking;
        }

        #Validate booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate if requested for Payemnt on Booking or Not
        if($booking->is_payment_requested) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment Requested on current Booking found',
                'success'       => true,
                'data'          => [
                    'booking_id' => $booking->id,
                    'vehicle_id' => (string)$booking->vehicle->id, 
                    'parking_type' => $booking->parking_type,
                    'paid_amount' => $booking->paid_amount,
                    'due_amount' => $booking->due_amount,
                ]
            ]);
        } else {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment Requested on current Booking not found',
                'success'       => false,
                'data'          => null
            ]);
        }
    }


    /**
     * @method to fetch Booking Detail
     * @param Request $request
     * @return json
     */
    public function bookingDetail(Request $request)
    {
        #Fetch User
        $user = Auth::user();
        $bookingId = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';

        #validate User
        if($user == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User, booking_id, paid_money not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate BookingTpe
        if(!in_array($bookingType, ['qrcode', 'scan_or_private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid(qrcode, scan_or_private).',
                'success'       => false,
                'data'          => null
            ]);
        }

        #relations
        $relations = [
            'bookings.vehicle',
            'scanPrivateBookings.vehicle',
            'bookings.supervisors.supervisor'
        ];

        #USer
        $user = $this->user->with($relations)->find($user->id);

        #FEtch booking
        if($bookingType == 'qrcode') {
            $booking = $user->bookings->where('id', $bookingId);
        } else {
             $booking = $user->scanPrivateBookings->where('id', $bookingId);
        }

        #Validate booking
        if($booking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Sorry! Booking not found on this booking_id.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $booking->first();

        #Validate if booking checked in or not
        if(!$booking->is_checked_in) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Sorry! Booking not checked in yet.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch vehicle
        $vehicle = $booking->vehicle;

        #Set BookingStataus
        $bookingStatus = '';
        if($booking->supervisors->isNotEmpty() OR $booking->drivers->isNotEmpty()) {
            if($booking->supervisors->isNotEmpty()) {
                $bookingStatus = $booking->supervisors->last()->fetch_booking_status;
            } else {
                $bookingStatus = $booking->drivers->last()->fetch_booking_status;
            }
        } else {
            $bookingStatus = $booking->fetch_booking_status;
        }

        #Set Data
        $data = [
            'booking_id'            => $bookingId,
            'key_number'            => $booking->key_number,
            'booking_type'          => $bookingType,
            'booking_unique_id'     => $booking->order_number,
            'vehicle_id'            => $vehicle->id,
            'vehicle_model_name'    => $vehicle->full_name,
            'vehicle_plate_number'  => $vehicle->plate_number,
            'vehicle_image'         => $vehicle->image_full_path,
            'vehicle_color'         => $vehicle->color_name,
            'vehicle_service'       => $booking->valet ? 'Taken' : 'Not Taken',
            'wash_type'             => $booking->wash_type != '' ? $booking->wash_type : '',
            'duration'              => '',
            'processing_time'       => '',
            'arrived_after_date'    => $booking->arrived_after_date,
            'arrived_after_time'    => $booking->arrived_after_time,
            'exit_after_date'       => $booking->exit_after_date,
            'exit_after_time'       => $booking->exit_after_time,
            'booking_status'        => $bookingStatus,
            'supervisor_name'       => $booking->supervisor_name,
            'employee_name'         => '',
        ];
        
        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking Details found.',
            'success'       => true,
            'data'          => $data
        ]);
    }
    

    /**
     * @method to fetch distance bw user lat long and Parking Lat Long
     * @return Distance in miles
     * @param $userLat, $userLong, $parkingLat, $parkingLong
     */
    function distanceInMiles($userLat, $userLong, $parkingLat, $parkingLong, $unit = '') {
      if (($userLat == $parkingLat) && ($userLong == $parkingLong)) {
        return 0;
      }
      else {
        $theta = $userLong - $parkingLong;
        $dist = sin(deg2rad($userLat)) * sin(deg2rad($parkingLat)) +  cos(deg2rad($userLat)) * cos(deg2rad($parkingLat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
          return $miles;
        }
      }
    }
}