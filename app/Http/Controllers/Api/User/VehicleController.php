<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Log;
use Auth;
use Validator;
use Carbon\Carbon;
use \Milon\Barcode\DNS2D;

# Models
use App\Models\User;
use App\Models\Brand;
use App\Models\Color;
use App\Models\UserVehicle;

# Interface
use App\Interfaces\ApiStatusInterface;
use App\Interfaces\UserBookingStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class VehicleController extends Controller implements ApiStatusInterface, UserBookingStatusInterface
{
    # Variable $user
    protected $user; 

    # Variable $brand
    protected $brand;

    # Variable $color
    protected $color;

    # Variable $userVehicle
    protected $userVehicle;
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(User $user, Brand $brand, Color $color, UserVehicle $userVehicle)
	{
		$this->user         = $user;
        $this->brand        = $brand;
        $this->color        = $color;
        $this->userVehicle  = $userVehicle;
	}

    /**
     * @method to fetch brands
     * @param 
     * @return json
     */
    public function fetchBrands()
    {
        #Fetch all brands
        $brands = $this->brand->active()->get();

        #validate brands
        if($brands->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Brands not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Brands Array
        $brandsArray = [];
        foreach($brands as $key => $brand) {
            $data = [
                'id' => (string)$brand->id, 
                'name' => $brand->name ?? ''
            ];

            #Push to array
            array_push($brandsArray, $data);
        }

        #return brands
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Brands found.',
            'success'       => true,
            'data'          => $brandsArray
        ]);
    }

    /**
     * @method to fetch modals on brand
     * @param 
     * @return json
     */
    public function fetchModals(Request $request)
    {
        #Fethc brandid
        $brandId = $request->brand_id;

        #Validate BrandID
        if($brandId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'brand_id is required.',
                'success'       =>  false,
                'data'          =>  null
            ]);
        }

        #Fetch brand
        $brand = $this->brand
                      ->with(['modals'])
                      ->active()
                      ->find($brandId);

        #validate brands
        if($brand == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Brand not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Modals on brand
        $modals = $brand->modals;

        #validate modals
        if($modals->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Modals not found on brand.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Modals Array
        $modalsArray = [];
        foreach($modals as $key => $modal) {
            $data = [
                'id'    => (string)$modal->id, 
                'name'  => (string)$modal->name ?? ''
            ];

            #Push to array
            array_push($modalsArray, $data);
        }

        #return brands
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Modals found.',
            'success'       => true,
            'data'          => $modalsArray
        ]);
    }

    /**
     * @method to fetch colors
     * @param 
     * @return json
     */
    public function fetchColors()
    {
        #Fetch all colors
        $colors = $this->color->active()->get();

        #validate colors
        if($colors->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'colors not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set colors Array
        $colorsArray = [];
        foreach($colors as $key => $color) {
            $data = [
                'id'    => (string)$color->id, 
                'name'  => $color->name ?? ''
            ];

            #Push to array
            array_push($colorsArray, $data);
        }

        #return brands
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Colors found.',
            'success'       => true,
            'data'          => $colorsArray
        ]);
    }

    /**
     * @method to save vehicle
     * @param Request $request
     * @return json
    */
    public function saveVehicle(Request $request)
    {
        #Fetch Data
        $vehicleType = $request->vehicle_type ?? '';
        $plateNumber = $request->plate_number ?? '';
        $brandId     = $request->brand_id ?? '';
        $brandName   = $request->brand_name ?? '';
        $modalId     = $request->modal_id ?? '';
        $modalName   = $request->modal_name ?? '';
        $colorId     = $request->color_id ?? '';
        $colorName   = $request->color_name ?? '';

        #Validate Data
        if($vehicleType == '' OR $plateNumber == '' OR $brandId == '' OR $brandName == '' OR 
            $modalId == '' OR $modalName == '' OR $colorId == '' OR $colorName == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'vehicle_type, plate_number, brand_id, brand_name, 
                                    modal_id, modal_name, color_id, color_name is required',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate Image
        if(!$request->hasFile('image')) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'image is required',
                'success'       => false,
                'data'          => null
            ]);
        }

        # Validate Image
        if($request->hasFile('image')) {
            #Fetch file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            $imageSize = (int)floor(filesize($request->file('image'))/1000);
            if($extension != 'jpg' AND $extension != 'jpeg' AND $extension != 'png') {
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'image does not have valid extension',
                    'success'       => false,
                    'data'          => null
                ]);
            }

            if($imageSize > 2048) {
                 return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'max limit of image should be 2 Mb',
                    'success'       => false,
                    'data'          => null
                ]);
            }
        }

        # set destination path.
        $databsePathForImage = '';
        if($request->hasFile('image')) {
            $destinationpath    = base_path() .'/public/images/vehicleImages';

            # Fetch photo
            $photo = $request->file('image');
            
            # get file name.
            $filename           = $photo->getClientOriginalName();

            # get today date.
            $today_date         = date('d-m-Y');

            # get a random number.
            $random_number      = rand(1111, 9999);

            # set filname with today date, random number, filename.
            $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

            # file move from current path to destination path.
            $movefilename       = $photo->move($destinationpath, $filenameData);

            # Set path for Database
            $databsePathForImage = 'images/vehicleImages/'.$filenameData;
        }

        #Set Name of Qr Code Image
        $qrCodeImageName = date('d-m-Y'). '_'.rand(1111, 9999).'_'.$brandName.'.png';

        #Set Data
        $data = [
            'user_id'           => Auth::user()->id,
            'plate_number'      => $plateNumber,
            'brand_id'          => $brandId,
            'brand_name'        => $brandName,
            'modal_id'          => $modalId,
            'modal_name'        => $modalName,
            'color_id'          => $colorId,
            'color_name'        => $colorName,
            'vehicle_type'      => $vehicleType,
            'image'             => $databsePathForImage,
            'qr_code_image'     => 'images/qrCodeImages/'.$qrCodeImageName,
        ];

        #Create userVehicle Modal
        $vehicle = $this->userVehicle->create($data);

        #Set json Data
        $data = [
            'id'            => $vehicle->id,
            'plate_number'  => $plateNumber,
            'brand_name'    => $brandName,
            'modal_name'    => $modalName,
            'color'         => $colorName,
        ];

        #set Data
        $data = json_encode($data);
        ##Qr Code String Data
        #$stringData = 'Plate Number : '.$plateNumber."\n";
        #$stringData .= ' Brand Name : '.$brandName."\n";
        #$stringData .= ' Modal Name : '.$modalName."\n";
        #$stringData .= ' Color : '.$colorName;
        
        #Upload Image for Qr Code
        \Storage::disk('public_uploads')->put($qrCodeImageName, base64_decode(DNS2D::getBarcodePNG($data,'QRCODE')));

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'User vehicle created',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to fetch vehicles of User
     * @param 
     * @return json
    */
    public function fetchVehicles()
    {
        #Fetch user
        $user = Auth::user();

        #Validate user
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'user not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch user with relations
        $user = $this->user
                     ->with(['vehicles.bookings.supervisors', 'vehicles.bookings.drivers'])
                     ->find($user->id);

        #Fetch User vehicles
        $vehicles = $user->vehicles->sortByDesc('id');

        #validate vehicles
        if($vehicles->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'vehicle not found',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Data
        $vehiclesArray = [];
        foreach ($vehicles as $key => $vehicle) {
            #FEtch booking on Intial Status on vehicle
            $booking = $vehicle->bookings->last();

            #ValidS Status(PARKED, GET_CAR_REQUEST, ON_THE_WAY_TO_PARK)
            $validStatus = [3,4,5,6,7,8];

            $supervisorGetBooking = collect();
            $driverGetBooking = collect();
            #Validate Booking 
            if($booking != '') {
                 #Fetch all Alloted Bookings
                $supervisorGetBooking = $booking->supervisors->where('set_or_get', 'get');

                #Fetch all Alloted Bookings to supervisors
                $driverGetBooking =$booking->drivers->where('set_or_get', 'get');
            }
            
            #validate $booking
            $bookingId = null;
            $parkingType = null;
            $hasValet = false;
            if($booking != '' AND ($supervisorGetBooking->isNotEmpty() OR $driverGetBooking->isNotEmpty())) {
                if($booking != '' AND $supervisorGetBooking->isNotEmpty()) {
                    if(in_array($supervisorGetBooking->last()->booking_status, $validStatus) AND 
                        $booking->booking_status != UserBookingStatusInterface::INITIAL) {
                        $bookingId = $booking->id;
                        $parkingType = $booking->parking_type;
                        $hasValet = $booking->is_valet_available;
                    }
                } elseif ($booking != '' AND  $driverGetBooking->isNotEmpty()) {
                    if(in_array( $driverGetBooking->last()->booking_status, $validStatus) AND 
                        $booking->booking_status != UserBookingStatusInterface::INITIAL) {
                        $bookingId = $booking->id;
                        $parkingType = $booking->parking_type;
                        $hasValet = $booking->is_valet_available;
                    }
                } 
            } else {
                if($booking != '' AND $booking->supervisors->isNotEmpty()) {
                    if(in_array($booking->supervisors->last()->booking_status, $validStatus) AND 
                        $booking->booking_status != UserBookingStatusInterface::INITIAL) {
                        $bookingId = $booking->id;
                        $parkingType = $booking->parking_type;
                        $hasValet = $booking->is_valet_available;
                    }
                } elseif ($booking != '' AND $booking->drivers->isNotEmpty()) {
                    if(in_array($booking->drivers->last()->booking_status, $validStatus) AND 
                        $booking->booking_status != UserBookingStatusInterface::INITIAL) {
                        $bookingId = $booking->id;
                        $parkingType = $booking->parking_type;
                        $hasValet = $booking->is_valet_available;
                    }
                } 
            }
        
            $data = [
                'id'                => (string)$vehicle->id ?? '',
                'booking_id'        => $bookingId,
                'parking_type'      => $parkingType,
                'has_valet'         => $hasValet,
                'plate_number'      => (string)$vehicle->plate_number ?? '',
                'vehicle_type'      => (string)$vehicle->vehicle_type ?? '',
                'brand_id'          => (string)$vehicle->brand_id ?? '',
                'brand_name'        => (string)$vehicle->brand_name ?? '',
                'modal_id'          => (string)$vehicle->modal_id ?? '',
                'modal_name'        => (string)$vehicle->modal_name ?? '',
                'color_id'          => (string)$vehicle->color_id ?? '',
                'color_name'        => (string)$vehicle->color_name ?? '',
                'color_code'        => (string)$vehicle->hex_code ?? '',
                'image'             => (string)$vehicle->image_full_path ?? '',
                'qr_code_image'     => (string)$vehicle->qr_code_image_full_path ?? '',
            ];  

            #Push data
            array_push($vehiclesArray, $data);
        }

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'vehicle found',
            'success'       => true,
            'data'          => $vehiclesArray
        ]);
    }
}