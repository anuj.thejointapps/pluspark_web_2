<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Log;
use Auth;
use Config;
use Validator;
use Carbon\Carbon;

# Models
use App\Models\User;
use App\Models\CompanyParking;

# Interface
use App\Interfaces\ApiStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class PromocodeController extends Controller implements ApiStatusInterface
{
    # Variable $user
    protected $user;

    # Variable $companyParking
    protected $companyParking;
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(
        User $user, CompanyParking $companyParking
       )
	{
		$this->user               = $user;
        $this->companyParking     = $companyParking;
	}

    /**
     * @method to  fetchPromocode
     * @return json
     * @param Request $request
     */
    public function fetchPromocode(Request $request)
    {
        #FEtch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch user
        $user = $this->user
                     ->with(['bookings'])
                     ->find($user->id);

        #Fetch parking_id
        $parkingId = $request->parking_id ?? '';

        #Validate parkingId
        if($parkingId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'parking_id is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch aprking
        $parking = $this->companyParking
                        ->with(['company.promocodes'])
                        ->find($parkingId);

        #Validate Parking
        if($parking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Parking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch promocodes on company
        $promoCodes = $parking->company->promocodes;

        #Validate Promocodes
        if($promoCodes->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Promocode not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #lopover promocodes
        $promocodeData = [];
        foreach ($promoCodes as $key => $promoCode) {
            if($promoCode->is_valid AND $promoCode->is_active) {
                 $alreadyUsedCount = $user->bookings
                                     ->where('promocode_id', $promoCode->id)
                                     ->count();

                #Set data
                $data = [
                    'id'            => $promoCode->id ?? '',
                    'code'          => $promoCode->code ?? '',
                    'discount_type' => $promoCode->offer_type ?? '',
                    'discount'      => $promoCode->discount ?? '',
                    'usable'        => ($promoCode->usage_allowed == $alreadyUsedCount) ? 'disabled' : 'active',
                    'till_date'     => $promoCode->valid_till_date_string,
                ];

                #push Data
                array_push($promocodeData, $data);
            }
        }

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Promocode found.',
            'success'       => true,
            'data'          => $promocodeData
        ]);
    }

    /**
     * @method to  fetchPromocode
     * @return json
     * @param Request $request
     */
    public function validatePromocode(Request $request)
    {
        #FEtch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch user
        $user = $this->user
                     ->with(['bookings'])
                     ->find($user->id);

        #Fetch data
        $parkingId  = $request->parking_id ?? '';
        $codePromo  = $request->promo_code ?? '';

        #Validate parkingId
        if($parkingId == '' OR $codePromo == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'parking_id and promo_code is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch aprking
        $parking = $this->companyParking
                        ->with(['company.promocodes'])
                        ->find($parkingId);

        #Validate Parking
        if($parking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Parking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Fetch promocodes on company
        $promoCodes = $parking->company->promocodes;

        #Validate Promocodes
        if($promoCodes->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Promocode not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Valid Promocodes
        $promoCodes = $promoCodes->filter(function($promoCode) use($user) {
            if($promoCode->is_valid AND $promoCode->is_active) {
                $alreadyUsedCount = $user->bookings
                                     ->where('promocode_id', $promoCode->id)
                                     ->count();
                if($promoCode->usage_allowed > $alreadyUsedCount) {
                    return $promoCode;
                }
            }
        });
       
        #Validate
        if(!in_array($codePromo, $promoCodes->pluck('code')->toArray())) {
            #return data
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Promocode not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Promocode whuch match
        $promoCode = $promoCodes->where('code', $codePromo)->last();
        
        #Set data
        $promocodeData = [
            'id'            => $promoCode->id ?? '',
            'code'          => $promoCode->code ?? '',
            'discount_type' => $promoCode->offer_type ?? '',
            'discount'      => $promoCode->discount ?? '',
            #'usable'        => ($promoCode->usage_allowed == $alreadyUsedCount) ? 'disabled' : 'active',
            'till_date'     => $promoCode->valid_till_date_string,
        ];

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Promocode found.',
            'success'       => true,
            'data'          => $promocodeData
        ]);
    }
}