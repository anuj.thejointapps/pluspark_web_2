<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Log;
use Auth;
use Config;
use Validator;
use Carbon\Carbon;
 use App\Utils\CommonUtil;

# Models
use App\Models\User;
use App\Models\Event;
use App\Models\CompanyParking;
use App\Models\UserSearchPlace;
use App\Models\ParkingSetupSpace;

# Interface
use App\Interfaces\ApiStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class EventController extends Controller implements ApiStatusInterface
{

    # Variable $user
    protected $user;

    # Variable $event
    protected $event;

    # Variable $companyParking
    protected $companyParking;

    # Variable $userSearchPlace
    protected $userSearchPlace;

    # Variable $parkingSpace
    protected $parkingSpace; 
    
    # Bind the util 
    protected $commonUtil;
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(
        User $user, Event $event, CompanyParking $companyParking,
        ParkingSetupSpace $parkingSpace, UserSearchPlace $userSearchPlace,
        CommonUtil $commonUtil)
	{
		$this->user               = $user;
        $this->event              = $event;
        $this->commonUtil         = $commonUtil;
        $this->parkingSpace       = $parkingSpace;
        $this->companyParking     = $companyParking;
        $this->userSearchPlace    = $userSearchPlace;
	}

    /**
     * @method to save search Place of User
     * @return json
     * @param Request $request
     */
    public function fetchEvents(Request $request)
    {
        #FEtch User
        $user = Auth::user();

        #validate User
        if($user == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'User not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set relations
        $relations = [
            'nearParkings.parking.parkingMarkOut',
            'nearParkings.parking.parkingSetupSpace.images',
            'nearParkings.parking.parkingDescription',
            'nearParkings.parking.parkingWasherFeatures',
        ];

        #Fetch all Events
        $events = $this->event->with($relations)->where('status', 1)->get();
        
        #Validate Events
        if($events->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No Event found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate Event
        $events = $events->filter(function($event) {
            $startDate = Carbon::parse($event->start_date);
            $endDate = Carbon::parse($event->end_date);
            if($endDate->gte(Carbon::now())) {
                return $event;
            }
        });

        #Validate Events
        if($events->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No Event found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Data
        $eventFullData = [];
        foreach ($events as $key => $event) {
            $eventData = [
                'id'            => $event->id ?? '',
                'name'          => $event->name ?? '',
                'location'      => $event->location ?? '',
                'latitude'      => $event->latitude ?? '',
                'longitude'     => $event->longitude ?? '',
                'start_time'    => $event->start_time_formatted ?? '',
                'end_time'      => $event->end_time_formatted ?? '',
            ];

            #Fetch Parking of Event
            $parkingsOnEvent = $event->nearParkings;

            $parkingDataOnEvent = [];
            if($parkingsOnEvent->isNotEmpty()) {
                foreach ($parkingsOnEvent as $key => $parkingOnEvent) {
                    #parking Data
                    $parking            = $parkingOnEvent->parking;
                    $parkingMarkout     = $parking->parkingMarkOut;
                    $parkingSpace       = $parking->parkingSetupSpace;
                    $parkingDescription = $parking->parkingDescription;

                    #Set Parking Data
                    $parkingData = [
                        'id'                        => $parking->id ?? '',
                        'name'                      => $parking->name ?? '',
                        'image'                     => $parking->image_full_path ?? '',
                        'before_you_go'             => $parkingMarkout ? $parkingMarkout->special_access_instructions : '',
                        'about_extended_time'       => $parkingMarkout ? $parkingMarkout->about_extended_time : '',
                        'location'                  => $parkingSpace ? $parkingSpace->location  :'',
                        'latitude'                  => $parkingSpace ? $parkingSpace->latitude  :'',
                        'longitude'                 => $parkingSpace ? $parkingSpace->longitude : '',
                        'about_parking'             => $parkingDescription ? $parkingDescription->description : '',
                        'amenities'                 => $parkingDescription ? $parkingDescription->all_amenities : '',
                        'book_price'                => $parkingMarkout ? $parkingMarkout->set_book_price : '0',
                        'valet_price'               => $parkingOnEvent ? $parkingMarkout->set_valet_price : '0',
                        'valet_and_wash_price'      => $parkingMarkout ? $parkingMarkout->set_valet_and_washing_price : '0',
                        'payment_methods_avail'     => $parkingMarkout ? $parkingMarkout->all_payment_methods : [],
                        'washing_cost'              => $parking->washing_cost,
                        'vehicle_types'             => $parking->vehicles_can_parked,
                    ];

                    #Push Data
                    array_push($parkingDataOnEvent, $parkingData);
                }
            }
            #dd($parkingDataOnEvent);
            #Push Data
            $eventData['parkings'] = $parkingDataOnEvent;
            array_push($eventFullData, $eventData);
        }
       
        #Paginate array
        $eventData = $this->commonUtil
                          ->paginate($eventFullData, 5, url()->current());

        #return json
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Event found.',
            'success'       => true,
            'data'          => $eventData
        ]);
    }
}