<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Log;
use Auth;
use Validator;
use Carbon\Carbon;

# Models
use App\Models\User;
use App\Models\UserOtp;
#use App\Models\NewRegisteredOtp;
#use App\Models\CustomerPersonalDetail;

# Interface
use App\Interfaces\ApiStatusInterface;

#Mail
use App\Mail\SendOtpMail;
use App\Mail\SendRegistartionWelcomeMail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller implements ApiStatusInterface
{
    # Variable $user
    protected $user;
    
    # Variable $userOtp
    protected $userOtp;

    # variable CustomerPersonal Detail
    protected $userDetail;

	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(User $user, UserOtp $userOtp)
	{
		$this->user       = $user;
        $this->userOtp    = $userOtp;
	}

	/**
	 * @method to register new User
	 * @param Request $request
	 * @return json
	 */
	public function register(Request $request)
	{
        # fetch Data from Request
        $option             = $request->get('option') ?? '';
        $data               = $request->all();

        # call the Method According to Option
        switch ($option) {
            case 'register':
                $response = $this->registerUser($data, $request);
                break;
            case 'social':
                $response = $this->socialLogin($data, $request);
                break;
            default:
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'option is required and should be valid.',
                    'success'    => false,
                    'data'       => null
                ];

                break;
        }

        return response()->json($response);
	}

    /**
     * @method to verify New email or Phone of User and Generate Otp
     * @param Request $request
     * @return Json
     */
    public function generateOtp(Request $request)
    {
        # Fetch email or Phone
        $emailOrPhone = $request->get('email_or_phone');
        $mobileCode = $request->get('mobile_code');
        $incomingPage = $request->get('incoming_page');

        #Validate required Field
        if($emailOrPhone == '' OR $incomingPage == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'   => 'email_or_phone and incoming_page is required.',
                'success'   => false,
                'data'      => null
            ]);
        }

        #Validate incoming Page
        if($incomingPage != 'signup' AND $incomingPage != 'forgotPassword' AND $incomingPage != 'login') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'   => 'incoming_page is not valid',
                'success'   => false,
                'data'      => null
            ]);
        }

        #Validate Email
        if(!is_numeric($emailOrPhone)) {
            if(!(filter_var($emailOrPhone, FILTER_VALIDATE_EMAIL))) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'email is not valid',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        #Validate mobile code
        if(is_numeric($emailOrPhone)) {
            if($mobileCode == '') {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'mobile_code is required',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        #if incoming page is forgot Password
        if($incomingPage == 'forgotPassword' OR $incomingPage == 'login') {
             #Fetch user on email
            if(!is_numeric($emailOrPhone)) {
                # Fetch User
                $user = $this->user
                             ->where('email', $emailOrPhone)
                             ->get();
            } else {
                # Fetch User
                $user = $this->user
                             ->where('mobile', $emailOrPhone)
                             ->where('mobile_code', $mobileCode)
                             ->get();
            }
            
            #Validate user
            if($user->isEmpty()) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'user not found with this phone number.',
                    'success'    => false,
                    'data'       => null
                ];

                return $response;
            }
        }

        #if incoming page is forgot Password
        if($incomingPage == 'signup') {
            #Fetch user on email
            if(!is_numeric($emailOrPhone)) {
                # Fetch User
                $user = $this->user
                             ->where('email', $emailOrPhone)
                             ->get();
                
                #Validate user
                if($user->isNotEmpty()) {
                    $response = [
                        'statusCode' => ApiStatusInterface::OK,
                        'message'    => 'Email is already taken. Please try with another email address.',
                        'success'    => false,
                        'data'       => null
                    ];

                    return $response;
                }
            } else {
                # Fetch User
                $user = $this->user
                             ->where('mobile', $emailOrPhone)
                             ->where('mobile_code', $mobileCode)
                             ->get();

                #Validate user
                if($user->isNotEmpty()) {
                    $response = [
                        'statusCode' => ApiStatusInterface::OK,
                        'message'    => 'mobile number is already taken. Please try with another mobile number.',
                        'success'    => false,
                        'data'       => null
                    ];

                    return $response;
                }
            }
        }
       
        if(!is_numeric($emailOrPhone)) {
            #Create Otp for User
            #$otp = mt_rand(1000, 9999);
            $otp = 1234;

            # Send Mail
            Mail::to($emailOrPhone)->send(new SendOtpMail($otp));
        
            // check for failures
            if (Mail::failures()) {
                return response()->json([
                    'statusCode'    => 401,
                    'message'   => 'No user found on this email id.'
                ]);
            }
        
            #Create User Otp
            $otpData = [
                'email_or_phone'    => $emailOrPhone,
                'otp'               => $otp
            ];

            #Create Otp for User
            $this->userOtp->create($otpData);

            #return response
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp sent on email.',
                'success'    => true,
                'data'       => ['email_or_phone' => $emailOrPhone, 'otp' => null]
            ];
        } else {
            #Create Otp for User
            $otp = mt_rand(1000, 9999);
            $otp = 1234;

            #Create User Otp
            $otpData = [
                'email_or_phone'    => $emailOrPhone,
                'mobile_code'       => $mobileCode,
                'otp'               => $otp
            ];

            #Create Otp for User
            $this->userOtp->create($otpData);

            #return response
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp sent on mobile.',
                'success'    => true,
                'data'       => ['email_or_phone' => $emailOrPhone,'otp' => $otp]
            ];
        }

        return response()->json($response);
    }

    /**
     * @method to verify otp of email or phone
     * @param Request $request
     * @return json
     */
    public function verifyOtp(Request $request)
    {
        # Fetch otp for new registered email or phone
        $otp          = $request->get('otp');
        $emailOrPhone = $request->get('email_or_phone');
        $mobileCode   = $request->get('mobile_code');
        $incomingPage = $request->get('incoming_page');

        #Validate incoming Page
        if($incomingPage != 'signup' AND $incomingPage != 'forgotPassword') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'   => 'incoming_page is not valid',
                'success'   => false,
                'data'      => null
            ]);
        }

        #Validate email or phone
        if($emailOrPhone == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'email_or_phone is required.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validate mobile code if mobile is here
        if(is_numeric($emailOrPhone)) {
            if($mobileCode == '') {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'mobile code is required.',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        if($otp == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'otp is required.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        $user = '';
        #if incoming page is forgot Password
        if($incomingPage == 'forgotPassword') {
            #Fetch user on email
            if(!is_numeric($emailOrPhone)) {
                # Fetch User
                $user = $this->user
                             ->where('email', $emailOrPhone)
                             ->get();
            } else {
                # Fetch User
                $user = $this->user
                             ->where('mobile', $emailOrPhone)
                             ->get();
            }
            
            #Validate user
            if($user->isEmpty()) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'user not found on email or phone.',
                    'success'    => false,
                    'data'       => null
                ];

                return $response;
            }
        }

        # Fetch otp related to email or phone
        if(!is_numeric($emailOrPhone)) {
            $userOtps = $this->userOtp
                         ->where('email_or_phone', $emailOrPhone)
                         ->get();
        } else {
            $userOtps = $this->userOtp
                         ->where('email_or_phone', $emailOrPhone)
                         ->where('mobile_code', $mobileCode)
                         ->get();
        }
       
        if($userOtps->isEmpty()) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'otp not found in system.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validate Otp
        if($userOtps->last()->otp != $otp) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp not matched.',
                'success'    => false,
                'data'       => ['email_or_phone' => $emailOrPhone]
            ];

            return $response;
        }

        # Verify Otp Validity
        if($userOtps->last()->created_at->diffInMinutes(Carbon::now()) > 20) {
            #return response
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp has been expired please resend.',
                'success'    => false,
                'data'       => ['email_or_phone' => $emailOrPhone]
            ];

            return $response;
        }

        #return response
        $response = [
            'statusCode'    => 200,
            'message'   => 'Otp has been verified.',
            'success'   => true,
            'data'      => [
                'id'                => $user != '' ? (string)$user->first()->id : null ,
                'username'          => $user != '' ? $user->first()->username : null,
                'email_or_phone'    => $emailOrPhone, 
            ]
        ];

        return $response;
    }

    /**
     * @method to update Password
     * @param Request $request
     * @return json
     */
    public function updatePassword(Request $request)
    {
        # fetch Data
        $userId             = $request->get('id') ?? '';
        $password           = $request->get('password') ?? '';
        //$confirmPassword    = $request->get('confirm_password') ?? '';

        #Fetch User
        $user = $this->user->find($userId);

        if($user == '') {
            #return response
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'user not found in system.',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Validate password Strength
        #$uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        if(!$lowercase || !$number || !$specialChars || strlen($password) < 8) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message' => 'Password should be at least 8 characters in length and should include at least alphabet one number, and one special character and 8 digit minimum.',
                'success'    => false,
                'data' => null
            ];

            #return Response
            return $response;
        }

        /*#Validate both passord
        if($password != $confirmPassword) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'Password and confirm_password not match',
                'success'   => false,
                'data'      => []
            ];

            #return Response
            return $response;
        }*/

        # Update Password
        $user->update(['password' =>  Hash::make($password)]);

        $response = [
            'statusCode'    => ApiStatusInterface::OK,
            'message'   => 'Password has been updated.',
            'success'   => true,
            'data'      => ['user_id' => $user->id]
        ];

        return $response;
    }
    
    /**
     * @method registerUser
     * @param $data
     * @return $response
     */
    public function registerUser($data, $request)
    {
        # Fetch data from Request
        $userName           = $data['user_name'] ?? '';
        $email              = $data['email'] ?? '';
        $mobileCode         = $data['mobile_code'] ?? '';
        $mobile             = $data['mobile'] ?? '';
        $password           = $data['password'] ?? '';
        //$confirmPassword    = $data['confirm_password'] ?? '';
        $name               = $data['name'] ?? '';
        $countryId          = 0;
        $countryName        = $data['country_name'] ?? '';
        $stateId            = 0;
        $stateName          = $data['state_name'] ?? '';
        $deviceId           = $data['device_id'] ?? '';
        $deviceType         = $data['device_type'] ?? '';
        $deviceToken        = $data['device_token'] ?? '';

        #Validate email
        if($email == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'email is required',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Validate Email
        if(!is_numeric($email)) {
            if(!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'email is not valid',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        #Validate mobile Code
        if($mobileCode == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'mobile_code is required',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Validate Phone
        if(is_numeric($mobile)) {
            if(strlen($mobile) < 9 OR strlen($mobile) > 14) {
                $response = [
                    'statusCode'    => ApiStatusInterface::OK,
                    'message'       => 'Mobile number should be atleast 9 or atmost 14.',
                    'success'       => false,
                    'data'          => null
                ];

                #return the Response
                return $response;
            }
        }

        # Validate name
        if(($name == '')) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'Name can not be empty and must be Alphabets.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        #Validate country
        if($countryName == '') {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'country_name is required.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        #Validate stateName and stateId
        if($stateName == '') {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'state_name is required.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        # Validate Image
        if($request->hasFile('image')) {
            #Fetch file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            $imageSize = (int)floor(filesize($request->file('image'))/1000);
            if($extension != 'jpg' AND $extension != 'jpeg' AND $extension != 'png') {
                $response = [
                    'statusCode'    => ApiStatusInterface::OK,
                    'message'   => 'image type should be .jpg, .png, .jpeg is required.',
                    'success'   => false,
                    'data'      => null
                ];

                #return Response
                return $response;
            }
        }

        # Validate name
        if($deviceType == '' OR ($deviceType != 1 AND $deviceType != 2)) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'invalid device_type.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        # Fetch User on Provided Email
        $user = $this->user
                     ->where('email', $email)
                     ->orWhere('mobile', $mobile)
                     ->get();

        # Validate User not exist on Email
        if($user->isNotEmpty()) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'       => 'User already Exist in System.',
                'success'       => false,
                'data'          => null
            ];

            #return Response
            return $response;
        }

        # set destination path.
        $databsePathForImage = '';
        if($request->hasFile('image')) {
            $destinationpath    = base_path() .'/public/images/userImages';

            # Fetch photo
            $photo = $request->file('image');
            
            # get file name.
            $filename           = $photo->getClientOriginalName();

            # get today date.
            $today_date         = date('d-m-Y');

            # get a random number.
            $random_number      = rand(1111, 9999);

            # set filname with today date, random number, filename.
            $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

            # file move from current path to destination path.
            $movefilename       = $photo->move($destinationpath, $filenameData);

            # Set path for Database
            $databsePathForImage = 'images/userImages/'.$filenameData;
        }

        #Set User Data
        $userData = [
            #'username'      => $userName, 
            'name'          => $name, 
            'email'         => $email, 
            'mobile_code'   => $mobileCode, 
            'mobile'        => $mobile, 
            'image'         => $databsePathForImage, 
            'account_type'  => 1, 
            'country_id'    => $countryId ?? 0, 
            'country_name'  => $countryName, 
            'state_id'      => $stateId ?? 0, 
            'state_name'    => $stateName, 
            'device_type'   => $deviceType ?? '',
            'device_token'  => $deviceToken ?? '',
        ];  

        DB::beginTransaction();
            #create the user
            $user = $this->user->create($userData);

            $user = Auth::loginUsingId($user->id);

            # Set the Toke for User
            $token =  $user->createToken('MyApp')->accessToken; 

            #Fetch user Data
            $userData = [
                'token'         => $token,
                'id'            => (string)$user->id ?? '',
                #'username'      => (string)$user->username ?? '',
                'name'          => (string)$user->name ?? '',
                'email'         => (string)$user->email ?? '',
                'mobile_code'   => (string)$user->mobile_code ?? '',
                'mobile'        => (string)$user->mobile ?? '',
                'image'         => (string)$user->image_full_path ?? '',
                'country_name'  => (string)$user->country_name ?? '',
                'state_name'    => (string)$user->state_name ?? '',
                'device_type'   => (string)$user->device_type ?? '',
                'device_token'  => (string)$user->device_token ?? '',
            ];

            # Set Response
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'       => 'Registration successful.',
                'success'       => true,
                'data'          => $userData
            ];
        DB::commit();
        
        return $response;
    }

    /**
     * @method socialLogin 
     * @param $data
     * @return $response
     */
    public function socialLogin($data, $request)
    {
        # Fetch Data
        $userName       = $data['user_name'] ?? '';
        $email          = $data['email'] ?? '';
        $facebookId     = $data['facebook_id'] ?? '';
        $googleId       = $data['google_id'] ?? '';
        $twitterId      = $data['twitter_id'] ?? '';
        $appleId        = $data['apple_id'] ?? '';
        $mobileCode     = $data['mobile_code'] ?? '';
        $mobile         = $data['mobile'] ?? '';
        $countryName    = $data['country_name'] ?? '';
        $stateName      = $data['state_name'] ?? '';
        $deviceType     = $data['device_type'] ?? '';
        $deviceToken    = $data['device_token'] ?? '';
        $imageUrl       = $data['image_url'] ?? '';

        # Check User Already Exist 
        if(($facebookId == '') AND ($googleId == '') AND ($twitterId == '') AND ($appleId == '')){
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'facebook_id, google_id, apple_id or twitter_id is required',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Validate Email
        if(!is_numeric($email)) {
            if(!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'email is not valid',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        #Validate mobile Code
        if($mobileCode == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'mobile_code is required',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Validate Phone
        if(is_numeric($mobile)) {
            if(strlen($mobile) < 9 OR strlen($mobile) > 14) {
                $response = [
                    'statusCode'    => ApiStatusInterface::OK,
                    'message'       => 'Mobile number should be atleast 9 or atmost 14.',
                    'success'       => false,
                    'data'          => null
                ];

                #return the Response
                return $response;
            }
        }

        #Validate country
        if($countryName == '') {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'country_name is required.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        #Validate stateName and stateId
        if($stateName == '') {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'state_name is required.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        # Validate name
        if($deviceType == '' OR ($deviceType != 1 AND $deviceType != 2)) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'invalid device_type.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        # Validate Image and image Url
        if($request->has('image') AND $imageUrl != '') {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'either image file or image url needed.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        $registerType = 0;
        if($facebookId != '') {

            #Set register Type to facebook
            $registerType = 2;

            $response = $this->registerSocialAccount($data, $registerType, $request);
            return $response;
        } elseif ($googleId != '') {
            #Set register Type to google
            $registerType = 3;

            $response = $this->registerSocialAccount($data, $registerType, $request);

            #return Response
            return $response;
        } elseif($twitterId != '') {
            #Set register Type to twitter
            $registerType = 4;

            $response = $this->registerSocialAccount($data, $registerType, $request);

            #return Response
            return $response;
        } else {
            #Set register Type to twitter
            $registerType = 5;

            $response = $this->registerSocialAccount($data, $registerType, $request);

            #return Response
            return $response;
        }
    }

    /**
     * @method registerSocialAccount
     * @param $data, $registerType
     * @return Response
     */
    public function registerSocialAccount($data, $registerType, $request)
    {
       # Fetch Data
        $userName       = $data['user_name'] ?? '';
        $email          = $data['email'] ?? '';
        $name           = $data['name'] ?? '';
        $facebookId     = $data['facebook_id'] ?? '';
        $googleId       = $data['google_id'] ?? '';
        $twitterId      = $data['twitter_id'] ?? '';
        $appleId        = $data['apple_id'] ?? '';
        $mobileCode     = $data['mobile_code'] ?? '';
        $mobile         = $data['mobile'] ?? '';
        $countryName    = $data['country_name'] ?? '';
        $stateName      = $data['state_name'] ?? '';
        $deviceType     = $data['device_type'] ?? '';
        $deviceToken    = $data['device_token'] ?? '';
        $imageUrl       = $data['image_url'] ?? '';

        # set destination path.
        $databsePathForImage = '';
        if($request->hasFile('image')) {
            $destinationpath    = base_path() .'/public/images/userImages';

            # Fetch photo
            $photo = $request->file('image');
            
            # get file name.
            $filename           = $photo->getClientOriginalName();

            # get today date.
            $today_date         = date('d-m-Y');

            # get a random number.
            $random_number      = rand(1111, 9999);

            # set filname with today date, random number, filename.
            $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

            # file move from current path to destination path.
            $movefilename       = $photo->move($destinationpath, $filenameData);

            # Set path for Database
            $databsePathForImage = 'images/userImages/'.$filenameData;
        }

        #Set User Data
        $userData = [
            #'username'      => $userName, 
            'name'          => '', 
            'email'         => $email, 
            'name'          => $name, 
            'mobile_code'   => $mobileCode, 
            'mobile'        => $mobile, 
            'account_type'  => $registerType, 
            'country_name'  => $countryName, 
            'state_name'    => $stateName, 
            'facebook_id'   => $facebookId,
            'google_id'     => $googleId,
            'twitter_id'    => $twitterId,
            'apple_id'      => $appleId,
            'image'         => $databsePathForImage,
            'image_url'     => $imageUrl,
            'device_type'   => $deviceType ?? '',
            'device_token'  => $deviceToken ?? '',
        ];  

        DB::beginTransaction();
            if($facebookId != '') {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('mobile', $mobile)
                             ->orWhere('facebook_id', $facebookId)
                             ->get();
            } elseif($googleId != '') {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('mobile', $mobile)
                             ->orWhere('google_id', $googleId)
                             ->get();
            } elseif($twitterId != '') {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('mobile', $mobile)
                             ->orWhere('twitter_id', $twitterId)
                             ->get();
            } else {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('mobile', $mobile)
                             ->orWhere('apple_id', $appleId)
                             ->get();
            }

            if($user->isNotEmpty()) {
                $user->first()->update($userData);
                $user = $this->user->find($user->first()->id);
            } else {
                $user = $this->user->create($userData);
            }

            #Delete all tokens before
            #DB::table('oauth_access_tokens')->where('user_id', $user->id)->delete();

            $user = Auth::loginUsingId($user->id);

            # Set the Toke for User
            $token =  $user->createToken('MyApp')->accessToken; 
            
            #Fetch user Data
            $userData = [
                'token'         => $token,
                'id'            => (string)$user->id ?? '',
                #'username'      => (string)$user->username ?? '',
                'name'          => (string)$user->name ?? '',
                'email'         => (string)$user->email ?? '',
                'mobile_code'   => (string)$user->mobile_code ?? '',
                'mobile'        => (string)$user->mobile ?? '',
                'image'         => (string)$user->image_full_path ?? '',
                'image_url'     => (string)$user->image_url ?? '',
                'country_name'  => (string)$user->country_name ?? '',
                'state_name'    => (string)$user->state_name ?? '',
                'device_type'   => (string)$user->device_type ?? '',
                'device_token'  => (string)$user->device_token ?? '',
            ];
            
        DB::commit();

        $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'Login Successfully.',
                'success'   => true,
                'data'      => $userData
        ];

        return $response;
    }
    
    /**
     * @method login
     * @param $data
     * @return json
     */
    public function login(Request $request)
    {
        # Fetch Data
        $otp            = $request->get('otp') ?? '';
        $mobile         = $request->get('mobile') ?? '';
        $mobileCode     = $request->get('mobile_code') ?? '';
        $deviceType     = $request->get('device_type') ?? '';
        $deviceToken    = $request->get('device_token') ?? '';

        #Validate Mobile Code
        if($mobileCode == '' OR $mobile == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'mobile and mobile code is required.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validate otp
        if($otp == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'otp is required.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        # Ftech user
        $user = $this->user
                     ->where('mobile_code', $mobileCode)
                     ->where('mobile', $mobile)
                     ->get();

        if($user->isEmpty()) {
            # Set Response
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'User Not Found.',
                'success'   => false,
                'data'      => null
            ];

            return $response;
        }

        #Validate deviceType and token
        if($deviceType == '' OR ($deviceType != 1 AND $deviceType != 2)) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'invalid device_type.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        #Validate devicetoken
        if($deviceToken == '') {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'device_token is required.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        #Fetch uer Otps
        $userOtps = $this->userOtp
                         ->where('email_or_phone', $mobile)
                         ->where('mobile_code', $mobileCode)
                         ->get();

        if($userOtps->isEmpty()) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'otp not found in system.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validate Otp
        if($userOtps->last()->otp != $otp) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp not matched.',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Verify Otp Validity
        if($userOtps->last()->created_at->diffInMinutes(Carbon::now()) > 20) {
            #return response
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp has been expired please resend.',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Fetch 
        $user = $user->first();

        # Set Authenticated User
        $user = Auth::loginUsingId($user->id); 

        # Set user type and token
        $userData = ['device_type' => $deviceType, 'device_token' => $deviceToken];

        # Update User
        $user->update($userData);

        # Fetch User
        $user = $this->user->find($user->id);

        #Delete all tokens before
        #DB::table('oauth_access_tokens')->where('user_id', $user->id)->delete();

        # Set the Toke for User
        $token =  $user->createToken('MyApp')->accessToken; 

        #Fetch user Data
        $userData = [
            'token'         => $token,
            'id'            => (string)$user->id ?? '',
            #'username'      => (string)$user->username ?? '',
            'name'          => (string)$user->name ?? '',
            'email'         => (string)$user->email ?? '',
            'mobile_code'   => (string)$user->mobile_code ?? '',
            'mobile'        => (string)$user->mobile ?? '',
            'image'         => (string)$user->image_full_path ?? '',
            'country_name'  => (string)$user->country_name ?? '',
            'state_name'    => (string)$user->state_name ?? '',
            'device_type'   => (string)$user->device_type ?? '',
            'device_token'  => (string)$user->device_token ?? '',
        ];

       $response = [
            'statusCode'    => ApiStatusInterface::OK,
            'message'   => 'Login Successfully.',
            'success'   => true,
            'data'      => $userData
        ];

        return $response;
    }

    /**
     * @method to fetch if user already present on social id or email
     * @param Request $request
     * @return json
     */
    public function availability(Request $request)
    {
        # Fetch Data
        $email          = $request->get('email');
        $facebookId     = $request->get('facebook_id');
        $googleId       = $request->get('google_id') ?? '';
        $twitterId      = $request->get('twitter_id') ?? '';
        $appleId        = $request->get('apple_id') ?? '';

        #Validate social Id
        if($appleId == '' AND $facebookId == '' AND $googleId == '' AND $twitterId == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'facebook_id, google_id, apple_id or twitter_id is required',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Validate Email
        if($email != '') {
            if(!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'email is not valid',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        #Fetch User
        if($facebookId != '') {
            if($email != '') {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('facebook_id', $facebookId)
                             ->get();
            } else {
                $user = $this->user
                             ->where('facebook_id', $facebookId)
                             ->get();
            }
        } elseif($googleId != '') {
             if($email != '') {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('google_id', $googleId)
                             ->get();
            } else {
                $user = $this->user
                             ->where('google_id', $googleId)
                             ->get();
            }
        } elseif($twitterId != '') {
             if($email != '') {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('twitter_id', $twitterId)
                             ->get();
            } else {
                $user = $this->user
                             ->where('twitter_id', $twitterId)
                             ->get();
            }
        } elseif($appleId != '') {
             if($email != '') {
                $user = $this->user
                             ->where('email', $email)
                             ->orWhere('apple_id', $appleId)
                             ->get();
            } else {
                $user = $this->user
                             ->where('apple_id', $appleId)
                             ->get();
            }
        }

        #Validate user
        if($user->isNotEmpty()) {
            $user = $user->first();

            #Set user Data and Update on Modal
            if($facebookId != '') {
                $user->update(['facebook_id' => $facebookId]);
            } elseif ($twitterId != '') {
                $user->update(['twitter_id' => $twitterId]);
            } elseif ($googleId != '') {
                $user->update(['google_id' => $googleId]);
            } elseif ($appleId != '') {
               $user->update(['apple_id' => $appleId]);
            }

            # Authenticate the User
            $user = Auth::loginUsingId($user->id);

            # Set the Toke for User
            $token =  $user->createToken('MyApp')->accessToken;

            #Fetch user Data
            $userData = [
                'token'         => $token,
                'id'            => (string)$user->id ?? '',
                'username'      => (string)$user->username ?? '',
                'name'          => (string)$user->name ?? '',
                'email'         => (string)$user->email ?? '',
                'mobile_code'   => (string)$user->mobile_code ?? '',
                'mobile'        => (string)$user->mobile ?? '',
                'image'         => (string)$user->image_full_path ?? '',
                'country_name'  => (string)$user->country_name ?? '',
                'state_name'    => (string)$user->state_name ?? '',
                'device_type'   => (string)$user->device_type ?? '',
                'device_token'  => (string)$user->device_token ?? '',
            ];

            $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'user already available',
                    'success'    => true,
                    'data'       => $userData
                ];

                #return the Response
                return $response;
        } else {
            $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'user not available',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
        }
    }

    /**
     * @method to check user Availability on mobile or email
     * @param Request $request
     * @return json
     */
    public function checkUserAvailabilityOnEmailOrPhone(Request $request)
    {
        # Fetch Data
        $emailOrPhone   = $request->get('email_or_phone');
        $mobileCode     = $request->get('mobile_code') ?? '';

        #validsate emailOrPhone
        if($emailOrPhone == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'email_or_phone is required',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        #validate mobileCode if emailOrPhone in Phone
        if(is_numeric($emailOrPhone)) {
            if($mobileCode == '') {
                $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'mobile_code is required',
                'success'    => false,
                'data'       => null
            ];

            return $response;
            }
        }

        #validate if email
        if(!is_numeric($emailOrPhone)) {
            if(!(filter_var($emailOrPhone, FILTER_VALIDATE_EMAIL))) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'email is not valid',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        #fetch User based on email or Phone
        if(!is_numeric($emailOrPhone)) {
            $user = $this->user
                         ->where('email', $emailOrPhone)
                         ->get();
        } else {
            $user = $this->user
                         ->where('mobile', $emailOrPhone)
                         ->where('mobile_code', $mobileCode)
                         ->get();
        }

        #Validate User
        if($user->isEmpty()) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'User not Found on provided'. (!is_numeric($emailOrPhone) ? ' email' : ' phone'),
                'success'    => true,
                'data'       => null
            ];

            #return the Response
            return $response;
        } else {
             $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'This '. (!is_numeric($emailOrPhone) ? 'email' : 'phone'). ' is already taken by other User. Please try with other.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }
        dd($user);
    }

    /**
     * @method to generate unique uswrame every time
     * @return string
     * @param
     */
    public function generateUserName($name)
    {
        #Fetch User
        $users = $this->user->get();

        #Fetch last userId
        $lastUserId = $users->isNotEmpty() ? $users->count() + 1 : 1;

        #make unique userid
        $name = $name[rand(0, strlen($name)-1)];

        #make unique userid
        $uniqueId = uniqid($name.$lastUserId);
        
        #return uniqueId
        return $uniqueId;
    }
}