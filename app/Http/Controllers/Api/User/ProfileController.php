<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Log;
use Auth;
use Validator;
use Carbon\Carbon;

# Models
use App\Models\User;

# Interface
use App\Interfaces\ApiStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ProfileController extends Controller implements ApiStatusInterface
{
    # Variable $user
    protected $user;
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(User $user)
	{
		$this->user       = $user;
	}

	/**
	 * @method to update Profile
	 * @param Request $request
	 * @return json
	 */
	public function updateProfile(Request $request)
	{
        # fetch User
        $user = Auth::user();

        #Validate User
        if($user == '') { 
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'user not found',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Fetch Data
        $name           = $request->name ?? '';
        #$email          = $request->email ?? '';
        #$mobileCode     = $request->mobile_code ?? '';
        #$mobile         = $request->mobile_number ?? '';
        $countryName    = $request->country_name ?? '';
        $stateName      = $request->state_name ?? '';

        /*#Validate data
        # Validate Email
        if($email  != '') {
            if(!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
                $response = [
                    'statusCode' => ApiStatusInterface::OK,
                    'message'    => 'email is not valid',
                    'success'    => false,
                    'data'       => null
                ];

                #return the Response
                return $response;
            }
        }

        #Validate Mobile
        # Validate Phone
        if($mobile != '') {
            if(strlen($mobile) != 10) {
                $response = [
                    'statusCode'    => ApiStatusInterface::OK,
                    'message'       => 'Mobile number is not valid.',
                    'success'       => false,
                    'data'          => null
                ];

                #return the Response
                return $response;
            }
        }*/

        /*#Verify if any other user already on email registeres
        $userOnEmail = $this->user
                            ->where('id', '<>', $user->id)
                            ->where('email', $email)
                            ->get();

        #Validate if other user already regsitered on this email
        if($userOnEmail->isNotEmpty()) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'       => 'Sorry, any other user registered on this email.',
                'success'       => false,
                'data'          => null
            ];

            #return the Response
            return $response;
        }

        #Verify if any other user already on mobile registeres
        $userOnMobile = $this->user
                            ->where('id', '<>', $user->id)
                            ->where('mobile', $mobile)
                            ->get();

        #Validate if other user already regsitered on this email
        if($userOnMobile->isNotEmpty()) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'       => 'Sorry, any other user registered on this mobile.',
                'success'       => false,
                'data'          => null
            ];

            #return the Response
            return $response;
        }*/

         # Validate Image
        if($request->hasFile('image')) {
            #Fetch file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            $imageSize = (int)floor(filesize($request->file('image'))/1000);
            if($extension != 'jpg' AND $extension != 'jpeg' AND $extension != 'png') {
                $response = [
                    'statusCode'    => ApiStatusInterface::OK,
                    'message'   => 'image type should be .jpg, .png, .jpeg is required.',
                    'success'   => false,
                    'data'      => null
                ];

                #return Response
                return $response;
            }
        }

        # set destination path.
        $databsePathForImage = '';
        if($request->hasFile('image')) {
            $destinationpath    = base_path() .'/public/images/userImages';

            # Fetch photo
            $photo = $request->file('image');
            
            # get file name.
            $filename           = $photo->getClientOriginalName();

            # get today date.
            $today_date         = date('d-m-Y');

            # get a random number.
            $random_number      = rand(1111, 9999);

            # set filname with today date, random number, filename.
            $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

            # file move from current path to destination path.
            $movefilename       = $photo->move($destinationpath, $filenameData);

            # Set path for Database
            $databsePathForImage = 'images/userImages/'.$filenameData;
        }

        #Set User Data
        $userData = [
            'name'          => $name, 
            #'email'         => $email, 
            #'mobile_code'   => $mobileCode, 
            #'mobile'        => $mobile, 
            'image'         => $databsePathForImage != '' ? $databsePathForImage : $user->image, 
            'country_name'  => $countryName, 
            'state_name'    => $stateName, 
        ];  

        DB::beginTransaction();
            #create the user
            $user->update($userData);
            $user = $this->user->find($user->id);

            #Fetch user Data
            $userData = [
                'id'            => (string)$user->id ?? '',
                'username'      => (string)$user->username ?? '',
                'name'          => (string)$user->name ?? '',
                'email'         => (string)$user->email ?? '',
                'mobile_code'   => (string)$user->mobile_code ?? '',
                'mobile'        => (string)$user->mobile ?? '',
                'image'         => (string)$user->image_full_path ?? '',
                'country_name'  => (string)$user->country_name ?? '',
                'state_name'    => (string)$user->state_name ?? '',
                'device_type'   => (string)$user->device_type ?? '',
                'device_token'  => (string)$user->device_token ?? '',
            ];

            # Set Response
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'Profile updated successfully.',
                'success'   => true,
                'data'      => $userData
            ];
        DB::commit();
        
        return $response;
	}

    /**
     * @method to change password
     * @param Request $request
     * @return json
     */
    public function changePassword(Request $request)
    {
        # fetch User
        $user = Auth::user();

        #Validate User
        if($user == '') { 
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'user not found',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Fetch Data
        $oldPassword    = $request->old_password ?? '';
        $newPassword    = $request->new_password ?? '';

        #validate passwords
        if($oldPassword == '' OR $newPassword == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'old password and new passwords are required',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validate User old Password
        if (!Hash::check($oldPassword, $user->password)) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'old password not matched.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validation for Password
        $lowercase = preg_match('@[a-z]@', $newPassword);
        $number    = preg_match('@[0-9]@', $newPassword);
        $specialChars = preg_match('@[^\w]@', $newPassword);

        if(!$lowercase || !$number || !$specialChars || strlen($newPassword) < 8) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message' => 'New Password should be at least 8 characters in length and should include at least alphabet one number, and one special character and 8 digit minimum.',
                'success'    => false,
                'data' => null
            ];

            #return Response
            return $response;
        }

        #Update User Password
        $user->update(['password' => Hash::make($newPassword)]);

        #return response
        $response = [
            'statusCode'    => ApiStatusInterface::OK,
            'message'   => 'Password Updated Successfully.',
            'success'   => true,
            'data'      => null
        ];

        #return Response
        return $response;
    }
}