<?php

namespace App\Http\Controllers\Api\WSD\Washer;

use DB;
use Log;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;

# Models
use App\Models\Booking;
use App\Models\Washerman;
use App\Models\UserVehicle;
use App\Models\WasherBooking;
use App\Models\SupervisorBooking;
use App\Models\ScanOrPrivateBooking;

# Interface
use App\Interfaces\ApiStatusInterface;
use App\Interfaces\WasherBookingStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller implements ApiStatusInterface, WasherBookingStatusInterface
{
    # Bind api token
    private $apiToken;

    # Variable $wsd(washer, supervisor or Driver)
    protected $wsd;

    # Variable $booking
    protected $booking;

    # Variable $userVehicle
    protected $userVehicle;

    # Variable $washerBooking
    protected $washerBooking;

    # Variable $supervisorBooking
    protected $supervisorBooking, $scanPrivateBooking;
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(Washerman $wsd, Booking $booking, UserVehicle $userVehicle, 
        WasherBooking $washerBooking, SupervisorBooking $supervisorBooking, ScanOrPrivateBooking $scanPrivateBooking)
	{
		$this->wsd                  = $wsd;
        $this->booking              = $booking;
        $this->userVehicle          = $userVehicle;
        $this->washerBooking        = $washerBooking;
        $this->supervisorBooking    = $supervisorBooking;
        $this->scanPrivateBooking   = $scanPrivateBooking;
        $this->apiToken             = uniqid(base64_encode(\Str::random(128)));
	}

    /**
     * @method to fetch set bookings of Supervisor
     * @return json
     * @param Request $request
     */
    public function fetchSetBookings(Request $request)
    {
        #Fetch Authenticated User
        $user = Session::get('wsdUser');

        #Validate User
        if($user == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Supervisor not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch washer
        $washer = $this->wsd
                           ->with(['washermanCompanies.company.parkings'])
                           ->washer()
                           ->find($user->id);

        #Validate washer
        if($washer == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Fetch all parkings 
        $washerParkings = $washer->washermanCompanies
                                 ->pluck('company.parkings')
                                 ->flatten()
                                 ->pluck('id')
                                 ->toArray();
      
        #Fetch bookings which are free and avail for washing
        $qrBookings = $this->booking
                            ->washingAvail()
                            ->with(['parking', 'vehicle', 'user', 'supervisor', 'driver', 'washer'])   
                            ->whereIn('parking_id', $washerParkings)
                            ->get();

        #Fetch Scan Private Bookings
        $scanOrPrivateBookings = $this->scanPrivateBooking 
                                      ->washingAvail()
                                      ->with(['parking', 'vehicle', 'user', 'supervisor', 'driver', 'washer'])   
                                      ->whereIn('parking_id', $washerParkings)
                                      ->get();
     
        #validate bookings
        if($qrBookings->isEmpty() AND $scanOrPrivateBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Bookings not found to set.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Qr bookings on which washer already assigned
        $qrBookingIdsWasherAssigned = $this->washerBooking
                                           ->qrBookings()
                                           ->whereIn('booking_id', $qrBookings->pluck('id')->toArray())
                                           ->get()
                                           ->pluck('booking_id')
                                           ->toArray();

        #Fetch scan orPrivate bookings on which washer already assigned
        $scanPrivateBookingIdsWasherAssigned = $this->washerBooking
                                                    ->nonQrBookings()
                                                    ->whereIn('booking_id', $scanOrPrivateBookings->pluck('id')->toArray())
                                                    ->get()
                                                    ->pluck('booking_id')
                                                    ->toArray();
      
        #validate Qr bookings which are not on any washera and supervisor allot those
        $validQrBookings = $qrBookings->filter(function($booking) use($qrBookingIdsWasherAssigned) {
            if(!in_array($booking->id, $qrBookingIdsWasherAssigned) AND 
                ($booking->supervisor != '' OR $booking->driver != '') AND 
                ( ($booking->supervisor != '' AND $booking->supervisor->fetch_booking_status == 'PARKED') OR ($booking->driver AND $booking->driver->fetch_booking_status == 'PARKED') )) {
                return $booking;
            }
        })->sortByDesc('id');

        #validate Qr bookings which are not on any washera and supervisor allot those
        $validNABookings = $qrBookings->filter(function($booking) use($qrBookingIdsWasherAssigned, $validQrBookings) {
            if(!in_array($booking->id, $qrBookingIdsWasherAssigned) AND 
                ( $booking->is_parked ) ) {
                return $booking;
            }
        })->sortByDesc('id');

        #validate Qr bookings which are not on any washera and supervisor allot those
        $validScanPrivateBookings = $scanOrPrivateBookings->filter(function($booking) use($scanPrivateBookingIdsWasherAssigned) {
            if(!in_array($booking->id, $scanPrivateBookingIdsWasherAssigned) AND 
                ( $booking->is_parked ) ) {
                return $booking;
            }
        })->sortByDesc('id');
        
        #validate bookings
        if($validQrBookings->isEmpty() AND $validScanPrivateBookings->isEmpty() AND $validNABookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Bookings not found to set.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #set Data for bookings
        $bookingsSetData = [];
        foreach ($validQrBookings as $key => $booking) {
            #Fetch Supervisor and Driver Set
            $supervisorInSet = $booking->supervisors->isNotEmpty() ?  $booking->supervisors->where('set_or_get', 'set')->first() : '';
            $driverInSet =  $booking->drivers->isNotEmpty() ? $booking->drivers->where('set_or_get', 'set')->first() : '';
           
            $data = [
                'booking_id'            => $booking->id ?? '',
                'booking_type'          => 'qr_code',
                'wash_status'           => 'WASH PENDING',
                'wash_type'             => $booking->fetch_wash_type,
                'key_no'                => $booking->supervisor != '' ? 
                                            $booking->supervisor->key_no : $booking->driver->key_no,
                'park_no'               => $booking->supervisor != '' ? 
                                            $booking->supervisor->park_no : $booking->driver->park_no ,
                'parking_id'            => $booking->parking->id ?? '',
                'parking_name'          => $booking->parking->name ?? '',
                'parking_image'         => $booking->parking->image_full_path ?? '',
                'vehicle_id'            => $booking->vehicle->id ?? '',
                'vehicle_name'          => $booking->vehicle->full_name ?? '',
                'vehicle_color'         => $booking->vehicle->color_name ?? '',
                'vehicle_color_code'    => $booking->vehicle->hex_code ?? '',
                'vehicle_plate_no'      => $booking->vehicle->plate_number ?? '',
                'vehicle_image'         => $booking->vehicle->image_full_path ?? '',
                'arrived_time'          => $booking->arrived_formatted_time ?? '',
                'arrived_timestamp'     => $booking->arrived_timestamp ?? '',
                'parked_time'           => $supervisorInSet != '' ? $supervisorInSet->parked_date_time_formatted : $driverInSet->parked_date_time_formatted,
            ];

            #push Araryt
            array_push($bookingsSetData, $data);
        }

         #set Data for scan bookings
        foreach ($validNABookings as $key => $booking) {
            if(!in_array($booking->id, ($validQrBookings->isNotEmpty() ? $validQrBookings->pluck('id')->toArray() : []))) {
                 $data = [
                'booking_id'            => $booking->id ?? '',
                'booking_type'          => $booking->parking_type,
                'wash_status'           => 'WASH PENDING',
                'wash_type'             => $booking->fetch_wash_type,
                'key_no'                => $booking->key_no, 
                'park_no'               => $booking->park_no, 
                'parking_id'            => $booking->parking->id ?? '',
                'parking_name'          => $booking->parking->name ?? '',
                'parking_image'         => $booking->parking->image_full_path ?? '',
                'vehicle_id'            => $booking->vehicle->id ?? '',
                'vehicle_name'          => $booking->vehicle->full_name ?? '',
                'vehicle_color'         => $booking->vehicle->color_name ?? '',
                'vehicle_color_code'    => $booking->vehicle->hex_code ?? '',
                'vehicle_plate_no'      => $booking->vehicle->plate_number ?? '',
                'vehicle_image'         => $booking->vehicle->image_full_path ?? '',
                'arrived_time'          => $booking->arrived_formatted_time ?? '',
                'arrived_timestamp'     => $booking->arrived_timestamp ?? '',
                'parked_time'           => $booking->parked_date_time_formatted,
                ];

                #push Araryt
                array_push($bookingsSetData, $data);
            }
           
        }

        #set Data for scan bookings
        foreach ($validScanPrivateBookings as $key => $booking) {
            $data = [
                'booking_id'            => $booking->id ?? '',
                'booking_type'          => $booking->parking_type,
                'wash_status'           => 'WASH PENDING',
                'wash_type'             => $booking->fetch_wash_type,
                'key_no'                => $booking->key_no, 
                'park_no'               => $booking->parking_no, 
                'parking_id'            => $booking->parking->id ?? '',
                'parking_name'          => $booking->parking->name ?? '',
                'parking_image'         => $booking->parking->image_full_path ?? '',
                'vehicle_id'            => $booking->vehicle->id ?? '',
                'vehicle_name'          => $booking->vehicle->full_name ?? '',
                'vehicle_color'         => $booking->vehicle->color_name ?? '',
                'vehicle_color_code'    => $booking->vehicle->hex_code ?? '',
                'vehicle_plate_no'      => $booking->vehicle->plate_number ?? '',
                'vehicle_image'         => $booking->vehicle->image_full_path ?? '',
                'arrived_time'          => $booking->arrived_formatted_time ?? '',
                'arrived_timestamp'     => $booking->arrived_timestamp ?? '',
                'parked_time'           => $booking->parked_date_time_formatted,
            ];

            #push Araryt
            array_push($bookingsSetData, $data);
        }

        #return data
        return response()->json([
           'statusCode'    =>  ApiStatusInterface::OK, 
           'message'       => 'Pending Bookings found.',
           'success'       => true,
           'data'          => $bookingsSetData
        ]);
    }

    /**
     * @method to book booking on supervisor
     * @return json
     * @param Request $request
     */
    public function booking(Request $request)
    {
        #Fetch Authenticated User
        $user = Session::get('wsdUser');
        $bookingType = strtolower($request->booking_type) ?? '';

        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'washing_start_date'    => 'required|date_format:Y-m-d H:i:s',
            #'washing_end_date'      => 'required|date_format:Y-m-d H:i:s',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washing_start_date format should be Y-m-d H:i:s',
                'success'       => false,
                'data'          => null
            ]);
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        #Validate User
        if($user == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Supervisor or booking_type not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate BookingTpe
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid(qrcode, scan_me, private).',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Supervisor
        $washer = $this->wsd
                       #->with(['company.companyParking'])
                       ->washer()
                       ->find($user->id);

        #Validate washer
        if($washer == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch Data
        $parkNumber = $request->park_number ?? '';
        $bookingId  = $request->booking_id ?? '';
        $keyNo      = $request->key_no ?? '';

        #Validate Data
        if($parkNumber == '' OR $bookingId == '' OR $keyNo == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'park_number, key_no and booking_id are required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch bookings which are free
        $booking = ($bookingType == 'qr_code') ? 
                    $this->booking->with(['parking', 'user'])->find($bookingId) : 
                    $this->scanPrivateBooking->with(['parking', 'user'])->find($bookingId);

        #validate booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set data
        $data = [
            'parking_id'            => $booking->parking_id,
            'booking_id'            => $booking->id,
            'booking_type'          => $bookingType,
            'washer_id'             => $washer->id,
            'key_no'                => $keyNo,
            'park_no'               => $parkNumber,
            'washing_status'        => WasherBookingStatusInterface::INPROGRESS,
            'washing_start_date'    => $request->washing_start_date ?? '',
            #'washing_end_date'      => $request->washing_end_date ?? '',
            'park_no'               => $parkNumber,
            'arrived_date_time'     => $booking->full_arrived_date,
            'exit_date_time'        => $booking->full_exit_date != '' ? $booking->full_exit_date : null,
        ];
        
        #Set data to Supervisor
        $this->washerBooking->create($data);

        #Fetch User
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status changed.",
                        "body" => "Washing Started."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking allot successfully to Washer.',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to fetch all  booking on supervisor
     * @return json
     * @param Request $request
     */
    public function getBookings(Request $request)
    {
        #Fetch Authenticated User
        $user = Session::get('wsdUser');

        #Validate User
        if($user == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set relations
        $relations = [
            'washerBookings.parking',
            'washerBookings.booking.user',
            'washerBookings.booking.vehicle',
            'washerBookings.booking.washer',
            'washerBookings.scanPrivateBooking.user',
            'washerBookings.scanPrivateBooking.vehicle',
            'washerBookings.scanPrivateBooking.washer',
        ];

        #Fetch Supervisor
        $washer = $this->wsd
                           ->with($relations)
                           ->washer()
                           ->find($user->id);

        #Validate washer
        if($washer == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch all Alloted Bookings
        $bookings = $washer->washerBookings
                           ->where('booking_type','qr_code')
                           ->where('washing_status', '<>', WasherBookingStatusInterface::COMPLETED)
                           ->sortByDesc('id');

        #Fetch all Alloted Bookings
        $scanPrivateBookings = $washer->washerBookings
                                      ->where('booking_type', '<>', 'qr_code')
                                      ->where('washing_status', '<>', WasherBookingStatusInterface::COMPLETED)
                                      ->sortByDesc('id');
        
        #validate Bookings
        if($bookings->isEmpty() AND $scanPrivateBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No booking alloted.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Data
        $allBookingAlloted = [];
        foreach ($bookings as $key => $allotedBooking) {
            $booking = $allotedBooking->booking;
            $vehicle = $allotedBooking->booking->vehicle;
            $parking = $allotedBooking->booking->parking;

            #Set data
            $data = [
                'parking_id'            => $booking->parking_id ?? '',
                'wash_status'           => $booking->washing_status,
                'wash_type'             => $booking->fetch_wash_type,
                'parking_name'          => $parking->name ?? '',
                'parking_image'         => $parking->image_full_path ?? '',
                'booking_id'            => $booking->id ?? '',
                'booking_type'          => $booking->parking_type ?? '',
                'washer_id'             => $washer->id ?? '',
                'key_no'                => $allotedBooking->key_no ?? '',
                'park_no'               => $allotedBooking->park_no ?? '',
                'plate_no'              => $vehicle->plate_number ?? '',
                'vehicle_color'         => $vehicle->color_name ?? '',
                'vehicle_color_code'    => $vehicle->hex_code ?? '',
                'vehicle_name'          => $vehicle->full_name ?? '',
                'vehicle_image'         => $vehicle->image_full_path ?? '',
                'arrived_date_time'     => $booking->arrived_formatted_time ?? '',
                'wash_start_timestamp'  => $booking->washer != '' ? $booking->washer->wash_start_date_timestamp : '',
                'exit_date_time'        => $booking->exit_formatted_time ?? '',
            ];

            #Push Data
            array_push($allBookingAlloted, $data);
        }

        #Set Data for Scan or Private Booking
        foreach ($scanPrivateBookings as $key => $allotedBooking) {
            $booking = $allotedBooking->scanPrivateBooking;
            $vehicle = $allotedBooking->scanPrivateBooking->vehicle;
            $parking = $allotedBooking->scanPrivateBooking->parking;

            #Set data
            $data = [
                'parking_id'            => $booking->parking_id ?? '',
                'wash_status'           => $booking->washing_status,
                'wash_type'             => $booking->fetch_wash_type,
                'parking_name'          => $parking->name ?? '',
                'parking_image'         => $parking->image_full_path ?? '',
                'booking_id'            => $booking->id ?? '',
                'booking_type'          => $booking->parking_type ?? '',
                'washer_id'             => $washer->id ?? '',
                'key_no'                => $allotedBooking->key_no ?? '',
                'park_no'               => $allotedBooking->park_no ?? '',
                'plate_no'              => $vehicle->plate_number ?? '',
                'vehicle_color'         => $vehicle->color_name ?? '',
                'vehicle_color_code'    => $vehicle->hex_code ?? '',
                'vehicle_name'          => $vehicle->full_name ?? '',
                'vehicle_image'         => $vehicle->image_full_path ?? '',
                'arrived_date_time'     => $booking->arrived_formatted_time ?? '',
                'wash_start_timestamp'  => $booking->washer != '' ? $booking->washer->wash_start_date_timestamp : '',
                'exit_date_time'        => $booking->exit_formatted_time ?? '',
            ];

            #Push Data
            array_push($allBookingAlloted, $data);
        }

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking alloted.',
            'success'       => true,
            'data'          => $allBookingAlloted
        ]);
    }

    /**
     * @method to fetch booking Bill Detail
     * @return json
     * @param Request $request
     */
    public function todayCheckin(Request $request)
    {
        #Fetch Authenticated User
        $washer = Session::get('wsdUser');

        #Validate washer
        if($washer == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Set relations
        $relations = [
            'washerBookings.parking',
            'washerBookings.booking.user',
            'washerBookings.booking.vehicle',
        ];

        #Fetch Supervisor
        $washer = $this->wsd
                           ->with($relations)
                           ->washer()
                           ->find($washer->id);

        #Validate washer
        if($washer == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch booking
        $bookingAllotWasher = $washer->washerBookings;
    
        #validate Bookings
        if($bookingAllotWasher->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Checking Today
        $bookingCheckInToday = $bookingAllotWasher->filter(function($washerBooking) {
            if($washerBooking->arrived_date_time->diffInDays(Carbon::now()) == 0) {
                return $washerBooking;
            }
        });

        #validate bookingCheckInToday
        if($bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for today.',
                'success'       => false,
                'data'          => null
            ]);
        }
        #dd($bookingCheckInToday);
        #Todays checkin
        $todaysCheckInBookings = [];
        foreach ($bookingCheckInToday as $key => $bookingAllotWasher) {
            #set Bill data
            $booking        = $bookingAllotWasher->booking;
            $vehicle        = $bookingAllotWasher->booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'booking_id'        => $booking->id ?? '',
                'washer_id'         => $washer->id ?? '',
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $bookingAllotWasher->arrived_time ?? '',
            ];

            #push
            array_push($todaysCheckInBookings, $data);
        }
           

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Todays checkin Data.',
            'success'       => true,
            'data'          => $todaysCheckInBookings
        ]);
    }

    /**
     * @method to fetch search todays checkin
     * @return json
     * @param Request $request
     */
    public function searchTodaysCheckin(Request $request)
    {
        #Fetch Authenticated User
        $washer = Session::get('wsdUser');

        #Validate washer
        if($washer == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Set relations
        $relations = [
            'washerBookings.parking',
            'washerBookings.booking.user',
            'washerBookings.booking.vehicle',
        ];

        #Fetch Supervisor
        $washer = $this->wsd
                           ->with($relations)
                           ->washer()
                           ->find($washer->id);

        #Validate washer
        if($washer == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch Serach String
        $searchString = $request->search_string ?? '';

        #Validate washer
        if($washer == '' OR $searchString == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer or search_string not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch booking
        $bookingAllotWasher = $washer->washerBookings;
    
        #validate Bookings
        if($bookingAllotWasher->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Checking Today
        $bookingCheckInToday = $bookingAllotWasher->filter(function($washerBooking) {
            if($washerBooking->arrived_date_time->diffInDays(Carbon::now()) == 0) {
                return $washerBooking;
            }
        });

        #validate bookingCheckInToday
        if($bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for today.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Search String based Bookings
        $bookingCheckInToday = $bookingCheckInToday->filter(function($washerBooking) use($searchString) {
            $booking = $washerBooking->booking;
            $vehicle = $booking->vehicle;
           
            #Fetch Vehicle If Exist
            $vehicleAvail = $this->userVehicle
                                 ->where('plate_number', 'Like', $searchString)
                                 ->orWhere('brand_name', 'Like', $searchString)
                                 ->orWhere('modal_name', 'Like', $searchString)
                                 ->orWhere('color_name', 'Like', $searchString)
                                 ->get();
            #Validate
            if($vehicleAvail->where('id', $vehicle->id)->isNotEmpty()) {
                return $washerBooking;
            }
        });

        #validate bookingCheckInToday
        if($bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for today.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Todays checkin
        $todaysCheckInBookings = [];
        foreach ($bookingCheckInToday as $key => $bookingAllotWasher) {
            #set Bill data
            $booking        = $bookingAllotWasher->booking;
            $vehicle        = $bookingAllotWasher->booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'booking_id'        => $booking->id ?? '',
                'washer_id'         => $washer->id ?? '',
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $bookingAllotWasher->arrived_time ?? '',
            ];

            #push
            array_push($todaysCheckInBookings, $data);
        }
           

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Todays checkin Data.',
            'success'       => true,
            'data'          => $todaysCheckInBookings
        ]);
    }

    /**
     * @method to Update Washer Booking Status
     * @return json
     * @param Request $request
     */
    public function updateBookingStatus(Request $request)
    {
        #Fetch Authenticated User
        $user       = Session::get('wsdUser');
        $status     = $request->status ?? '';
        $bookingId  = $request->booking_id ?? '';
        $bookingType  = $request->booking_type ?? '';

         # Validate request data
        $validator = Validator::make($request->all(), [ 
            'washing_end_date'      => 'required|date_format:Y-m-d H:i:s',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washing_end_date format should be Y-m-d H:i:s',
                'success'       => false,
                'data'          => null
            ]);
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        #Validate User
        if($user == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Washer or booking_type not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate BookingTpe
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid(qrcode, scan_me, private).',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set relations
        $relations = [
            'washerBookings',
            #'supervisorBookings.booking.user',
            #'supervisorBookings.booking.vehicle',
        ];

        #Fetch washer
        $washer = $this->wsd
                       ->with($relations)
                       ->washer()
                       ->find($user->id);

        #Validate washer
        if($washer == '' OR $status == '' OR $bookingId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'washer, status or booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Validate Status
        $validStatus = [2];    
        if(!in_array($status, $validStatus)) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Status is not Valid',
                'success'       => false,
                'data'          => null
            ]);
        }   

        #Fetch all Alloted Bookings
        $washerBooking = $bookingType == 'qr_code' ? 
                            $washer->washerBookings->where('booking_id', $bookingId)->where('booking_type', 'qr_code')->sortByDesc('id') : 
                            $washer->washerBookings->where('booking_id', $bookingId)->where('booking_type', '<>', 'qr_code')->sortByDesc('id');
        
        #validate washerBooking
        if($washerBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No booking Found on this id.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Fisrt washerBooking
        $washerBooking = $washerBooking->first();

        #Updste booking Status
        $washerBooking->update(['washing_status' => $status, 'washing_end_date' => $request->washing_end_date]);

        #Fetch WasherBoioking
        $washerBooking = $bookingType == 'qr_code' ? 
                         $this->washerBooking->qrBookings()->find($washerBooking->id) :
                         $this->washerBooking->nonQrBookings()->find($washerBooking->id) ;
        
        #Set data
        $data = [
            'wash_start_timestamp'  => $washerBooking->washing_start_date != '' ? 
                                        $washerBooking->washing_start_date->format('h:i A') : '',
            'wash_end_timestamp'    => $washerBooking->washing_end_date != '' ?
                                        $washerBooking->washing_end_date->format('h:i A') : '',
            'time_diff'             => $washerBooking->diff_start_end_wash
        ];

         #Fetch User
        $booking = $bookingType == 'qr_code' ? 
                         $this->booking->with(['user'])->find($bookingId) :
                         $this->scanOrPrivateBooking->with(['user'])->find($bookingId) ;

        #FEtch user
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status changed.",
                        "body" => $this->fetchStatusString($status);
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #return
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Wash Status Updated Successfully.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to define atrribute to Set booking Status
     * @return string
     * @param
     */
    public function fetchStatusString($status)
    {
        if($status == WasherBookingStatusInterface::INPROGRESS) {
            return 'WASH_IN_PROGRESS';
        } elseif ($status == WasherBookingStatusInterface::COMPLETED) {
            return 'WASH_COMPLETED';
        } elseif ($status == WasherBookingStatusInterface::PENDING) {
            return 'WASH_PENDING';
        } elseif ($status == WasherBookingStatusInterface::WASH_INITIAL) {
            return 'WASH_INITIAL';
        } elseif ($status == WasherBookingStatusInterface::NO_WASH) {
            return 'NO_WASH';
        } else {
            return 'NO_STATUS';
        }
    }
    
}