<?php

namespace App\Http\Controllers\Api\WSD\Driver;

use DB;
use Log;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use App\Utils\CommonUtil;

# Models
use App\Models\Booking;
use App\Models\Payment;
use App\Models\Washerman;
use App\Models\UserVehicle;
use App\Models\DriverBooking;
use App\Models\SupervisorBooking;
use App\Models\ScanOrPrivateBooking;
use App\Models\PaymentForScanPrivateBooking;

# Interface
use App\Interfaces\ApiStatusInterface;
use App\Interfaces\UserBookingStatusInterface;
use App\Interfaces\DriverBookingStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller implements ApiStatusInterface, DriverBookingStatusInterface, UserBookingStatusInterface
{
    # Bind commonUtil
    private $commonUtil;

    # Bind api token
    private $apiToken;

    # Variable $wsd(washer, supervisor or Driver)
    protected $wsd, $payment;

    # Variable $booking
    protected $booking;

    # Variable $userVehicle
    protected $userVehicle;

    # Variable $driverBooking
    protected $driverBooking;

    # Variable $supervisorBooking
    protected $supervisorBooking, $scanPrivateBooking, $paymentForScanPrivate;
    
	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(Washerman $wsd, Booking $booking, Payment $payment, UserVehicle $userVehicle,
        SupervisorBooking $supervisorBooking, CommonUtil $commonUtil, DriverBooking $driverBooking, 
        ScanOrPrivateBooking $scanOrPrivateBooking, PaymentForScanPrivateBooking $paymentForScanPrivate)
	{
		$this->wsd                  = $wsd;
        $this->booking              = $booking;
        $this->payment              = $payment;
        $this->commonUtil           = $commonUtil;
        $this->userVehicle          = $userVehicle;
        $this->driverBooking        = $driverBooking;
        $this->supervisorBooking    = $supervisorBooking;
        $this->scanOrPrivateBooking = $scanOrPrivateBooking;
        $this->paymentForScanPrivate = $paymentForScanPrivate;
        $this->apiToken             = uniqid(base64_encode(\Str::random(128)));
	}

    /**
     * @method to fetch set bookings of Driver
     * @return json
     * @param Request $request
     */
    public function fetchSetBookings(Request $request)
    {
        #Fetch Authenticated User
        $user = Session::get('wsdUser');

        #Validate User
        if($user == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           ->with(['company.companyParking', 'currentParking'])
                           ->driver()
                           ->find($user->id);

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver Current parking
        $driverCurrentParking = $driver->currentParking;

        #Validate driver Current parking
        if($driverCurrentParking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver Current parking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch all parkings 
        $driverParkings = $driver->company->companyParking->isNotEmpty() ? 
                              $driver->company->companyParking->pluck('id')->toArray() : [];
        
        #Fetch bookings which are free
        $allBookings = $this->booking
                            ->with(['parking', 'vehicle', 'user'])   
                            ->whereIn('parking_id', $driverParkings)
                            ->get();
        #validate bookings
        if($allBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Bookings not found to set.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch all bookings on which Driver already assigned
        $bookingIdsDriverAssigned = $this->driverBooking
                                         ->whereIn('booking_id', $allBookings->pluck('id')->toArray())
                                         ->get()
                                         ->pluck('booking_id')
                                         ->toArray();

        #Fetch all booking Id which are alloted to Driver
        $bookingIdsAllotedtoSupervisor = $this->supervisorBooking
                                              ->get()
                                              ->pluck('booking_id')
                                              ->toArray();

        #validate all bookings which are not on any supervisor
        $validBookings = $allBookings->filter(function($booking) use($bookingIdsDriverAssigned, $bookingIdsAllotedtoSupervisor, $driverCurrentParking) {
            if(!in_array($booking->id, $bookingIdsDriverAssigned) AND 
                !in_array($booking->id, $bookingIdsAllotedtoSupervisor) AND 
                ($booking->booking_status == UserBookingStatusInterface::AVAILABLE_TO_PICK) AND 
                ($booking->parking_id == $driverCurrentParking->id)) {
                return $booking;
            }
        });

        #set Data for bookings
        $bookingsSetData = [];
        foreach ($validBookings as $key => $booking) {
            $parkingMarkout = $booking->parking->parkingMarkOut;

            #FEtch data
            $data = [
                'booking_id'            => $booking->id ?? '',
                'user_id'               => (string)$booking->user->id ?? '',
                'hex_code'              => ($key % 2 == 0) ? '#FF0000' : '#00FF00',
                'parking_id'            => $booking->parking->id ?? '',
                'parking_name'          => $booking->parking->name ?? '',
                'parking_image'         => $booking->parking->image_full_path ?? '',
                'user_name'             => $booking->user->name ?? '',
                'vehicle_id'            => $booking->vehicle->id ?? '',
                'vehicle_name'          => $booking->vehicle->full_name ?? '',
                'vehicle_color'         => $booking->vehicle->color_name ?? '',
                'vehicle_color_code'    => $booking->vehicle->hex_code ?? '',
                'vehicle_plate_no'      => $booking->vehicle->plate_number ?? '',
                'vehicle_image'         => $booking->vehicle->image_full_path ?? '',
                'arrived_time'          => $booking->min_bw_created_arrival ?? '',
                'arrived_timestamp'     => $booking->arrived_timestamp ?? '',
                'valet'                 => $booking->valet ? true : false,
                'washing'                   => $booking->wash_available ? true : false,
                'full_wash_price'           => $parkingMarkout->fetch_full_wash_cost,
                'exterior_wash_price'       => $parkingMarkout->fetch_exterior_wash_cost,
                'interior_wash_price'       => $parkingMarkout->fetch_interior_wash_cost,
            ];

            #push Arary
            array_push($bookingsSetData, $data);
        }
        
        #Set page
        $page = $request->page != '' ? $request->page : null;

        #convert to pagunation
        $bookingsSetData = $this->commonUtil->customPaginate($bookingsSetData, 5, url()->current());
        
        #return data
        return response()->json([
           'statusCode'    =>  ApiStatusInterface::OK, 
           'message'       => 'Bookings found to set.',
           'success'       => true,
           'data'          => $bookingsSetData 
        ]);
    }

    /**
     * @method to book booking on driver
     * @return json
     * @param Request $request
     */
    public function booking(Request $request)
    {
        #Fetch Authenticated User
        $user = Session::get('wsdUser');

        #Validate User
        if($user == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           ->with(['company.companyParking', 'driverBookings'])
                           ->driver()
                           ->find($user->id);

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch Data
        $parkNumber = $request->park_number ?? '';
        $bookingId  = $request->booking_id ?? '';
        $latitude   = $request->latitude ?? '';
        $longitude  = $request->longitude ?? '';

        #Validate Data
        if($parkNumber == '' OR $bookingId == '' OR $latitude == '' OR $longitude == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'park_number, booking_id, latitude, longitude are required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch bookings which are free
        $booking = $this->booking
                        ->with(['parking'])   
                        ->find($bookingId);

        #validate booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #check booking on Driver or not
        $driverBooking = $driver->driverBookings->where('booking_id', $booking->id);
        
        #Validate Booking on driver
        if($driverBooking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on Driver. ',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set data
        $data = [
            'park_no'           => $parkNumber,
            'pick_up_lat'       => $latitude,
            'pick_up_long'      => $longitude,
            'booking_status'    => DriverBookingStatusInterface::PARKED,
            'parked_date_time'  => Carbon::now()->format('Y-m-d H:i:s'),
        ];
        
        #Set data to Driver
        $driverBooking->first()->update($data);
        
        #Set Date for response
        $driverBooking = $driverBooking->first();

        #Set data
        $data = [
            'exit_time'         => $driverBooking->exit_time ?? '',
            'parking_id'        => $driverBooking->parking_id ?? '',
            'booking_id'        => $driverBooking->booking_id ?? '',
            'key_no'            => $driverBooking->key_no ?? '',
            'driver_id'         => $driverBooking->driver_id ?? '',
            'park_no'           => $parkNumber,
            'pick_up_lat'       => $latitude,
            'pick_up_long'      => $longitude,
            'booking_status'    => 'Parked',
        ];

        #mark booking as checked in
        $booking->update(['actual_in_dateTime' => Carbon::now()->format('Y-m-d H:i:s')]);

         #Fetch User
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status chnaged.",
                        "body" => "Your vehicle is Parked."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking Update successfully to Driver.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to fetch all  booking on driver
     * @return json
     * @param Request $request
     */
    public function getBookings(Request $request)
    {
        #Fetch Authenticated User
        $user = Session::get('wsdUser');

        #Validate User
        if($user == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set relations
        $relations = [
            'parking.parkingSetupSpace',
            'booking.user',
            'booking.vehicle',
        ];

        #Fetch driver
        $driver = $this->wsd
                           #->with($relations)
                           ->driver()
                           ->find($user->id);

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Current parking of driver
        $currentParking = $driver->currentParking;

        #Validate
        if($currentParking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Current parking not found on driver.',
                'success'       => false,
                'data'          => null
            ]);
        }

        
        #Fetch all set type Bookings of Supervisor
        $allSetTypeBookingsIdsOfSupervisor = $this->supervisorBooking
                                                  ->where('parking_id', $currentParking->id)
                                                  ->setType()
                                                  ->pluck('booking_id');

        #Fetch all get type Bookings of Supervisor
        $allGetTypeBookingsIdsOfSupervisor = $this->supervisorBooking
                                                  ->where('parking_id', $currentParking->id)
                                                  ->getType()
                                                  ->pluck('booking_id');

        #Fetch all set type Bookings of driver
        $allSetTypeBookingsIdsOfDriver = $this->driverBooking
                                              ->where('parking_id', $currentParking->id)
                                              ->setType()
                                              ->pluck('booking_id');

        #Fetch all get type Bookings of driver
        $allGetTypeBookingsIdsOfDriver = $this->driverBooking
                                              ->where('parking_id', $currentParking->id)
                                              ->getType()
                                              ->pluck('booking_id');
        
        #Fetch all Booking ids which are in get List of Either Supervior Or Driver
        $allBookingIdsInGetList = $allGetTypeBookingsIdsOfSupervisor->merge($allGetTypeBookingsIdsOfDriver);

        #Fetch Supervisor Bookings which are not in get of anyone(Driver/ Supervior)
        $supervisorBookingIdsOnlyinSet = $allSetTypeBookingsIdsOfSupervisor->diff($allBookingIdsInGetList);

        #Fetch Driver Bookings which are not in get of anyone(Driver/ Supervior)
        $driverBookingIdsOnlyinSet = $allSetTypeBookingsIdsOfDriver->diff($allBookingIdsInGetList);
        
        #Fetch Supervisor Bookings
        $validSupervisorBookings = $this->supervisorBooking
                                        ->with($relations)
                                        ->getCarRequest()
                                        ->whereIn('booking_id', $supervisorBookingIdsOnlyinSet->toArray())
                                        ->get();

        #Fetch Driver Bookings
        $validDriverBookings = $this->driverBooking
                                    ->with($relations)
                                    ->getCarRequest()
                                    ->whereIn('booking_id', $driverBookingIdsOnlyinSet->toArray())
                                    ->get();

        #validate Bookings
        if($validSupervisorBookings->isEmpty() AND $validDriverBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No booking found.',
                'success'       => false,
                'data'          => null
            ]);
        }

         #Set Data
        $allBookingAlloted = [];
        foreach ($validSupervisorBookings as $key => $allotedBooking) {
            $booking        = $allotedBooking->booking;
            $user           = $booking->user;
            $vehicle        = $allotedBooking->booking->vehicle;
            $parking        = $allotedBooking->booking->parking;
            $parkingSpace   = $allotedBooking->booking->parking->parkingSetupSpace;
            $parkingMarkout = $allotedBooking->booking->parking->parkingMarkOut;
            
            #Set data
            $data = [
                'booking_id'            => $booking->id ?? '',
                'user_id'               => (string)$booking->user->id ?? '',
                'parking_id'            => $booking->parking_id ?? '',
                'parking_type'          => $booking->parking_type ?? '',
                'user_name'             => $booking->user->name ?? '',
                'user_mobile'           => $user->mobile ?? '',
                'hex_code'              => ($key % 2 == 0) ? '#FF0000' : '#00FF00',
                'parking_name'          => $parking->name ?? '',
                'parking_image'         => $parking->image_full_path ?? '',
                'parking_lat'           => $parkingSpace->latitude ?? '',
                'parking_long'          => $parkingSpace->longitude ?? '',
                'booking_id'            => $booking->id ?? '',
                'supervisor_id'         => $allotedBooking->supervisor_id ?? '',
                'key_no'                => $allotedBooking->key_no ?? '',
                'park_no'               => $allotedBooking->park_no ?? '',
                'plate_no'              => $vehicle->plate_number ?? '',
                'wash_status'           => $booking->washing_status,
                'vehicle_color'         => $vehicle->color_name ?? '',
                'vehicle_color_code'    => $vehicle->hex_code ?? '',
                'vehicle_name'          => $vehicle->full_name ?? '',
                'vehicle_image'         => $vehicle->image_full_path ?? '',
                'arrived_date_time'     => $booking->arrived_formatted_time ?? '',
                'requested_date_time'   => $allotedBooking->request_timestamp ?? '',
                'exit_date_time'        => $booking->exit_formatted_time ?? '',
                'full_wash_price'           => $parkingMarkout->fetch_full_wash_cost,
                'exterior_wash_price'       => $parkingMarkout->fetch_exterior_wash_cost,
                'interior_wash_price'       => $parkingMarkout->fetch_interior_wash_cost,
            ];

            #Push Data
            array_push($allBookingAlloted, $data);
        }

        
        #Set Data
        foreach ($validDriverBookings as $key => $allotedBooking) {
            $booking        = $allotedBooking->booking;
            $user           = $booking->user;
            $vehicle        = $allotedBooking->booking->vehicle;
            $parking        = $allotedBooking->booking->parking;
            $parkingSpace   = $allotedBooking->booking->parking->parkingSetupSpace;

            #Set data
            $data = [
                'booking_id'            => $booking->id ?? '',
                'parking_id'            => $booking->parking_id ?? '',
                'parking_type'          => $booking->parking_type ?? '',
                'user_name'             => $booking->user->name ?? '',
                'user_mobile'           => $user->mobile ?? '',
                'hex_code'              => ($key % 2 == 0) ? '#FF0000' : '#00FF00',
                'parking_name'          => $parking->name ?? '',
                'parking_image'         => $parking->image_full_path ?? '',
                'parking_lat'           => $parkingSpace->latitude ?? '',
                'parking_long'          => $parkingSpace->longitude ?? '',
                'booking_id'            => $booking->id ?? '',
                'driver_id'             => $allotedBooking->driver_id ?? '',
                'key_no'                => $allotedBooking->key_no ?? '',
                'park_no'               => $allotedBooking->park_no ?? '',
                'plate_no'              => $vehicle->plate_number ?? '',
                'wash_status'           => $booking->washing_status,
                'vehicle_color'         => $vehicle->color_name ?? '',
                'vehicle_id'            => $vehicle->id ?? '',
                'vehicle_color_code'    => $vehicle->hex_code ?? '',
                'vehicle_name'          => $vehicle->full_name ?? '',
                'vehicle_image'         => $vehicle->image_full_path ?? '',
                'arrived_date_time'     => $booking->arrived_formatted_time ?? '',
                'requested_date_time'   => $allotedBooking->request_timestamp ?? '',
                'exit_date_time'        => $booking->exit_formatted_time ?? '',
            ];

            #Push Data
            array_push($allBookingAlloted, $data);
        }

        #convert to pagination
        $allBookingAlloted = $this->commonUtil
                                  ->customPaginate($allBookingAlloted, 5, url()->current());

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking alloted.',
            'success'       => true,
            'data'          => $allBookingAlloted
        ]);
    }

    /**
     * @method to pick the get Booking
     * @param Request
     * @return
     */
    public function pickTheGetBooking(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Fetch bookingId
        $bookingId = $request->booking_id ?? '';

        #Validate driver
        if($driver == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Validate boookingId
        if($bookingId == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_id is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch booking Allot on Supervisor or Driver
        $bookingOnSupervisor = $this->supervisorBooking->where('booking_id', $bookingId)->get();
        $bookingOnDriver = $this->driverBooking->where('booking_id', $bookingId)->get();

        #validate
        if($bookingOnSupervisor->isEmpty() AND $bookingOnDriver->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_id is niether assigned to Supervisor nor Driver.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate booking already pick by anyone
        $bookingPickBySupervisor = $bookingOnSupervisor->where('set_or_get', 'get');
        $bookingPickByDriver = $bookingOnDriver->where('set_or_get', 'get');

        #Booking Set on Supervsior or Driver
        $bookingSetSupervisor = $bookingOnSupervisor->where('set_or_get', 'set');
        $bookingSetDriver = $bookingOnDriver->where('set_or_get', 'set');

        #set Booking
        $setBooking = $bookingSetSupervisor->isNotEmpty() ? 
                        $bookingSetSupervisor->first() : 
                        $bookingSetDriver->first();


        #Validate
        if($bookingPickBySupervisor->isNotEmpty() OR $bookingPickByDriver->isNotEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking already picked by Someone.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Assigned booking
        if($bookingOnSupervisor->isNotEmpty()) {
            $bookingOnSupervisor = $bookingOnSupervisor->first();

            #Allot booking to Supervisor
            $data = [
                'parking_id'        => $driver->current_parking_id,
                'booking_id'        => $bookingOnSupervisor->booking_id,
                'driver_id'         => $driver->id,
                'key_no'            => $bookingOnSupervisor->key_no,
                'set_or_get'        => 'get',
                'booking_status'    => DriverBookingStatusInterface::ON_THE_WAY_TO_DROPD,
                'arrived_date_time' => $bookingOnSupervisor->arrived_date_time->format('Y-m-d H:i:s'),
                'requested_date_time'  => $setBooking->requested_date_time->format('Y-m-d H:i:s'),
            ];

            #Create 
            $this->driverBooking->create($data);

            #Assign current booking to driver
            $driver->update(['current_booking_id' => $bookingOnSupervisor->booking_id]);
        } else {
            $bookingOnDriver = $bookingOnDriver->first();

            #Allot booking to Supervisor
            $data = [
                'parking_id'        => $driver->current_parking_id,
                'booking_id'        => $bookingOnDriver->booking_id,
                'driver_id'         => $driver->id,
                'key_no'            => $bookingOnDriver->key_no,
                'set_or_get'        => 'get',
                'booking_status'    => DriverBookingStatusInterface::ON_THE_WAY_TO_DROPD,
                'arrived_date_time' => $bookingOnDriver->arrived_date_time->format('Y-m-d H:i:s'),
                'requested_date_time'  => $setBooking->requested_date_time->format('Y-m-d H:i:s'),
            ];

            #Create 
            $this->driverBooking->create($data);

            #Assign current booking to driver
            $driver->update(['current_booking_id' => $bookingOnDriver->booking_id]);
        }

         #FEthc booking
        $booking = $this->booking->with(['user'])->find($bookingId);

         #Fetch User
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status changed.",
                        "body" => "Your Booking has been picked."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Picked by you.',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to pick the get Booking
     * @param Request
     * @return
     */
    public function fetchCurrentBookingStatus()
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Set relations
        $relations = [
            'currentBookingOnDriver.parking.parkingSetupSpace',
            'currentBookingOnDriver.booking.user',
            'currentBookingOnDriver.booking.vehicle',
        ];

        #Fetch driver
        $driver = $this->wsd
                           ->with($relations)
                           ->driver()
                           ->find($driver->id);

        #Fethc Current Booking
        $currentDriverBooking = $driver->currentBookingOnDriver;

        #Validate
        if($currentDriverBooking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No Booking Found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        $booking        = $currentDriverBooking->booking;
        $user           = $booking->user;
        $vehicle        = $currentDriverBooking->booking->vehicle;
        $parking        = $currentDriverBooking->booking->parking;
        $parkingSpace   = $currentDriverBooking->booking->parking->parkingSetupSpace;

        #Set data
        $data = [
            'parking_id'            => $booking->parking_id ?? '',
            'user_name'             => $booking->user->name ?? '',
            'user_id'               => (string)$booking->user->id ?? '',
            'parking_type'          => $booking->parking_type ?? '',
            'valet_service'         => $booking->is_valet_available ?? '',
            'user_mobile'           => $user->mobile ?? '',
            'hex_code'              => '#00FF00',
            'parking_name'          => $parking->name ?? '',
            'parking_image'         => $parking->image_full_path ?? '',
            'parking_lat'           => $parkingSpace->latitude ?? '',
            'parking_long'          => $parkingSpace->longitude ?? '',
            'booking_id'            => $booking->id ?? '',
            'driver_id'             => $currentDriverBooking->driver_id ?? '',
            'key_no'                => $currentDriverBooking->key_no ?? '',
            'park_no'               => $currentDriverBooking->park_no ?? '',
            'plate_no'              => $vehicle->plate_number ?? '',
            'wash_status'           => $booking->washing_status,
            'booking_status'        => $currentDriverBooking->fetch_booking_status,
            'vehicle_color'         => $vehicle->color_name ?? '',
            'vehicle_color_code'    => $vehicle->hex_code ?? '',
            'vehicle_name'          => $vehicle->full_name ?? '',
            'vehicle_image'         => $vehicle->image_full_path ?? '',
            'arrived_date_time'     => $booking->arrived_formatted_time ?? '',
            'exit_date_time'        => $booking->exit_formatted_time ?? '',
            ];


        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Current Booking Data.',
            'success'       => true,
            'data'          => $data
        ]);
    }   

    /**
     * @method to fetch booking Bill Detail
     * @return json
     * @param Request $request
     */
    public function bookingBillDetail(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Fetch bookingId
        $bookingId = $request->booking_id ?? '';
        $bookingType    = $request->booking_type ?? '';

        #Validate driver
        if($driver == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Validate boookingId
        if($bookingId == '' OR $bookingType == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_id, bookin_type is required.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate booking ztype
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking Type is not valid',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set relations
        $relations = [
            'parking.parkingSetupSpace',
            'parking.company',
            'user',
            'vehicle',
            'supervisors',
            'drivers',
            'parking.company.driverCommission',
            'parking.company.washerCommission',
        ];

        #FEtch bookinf
        if($bookingType == 'qr_code') {
            #User booking
            $booking = $this->booking->with($relations)->where('id', $bookingId)->get();      
        } else {
            #User booking
            $booking = $this->scanOrPrivateBooking->with($relations)->where('id', $bookingId)->get(); 
        }

        #Validate booking
        if($booking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        $booking = $booking->last();

        #Validate booking Payment method is set or not
        if($booking->payment_method == '') {
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment method is not available on Booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate booking type is scan 
        if(in_array($bookingType, ['scan_me', 'private'])) {
            #Validate 
            if(!$booking->bill['status']) {
                #return 
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => $booking->bill['message'],
                    'success'       => false,
                    'data'          => null
                ]);
            }

            #return 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment Method set on Booking.',
                'success'       => true,
                'data'          => $booking->bill['data']
            ]);
        }

        #Fetch booking is in whose get (Supervisor or Driver)
        $supervisors = $booking->supervisors->where('set_or_get','get');
        $drivers = $booking->drivers->where('set_or_get','get');

        #validate if Booking is on Supervisor or Driver Get and valet is selected on booking
        if(($supervisors->isNotEmpty() OR $drivers->isNotEmpty()) AND $booking->valet) {
            #Fetch status of Booking
            $bookingStatus = '';
            if($supervisors->isNotEmpty()) {
                $bookingStatus = $supervisors->last()->booking_status;
            } elseif ($drivers->isNotEmpty()) {
                $bookingStatus = $drivers->last()->booking_status;
            }
            
            #Validate if booking is in Bill Generate Stage(GET_CAR_REQUEST,ON_THE_WAY_TO_DROP,ARRIVED,BILL_GENERATED)
            if(!in_array($bookingStatus, [5,6,7,8])) {
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Booking not in Bill Generate Stage yet.',
                    'success'       => false,
                    'data'          => null
                ]);
            }
            
            #Validate 
            if(!$booking->bill['status']) {
                #return 
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => $booking->bill['message'],
                    'success'       => false,
                    'data'          => null
                ]);
            }

            #FEtch data
            $data =  $booking->bill['data'];

            #Set arrived time of Booking
           $data['arrived_time'] = $booking->arrived_formatted_time ?? '';

            #Set arrived Date of Booking
           $data['arrived_date'] = $booking->arrived_date_formatted ?? '';

            #Set requested time of Booking
           $data['requested_time'] = $supervisors->isNotEmpty() ?  
                                       $supervisors->last()->requested_formatted_time : 
                                       $drivers->last()->requested_formatted_time;

            #Set requested date of Booking
           $data['requested_date'] = $supervisors->isNotEmpty() ?  
                                       $supervisors->last()->requested_date_formatted : 
                                       $drivers->last()->requested_date_formatted;
            
            #return 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment Method set on Booking.',
                'success'       => true,
                'data'          => $data
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           //->with($relations)
                           ->driver()
                           ->find($driver->id);

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        /*       #Fetch booking
        $bookingAllotDriver = $driver->driverBookings
                                             ->where('booking_id', $bookingId)
                                             ->first();
    
        #validate Bookings
        if($bookingAllotDriver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not alloted to Supervisor.',
                'success'       => false,
                'data'          => null
            ]);
        }

         #Set Booking
        $booking =  $bookingAllotDriver->booking;
        
        #Validate booking Payment method is set or not
        if($booking->payment_method == '') {
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment method is not available on Booking.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch booking is in whose get (Supervisor or Driver)
        $supervisors = $booking->supervisors->where('set_or_get','get');
        $drivers = $booking->drivers->where('set_or_get','get');
        
        #Validate booking if niether picked by Supervisor nor driver
        if($supervisors->isEmpty() AND $drivers->isEmpty()) {
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking niether picked by supervisor nor any driver.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        #Fetch status of Booking
        $bookingStatus = '';
        if($supervisors->isNotEmpty()) {
            $bookingStatus = $supervisors->last()->booking_status;
        } elseif ($drivers->isNotEmpty()) {
            $bookingStatus = $drivers->last()->booking_status;
        }
        
        /*#Validate if booking is in Bill Generate Stage(GET_CAR_REQUEST,ON_THE_WAY_TO_DROP,ARRIVED,BILL_GENERATED)
        if(!in_array($bookingStatus, [5,6,7,8])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not in Bill Generate Stage yet.',
                'success'       => false,
                'data'          => null
            ]);
        }*/
        
        #Validate 
        if(!$booking->bill['status']) {
            #return 
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => $booking->bill['message'],
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch data
        $data =  $booking->bill['data'];

        #Set arrived time of Booking
       $data['arrived_time'] = $booking->arrived_formatted_time ?? '';

        #Set arrived Date of Booking
       $data['arrived_date'] = $booking->arrived_date_formatted ?? '';

        /*#Set requested time of Booking
       $data['requested_time'] = $supervisors->isNotEmpty() ?  
                                   $supervisors->last()->requested_formatted_time : 
                                   $drivers->last()->requested_formatted_time;*/
        $data['requested_time'] = '';

        #Set requested date of Booking
       /*$data['requested_date'] = $supervisors->isNotEmpty() ?  
                                   $supervisors->last()->requested_date_formatted : 
                                   $drivers->last()->requested_date_formatted;*/
        $data['requested_date'] = '';
        
        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Payment Method set on Booking.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to fetch booking Bill Detail
     * @return json
     * @param Request $request
     */
    public function todayCheckin(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Fetch bookingId
        $bookingId = $request->booking_id ?? '';

        #Validate driver
        if($driver == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Set relations
        $relations = [
            'driverBookings.parking.parkingSetupSpace',
            'driverBookings.parking.company',
            'driverBookings.booking.user',
            'driverBookings.booking.vehicle',
        ];

        #Fetch driver
        $driver = $this->wsd
                           ->with($relations)
                           ->driver()
                           ->find($driver->id);

        #Validate driver
        if($driver == ''  OR $driver->current_parking_id == 0) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or current booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch booking
        #$bookingAllotDriver = $driver->driverBookings;
        $bookingsDriver = $this->driverBooking->where('parking_id', $driver->current_parking_id)->get();
        $bookingAllotDriver = $this->booking
                                      ->where('parking_id', $driver->current_parking_id)
                                      ->where('booking_status', UserBookingStatusInterface::VEHICLE_PARKED)
                                      ->get();
    

         #Fetch scan Private Bookings
        $scanOrPrivateBookings = $this->scanOrPrivateBooking
                                      ->where('parking_id', $driver->current_parking_id)
                                      ->where('booking_status', UserBookingStatusInterface::VEHICLE_PARKED)
                                      ->get();
    
        #validate Bookings
        // if($bookingAllotDriver->isEmpty() AND $scanOrPrivateBookings->isEmpty()) {
        //     return response()->json([
        //         'statusCode'    =>  ApiStatusInterface::OK, 
        //         'message'       => 'No checkin.',
        //         'success'       => false,
        //         'data'          => null
        //     ]);
        // }

        /*#Checking Today
        $bookingCheckInToday = $bookingsDriver->filter(function($driverBooking) {
            if($driverBooking->arrived_date_time->diffInDays(Carbon::now()) == 0 AND 
                ($driverBooking->booking_status == DriverBookingStatusInterface::PARKED)) {
                return $driverBooking;
            }
        });*/

        #Checking Today
        $bookingCheckInToday = $bookingsDriver->filter(function($driverBooking) {
            if($driverBooking->booking_status == DriverBookingStatusInterface::PARKED AND 
                (!$driverBooking->booking->is_completed)) {
                return $driverBooking;
            }
        });
        
        #validate bookingCheckInToday
        if($bookingAllotDriver->isEmpty() AND $scanOrPrivateBookings->isEmpty() AND $bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for today.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Todays checkin
        $todaysCheckInBookings = [];
        foreach ($bookingAllotDriver as $key => $booking) {
            #set Bill data
            $vehicle        = $booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'parking_type'      => $booking->parking_type ?? '',
                'booking_id'        => $booking->id ?? '',
                'driver_id'         => $driver->id ?? '',
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $booking->arrived_formatted ?? '',
            ];

            #push
            array_push($todaysCheckInBookings, $data);
        }

        foreach ($bookingCheckInToday as $key => $driverBooking) {
            #set Bill data
            $booking        = $driverBooking->booking;
            $vehicle        = $driverBooking->booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'parking_type'      => $booking->parking_type ?? '',
                'booking_id'        => $booking->id ?? '',
                'driver_id'         => $driver->id ?? '',
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $booking->arrived_formatted ?? '',
            ];

            #push
            array_push($todaysCheckInBookings, $data);
        }

        #Scan Private Bookings
        foreach ($scanOrPrivateBookings as $key => $scanPrivateBooking) {
            #set Bill data
            $booking        = $scanPrivateBooking;
            $vehicle        = $scanPrivateBooking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'parking_type'      => $booking->parking_type ?? '',
                'booking_id'        => $booking->id ?? '',
                'supervisor_id'     => $supervisor->id ?? null,
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $scanPrivateBooking->arrived_formatted ?? '',
            ];

            #push
            array_push($todaysCheckInBookings, $data);
        }
           

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Todays checkin Data.',
            'success'       => true,
            'data'          => $todaysCheckInBookings
        ]);
    }

    /**
     * @method to fetch search todays checkin
     * @return json
     * @param Request $request
     */
    public function searchTodaysCheckin(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Fetch bookingId
        $bookingId = $request->booking_id ?? '';

        #Validate driver
        if($driver == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Set relations
        $relations = [
            'driverBookings.parking.parkingSetupSpace',
            'driverBookings.parking.company',
            'driverBookings.booking.user',
            'driverBookings.booking.vehicle',
        ];

        #Fetch driver
        $driver = $this->wsd
                           ->with($relations)
                           ->driver()
                           ->find($driver->id);

        #Fetch Serach String
        $searchString = $request->search_string ?? '';

        #Validate driver
        if($driver == '' OR $searchString == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or search_string not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch booking
        $bookingAllotDriver = $driver->driverBookings;
    
        #validate Bookings
        if($bookingAllotDriver->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Checking Today
        $bookingCheckInToday = $bookingAllotDriver->filter(function($driverBooking) {
            if($driverBooking->arrived_date_time->diffInDays(Carbon::now()) == 0) {
                return $driverBooking;
            }
        });

        #validate bookingCheckInToday
        if($bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for today.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Search String based Bookings
        $bookingCheckInToday = $bookingCheckInToday->filter(function($driverBooking) use($searchString) {
            $booking = $driverBooking->booking;
            $vehicle = $booking->vehicle;
           
            #Fetch Vehicle If Exist
            $vehicleAvail = $this->userVehicle
                                 ->where('plate_number', 'Like', $searchString)
                                 ->orWhere('brand_name', 'Like', $searchString)
                                 ->orWhere('modal_name', 'Like', $searchString)
                                 ->orWhere('color_name', 'Like', $searchString)
                                 ->get();
            #Validate
            if($vehicleAvail->where('id', $vehicle->id)->isNotEmpty()) {
                return $driverBooking;
            }
        });

        #validate bookingCheckInToday
        if($bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for today.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Todays checkin
        $todaysCheckInBookings = [];
        foreach ($bookingCheckInToday as $key => $bookingAllotDriver) {
            #set Bill data
            $booking        = $bookingAllotDriver->booking;
            $vehicle        = $bookingAllotDriver->booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'booking_id'        => $booking->id ?? '',
                'driver_id'         => $driver->id ?? '',
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $bookingAllotDriver->arrived_time ?? '',
                'driver_status'     => $bookingAllotDriver->fetch_booking_status ?? '',
                'supervsior_status' => $booking->supervisor != '' ? $booking->supervisor->fetch_booking_status :  '',
            ];

            #push
            array_push($todaysCheckInBookings, $data);
        }
           

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Todays checkin Data.',
            'success'       => true,
            'data'          => $todaysCheckInBookings
        ]);
    }

    /**
     * @method to fetch if any booking on vehicle and Parking which is not on Supervisor yet
     * @return json
     * @param Request $request
     */
    public function scanForBookingCheck(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Fetch Data
        $vehicleId = $request->vehicle_id ?? '';
        $bookingId = $request->booking_id ?? '';

        #Validate supervisor
        if($driver == '' OR ($vehicleId == '' AND $bookingId == '')) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Supervisor, vehicle_id, booking_id  not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Set relations
        $relations = [
            'driverBookings.parking.parkingSetupSpace',
            'driverBookings.parking.company',
            'driverBookings.booking.user',
            'driverBookings.booking.vehicle',
        ];

        #Fetch driver
        $driver = $this->wsd
                           ->with($relations)
                           ->driver()
                           ->find($driver->id);

        #FEtch Vehicle
        if($vehicleId != '') {
            $vehicle = $this->userVehicle
                        ->with(['user'])
                        ->find($vehicleId);
        } else {
            $booking = $this->booking->with(['vehicle'])->find($bookingId);

            #Validate booking
            if($booking == '') {
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Booking on booking_id  not found.',
                    'success'       => false,
                    'data'          => null
                ]);
            }

            #Fetch vehicle
            $vehicle = $booking->vehicle;
        }

        #Validate driver
        if($driver == ''OR $vehicle == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or vehicle not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Data for vehcile
        $vehicleData = [
            'vehicle_id'    => (string)$vehicle->id ?? '',
            'plate_number'  => (string)$vehicle->plate_number ?? '',
            'user_id'       => (string)$vehicle->user->id ?? '',
            'brand_name'    => (string)$vehicle->full_name ?? '',
            'color_name'    => (string)$vehicle->color_name ?? '',
            'color_code'    => (string)$vehicle->hex_code ?? '',
            'image'         => (string)$vehicle->image_full_path ?? '',
        ];

        #Set relations
        $relations = [
            'user', 
            'vehicle', 
            'promocode', 
            'parking.parkingSetupSpace', 
            'parking.parkingMarkOut'
        ];

        #Fetch all bookings on vehcile Id which arrival and exit date time is for future and not on any Supervior
        if($vehicleId != '') {
            $bookingsOnVehicle = $this->booking
                                  #->onInitialStatus()
                                  ->with($relations)
                                  ->where('vehicle_id', $vehicleId)
                                  ->get();
        } else {
            $bookingsOnVehicle = $this->booking
                                  #->onInitialStatus()
                                  ->with($relations)
                                  ->where('id', $bookingId)
                                  ->get();
        }

        #validate Bookings
        if($bookingsOnVehicle->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No Booking found on vehicle.',
                'success'       => true,
                'data'          => ['booking_data' => null, 'vehicle_data' => $vehicleData]
            ]);
        }

        #FEtch those bookings which are for future
        $futureBookings = $bookingsOnVehicle->filter(function($booking) {
            if(($booking->arrival_date_time->gt(Carbon::now()) OR 
                ($booking->arrival_date_time->diffInMinutes(Carbon::now()) <= 60)) 
                ) {
                return $booking;
            }
        });

        #Fetch Booking which is in Available to Pick Status
        $availToPickBookings = $futureBookings->filter(function($booking) {
            if($booking->is_in_avail_to_pick) {
                return $booking;
            }
        });
        
        #return if any booking is in Avail to Pick
        if($availToPickBookings->isNotEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Scan already.',
                'success'       => false,
                'data'          => ['booking_data' => null, 'vehicle_data' => $vehicleData]
            ]);
        }

        #Validate If no future booking
        if($futureBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No future Booking found on vehicle.',
                'success'       => true,
                'data'          => ['booking_data' => null, 'vehicle_data' => $vehicleData]
            ]);
        }

        #Fetch Supervisor Bookings
        $supervisorBookingsId = $this->supervisorBooking->get()->pluck('booking_id')->toArray();

        #Fetch those Future Bookings which not on any Supervsior
        $futureBookingsNotAlloted = $futureBookings->filter(function($futureBooking) use($supervisorBookingsId) {
            if(!in_array($futureBooking->id, $supervisorBookingsId)) {
                return $futureBooking;
            }
        });
        
        #Validate
        if($futureBookingsNotAlloted->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No future Booking found on vehicle.',
                'success'       => true,
                'data'          => ['booking_data' => null, 'vehicle_data' => $vehicleData]
            ]);
        }

        #first Bioking
        $booking = $futureBookingsNotAlloted->first();

        #Fetch Supervisor Bookings 
        #Fetch modals
        $parkingMarkout = $booking->parking->parkingMarkOut;
        $vehicle        = $booking->vehicle;
        $user           = $booking->user;
        
         #Set data for response
        $data = [
            'booking_data' => [
                'booking_id'                => $booking->id ?? '',
                'parking_type'              => $booking->parking_type ?? '',
                'booking_type'              => 'qr_code',
                'user_id'                   => (string)$booking->user->id ?? '',
                'user_name'                 => $user->name ?? '',
                'promocode_id'              => $booking->promocode_id,
                'promocode'                 => $booking->promocode,
                'promocode_discount'        => $booking->promocode_discount,
                'email'                     => $booking->user->email ?? '',
                'parking_image'             => $booking->parking->image_full_path ?? '',
                'parking_name'              => $booking->parking->name ?? '',
                'parking_location'          => $booking->parking->parkingSetupSpace->location ?? '',
                'parking_latitude'          => $booking->parking->parkingSetupSpace->latitude ?? '',
                'parking_longitude'         => $booking->parking->parkingSetupSpace->longitude ?? '',
                'in_time'                   => $booking->arrived_formatted ?? '',
                'out_time'                  => $booking->exit_formatted ?? '',
                'vehicle'                   => $booking->vehicle->full_name ?? '',
                'valet'                     => $booking->valet ? true : false,
                'washing'                   => $booking->wash_available ? true : false,
                'valet_price'               => $parkingMarkout->set_valet_price ?? '',
                'full_wash_price'           => $parkingMarkout->fetch_full_wash_cost,
                'exterior_wash_price'       => $parkingMarkout->fetch_exterior_wash_cost,
                'interior_wash_price'       => $parkingMarkout->fetch_interior_wash_cost,
                'valet_and_car_wash_price'  => $parkingMarkout->set_valet_and_washing_price ?? '',
                'subtotal'                  => $booking->sub_total ?? '',
                'total'                     => $booking->paid_money ?? '',
                'payment_way'               => $booking->payment_method ?? '',
                'booking_status'            => $booking->fetch_booking_status ?? '',
                'arrived_timestamp'         => $booking->arrived_timestamp ?? '',
            ],
            'vehicle_data' => $vehicleData
        ];

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Vehicle Scanned",
                        "body" => "Vehicle Scanned"
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "qr_scanned_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }
        

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking Data.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to Update Driver Booking Status
     * @return json
     * @param Request $request
     */
    public function updateBookingStatus(Request $request)
    {
        #Fetch Authenticated User
        $user       = Session::get('wsdUser');
        $status     = $request->status ?? '';
        $bookingId  = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';

        #Validate User
        if($user == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver or booking_type not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

         #validate booking ztype
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking Type is not valid',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set relations
        $relations = [
            'driverBookings',
            #'supervisorBookings.booking.user',
            #'supervisorBookings.booking.vehicle',
        ];

        #Fetch driver
        $driver = $this->wsd
                       ->with($relations)
                       ->driver()
                       ->find($user->id);

        #Validate driver
        if($driver == '' OR $status == '' OR $bookingId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver, status or booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Validate Status
        $validStatus = [2,7,8];    
        if(!in_array($status, $validStatus)) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Status is not Valid',
                'success'       => false,
                'data'          => null
            ]);
        }   

        if($bookingType == 'qr_code') {
            if(in_array($status, [2,7, 8])) {
                #Fetch all Alloted Bookings
                $driverBooking = $driver->driverBookings
                                        ->where('set_or_get', 'get')
                                        ->where('booking_id', $bookingId)
                                        ->sortByDesc('id');
            
            } else {
                #Fetch all Alloted Bookings
                $driverBooking = $driver->driverBookings
                                        ->where('set_or_get', 'set')
                                        ->where('booking_id', $bookingId)
                                        ->sortByDesc('id');

                #Update Driver booking to 0
                $driver->update(['current_booking_id' => 0]);
            
            }
        }

        #validate driverBooking
        // if($driverBooking->isEmpty()) {
        //     return response()->json([
        //         'statusCode'    =>  ApiStatusInterface::OK, 
        //         'message'       => 'No booking Found on this id.',
        //         'success'       => false,
        //         'data'          => null
        //     ]);
        // }

         #Fetch Fisrt supervisorBooking
        if($bookingType == 'qr_code') {
            $booking = $this->booking->with(['user'])->find($bookingId);
        } else {
            $booking = $this->scanOrPrivateBooking->with(['user'])->find($bookingId);
        }

        #VAlidate and Run
        if($bookingType == 'qr_code') {
            #VAlidate driver Booking
            if($driverBooking->isNotEmpty()) {
                  $driverBooking = $driverBooking->last();

                #Updste booking Status
                $driverBooking->update(['booking_status' => $status]);
            }
          

            #Validat status if completed reste Sueprvisoor booking to 0
            if($status ==  2) {
                #Update driver booking to 0
                $driver->update(['current_booking_id' => 0]);

                 #Update Booking Status to completed
                $booking->update(['booking_status' => UserBookingStatusInterface::BOOKING_COMPLETED]);
            }
        } else {
             #Update Booking Status to completed
                $booking->update(['booking_status' => UserBookingStatusInterface::BOOKING_COMPLETED]);
        }

         #Fetch User
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status changed.",
                        "body" => "Your Booking has been completed."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #return
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking Status Updated Successfully.',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to fetch all parkings on Driver
     * @return all Parkings
     * @param 
     */
    public function fetchAllParkings()
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                       ->with(['company.companyParking.parkingSetupSpace'])
                       ->driver()
                       ->find($driver->id);

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver companies
        $parkings = $driver->company->companyParking;

        #Validate Parkings
        if($parkings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No Parking found on Driver.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set Parkings
        $parkingArray = [];
        foreach ($parkings as $key => $parking) {
            $data = [
                'id'            => $parking->id ?? '',
                'name'          => $parking->name ?? '',
                'image'         => $parking->image_full_path ?? '',
                'parking_type'         => $parking->parking_type ?? '',
                'location'      => $parking->parkingSetupSpace->location ?? '',
                'latitude'      => $parking->parkingSetupSpace->latitude ?? '',
                'longitude'     => $parking->parkingSetupSpace->longitude ?? '',
                'total_slot'    => $parking->parkingSetupSpace->total_slot ?? 0,
            ];

            #push 
            array_push($parkingArray, $data);
        }

        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Parking found on Driver.',
            'success'       => true,
            'data'          => $parkingArray
        ]);
    }

     /**
     * @method to fetch Booking Detail
     * @param Request $request
     * @return json
     */
    public function bookingDetail(Request $request)
    {
        #Fetch Authenticated driver
        $driver = Session::get('wsdUser');
        $bookingId = $request->booking_id ?? '';
        $bookingType = $request->booking_type ?? '';

        #validate driver
        if($driver == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver, booking_id not found',
                'success'       => false,
                'data'          => null
            ]);
        }

       #validate BookingTpe
        if(!in_array($bookingType, ['qr_code', 'scan_me','private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid(qr_code, scan, private).',
                'success'       => false,
                'data'          => null
            ]);
        }

        if($bookingType == 'qr_code') {
            #FEtch booking
            $booking = $this->booking
                        ->with(['vehicle','supervisors'])
                        ->find($bookingId);
        } else {
            #FEtch booking
            $booking = $this->scanOrPrivateBooking
                        ->with(['vehicle','supervisors'])
                        ->find($bookingId);
        }
        
        #Validate booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found',
                'success'       => false,
                'data'          => null
            ]);
        }

       /* #relations
        $relations = [
            'bookings.vehicle',
            'scanPrivateBookings.vehicle',
            'bookings.supervisors.supervisor'
        ];

        #USer
        $user = $this->user->with($relations)->find($user->id);

        #FEtch booking
        if($bookingType == 'qrcode') {
            $booking = $user->bookings->where('id', $bookingId);
        } else {
             $booking = $user->scanPrivateBookings->where('id', $bookingId);
        }

        #Validate booking
        if($booking->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Sorry! Booking not found on this booking_id.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Booking
        $booking = $booking->first();

        #Validate if booking checked in or not
        if(!$booking->is_checked_in) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Sorry! Booking not checked in yet.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        #Fetch vehicle
        $vehicle = $booking->vehicle;

        #Set BookingStataus
        $bookingStatus = '';
        if($bookingType == 'qr_code') {
            if($booking->supervisors->isNotEmpty() OR $booking->drivers->isNotEmpty()) {
                if($booking->supervisors->isNotEmpty()) {
                    $bookingStatus = $booking->supervisors->last()->fetch_booking_status;
                } else {
                    $bookingStatus = $booking->drivers->last()->fetch_booking_status;
                }
            } else {
                $bookingStatus = $booking->fetch_booking_status;
            }
        } else {

        }

        #Set Data
        $data = [
            'booking_id'            => $bookingId,
            'parking_type'          => $booking->parking_type ?? '',
            'key_number'            => $booking->key_no ?? '',
            #'booking_type'          => 'qr_code',
            'booking_unique_id'     => $booking->order_number,
            'vehicle_id'            => $vehicle->id,
            'vehicle_model_name'    => $vehicle->full_name,
            'vehicle_plate_number'  => $vehicle->plate_number,
            'vehicle_image'         => $vehicle->image_full_path,
            'vehicle_color'         => $vehicle->color_name,
            'vehicle_service'       => $booking->valet ? 'Taken' : 'Not Taken',
            'wash_type'             => $booking->fetch_wash_type,
            'duration'              => '',
            'processing_time'       => '',
             'arrived_after_date'    => $booking->arrived_after_date,
            'arrived_after_time'    => $booking->arrived_after_time,
            'exit_after_date'       => $booking->exit_after_date,
            'exit_after_time'       => $booking->exit_after_time,
            'booking_type'          => $bookingType,
            'booking_status'        => $bookingStatus,
            'supervisor_name'       => $booking->supervisor_name,
            'employee_name'         => $booking->employee_name,
        ];
        
        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking Details found.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to set the Payment Requested on Booking
     * @return json Reposne
     * @param Request $request
     */
    public function requestForPayment(Request $request)
    {
        #Fetch Authenticated Driver
        $driver         = Session::get('wsdUser');
        $bookingId      = $request->booking_id ?? '';
        $bookingType    = $request->booking_type ?? '';

        #Validate driver
        if($driver == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver, booking_id, booking_type not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate BookingType
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch Booking
        if($bookingType == 'qr_code') {
            $booking = $this->booking->find($bookingId);
        } else {
            $booking = $this->scanOrPrivateBooking->find($bookingId);
        }

        #Validate Booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate is_payment_requested for Booking
        if($booking->is_payment_requested) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'You already requested for payment regarding this booking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Update payment requested on Booking
        $booking->update(['is_payment_requested' => true]);

        $user = $booking->user;

        $billData = [];
        #FEtch Bill Data
        if($booking->bill['status']) {
            $billData = $user->bill['data'];

            #SET more data
            $billData['user_address'] = $user->address;
            $billData['vehucle_type'] = $booking->vehicle->vehicle_type;
            $billData['vehicle_id'] = $booking->vehicle->id;
            $billData['parking_id'] = $booking->parking_id;
        }
        
        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                         "title" => "Payment Requested By ". $driver->name,
                        "body" => "Click to pay now."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "payment_requested_noti",
                        "bill" => $billData
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Payment Requested Successfully on Booking.',
            'success'       => true,
            'data'          => ['booking_id' => $bookingId, 'parking_type' => $bookingType]
        ]);
    }

    /**
     * @method checkedInBooking to checked in bookings
     * @return json
     * @param Request $request
     */
    public function checkedInBooking(Request $request)
    {
        #Fetch Authenticated driver
        $driver     = Session::get('wsdUser');
        $bookingId      = $request->booking_id ?? '';
        $parkingNo      = $request->slot_no ?? '';

        #Validate driver
        if($driver == '' OR $bookingId == '' OR $parkingNo == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver, booking_id, slot_no not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        #FEtch booking on Supervisor
        $bookingonDriver = $this->driverBooking->where('booking_id', $bookingId)->get();
        /*$bookingonSupervisor = $this->supervisorBooking->where('booking_id', $bookingId)->where('set_or_get', 'set')->get();

        #Validate if assigned already on supervisor or driver
        if($bookingonSupervisor->isNotEmpty() OR $bookingonDriver->isNotEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking already assigned to supervisor or driver.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        #fetch booking on User
        $booking = $this->booking->with(['user'])->find($bookingId);

        #Validate booking
        if($booking == '') {
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #set Unique key Number
        $bookingCount = ($this->driverBooking->get()->count() > 0) ? 
                            $this->driverBooking->get()->count() + 1 : 1;

        $uniquekeyNumber = 'KEY_'.'0000'.$bookingCount;

       /* #Set data to allot booking on Supervisor
        $data = [
            'parking_id'            => $booking->parking_id,
            'parking_type'          => $booking->parking_type ?? '',
            'booking_id'            => $booking->id,
            'driver_id'             => $driver->id,
            'set_or_get'            => 'set',
            'key_no'                => $uniquekeyNumber,
            'park_no'               => $parkingNo,
            'pick_up_lat'           => '',
            'pick_up_long'          => '',
            'booking_status'        => DriverBookingStatusInterface::PARKED,
            'arrived_date_time'     => Carbon::now()->format('Y-m-d H:i:s'),
            'parked_date_time'      => Carbon::now()->format('Y-m-d H:i:s')      
        ];

        #Assigned booking to Driver
        $this->driverBooking->create($data);*/

        #mark booking as checked in
        $booking->update([
            'is_checked_in' => true,  
            'actual_in_dateTime' => Carbon::now()->format('Y-m-d H:i:s'), 
            'booking_status' => UserBookingStatusInterface::VEHICLE_PARKED
        ]);

        #Update Supervisor Bookin g Status
        $bookingonDriver->last()
                          ->update(['booking_status' => DriverBookingStatusInterface::PARKED, 'park_no' => $parkingNo]);

         #Fetch User
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status chnaged.",
                        "body" => "Your vehicle is Parked."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }
        
        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking assigned to Driver.',
            'success'       => true,
            'data'          => null
        ]);
    }
    

    /**
     * @method to fetch all parkings on Supervsior
     * @return all Parkings
     * @param 
     */
    public function updateCurrentParking(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #FEtch parkingId
        $parkingId = $request->parking_id ?? '';

        #Validate driver
        if($driver == '' OR $parkingId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver, parking_id  not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           ->driver()
                           ->find($driver->id);

        #Validate Supervisor
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Update driver current parking Id
        $driver->update(['current_parking_id' => $parkingId]);

        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Parking found on driver.',
            'success'       => true,
            'data'          => null
        ]);
    }

    /**
     * @method to create booking for User if not avail and allot Supervisor
     * @param Request $request
     * @return json
     */
    public function createUserBooking(Request $request)
    {
        #Fetch Data
        $vehicleId = $request->vehicle_id ?? '';
        $bookingId = $request->booking_id ?? '';
        $valetAvailable = $request->valet ?? '';

        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '' OR $vehicleId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver, vehicleId  not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch vehcile
        $vehicle = $this->userVehicle->with(['user'])->find($vehicleId);
      
        #Validate vehicle
        if($vehicle == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'vehicle not found on vehicle_id.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate valet avaialble
        if(!in_array($valetAvailable, [true, false, null])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'valet values are wrong.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           ->driver()
                           ->find($driver->id);

        #Validate driver
        if($driver == '' OR $driver->current_parking_id == 0) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or driver current parking Id not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate Booking 
        $booking = '';
        if($bookingId != '') {
            $booking = $this->booking->find($bookingId);

            #Validate Booking
            if($booking == '') {
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Booking not found on booking_id.',
                    'success'       => false,
                    'data'          => null
                ]);
            }
        } else {
            #Cerate Unique Order Number
            $today = time();
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
            $unique = 'PP'.$today . $rand;

            #create booking for user
            $data = [
                'order_number'      => $unique,
                'parking_type'      => 'qr_code',
                'user_id'           => $vehicle->user->id,
                'vehicle_id'        => $vehicleId,
                'parking_id'        => $driver->current_parking_id,
                'booking_price'     => $driver->currentParking->parkingMarkOut != '' ? 
                                       $driver->currentParking->parkingMarkOut->set_book_price : '',
                'booking_status'    => UserBookingStatusInterface::ON_THE_WAY_PARK,
                'arrival_date_time' => Carbon::now()->format('Y-m-d H:i:s'),
                'valet'             => $valetAvailable ? true : false,
            ];

            #Create booking
            $booking = $this->booking->create($data);
        }

        #set Unique key Number
        $bookingCount = ($this->driverBooking->get()->count() > 0) ? 
                            $this->driverBooking->get()->count() + 1 : 1;

        $uniquekeyNumber = 'KEY_'.'0000'.$bookingCount;

        #Allot booking to driver
        $data = [
            'parking_id'        => $driver->current_parking_id,
            'booking_id'        => $booking->id,
            'driver_id'         => $driver->id,
            'key_no'            => $uniquekeyNumber,
            'booking_status'    => DriverBookingStatusInterface::ON_THE_WAY_TO_PARK,
            'arrived_date_time' => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        # Allot booking to Driver Before that check if already alloted to Supervsior
        $bookingOnDriver = $this->driverBooking->where('booking_id', $bookingId)->get();
        if($bookingOnDriver->isEmpty()) {
            $driverBooking = $this->driverBooking->create($data);
        }

        #Assign Current booking Id and Switch Screen user to True to User
        $user = $vehicle->user;
        $user->update(['current_booking_id' => $booking->id, 'current_booking_type' => 'qr_code', 'switch_screen' => true]);

        #Assign booking to Supervisor
        $driver->update(['current_booking_id' => $booking->id]);

        #Update Booking
        $booking->update(['arrival_date_time' => Carbon::now()->format('Y-m-d H:i:s')]);

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status chnaged.",
                        "body" => "Status updated to on the way to Park."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking alloted to Driver.',
            'success'       => true,
            'data'          => ['booking_id' => $booking->id, 'booking_type' => 'qr_code']
        ]);
    }

    /**
     * @method to save payment Mode on Booking
     * @param Request $request
     * @return json
     */
    public function savePaymentMode(Request $request)
    {
        #fetch bookingId
        $bookingId      = $request->booking_id ?? '';
        $bookingType    = $request->booking_type ?? '';
        $paymentMethod  = $request->payment_method ?? '';

        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #validate driver
        if($driver == '' OR $bookingId == '' OR $paymentMethod == '' OR !in_array($paymentMethod, ['cash', 'card'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver, booking_id, payment_method not found or payment_method is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #validate Booking Type
        if($bookingType == '' OR !in_array($bookingType, ['qr_code', 'scan_or_private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not found or not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #FEtch Booking
        if($bookingType == 'qr_code') {
            $booking = $this->booking->find($bookingId);
        } else {
            $booking = $this->scanOrPrivateBooking->find($bookingId);
        }

        #Validate Booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found on user.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate if Payment method already on booking
        if($booking->payment_method != '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Payment method is already exist on booking.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Set Payment method on Booking
        $booking->update(['payment_method' => $paymentMethod]);

        #return 
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Payment Method set on Booking.',
            'success'       => true,
            'data'          => []
        ]);
    }

    /**
     * @method to create booking for User if not avail and allot Supervisor
     * @param Request $request
     * @return json
     */
    public function createUserBookingOnClose(Request $request)
    {
        #Fetch Data
        $vehicleId = $request->vehicle_id ?? '';
        $bookingId = $request->booking_id ?? '';
        $valetAvailable = $request->valet ?? '';

        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '' OR $vehicleId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver, vehicleId  not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch vehcile
        $vehicle = $this->userVehicle->with(['user'])->find($vehicleId);
      
        #Validate vehicle
        if($vehicle == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'vehicle not found on vehicle_id.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate valet avaialble
        if(!in_array($valetAvailable, [true, false, null])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'valet values are wrong.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           ->driver()
                           ->find($driver->id);

        #Validate driver
        if($driver == '' OR $driver->current_parking_id == 0) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or driver current parking Id not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate Booking 
        $booking = '';
        if($bookingId != '') {
            $booking = $this->booking->find($bookingId);

            #Validate Booking
            if($booking == '') {
                return response()->json([
                    'statusCode'    =>  ApiStatusInterface::OK, 
                    'message'       => 'Booking not found on booking_id.',
                    'success'       => false,
                    'data'          => null
                ]);
            }

            #Update Booking Status
            $booking->update([
                'booking_status' => UserBookingStatusInterface::AVAILABLE_TO_PICK,
                'scan_timestamp' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

        } else {
            #Cerate Unique Order Number
            $today = time();
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
            $unique = 'PP'.$today . $rand;

            #create booking for user
            $data = [
                'order_number'      => $unique,
                'parking_type'      => 'qr_code',
                'user_id'           => $vehicle->user->id,
                'vehicle_id'        => $vehicleId,
                'parking_id'        => $driver->current_parking_id,
                'booking_price'     => $driver->currentParking->parkingMarkOut != '' ? 
                                       $driver->currentParking->parkingMarkOut->set_book_price : '',
                'booking_status'    => UserBookingStatusInterface::AVAILABLE_TO_PICK,
                'arrival_date_time' => Carbon::now()->format('Y-m-d H:i:s'),
                'scan_timestamp'    => Carbon::now()->format('Y-m-d H:i:s'),
                'valet'             => $valetAvailable ? true : false,
            ];

            #Create booking
            $booking = $this->booking->create($data);

            #Set Booking id
            $data ['booking_id'] = $booking->id;
            $data ['booking_type'] = 'qr_code';
        }

        #Fetch Sacn time stamp
        // $data = [
        //     'scan_timestamp' => $booking->scan_timestamp->timestamp
        // ];

        $data['scan_timestamp'] = $booking->scan_timestamp->timestamp;
        $data['booking_id'] = $booking->id;
        $data['booking_type'] = 'qr_code';

        #Fetch user
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status chnaged.",
                        "body" => "Status updated to available to pick."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking created for user.',
            'success'       => true,
            'data'          => $data
        ]);
    }

    /**
     * @method to fetch all bookings on Supervisor on todays arrival
     * @param Request $request
     * @return json
     */
    public function todaysBookingList(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '' OR $driver->current_parking_id == 0) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or Current  not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           ->with(['driverBookings.booking.vehicle'])
                           ->driver()
                           ->find($driver->id);

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #vAlidate driverBookings
        #$allBookings = $driver->driverBookings;
        $allBookings = $this->driverBooking->where('parking_id', $driver->current_parking_id)->get();
        if($allBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No booking found on Driver.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch todays booking only
        $driverBookings = $allBookings->filter(function($booking) {
            if($booking->arrived_date_time->diffInDays(Carbon::now()) == 0) {
                return $booking;
            }
        });
        
        #Validate Todays Booking
        if($driverBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No booking found on Driver for today.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch BookingIds
        $bookingIds = $driverBookings->pluck('booking_id')->toArray();

        #Count array values
        $countBookingIds = array_count_values($bookingIds);

        #Set bookign today
        $bookingTodaysValid = collect();
        foreach(array_unique($bookingIds) as $bookingId) {
            if($countBookingIds[$bookingId] == 2) {
                $bookingTodaysValid->push($driverBookings->where('booking_id', $bookingId)->where('set_or_get', 'get')->first());
            } else {
                $bookingTodaysValid->push($driverBookings->where('booking_id', $bookingId)->where('set_or_get', 'set')->first());
            }
        }

        #Set data
        $todaysBookings = [];
        foreach ($bookingTodaysValid as $key => $bookingAllotDriver) {
            #set Bill data
            $booking        = $bookingAllotDriver->booking;
            $vehicle        = $bookingAllotDriver->booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'parking_type'      => $booking->parking_type ?? '',
                'booking_id'        => $booking->id ?? '',
                'supervisor_id'     => $supervisor->id ?? null,
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $bookingAllotDriver->arrived_time ?? '',
                'status'            => $bookingAllotDriver->fetch_booking_status ?? '',
            ];

            #push
            array_push($todaysBookings, $data);
        }

        #return json
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking found on Driver for today.',
            'success'       => true,
            'data'          => $todaysBookings
        ]);
    }

    /**
     * @method to fetch all bookings on Supervisor on futureBookingList arrival
     * @param Request $request
     * @return json
     */
    public function futureBookingList(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch driver
        $driver = $this->wsd
                           ->with(['driverBookings.booking.vehicle', 'currentParking'])
                           ->driver()
                           ->find($driver->id);

        #Validate driver
        if($driver == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

         #Fetch Supervisor Current Parking
        $currentParking = $driver->currentParking;

        #Validate Supervisor
        if($currentParking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Driver current booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch all bookings of User based on parkingId
        $allBookings = $this->booking
                            ->with(['vehicle'])
                            ->where('parking_id', $currentParking->id)
                            ->get();

        #validate
        if($allBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No booking found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch todays booking only
        $allBookings = $allBookings->filter(function($booking) {
            if($booking->arrival_date_time->gt(Carbon::now()) AND $booking->arrival_date_time->diffInDays(Carbon::now()) > 0) {
                return $booking;
            }
        });
        
        #vAlidate driverBookings
        if($allBookings->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No booking found on Driver.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Set data
        $todaysBookings = [];
        foreach ($allBookings as $key => $booking) {
            #set Bill data
            $vehicle        = $booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'parking_type'      => $booking->parking_type ?? '',
                'booking_id'        => $booking->id ?? '',
                'driver_id'         => $driver->id ?? '',
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $booking->arrived_time ?? '',
                'status'            => $booking->fetch_booking_status ?? '',
            ];

            #push
            array_push($todaysBookings, $data);
        }

        #return json
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking found on Supervsior for today.',
            'success'       => true,
            'data'          => $todaysBookings
        ]);
    }

    /**
     * @method to fetch search future checkin
     * @return json
     * @param Request $request
     */
    public function searchfutureBookingList(Request $request)
    {
        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Fetch bookingId
        $bookingId = $request->booking_id ?? '';

        #Validate driver
        if($driver == '' ) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
       
        #Set relations
        $relations = [
            'driverBookings.parking.parkingSetupSpace',
            'driverBookings.parking.company',
            'driverBookings.booking.user',
            'driverBookings.booking.vehicle',
        ];

        #Fetch driver
        $driver = $this->wsd
                           ->with($relations)
                           ->driver()
                           ->find($driver->id);

        #Fetch Serach String
        $searchString = $request->search_string ?? '';

        #Validate driver
        if($driver == '' OR $searchString == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or search_string not found.',
                'success'       => false,
                'data'          => null
            ]);
        }
        
        #Fetch booking
        $bookingAllotDriver = $driver->driverBookings;
    
        #validate Bookings
        if($bookingAllotDriver->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Checking Today
        $bookingCheckInToday = $bookingAllotDriver->filter(function($driverBooking) {
            if($driverBooking->arrived_date_time->diffInDays(Carbon::now()) > 0) {
                return $driverBooking;
            }
        });

        #validate bookingCheckInToday
        if($bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for future.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Search String based Bookings
        $bookingCheckInToday = $bookingCheckInToday->filter(function($driverBooking) use($searchString) {
            $booking = $driverBooking->booking;
            $vehicle = $booking->vehicle;
           
            #Fetch Vehicle If Exist
            $vehicleAvail = $this->userVehicle
                                 ->where('plate_number', 'Like', $searchString)
                                 ->orWhere('brand_name', 'Like', $searchString)
                                 ->orWhere('modal_name', 'Like', $searchString)
                                 ->orWhere('color_name', 'Like', $searchString)
                                 ->get();
            #Validate
            if($vehicleAvail->where('id', $vehicle->id)->isNotEmpty()) {
                return $driverBooking;
            }
        });

        #validate bookingCheckInToday
        if($bookingCheckInToday->isEmpty()) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'No checkin for future.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Todays checkin
        $todaysCheckInBookings = [];
        foreach ($bookingCheckInToday as $key => $bookingAllotDriver) {
            #set Bill data
            $booking        = $bookingAllotDriver->booking;
            $vehicle        = $bookingAllotDriver->booking->vehicle;

            #Set data
            $data = [
                'parking_id'        => $booking->parking_id ?? '',
                'booking_id'        => $booking->id ?? '',
                'driver_id'         => $driver->id ?? '',
                'vehicle_plate_no'  => $vehicle->plate_number ?? '',
                'vehicle_name'      => $vehicle->full_name ?? '',
                'arrived_time'      => $bookingAllotDriver->arrived_time ?? '',
                'driver_status'     => $bookingAllotDriver->fetch_booking_status ?? '',
                'supervsior_status' => $booking->supervisor != '' ? $booking->supervisor->fetch_booking_status :  '',
            ];

            #push
            array_push($todaysCheckInBookings, $data);
        }
           

        #return data
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Todays checkin future.',
            'success'       => true,
            'data'          => $todaysCheckInBookings
        ]);
    }


    /**
     * @method to approve Cash Payment of any Booking
     * @return json
     * @param Request $request
     */
    public function markAsPaid(Request $request)
    {
        #Fetch BookingId
        $bookingId = $request->booking_id;
        $bookingType = $request->booking_type;

        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '' OR $bookingId == '' OR $bookingType == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or Bookking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate BookingType
        if(!in_array($bookingType, ['qr_code', 'scan_me', 'private'])) {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'booking_type is not valid.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Fetch Booking
        if($bookingType == 'qr_code') {
            $booking = $this->booking->with(['user','payment'])->find($bookingId);
        } else {
            $booking = $this->scanOrPrivateBooking->with(['user','payment'])->find($bookingId);
        }

        #Validate driver
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate Booking on driver
        $driverBooking = $driver->driverBookings
                                        ->where('set_or_get', 'get')
                                        ->where('booking_id', $bookingId);

        #Validate Booking
        // if($driverBooking->isEmpty()) {
        //     return response()->json([
        //         'statusCode'    =>  ApiStatusInterface::OK, 
        //         'message'       => 'Booking not found on Driver.',
        //         'success'       => false,
        //         'data'          => null
        //     ]);
        // }

        #Check booking on same parking if as on Supervisor
        if($booking->parking_id != $driver->current_parking_id) {
             return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not available on Driver Parking.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #Validate payment
        /*if($booking->payment != '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking Payment is already Done.',
                'success'       => false,
                'data'          => null
            ]);
        }*/

        #Fetch total money to pay
        $paidMoney = $booking->bill['status'] ? $booking->bill['data']['payable_money'] : 0;

        #Do Payment and Update Sttaus
        if($bookingType == 'qr_code') {
            if($driverBooking->isNotEmpty() AND $driverBooking->last()->is_bill_generated) {
                if($booking->payment != '') {
                    $booking->update(['is_paid' => true]);
                    $driver->update(['current_booking_id' => 0]);
                    $booking->user->update(['current_booking_id' => 0]);
                } else {
                    $data = [
                        'booking_id'    => $booking->id,
                        'paid_money'    => $paidMoney,
                        'payment_type'  => 'cash',
                    ];

                    #create Payment
                    $this->payment->create($data);

                    #Update 
                    $booking->update(['is_paid' => true]);
                    $driver->update(['current_booking_id' => 0]);
                    $booking->user->update(['current_booking_id' => 0]);

                }
            } else {
                if($booking->payment == '') {
                    $data = [
                        'booking_id'    => $booking->id,
                        'paid_money'    => $paidMoney,
                        'payment_type'  => 'cash',
                    ];

                    #create Payment
                    $this->payment->create($data);

                    $booking->update(['is_paid' => true]);
                } else {
                    $booking->update(['is_paid' => true]);
                }
            }
        } else {
            if($booking->payment->isEmpty()) {
                $data = [
                    'booking_id'    => $booking->id,
                    'paid_money'    => $paidMoney,
                    'payment_type'  => 'cash',
                ];

                #create Payment
                $this->paymentForScanPrivate->create($data);

                $booking->update(['is_paid' => true, 'booking_status' => UserBookingStatusInterface::BOOKING_COMPLETED]);
            } else {
                $booking->update(['is_paid' => true, 'booking_status' => UserBookingStatusInterface::BOOKING_COMPLETED]);
            }
        }

        #Fetch User
        $user = $booking->user;

        #validate User token
        if($user->device_token != '' AND in_array($user->device_type, [1,2])) {
            #validate deviceType
            if($user->device_type == 0) {
                #Call Android Notification
            } else {
                #call Ios Notification
                $postData = [
                    "registration_ids" => [$user->device_token],
                    "notification" => [
                        "title" => "Status changed.",
                        "body" => "Your Booking has been completed."
                    ],
                    "mutable_content" => true,
                    "content_available" => true,
                    "data" => [
                        "type"=> "booking_status_changed_noti"
                    ]
                ];

                #Call method and Fetch Status
                $status = $this->sendPushNotificationIos($postData);
            }
        }

        #return json
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Paid Successfully',
            'success'       => true,
            'data'          => []
        ]);
    }

     /**
     * @author Nishikant Tysgi
     * @method to checkout the booking
     * @retunr json
     * @param Request $request
     */
    public function checkout(Request $request)
    {
       #Fetch BookingId
        $bookingId = $request->booking_id;

        #Fetch Authenticated User
        $driver = Session::get('wsdUser');

        #Validate driver
        if($driver == '' OR $bookingId == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'driver or Bookking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

        #FEtch booking
        $booking = $this->booking->find($bookingId);
        #Fetch driver
        $driver = $this->wsd
                           ->with(['company.companyParking', 'currentParking', 'driverBookings'])
                           ->driver()
                           ->find($driver->id);

        #Validate booking
        if($booking == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'       => 'Booking not found.',
                'success'       => false,
                'data'          => null
            ]);
        }

         #set Unique key Number
        $bookingCount = ($this->driverBooking->get()->count() > 0) ? 
                            $this->driverBooking->get()->count() + 1 : 1;

        $uniquekeyNumber = 'KEY_'.'0000'.$bookingCount;

        #check wether valet is choosen or not
        if($booking->is_valet_available) {
            $data = [
                'parking_id'           => $booking->parking_id,
                'booking_id'           => $booking->id,
                'driver_id'             => $driver->id,
                'set_or_get'           => 'get',
                'key_no'               => $uniquekeyNumber,
                #'park_no'              => ,
                #'pick_up_lat'          => ,
                #'pick_up_long'         => ,
                'booking_status'       => DriverBookingStatusInterface::ON_THE_WAY_TO_DROPD,
                #'arrived_date_time'    => ,
                'exit_date_time'       => Carbon::now()->format('Y-m-d H:i:s'),
            ];

            #assign Booking in Driver Set
            $this->driverBooking->create($data);
        } else {
            #FEtch booking on Supervisor
            $driverBooking = $driver->driverBookings
                                            ->where('booking_id', $bookingId)
                                            ->where('set_or_get', 'set');
           
            #Validate supervisor Booking
            if($driverBooking->isNotEmpty()) {
                $driverBooking->first()->update([
                    'booking_status' => DriverBookingStatusInterface::BILL_GENERATEDD
                ]);
            } else {
                #Set data
                $data = [
                    'parking_id'           => $booking->parking_id,
                    'booking_id'           => $booking->id,
                    'driver_id'             => $driver->id,
                    'set_or_get'           => 'set',
                    'key_no'               => $uniquekeyNumber,
                    #'park_no'              => ,
                    #'pick_up_lat'          => ,
                    #'pick_up_long'         => ,
                    'booking_status'       => DriverBookingStatusInterface::BILL_GENERATEDD,
                    #'arrived_date_time'    => ,
                    'exit_date_time'       => Carbon::now()->format('Y-m-d H:i:s'),
                ];

                #assign Booking in Supervisor Set
                $this->driverBooking->create($data);
            }
           
        }

        #return response
        return response()->json([
            'statusCode'    =>  ApiStatusInterface::OK, 
            'message'       => 'Booking assigned.',
            'success'       => true,
            'data'          => null
        ]);
    }
}