<?php

namespace App\Http\Controllers\Api\WSD;

use DB;
use Log;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;

# Models
use App\Models\WSDOtp;
use App\Models\Washerman;

# Interface
use App\Interfaces\ApiStatusInterface;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller implements ApiStatusInterface
{
    # Bind api token
    private $apiToken;

    # Variable $wsd(washer, supervisor or Driver)
    protected $wsd;
    
    # Variable $wsdOtp
    protected $wsdOtp;

	/**
	 * @method constructor for Controller
	 * @param 
	 * @return 
	 */
	public function __construct(Washerman $wsd, WSDOtp $wsdOtp)
	{
        $this->apiToken   = uniqid(base64_encode(\Str::random(128)));
		$this->wsd        = $wsd;
        $this->wsdOtp     = $wsdOtp;
	}

    /**
     * @method to verify New email or Phone of User and Generate Otp
     * @param Request $request
     * @return Json
     */
    public function generateOtp(Request $request)
    {
        # Fetch mobile
        $mobile         = $request->get('mobile');
        $mobileCode     = $request->get('mobile_code');

        #Validate required Field
        if($mobile == '' OR $mobileCode == '') {
            return response()->json([
                'statusCode'    =>  ApiStatusInterface::OK, 
                'message'   => 'mobile and mobileCode is required.',
                'success'   => false,
                'data'      => null
            ]);
        }

        # Fetch User
        $user = $this->wsd
                     ->where('mobile', $mobile)
                     ->where('country_code', $mobileCode)
                     ->get();

        if($user->isEmpty()) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'user not found with this mobile number.',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        #Create Otp for User
        $otp = mt_rand(1000, 9999);
        $otp = 1234;

        #Create wsd Otp
        $otpData = [
            'mobile'       => $mobile,
            'mobile_code'  => $mobileCode,
            'otp'          => $otp
        ];

        #Create Otp for User
        $this->wsdOtp->create($otpData);

        #return response
        $response = [
            'statusCode' => ApiStatusInterface::OK,
            'message'    => 'Otp sent on mobile.',
            'success'    => true,
            'data'       => ['mobile' => $mobile,'otp' => $otp]
        ];

        return response()->json($response);
    }

    /**
     * @method login
     * @param $data
     * @return json
     */
    public function login(Request $request)
    {
        # Fetch Data
        $otp            = $request->get('otp') ?? '';
        $mobile         = $request->get('mobile') ?? '';
        $mobileCode     = $request->get('mobile_code') ?? '';
        $deviceType     = $request->get('device_type') ?? '';
        $deviceToken    = $request->get('device_token') ?? '';

        #Validate Mobile Code
        if($mobileCode == '' OR $mobile == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'mobile and mobile code is required.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validate otp
        if($otp == '') {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'otp is required.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        # Ftech user
        $user = $this->wsd
                     ->where('country_code', $mobileCode)
                     ->where('mobile', $mobile)
                     ->get();

        if($user->isEmpty()) {
            # Set Response
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'User Not Found.',
                'success'   => false,
                'data'      => null
            ];

            return $response;
        }
        
        #Validate deviceType and token
        if($deviceType == '' OR ($deviceType != 1 AND $deviceType != 2)) {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'invalid device_type.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        #Validate devicetoken
        if($deviceToken == '') {
            $response = [
                'statusCode'    => ApiStatusInterface::OK,
                'message'   => 'device_token is required.',
                'success'   => false,
                'data'      => null
            ];

            #return Response
            return $response;
        }

        #Fetch uer Otps
        $userOtps = $this->wsdOtp
                         ->where('mobile', $mobile)
                         ->where('mobile_code', $mobileCode)
                         ->get();

        if($userOtps->isEmpty()) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'otp not found in system.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return $response;
        }

        #Validate Otp
        if($userOtps->last()->otp != $otp) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp not matched.',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Verify Otp Validity
        if($userOtps->last()->created_at->diffInMinutes(Carbon::now()) > 20) {
            #return response
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Otp has been expired please resend.',
                'success'    => false,
                'data'       => null
            ];

            return $response;
        }

        # Fetch 
        $user = $user->first();

        # Set user type and token
        $userData = [
            'device_type'   => $deviceType, 
            'device_token'  => $deviceToken, 
            'token'         => $this->apiToken
        ];

        # Update User
        $user->update($userData);

        # Fetch User
        $wsd = $this->wsd->find($user->id);

        #Fetch wsd Data
        $wsdData = [
            'token'         => $wsd->token,
            'id'            => (string)$wsd->id ?? '',
            'email'         => (string)$wsd->email ?? '',
            'user_type'     => (string)$wsd->job_type ?? '',
            'unique_id'     => (string)$wsd->washer_auto_id ?? '',
            'name'          => (string)$wsd->name ?? '',
            'mobile_code'   => (string)$wsd->country_code ?? '',
            'mobile'        => (string)$wsd->mobile ?? '',
            'image'         => (string)$wsd->image_full_path ?? '',
            'country_name'  => (string)$wsd->country ?? '',
            'city'          => (string)$wsd->city ?? '',
            'device_type'   => (string)$wsd->device_type ?? '',
            'device_token'  => (string)$wsd->device_token ?? '',
        ];

       $response = [
            'statusCode'    => ApiStatusInterface::OK,
            'message'   => 'Login Successfully.',
            'success'   => true,
            'data'      => $wsdData
        ];

        return $response;
    }

    /**
     *
     * Block comment
     *
     */
    public function fetchUser(Request $request)
    {
        dd(Session::get('wsdUser'));
    }
}