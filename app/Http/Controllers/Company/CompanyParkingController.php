<?php

namespace App\Http\Controllers\Company;

use Auth;
use \Milon\Barcode\DNS2D;
use App\Utils\CommonUtil;
use Illuminate\Http\Request;

#Models
use App\Models\ParkingFeature;
use App\Models\CompanyParking;
use App\Models\ParkingMarkOut;
use App\Models\ParkingSetupSpace;
use App\Models\ParkingDescription;
use App\Models\ParkingPayoutSetting;
use App\Models\ParkingAdvanceSetting;
use App\Models\ParkingDriverFeature;
use App\Models\ParkingWasherFeature;
use App\Models\ParkingBookingFeature;
use App\Models\ParkingSpaceImage;
use App\Models\ParkingSubscriptionFeature;

#VEndor
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CompanyParkingController extends Controller
{
    # Bind the util 
    protected $commonUtil;

    # Bind the CompanyParking Model
    protected $companyParkings;

    # Bind the ParkingSetupSpace Model
    protected $parkingSetupSpace;

    # Bind the ParkingMarkOut Model
    protected $parkingMarkOut;

    # Bind the ParkingDescription Model
    protected $parkingDescription;

    # Bind the ParkingPayoutSetting Model
    protected $parkingPayoutSetting;

    # Bind the ParkingSpaceImage Model
    protected $parkingSpaceImages;

    # Bind the ParkingAdvanceSetting Model
    protected $parkingAdvanceSetting;

    # Bind the ParkingDriverFeature Model
    protected $parkingDriverFeature;

    # Bind the ParkingSubscriptionFeature Model
    protected $parkingSubscriptionFeature;

    # Bind the ParkingWasherFeature Model
    protected $parkingWasherFeature;

    # Bind the ParkingBookingFeature Model
    protected $parkingBookingFeature;

    # Bind the ParkingFeature Model
    protected $parkingFeature;

    # Bind the view path of company
    protected $view = 'company.company_parking.';

    # Bind the type for create sequence
    protected $type = 'parking';

    /**
     * Define the constructor of controller
     *
     * @return bind models
     */
    public function __construct(
                                CommonUtil $commonUtil,
                                CompanyParking $companyParkings,
                                ParkingMarkOut $parkingMarkOut,
                                ParkingSetupSpace $parkingSetupSpace,
                                ParkingDescription $parkingDescription,
                                ParkingPayoutSetting $parkingPayoutSetting,
                                ParkingAdvanceSetting $parkingAdvanceSetting,
                                ParkingDriverFeature $parkingDriverFeature,
                                ParkingWasherFeature $parkingWasherFeature,
                                ParkingSubscriptionFeature $parkingSubscriptionFeature,
                                ParkingBookingFeature $parkingBookingFeature,
                                ParkingFeature $parkingFeature,
                                ParkingSpaceImage $parkingSpaceImages
                                )
    {
        $this->commonUtil            = $commonUtil;
        $this->companyParkings       = $companyParkings;
        $this->parkingMarkOut        = $parkingMarkOut;
        $this->parkingSetupSpace     = $parkingSetupSpace;
        $this->parkingDescription    = $parkingDescription;
        $this->parkingPayoutSetting  = $parkingPayoutSetting;
        $this->parkingAdvanceSetting = $parkingAdvanceSetting;
        $this->parkingDriverFeature  = $parkingDriverFeature;
        $this->parkingWasherFeature  = $parkingWasherFeature;
        $this->parkingSubscriptionFeature  = $parkingSubscriptionFeature;
        $this->parkingBookingFeature  = $parkingBookingFeature;
        $this->parkingFeature         = $parkingFeature;
        $this->parkingSpaceImages         = $parkingSpaceImages;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Gather the login company id.
        $companyId = Auth::guard('company')->user()->id;

        # gather the company parkings.
        $companyParkings = $this->companyParkings->onlyCompany($companyId)->get();

        # display the company parking list
        return view($this->view.'index', compact('companyParkings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # get company last refcount
        $refCount = $this->commonUtil->lastReferenceCount($this->type);

        # create company unique id and get company sequence.
        $companyParkingSequence = $this->commonUtil->genrateSequences($refCount, $this->type);

        # show create page.
        return view($this->view.'create', compact('companyParkingSequence'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'parking_type'  =>  'required',
            'parking_name'  =>  'required',
        ]);

        # Get Auth Company
        $companyId = $this->commonUtil->getLoginCompanyId();
        $numberOfParkingAllow = Auth::guard('company')->user()->number_of_parking ?? 0;

        # get count of parking
        $companyParkingCount = $this->companyParkings->onlyCompany($companyId)->count();

        if ($numberOfParkingAllow <= $companyParkingCount) {
            return redirect()->back()->with(['status' => 200, 'success_message' => 'You can not add more than '.$numberOfParkingAllow.' parking on company']);
        }

        # Gather the form data.
        $companyParkingData = [
            'parking_auto_id' => $request->get('parking_auto_id'),
            'name'            => $request->get('parking_name'),
            'company_id'      => $companyId,
            'parking_type'    => $request->get('parking_type'),
            'city'            => $request->get('city'),
            'country'         => $request->get('country'),
            'location'        => $request->get('location'),
        ];

        # upload file
        if(Input::hasFile('parking_image')) {
            $filename        = $request->file('parking_image')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/company_parking_image/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('parking_image')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $companyParkingData['image_path'] = 'assets/images/company_parking_image/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               // return response()->json($output);
            }
        }

        # create a company parking
        $this->companyParkings->create($companyParkingData);

        # update ref. count of company parking
        $refCount = $this->commonUtil->referenceCount($this->type);

        return redirect()->back()->with(['success_message' => 'Parking created successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        # get parking data
        $parking = $this->companyParkings->findOrFail($id);

        # show edit page.
        return view($this->view.'show', compact('parking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # get parking data
        $parking = $this->companyParkings->findOrFail($id);

        # show edit page.
        return view($this->view.'edit', compact('parking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'parking_type'  =>  'required',
            'parking_name'  =>  'required',
        ]);

        # Gather the form data.
        $companyParkingData = [
            'name'            => $request->get('parking_name'),
            'parking_type'    => $request->get('parking_type'),
            'city'            => $request->get('city'),
            'country'         => $request->get('country'),
            'location'        => $request->get('location'),
        ];

        # upload file
        if(Input::hasFile('parking_image')) {
            $filename        = $request->file('parking_image')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/company_parking_image/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('parking_image')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $companyParkingData['image_path'] = 'assets/images/company_parking_image/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               // return response()->json($output);
            }
        }

        # update a company parking
        $this->companyParkings->findOrFail($id)->update($companyParkingData);

        return redirect()->back()->with(['success_message' => 'Parking updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function companyParkingEnableDisabled(Request $request)
    {
        # get companyParking details
        $companyParking = $this->companyParkings->findOrFail($request->get('company_parking_id'));

        # define $companyParking data as an array
        $companyParkingData = [];
        $message = '';

        # check companyParking status
        if ($companyParking->status) {
            $companyParkingData['status']  = false;
            $message = "Company Parking Inactive Successfully!";
        } else {
            $companyParkingData['status']  = true;
            $message = "Company Parking Active Successfully!";
        }

        # update companyParking status
        $companyParking->update($companyParkingData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * add space of parking.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createSpaceOfParking($id)
    {
        # gather the parking id
        $parking_id  = $id;

        #Set Relations
        $relations = [
            'parkingDriveFeatures',
            'parkingWasherFeatures',
            'parkingBookingFeature',
            'parkingSubscriptionFeature',
            'parkingFeature',
            'parkingSetupSpace',
            'parkingSetupSpace.parkingSpaceImages',
            'parkingDescription',
            'parkingMarkOut',
            'parkingPayoutSetting',
            'parkingAdvanceSetting'
        ];

        # get parking 
        $parking = $this->companyParkings
                        ->with($relations)
                        ->findOrFail($id);

        # Get All feature data
        $setUpSpace             = $parking->parkingSetupSpace;
        $spaceDescription       = $parking->parkingDescription;
        $markOutSpace           = $parking->parkingMarkOut;
        $payoutSetting          = $parking->parkingPayoutSetting;
        $advanceSpaceSetting    = $parking->parkingAdvanceSetting;

        // dd($parking->parkingDriveFeatures->toArray());
        # get parking set up space data
        // $setUpSpace = $this->parkingSetupSpace->onlyParking($parking_id)->first();

        // # get parking set up space data
        // $spaceDescription = $this->parkingDescription->onlyParking($parking_id)->first();

        // # get parking set up space data
        // $markOutSpace = $this->parkingMarkOut->onlyParking($parking_id)->first();

        // # get parking set up payout setting data
        // $payoutSetting = $this->parkingPayoutSetting->onlyParking($parking_id)->first();

        // # get parking set up payout setting data
        // $advanceSpaceSetting = $this->parkingAdvanceSetting->onlyParking($parking_id)->first();

        # display space of parking.
        return view($this->view.'create_space_of_parking', compact(
                                                                    'parking',
                                                                    'parking_id',
                                                                    'setUpSpace',
                                                                    'spaceDescription',
                                                                    'markOutSpace',
                                                                    'payoutSetting',
                                                                    'advanceSpaceSetting'
                                                                    ));
    }

    /**
     * set up space of parking.
     *
     * @param 
     * @return \Illuminate\Http\Response
     */
    public function setUpSpace(Request $request)
    {   
        # check location enter or not
        // $addressData = [];
        // if (!empty($request->get('location'))) {
        //     # get lat long and pincode from google api
        //     $addressData = $this->commonUtil->geocode($request->get('location'));
        // }
       
        # set the space data
        $setupSpaceData = [
            'parking_id'                =>  $request->get('parking_id'),
            'space_headline'            =>  $request->get('space_headline'),
            'space_type'                =>  $request->get('space_type'),
            'location'                  =>  $request->get('location'),
            'parker_book_space'         =>  !empty($request->get('daily')) ? 'daily' : '',
            'washer_service_available'  =>  !empty($request->get('washer_service_available')) ? true : false,
            'valet_service_available'   =>  !empty($request->get('valet_service_available')) ? true : false,
            'pass_type'                 =>  !empty($request->get('pass_type')) ? $request->get('pass_type') : '',
            // 'latitude'          => ($addressData) ? $addressData[0] : '',
            // 'longitude'         => ($addressData) ? $addressData[1] : '',
            // 'pincode'           => ($addressData) ? $addressData[2] : '',
            'latitude'                  => $request->get('latitude'),
            'longitude'                 => $request->get('longitude'),
            'pincode'                   => $request->get('pincode'),
            'total_slot'                => $request->get('total_slot'),
            'booking_cancel_edit_time'  => $request->get('edit_or_cancel_time'),
            'booking_confirm_time'      => $request->get('confirm_time'),
            'country_id'                => $request->get('country_id')
        ];
        #dd($request->all());
        # Assign parking spache id
        $parkingSpaceId = $request->get('parking_setup_space_id');
        if (!empty($request->get('parking_setup_space_id'))) {
            $this->parkingSetupSpace
                    ->findOrFail($request->get('parking_setup_space_id'))
                    ->update($setupSpaceData);

            #Fetch Parking Setup Space
            $parkingSetupSpace = $this->parkingSetupSpace
                                      ->find($request->get('parking_setup_space_id'));
            
            #validate pass_type and Update accordinly
            $passType = $request->get('pass_type');

            #Validate PassType
            if($passType == 'scan_me' AND $parkingSetupSpace->scan_me_image == '') {
                #Set Name of Scan me Code Image
                $scanMeImageName = date('d-m-Y'). '_'.rand(1111, 9999).'_'.$parkingSetupSpace->parking_id.'.png';

                #Set json Data
                $data = [
                    'parking_id' => $parkingSetupSpace->parking_id,
                    'type' =>'pass',
                ];

                #set Data
                $data = json_encode($data);
                
                #Upload Image for Qr Code
                \Storage::disk('public_uploads')
                        ->put($scanMeImageName, base64_decode(DNS2D::getBarcodePNG($data,'QRCODE')));

                #Update Parking Setup Space
                $parkingSetupSpace->update(['scan_me_image' => 'images/qrCodeImages/'.$scanMeImageName]);
            } elseif ($passType == 'private' AND $parkingSetupSpace->private_code == '') {
                $privateCode = $request->private_code;

                #Update Private Code
                $parkingSetupSpace->update(['private_code' => $privateCode]);
            }

            $output = ['status' => 200, 'success_message' => 'Parking set up space updated successfully!'];
        } else {
            $parkingSpace = $this->parkingSetupSpace->create($setupSpaceData);
            $parkingSpaceId = $parkingSpace->id;

            #Validate PassType
            if($passType == 'scan_me' AND $parkingSpace->scan_me_image == '') {
                #Set Name of Scan me Code Image
                $scanMeImageName = date('d-m-Y'). '_'.rand(1111, 9999).'_'.$parkingSpace->parking_id.'.png';

                #Set json Data
                $data = [
                    'parking_id' => $parkingSpace->parking_id,
                ];

                #set Data
                $data = json_encode($data);
                
                #Upload Image for Qr Code
                \Storage::disk('public_uploads')
                        ->put($scanMeImageName, base64_decode(DNS2D::getBarcodePNG($data,'QRCODE')));

                #Update Parking Setup Space
                $parkingSpace->update(['scan_me_image' => 'images/qrCodeImages/'.$scanMeImageName]);
            } elseif ($passType == 'private' AND $parkingSpace->private_code == '') {
                $privateCode = $request->private_code;

                #Update Private Code
                $parkingSpace->update(['private_code' => $privateCode]);
            }
            
            $output = ['status' => 200, 'success_message' => 'Parking set up space created successfully!'];
        }

        # upload file
        if(Input::hasFile('space_photos')) {
            foreach ($request->file('space_photos') as $key => $image) {
                $filename        = $image->getClientOriginalName();
                $destinationpath = base_path() . '/public/assets/images/space_photo/';

                $today_date     = date('d-m-Y');
                $random_number  = rand(1111, 9999);

                $filenameData = $today_date.'_'.$random_number.$filename;
                $movefilename = $image->move($destinationpath, $filenameData);

                if ($movefilename) {
                    $parkingSpaceImageData = [
                                            'image_path' => 'assets/images/space_photo/'.$filenameData,
                                            'parking_space_id' => $parkingSpaceId
                                        ];

                    # create parking space image
                    $this->parkingSpaceImages->create($parkingSpaceImageData);
                } else {
                   $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
                }
            }
        }

        return response()->json($output);
    }

    /**
     * space description of parking.
     *
     * @param 
     * @return \Illuminate\Http\Response
     */
    public function spaceDescription(Request $request)
    {
        $spaceDescriptionData = [
            'parking_id'            => $request->get('parking_id'),
            'description'           => $request->get('description'),
            'idle_page_for_people'  => $request->get('ideal_for_people_who'),
            // 'select_vehicle_type'   => $request->get('vehicle_type'),
        ];

        $spaceFeatures = [];
        if (!empty($request->get('space_features'))) {
            foreach ($request->get('space_features') as $spaceFeature) {
                $spaceFeatures[] = $spaceFeature;
            }
        }

        $vehicleTypes = [];
        if (!empty($request->get('vehicle_type'))) {
            foreach ($request->get('vehicle_type') as $vehicleType) {
                $vehicleTypes[] = $vehicleType;
            }
        }

        $spaceDescriptionData['select_vehicle_type'] = (json_encode($vehicleTypes, true));
        $spaceDescriptionData['select_space_features'] = (json_encode($spaceFeatures, true));

        if (!empty($request->get('space_description_id'))) {
            $this->parkingDescription->findOrFail($request->get('space_description_id'))->update($spaceDescriptionData);
            
            $output = ['status' => 200, 'success_message' => 'Parking space description updated successfully!'];

        } else {
            $this->parkingDescription->create($spaceDescriptionData);
            
            $output = ['status' => 200, 'success_message' => 'Parking space description created successfully!'];
        }

        return response()->json($output);
    }

    /**
     * space description of parking.
     *
     * @param 
     * @return \Illuminate\Http\Response
     */
    public function parkingFeature(Request $request)
    {
        $spaceFeatures = [];
        if (!empty($request->get('features'))) {
            foreach ($request->get('features') as $spaceFeature) {
                $spaceFeatures[] = $spaceFeature;
            }
        }

        $parkingFeatureData = [
                                'selected_feature' => json_encode($spaceFeatures, true),
                                'parking_id' => $request->get('parking_id')
                                ];

        if (!empty($request->get('space_feature_id'))) {
            $this->parkingFeature->findOrFail($request->get('space_feature_id'))->update($parkingFeatureData);
            
            $output = ['status' => 200, 'success_message' => 'Parking feature updated successfully!'];

        } else {
            $this->parkingFeature->create($parkingFeatureData);
            
            $output = ['status' => 200, 'success_message' => 'Parking feature created successfully!'];
        }

        return response()->json($output);
    }

    /**
     * Mark out space of parking.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function markOutSpace(Request $request)
    {
        $markOutSpaceData = [
            'parking_id'                    => $request->get('parking_id'),
            'parking_space_label'           => $request->get('parking_space_label'),
            'special_access_instructions'   => $request->get('special_access_instruction'),
            'about_extended_time'           => $request->get('about_extended_time'),
            'set_book_price'                => $request->get('set_booking_price'),
            'set_valet_and_washing_price'   => $request->get('set_valet_washing_price'),
            'set_valet_price'               => $request->get('set_valet_price'),
            'full_wash_cost'                => $request->get('full_wash_price'),
            'interior_wash_cost'            => $request->get('interior_wash_price'),
            'exterior_wash_cost'            => $request->get('exterior_wash_price'),
            'days_mark_space_unavailable'   => !empty($request->get('mark_specific_day_avaiable')) ? date('Y-m-d', strtotime($request->get('mark_specific_day_avaiable'))) : Null,
        ];

        if (!empty($request->get('set_available_time')) AND $request->get('set_available_time') == '24') {
            $markOutSpaceData['set_available_time']       = $request->get('set_available_time');
            $markOutSpaceData['set_available_time_am_pm'] = null;
        }

        if (!empty($request->get('set_available_time')) AND $request->get('set_available_time') == 'customize') {
            $markOutSpaceData['set_available_time']  = $request->get('set_available_time');
            $markOutSpaceData['start_time']       = date('h:i', strtotime($request->get('start_time')));
            $markOutSpaceData['start_time_am_pm'] = date('a', strtotime($request->get('start_time')));
            $markOutSpaceData['end_time']       = date('h:i', strtotime($request->get('end_time')));
            $markOutSpaceData['end_time_am_pm'] =  date('a', strtotime($request->get('end_time')));
        }

        $paymentMethod = [];
        $daySpaceData = [];
        if (!empty($request->get('method_of_payment'))) {
            foreach ($request->get('method_of_payment') as $key => $method_of_payment) {
                $paymentMethod[] = $method_of_payment;
            }
        }
        
        if (!empty($request->get('week_days'))) {
            foreach ($request->get('week_days') as $weekDay) {
                $daySpaceData[] = $weekDay;
            }
        }
        
        $markOutSpaceData['days_space_available']  = json_encode($daySpaceData, true);
        $markOutSpaceData['method_of_payment']  = json_encode($paymentMethod, true);

        if (!empty($request->get('mark_out_space_id'))) {
            $this->parkingMarkOut->findOrFail($request->get('mark_out_space_id'))->update($markOutSpaceData);
            $output = ['status' => 200, 'success_message' => 'Parking mark out space updated successfully!'];
        } else {
            $this->parkingMarkOut->create($markOutSpaceData);
            $output = ['status' => 200, 'success_message' => 'Parking mark out space created successfully!'];
        }

        return response()->json($output);
    }

    /**
     * payout of parking.
     *
     * @param 
     * @return \Illuminate\Http\Response
     */
    public function payoutSetting(Request $request)
    {
        $payoutSettingData = [
            'parking_id'        => $request->get('parking_id'),
            'first_name'        => $request->get('first_name'),
            'last_name'         => $request->get('last_name'),
            'address'           => $request->get('street_address'),
            'country'           => $request->get('district_state'),
            'pincode'           => $request->get('pincode'),
            'date_of_birth'     => !empty($request->get('date_of_birth')) ? date('Y-m-d', strtotime($request->get('date_of_birth'))) : Null,
            'identification'    => $request->get('identification'),
        ];

        # upload file
        if(Input::hasFile('identification_pic')) {
            $filename        = $request->file('identification_pic')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/company_identification_pic/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('identification_pic')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $payoutSettingData['identification_photo'] = 'assets/images/company_identification_pic/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               // return response()->json($output);
            }
        }

        if (!empty($request->get('payout_setting_id'))) {
            $this->parkingPayoutSetting->findOrFail($request->get('payout_setting_id'))->update($payoutSettingData);
            $output = ['status' => 200, 'success_message' => 'Parking Payout Setting Updated Successfully!'];
        } else {
            $this->parkingPayoutSetting->create($payoutSettingData);
            $output = ['status' => 200, 'success_message' => 'Parking Payout Setting Created Successfully!'];
        }

        return response()->json($output);
    }

    /**
     * advance space setting of parking.
     *
     * @param 
     * @return \Illuminate\Http\Response
     */
    public function advanceSpaceSetting(Request $request)
    {
        # gather the advanceSpaceSetting form data
        $advanceSpaceSettingData = [
            'parking_id'            => $request->get('parking_id'),
            'private_space_link'    => $request->get('private_space'),
            'hidden_space_link'     => $request->get('hidden_space'),
            'discounts'             => !empty($request->get('discount')) ? true : false,
            'private_space'         => !empty($request->get('private_space_check')) ? true : false,
            'hidden_space'          => !empty($request->get('hidden_space_check')) ? true : false,
            'customer_t_and_c'      => !empty($request->get('customer_t_check')) ? true : false,
            'custom_t_and_c_text'   => $request->get('custom_t_and_c_text')
        ];

        if (!empty($request->get('advance_space_setting_id'))) {
            $this->parkingAdvanceSetting->findOrFail($request->get('advance_space_setting_id'))->update($advanceSpaceSettingData);
            $output = ['status' => 200, 'success_message' => 'Parking Advance Setting Updated Successfully!'];
        } else {
           $this->parkingAdvanceSetting->create($advanceSpaceSettingData);
            $output = ['status' => 200, 'success_message' => 'Parking Advance Setting Created Successfully!'];
        }

        return response()->json($output);
    }

    /**
     * driver feature of parking.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function parkingDriverFeature(Request $request)
    {
        $parkingDriverFeatureData = [
            'parking_id'    => $request->get('parking_id'),
            'cash'          => $request->get('cash'),
            'visa_master'   => $request->get('visa_master'),
            'cost_of_valet' => $request->get('cost_of_valet') ?? null,
            'type'          => $request->valet_type,
        ];
        
        #Fetch parking
        $parking = $this->companyParkings
                        ->with(['parkingDriveFeatures'])
                        ->find($request->parking_id);
        
        #Validate Driver Parking 
        if ($parking->parkingDriveFeatures->isNotEmpty()) {
            #Update Data
            $parking->parkingDriveFeatures
                    ->first()
                    ->update($parkingDriverFeatureData);

            #Set Output
            $output = [
                'status' => 200, 
                'success_message' => 'Parking Driver Feature Updated Successfully!'
            ];
        } else {
            $this->parkingDriverFeature->create($parkingDriverFeatureData);

            $output = ['status' => 200, 'success_message' => 'Parking Driver Feature Created Successfully!'];
        }

        return response()->json($output);
    }

    /**
     * washer feature of parking.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function parkingWasherFeature(Request $request)
    {
        $parkingWasherFeatureData = [
            'parking_id'                => $request->get('parking_id'),
            'cash'                      => $request->get('cash'),
            'visa_master'               => $request->get('visa_master'),
            'type'                      => $request->get('washer_type'),
            'cost_of_full_wash'         => $request->get('cost_of_full_wash'),
            'cost_of_exterior_wash'     => $request->get('cost_of_exterior_wash'), 
            'cost_of_interior_wash'     => $request->get('cost_of_interior_wash'),
        ];

        #Fetch parking
        $parking = $this->companyParkings
                        ->with(['parkingWasherFeatures'])
                        ->find($request->parking_id);

        if ($parking->parkingWasherFeatures->isNotEmpty()) {
            #Update Data
            $parking->parkingWasherFeatures
                    ->first()
                    ->update($parkingWasherFeatureData);

            $output = ['status' => 200, 'success_message' => 'Parking Washer Feature Updated Successfully!'];
        } else {
            $this->parkingWasherFeature->create($parkingWasherFeatureData);

            $output = ['status' => 200, 'success_message' => 'Parking Washer Feature Created Successfully!'];
        }

        return response()->json($output);
    }

    /**
     * Booking feature of parking.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function parkingBookingFeature(Request $request)
    { 
        $parkingBookingFeatureData = [
            'parking_id'    => $request->get('parking_id'),
            'cash'          => $request->get('cash'),
            'visa_master'   => $request->get('visa_master'),
        ];

        if (!empty($request->get('booking_feature_id'))) {
            $this->parkingBookingFeature->findOrFail($request->get('booking_feature_id'))
                    ->update($parkingBookingFeatureData);
            $output = ['status' => 200, 'success_message' => 'Parking Booking Feature Updated Successfully!'];
        } else {
            $this->parkingBookingFeature->create($parkingBookingFeatureData);
            $output = ['status' => 200, 'success_message' => 'Parking Booking Feature Created Successfully!'];
        }

        return response()->json($output);
    }

    /**
     * Subscription feature of parking.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function parkingSubscriptionFeature(Request $request)
    {
        $parkingSubscriptionFeatureData = [
            'parking_id'    => $request->get('parking_id'),
            'cash'          => $request->get('cash'),
            'visa_master'   => $request->get('visa_master'),
        ];

        if (!empty($request->get('subscription_feature_id'))) {
            $this->parkingSubscriptionFeature->findOrFail($request->get('subscription_feature_id'))
                    ->update($parkingSubscriptionFeatureData);

            $output = ['status' => 200, 'success_message' => 'Parking subscription feature updated successfully!'];
        } else {
            $this->parkingSubscriptionFeature->create($parkingSubscriptionFeatureData);
            $output = ['status' => 200, 'success_message' => 'Parking subscription feature created successfully!'];
        }

        return response()->json($output);
    }
}
