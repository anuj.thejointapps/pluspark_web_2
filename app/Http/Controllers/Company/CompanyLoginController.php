<?php

namespace App\Http\Controllers\Company;

use Auth;
use Validate;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyLoginController extends Controller
{
    # Bind the Company Model
    protected $companies;

    # Bind the view path of company
    protected $view = 'company.';

    /**
     * Define the constructor of controlle
     *
     * @return bind models
     */
    public function __construct(Company $companies)
    {
        $this->companies = $companies;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # display the login page of company
        return view($this->view.'login');
    }

    /**
     * Attempt Company Login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function attemptLogin(Request $request)
    {   
        try {
            $this->validate($request,   [ 
                                            'email'     => 'required|email', 
                                            'password'  => 'required|min:3'
                                        ]);

            $companyData = array('email' => $request->email, 'password' => $request->password);

            if(Auth::guard('company')->attempt($companyData)) {
                $request->session()->regenerate();

                return redirect()->route('company.dashboard.index');
            } else {
                return back()->with('alert', 'Wrong Credentials!');
            }
        } catch(\Exception $ex) {
            return back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Logout company
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        # logout auth company guard
        Auth::guard('company')->logout();

        return redirect()->route('company.login');
    }

}
