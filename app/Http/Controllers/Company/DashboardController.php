<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    # Base View
    protected $view = "company.dashboard.";

    /**
    * @method constructor for Controller
    * @return 
    * @param
    */
    public function __construct()
    {
        # code...
    }

    /**
    * @method index for class
    * @param
    * @return to view
    */
    public function index()
    {
        # return view
        return view($this->view.'index');
    }
}
