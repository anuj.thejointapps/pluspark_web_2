<?php

namespace App\Http\Controllers\Company;

#Models
use App\Models\Booking;
use App\Models\ScanOrPrivateBooking;

#Packages
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    #variables
    protected $booking, $scanPrivateBookings;

    /**
     * @method constructor for Controller
     * @return 
     * @param
     */
    public function __construct(Booking $booking, ScanOrPrivateBooking $scanPrivateBookings)
    {
        $this->booking              = $booking;
        $this->scanPrivateBookings  = $scanPrivateBookings;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #Fetch All bookings for Online or coming
        $qrBookings = $this->booking->with(['parking', 'user'])->get();
        $scanAndPrivateBookings = $this->scanPrivateBookings->with(['parking', 'user'])->get();

        #Fetch Qr Booking which are coming or onGoing
        $qrBookingsOnline = $qrBookings->filter(function($booking){
            if($booking->exit_date_time != '' AND $booking->exit_date_time->gte(Carbon::now()) AND !$booking->is_cancelled AND !$booking->is_expired){
                return $booking;
            }
        });
       
        #Fetch Scan Booking which are coming or onGoing
        $scanPrivateOnline = $scanAndPrivateBookings->filter(function($booking){
            if($booking->exit_date_time != '' AND $booking->exit_date_time->gte(Carbon::now()) AND !$booking->is_cancelled AND !$booking->is_expired){
                return $booking;
            }
        });

        #merge Bookings
        $bookings = $qrBookingsOnline->merge($scanPrivateOnline);

        #return to view with data
        return view('company.booking.index')->with(['bookings' => $bookings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $bookingType)
    {
        #fetch Relation
        $relations = ['user', 'parking', 'drivers', 'washer', 'vehicle'];

        #FEtch Booking
        $booking = ($bookingType == 'scan_me_or_private_bookings') ? 
                                    $this->scanPrivateBookings->with($relations)->find($id) : 
                                    $this->booking->with($relations)->find($id);

        #Fetch Driver and washer if avaialable on Booking
        $washer = ''; $driver = '';
        if($bookingType == 'user_bookings') {
            $driver = $booking->drivers->isNotEmpty() ? $booking->drivers->last() : '';
            $washer = $booking->washer != '' ? $booking->washer : '';
        }

        #return to view with data
        return view('company.booking.show')->with([
            'booking' => $booking, 
            'bookingType' => $bookingType, 
            'driver' => $driver, 
            'washer' => $washer
        ]);
    }

    /**
     * Excel Export
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportdata(Request $request)
    {
       dd($request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
