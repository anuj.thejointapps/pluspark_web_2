<?php

namespace App\Http\Controllers\Company;

use Auth;
use Validate;
use App\Models\Event;
use App\Models\Company;
use App\Utils\CommonUtil;
use App\Models\EventPopularPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    # Bind the Event Model
    protected $events;

    # Bind the EventPopularPlace Model
    protected $eventPopularPlaces;

    # Bind the CommonUtil Model
    protected $CommonUtil;

    # Bind the Company Model
    protected $companies;

    # Bind the type for create sequence
    protected $type = 'event';

    # Base View
    protected $view = 'company.event.';
    
    # Bind the pagination 
    protected $pagination = 10;

    /**
     * @method Define the constructor of controller.
     *  @param eventPopularPlaces $eventPopularPlaces
     * @return
     */    
    public function __construct(
                                Event $events, 
                                Company $companies,
                                EventPopularPlace $eventPopularPlaces, 
                                CommonUtil $commonUtil
                            )
    {
        $this->events       = $events;
        $this->companies    = $companies;
        $this->commonUtil   = $commonUtil;
        $this->eventPopularPlaces = $eventPopularPlaces;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Gather the filter input
        // $countryName    = $request->get('country_name');
        // $stateName      = $request->get('state_name');
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');

        # Gat all events
        $events =   $this->events->with(['eventPopularPlaces'])
                            ->searchFilter($fromDate, $todate)
                            ->paginate($this->pagination);

        # Display event listing
        return view($this->view.'index', compact(
                                                'events',
                                                'fromDate',
                                                'todate'
                                            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # Get Auth Company
        $companyId = $this->commonUtil->getLoginCompanyId();

        # Get the all company
        $company = $this->companies
                        ->with(['companyParking.parkingSetupSpace'])
                        ->findOrFail($companyId);

        # display the create page
        return view($this->view.'create', compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,   [
                                        'name'        => 'required', 
                                        'location'    => 'required',
                                        'start_date'  => 'required',
                                        'end_date'    => 'required',
                                        'start_time'  => 'required',
                                        'end_time'    => 'required'
                                    ]);

        # check location enter or not
        $addressData = [];
        if (!empty($request->get('location'))) {
            # get lat long and pincode from google api
            $addressData = $this->commonUtil->geocode($request->get('location'));
        }


        # Get Auth Company
        $companyId = $this->commonUtil->getLoginCompanyId();

        # get event last refcount
        $refCount = $this->commonUtil->lastReferenceCount($this->type);

        # create event unique id and get event sequence.
        $eventSequence = $this->commonUtil->genrateSequences($refCount, $this->type);

        # Gather the form data.
        $eventData = [
            'company_id'     => $companyId,
            'event_auto_id'  => $eventSequence,
            'name'           => $request->get('name'),
            'location'       => $request->get('location'),
            'start_date'     => date('Y-m-d', strtotime($request->get('start_date'))),
            'end_date'       => date('Y-m-d', strtotime($request->get('end_date'))),
            'start_time'     => date('H:i:s', strtotime($request->get('start_time'))),
            'end_time'       => date('H:i:s', strtotime($request->get('end_time'))),
            'latitude'       => ($addressData) ? $addressData[0] : '',
            'longitude'      => ($addressData) ? $addressData[1] : '',
            'pincode'        => ($addressData) ? $addressData[2] : '',
        ];

        # create a event
        $event = $this->events->create($eventData);

        # update ref. count of company
        $refCount = $this->commonUtil->referenceCount($this->type);

        $popularPlacesData = $request->get('parking_place');
        if (!empty($popularPlacesData)) {
            foreach ($popularPlacesData as $popularPlace) {
                # check popular place selected or not
                if (isset($popularPlace['popular_place_id'])) {
                    # set the popular place array
                    $eventPopularPlaceData = [
                        'event_id' => $event->id,
                        'parking_id' => $popularPlace['popular_place_id'],
                        'valet_price' => $popularPlace['valet_price']
                    ];

                    # create new event popular place.
                    $this->eventPopularPlaces->create($eventPopularPlaceData);
                }
            }
        }

        # redirect back
        return redirect()
                    ->route('events.index')
                    ->with(['status' => 200, 'success_message' => 'Event Created Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        # Get Auth Company
        $companyId = $this->commonUtil->getLoginCompanyId();

        # Get the all company
        $company = $this->companies
                        ->with(['companyParking.parkingSetupSpace'])
                        ->findOrFail($companyId);

        # get event
        $event  = $this->events->with(['eventPopularPlaces'])->findOrFail($id);

        # show edit page
        return view($this->view.'show', compact('event', 'company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # Get Auth Company
        $companyId = $this->commonUtil->getLoginCompanyId();

        # Get the all company
        $company = $this->companies
                        ->with(['companyParking.parkingSetupSpace'])
                        ->findOrFail($companyId);

        # get event
        $event  = $this->events->with(['eventPopularPlaces'])->findOrFail($id);

        # show edit page
        return view($this->view.'edit', compact('event', 'company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,   [
                                        'name'        => 'required', 
                                        'location'    => 'required',
                                        'start_date'  => 'required',
                                        'end_date'    => 'required',
                                        'start_time'  => 'required',
                                        'end_time'    => 'required'
                                    ]);

        # check location enter or not
        $addressData = [];
        if (!empty($request->get('location'))) {
            # get lat long and pincode from google api
            $addressData = $this->commonUtil->geocode($request->get('location'));
        }

        # Gather the form data.
        $eventData = [
            'name'           => $request->get('name'),
            'location'       => $request->get('location'),
            'start_date'     => date('Y-m-d', strtotime($request->get('start_date'))),
            'end_date'       => date('Y-m-d', strtotime($request->get('end_date'))),
            'start_time'     => date('H:i:s', strtotime($request->get('start_time'))),
            'end_time'       => date('H:i:s', strtotime($request->get('end_time'))),
            'latitude'       => ($addressData) ? $addressData[0] : '',
            'longitude'      => ($addressData) ? $addressData[1] : '',
            'pincode'        => ($addressData) ? $addressData[2] : '',
        ];

        # update event
        $this->events->findOrFail($id)->update($eventData);

        # get event
        $eventPopularPlaceData  = $this->eventPopularPlaces->onlyEvent($id)->get()->toArray();

        $popularPlacesData = $request->get('parking_place') ?? [];
        if (!empty($popularPlacesData)) {
            foreach ($popularPlacesData as $popularPlace) {
                # check popular place selected or not
                if (isset($popularPlace['popular_place_id'])) {
                    # check selected popular place already in DB or not.
                    if (!in_array($popularPlace['popular_place_id'], array_column($eventPopularPlaceData, 'parking_id'))) {

                        # set the popular place array
                        $popularPlaceData = [
                            'event_id' => $id,
                            'parking_id' => $popularPlace['popular_place_id'],
                            'valet_price' => $popularPlace['valet_price']
                        ];

                        # create new popularplace.
                        $this->eventPopularPlaces->create($popularPlaceData);
                    } else {
                        # set the popular place array
                        $popularPlaceData = [
                            'event_id' => $id,
                            'parking_id' => $popularPlace['popular_place_id'],
                            'valet_price' => $popularPlace['valet_price']
                        ];

                        # update new popularplace.
                        $this->eventPopularPlaces->findOrFail($popularPlace['event_popular_id'])->update($popularPlaceData);
                    }
                }
            }
        }

        # delete unselected popular place
        foreach ($eventPopularPlaceData as $key => $eventPopularPlaceD) {
            # check unselected data for delete
            if (!in_array($eventPopularPlaceD['parking_id'], 
                array_column($popularPlacesData, 'popular_place_id'))) {
                $this->eventPopularPlaces->findOrFail($eventPopularPlaceD['id'])->delete();
            }
        }

        # redirect back
        return redirect()
                    ->route('events.index')
                    ->with(['status' => 200, 'success_message' => 'Event Updated Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eventEnableDisabled(Request $request)
    {
        # get event details
        $event = $this->events->findOrFail($request->get('event_id'));

        # define $event data as an array
        $eventData = [];
        $message = '';

        # check event status
        if ($event->status == '1') {
            $eventData['status']  = '0';
            $message = "Event Inactive Successfully!";
        } else {
            $eventData['status']  = '1';
            $message = "Event Active Successfully!";
        }

        # update event status
        $event->update($eventData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

}
