<?php

namespace App\Http\Controllers\Company;

use App\Models\Company;
use App\Models\CompanyPasswordReset;

use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    # Company Model Bind
    protected $company;

    # CompanyPasswordReset Model Bind
    protected $companyPasswordReset;

    # Base View
    protected $view = 'company.forgot-password.';

    /**
    * @method constructor for Controller
    * @return 
    * @param
    */
    public function __construct(Company $company, CompanyPasswordReset $companyPasswordReset)
    {
      $this->company              = $company;
      $this->companyPasswordReset = $companyPasswordReset;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #return to view
        return view($this->view.'index');
    }

    /**
     * @method to send reset password link on email
     * @return json
     * @param Request $request
     */
    public function sendEmail(Request $request)
    {
        #fetch Email
        $email = $request->get('email') ?? '';

        #Validate Email
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) { 
            return response()->json(['status' => 401, 'msg' => 'Email is Invalid']);
        }

        #Fetch Company Data
        $company = $this->company->where('email', $email)->get();

        #Validate Company
        if($company->isEmpty()) {
            return response()->json(['status' => 401, 'msg' => 'Company not found on email']);
        }

        #Set data for company Passowrd Reset
        $data = [
            'email'  => $email,
            'token'  => str_random(60),
        ];

        # Create token for Email
        $companyEmailToken = $this->companyPasswordReset->create($data);

        # Send link to Email
        $link = Url('/') . '/company/password-reset/' . $companyEmailToken->token . '?email=' . urlencode($email);
    
        #mailData
        $mailData = ['company' => $company->first(), 'link' => $link];

        #Send Email on Company Email
        $mail = Mail::send('company.forgot-password.email', $mailData, function($message) use($email) {
                 $message->to($email, 'Plus Spark')->subject('Password Reset Link is');
                 $message->from('nishikant.thejointapps@gmail.com','Nishikant Tyagi');
              });

        # Validate Mail Send
        if(count(Mail::failures()) > 0) {
            return response()->json(['status' => 401, 'msg' => 'Something went wrong']); 
        }
        
        #Mail Sent
        return response()->json(['status' => 200, 'msg' => 'Reset Password link sent on email.']);
    }

    /**
     * @method to reset Pssword
     * @return to view
     * @param 
     */
    public function resetPassword($token, Request $request)
    {
        #Fetch Data
        $email = $request->email;

        #Create token for Email
        $companyEmailToken = $this->companyPasswordReset
                                ->where('email', $email)
                                ->where('token', $token)
                                ->get();

        #Fetch Company
        $company = $this->company->where('email', $email)->get();

        # validate if not Found
        if($companyEmailToken->isEmpty() OR $company->isEmpty()) {
            return view($this->view.'reset-password')->with(['status' => 401, 'msg' => 'Sorry! User Not found in records']);
        }

        #Fetch last Token Model
        $companyEmailToken = $companyEmailToken->last();

        #Validate token Expiry
        if($companyEmailToken->created_at->diffInMinutes(Carbon::now()) > 160) {
            return view($this->view.'reset-password')->with(['status' => 401, 'msg' => 'Sorry! Link expired Please try again']);
        }

        #return to view
        return view($this->view.'reset-password')->with(['status' => 200, 'msg' => '', 'company' => $company->first()]);
    }

    /**
     * @method to reset the company password
     * @return json
     * @param Request $request
     */
    public function resetCompanyPassword(Request $request)
    {
        #Fetch Data
        $data = [
            'password' => Hash::make($request->password),
        ];

        #Fetch company
        $company = $this->company->find($request->companyId);

        #Update company Password
        $company->update($data);

        #return to login
        return response()->json(['status' => 200, 'msg' => 'Password Updated Successfully']);
    }
}
