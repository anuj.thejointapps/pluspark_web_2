<?php

namespace App\Http\Controllers\Company;

use Mail;
use App\Utils\CommonUtil;
use App\Models\Washerman;
use Illuminate\Http\Request;
use App\Models\CompanyParking;
use App\Models\EmployeeParking;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    # Bind the util 
    protected $commonUtil;

    # Bind the CompanyParking Model
    protected $companyParkings;

    # Bind the EmployeeParking Model
    protected $employeeParkings;

    # Bind the Washerman Model as Employee
    protected $employees;

    # Bind the view path of company
    protected $view = 'company.employee.';

    # Bind the type for create sequence
    protected $type = 'employee';

    # Bind the pagination 
    protected $pagination = 10;
    
    /**
     * Define the constructor of controller
     *
     * @return bind models
     */
    public function __construct(Washerman $employees, CommonUtil $commonUtil, CompanyParking $companyParkings, EmployeeParking $employeeParkings)
    {
        $this->employees = $employees;
        $this->companyParkings = $companyParkings;
        $this->employeeParkings = $employeeParkings;
        $this->commonUtil = $commonUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Get Auth company id.
        $companyId = $this->commonUtil->getLoginCompanyId();

        # Gather the filter input
        $countryName    = $request->get('country_name');
        $stateName      = $request->get('state_name');
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');
        $month          = $request->get('month');

        # Get all washerman
        $employees = $this->employees
                            ->searchFilter($countryName, $stateName, $fromDate, $todate, $month)
                            ->isNotWasher()
                            ->onlyCompany($companyId)
                            ->paginate($this->pagination);

        # return view.
        return view($this->view.'index', compact(
                                                    'employees',
                                                    'countryName', 
                                                    'stateName', 
                                                    'fromDate',
                                                    'todate',
                                                    'month'
                                                ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # get washer last refcount
        $refCount = $this->commonUtil->lastReferenceCount($this->type);

        # create washer unique id and get employee sequence.
        $employeeSequence = $this->commonUtil->genrateSequences($refCount, $this->type);

        # display the washer model pop.
        return view($this->view.'create', compact('employeeSequence'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # check address enter or not
        $addressData = [];
        if (!empty($request->get('address'))) {
            # get lat long and pincode from google api
            $addressData = $this->commonUtil->geocode($request->get('address'));
        }

        # Get Auth company id.
        $companyId = $this->commonUtil->getLoginCompanyId();

        # Generate Random password.
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomNumber = substr(str_shuffle($str_result), 0, 8);
        $password = $randomNumber;
        $email = $request->get('email');

        # Gather the form data
        $employeeData = [
            'washer_auto_id'    => $request->get('employee_auto_id'),
            'name'              => $request->get('name'),
            'company_id'        => $companyId,
            'job_type_id'       => $request->get('job_type_id'),
            'email'             => $email,
            'password'          => Hash::make($password),
            'country_code'      => $request->get('country_code'),
            'mobile'            => $request->get('mobile'),
            'city'              => $request->get('city_name'),
            'country'           => $request->get('country_name'),
            'gender'            => $request->get('gender'),
            'date_of_join'      => date('Y-m-d', strtotime($request->get('date_of_join'))),
            'address'           => $request->get('address'),
            'latitude'          => !empty($addressData[0]) ? $addressData[0] : '',
            'longitude'         => !empty($addressData[1]) ? $addressData[1] : '',
            'pincode'           => (($addressData) AND (is_integer($addressData[2]))) ? $addressData[2] : null,
        ];
        
        # upload file
        if(Input::hasFile('employee_image')) {
            $filename        = $request->file('employee_image')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/employee_image/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('employee_image')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $employeeData['image_path'] = 'assets/images/employee_image/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               return response()->json($output);
            }
        }

        # create a employee.
        $this->employees->create($employeeData);

        # update ref. count of employee
        $refCount = $this->commonUtil->referenceCount($this->type);

        // # send employee credential into mail
        // $credentials = [
        //     'email' => $email,
        //     'password' => $password
        // ];

        // # send mail with usename password
        // $mail =     Mail::send('admin.forgot-password.staff_credential_send_into_mail', $credentials, 
        //                 function($message) use($email) {
        //                     $message->to($email, 'Plus Spark')
        //                     ->subject('Company Credential');
        //                     $message->from('nishikant.thejointapps@gmail.com','Nishikant Tyagi');
        //                 }
        //             );

        # return response json
        return response()->json(['status' => 200, 'success_message' => 'Employee Added Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        # Gather the employee info.
        $employee  = $this->employees->findorFail($id);

        # display the employee show page.
        return view($this->view.'show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # Gather the employee info.
        $employee  = $this->employees->findorFail($id);

        # display the employee show page.
        return view($this->view.'edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        # check address enter or not
        $addressData = [];
        if (!empty($request->get('address'))) {
            # get lat long and pincode from google api
            $addressData = $this->commonUtil->geocode($request->get('address'));
        }

        # Gather the form data
        $EmployeeData = [
            'name'              => $request->get('name'),
            'job_type_id'       => $request->get('job_type_id'),
            'country_code'      => $request->get('country_code'),
            'city'              => $request->get('city_name'),
            'country'           => $request->get('country_name'),
            'gender'            => $request->get('gender'),
            'address'           => $request->get('address'),
            'latitude'          => !empty($addressData[0]) ? $addressData[0] : '',
            'longitude'         => !empty($addressData[1]) ? $addressData[1] : '',
            'pincode'           => !empty($addressData[2]) ? $addressData[2] : Null,
        ];

        # upload file
        if(Input::hasFile('employee_image')) {
            $filename        = $request->file('employee_image')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/employee_image/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('employee_image')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $EmployeeData['image_path'] = 'assets/images/employee_image/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               return response()->json($output);
            }
        }

        # update employees.
        $this->employees->findOrFail($id)->update($EmployeeData);

        # return response json
        return response()->json(['status' => 200, 'success_message' => 'Employee Updated Successfully!']);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function employeeEnableDisabled(Request $request)
    {
        # get employee details
        $employee = $this->employees->findOrFail($request->get('employee_id'));

        # define $employee data as an array
        $employeeData = [];
        $message = '';

        # check employee status
        if ($employee->status) {
            $employeeData['status']  = false;
            $message = "Employee Inactive Successfully!";
        } else {
            $employeeData['status']  = true;
            $message = "Employee Active Successfully!";
        }

        # update employee status
        $employee->update($employeeData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * User Check Already is in record or not.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function emailCheck(Request $request)
    {
        $email = $request->input('email');

        $count = $this->employees->where('email', $email)->count();
        if ($count == 0) {
            echo "true";
            exit;
        } else {
            echo "false";
            exit;
        }
    }

    /**
     * User Check Already is in record or not.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mobileCheck(Request $request)
    {
        $mobile = $request->input('mobile');

        $count = $this->employees->where('mobile', $mobile)->count();
        if ($count == 0) {
            echo "true";
            exit;
        } else {
            echo "false";
            exit;
        }
    }

    /**
     * Get Employee Parking allot
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function employeeParkingAllot(Request $request, $id)
    {
        $employeeId =$id;

        # get auth company
        $companyId = $this->commonUtil->getLoginCompanyId();

        # get all parking of company
        $parkings = $this->companyParkings->onlyCompany($companyId)->get();

        # get employee parking
        $employeeAllot = $this->employeeParkings->onlyEmployee($employeeId)->get()->toArray();

        # display the parking allot page
        return view($this->view.'employee_parking_allot', compact('employeeAllot', 'parkings', 'employeeId'));
    }

    /*
    * Post Employee Parking allot
    * 
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function employeeParkingAllocate(Request $request, $id)
    {
        # check validation
        $request->validate([
            'parkings' => 'required'
        ]);

        $employeeId = $id;

        # Gather the all employee
        $parkings   = $request->get('parkings');
        $employeeId  = $request->get('employee_id');

        # Get all employee of alloted on company.
        $allotedEmployee  = $this->employeeParkings
                                ->select(['employee_id', 'parking_id', 'id'])
                                ->onlyEmployee($employeeId)
                                ->get()
                                ->toArray();

        if (!empty($parkings)) {
            foreach ($parkings as $parking) {
                if (!in_array($parking['create_id'], array_column($allotedEmployee, 'parking_id'))) {
                    # Gather the form data.
                    $allotparkingData = [
                        'employee_id'  => $employeeId,
                        'parking_id'  => $parking['create_id'],
                    ];

                    # create a employee parking.
                    $this->employeeParkings->create($allotparkingData);
                }
            }

            foreach ($allotedEmployee as $emplAllot) {
                if (!in_array($emplAllot['parking_id'], array_column($parkings, 'create_id'))) {
                    # delete a employee parking.
                    $this->employeeParkings->findOrFail($emplAllot['id'])->delete();
                }
            }
        }

        # redirect back to on parking allot to employee.
        return redirect()->back()->with([ 'status' => 200, 'success_message' => 'Parking Alloted to employee Successfully!']);
    }
}
