<?php

namespace App\Http\Controllers\Company;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Promocode;

class PromoCodeController extends Controller
{
    # Bind the Company Model
    protected $company;

    # Bind the Promocode Model
    protected $promocode;

    # Base View
    protected $view = 'company.promocode.';

    # Bind the pagination 
    protected $pagination = 10;
    
    /**
     * @method Define the constructor of controller.
     *  @param Discount $discount
     * @return
     */    
    public function __construct(Company $company, Promocode $promocode)
    {
        $this->company      = $company;
        $this->promocode    = $promocode;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # get auth user
        $companyId = Auth::guard('company')->user()->id ?? 0;

        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');
        $month          = $request->get('month');

        # get the all promocodes
        $promocodes =   $this->promocode
                            ->with(['company'])
                            ->searchFilter($fromDate, $todate, $month)
                            // ->onlyCompany($companyId)
                            ->paginate($this->pagination);

        # Set next Promocode Unique Id
        $totalCount = $promocodes->count() != 0 ? $promocodes->count() + 1 : 1;

        $nextPromoCodeUniqueId = 'Promo00000'.$totalCount;

        # display the listing of color
        return view($this->view.'index', compact(
                                                    'promocodes', 
                                                    'nextPromoCodeUniqueId',
                                                    'fromDate',
                                                    'todate',
                                                    'month'
                                                ));
    }

    /**
     * @method to fetch company Id
     * @return json
     * @param Request $request
     */
    public function fetchCompanyId(Request $request)
    {
        #Fetch CompanyId
        $companyId = $request->id;

        #Fetch Company
        $company = $this->company->find($companyId);

        #Fetch Company Auto generate id
        $companyId = $company->company_auto_id ?? '';

        #return response
        return response()->json(['status' => 200, 'id' => $companyId]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # Display the create color page
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #Fetch Data
        $startDate  = $request->start_date.' 00:00:00';
        $endDate    = $request->end_date.' 00:00:00';

        # Fetch Discount 
        $promocode = $this->promocode
                        ->whereDate('start_date', '>=', $startDate)
                        ->whereDate('end_date', '<=', $endDate)
                        ->first();

        if (!empty($promocode->id)) {
            redirect()->back()->with(['success_message' => 'Promocode already added on this start date and end date!']);
        }

        #Set Date to Save
        $data = [
            'unique_id'         => $request->promocode_id ?? '',
            'company_id'        => 0,
            'code'              => $request->code ?? '',
            'usage_allowed'     => $request->usage_allowed ?? '',
            'offer_type'        => $request->discount_type ?? '',
            'discount'          => $request->discount ?? '',
            'on_booking'        => isset($request->booking) ? true : false,
            'on_subscription'   => isset($request->subscription) ? true : false,
            'on_wallet'         => isset($request->valet) ? true : false,
            'start_date'        => $startDate,
            'end_date'          => $endDate,
            'status'            => true,
            'added_by'          => 'company',
        ];

        #Cerate Promocode
        $this->promocode->create($data);

        #return redirect
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Code
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # get the color details
        $color = $this->colors->findOrFail($id);

        # Display the edit color page
        return view($this->view.'edit', compact('color'));
    }

    /**
     * @method to open update Modal
     * @param Request $request
     * @return json
     */
    public function openPromocodeUpdate(Request $request, $id)
    {
        #Fetch id
        $promocodeId = $id;

        #Fetch Discount 
        $promocode = $this->promocode
                          ->with(['company'])
                          ->find($promocodeId);
        
        # return to Modal View
        return view($this->view.'update')->with(['promocode' => $promocode])->render();
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # Fetch Data
        $startDate  = $request->start_date.' 00:00:00';
        $endDate    = $request->end_date.' 00:00:00';

        # Fetch Promocode 
        $promocodeExist = $this->promocode
                        ->whereDate('start_date', '>=', $startDate)
                        ->whereDate('end_date', '<=', $endDate)
                        ->where('id', '!=', $id)
                        ->first();

        if (!empty($promocodeExist->id)) {
            redirect()->back()->with(['success_message' => 'Promocode already added on this start date and end date!']);
        }

        #Set Date to Save
        $data = [
            // 'unique_id'         => $promocode->unique_id ?? '',
            // 'company_id'        => $promocode->company_id ?? '',
            #'code'              => $request->code ?? '',
            'usage_allowed'     => $request->usage_allowed ?? '',
            'offer_type'        => $request->discount_type ?? '',
            'discount'          => $request->discount ?? '',
            'on_booking'        => isset($request->booking) ? true : false,
            'on_subscription'   => isset($request->subscription) ? true : false,
            'on_wallet'         => isset($request->valet) ? true : false,
            'start_date'        => $startDate,
            'end_date'          => $endDate,
            'status'            => true,
            'added_by'          => 'company',
        ];

        #Cerate Discount
        $this->promocode->findOrFail($id)->update($data);

        #return redirect        
        return redirect()->back();

    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request)
    {
        # get promocode details
        $promocode = $this->promocode->findOrFail($request->id);

        # define $promocode data as an array
        $promocodeData = [];
        $message = '';

        # check color status
        if ($promocode->status) {
            $promocodeData['status']  = false;
            $message = "Promocode Inactive Successfully!";
        } else {
            $promocodeData['status']  = true;
            $message = "promocode Active Successfully!";
        }

        # update promocode status
        $promocode->update($promocodeData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
