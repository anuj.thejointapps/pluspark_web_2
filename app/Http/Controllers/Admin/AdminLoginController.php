<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Models\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminLoginController extends Controller
{

	# Bind the admin model
	protected $admin;

	/*
	* Define the constructor of model
	*/ 
	public function __construct(Admin $admin)
	{
		$this->admin = $admin;
	}

    /*
		Admin login view page redirection.
    */

	public function index(Request $request)
	{
		try
		{
			return view('admin.login');
		}catch(Exception $ex)
		{
		    return back()->with('error', $ex->getMessage());
		}
	}

	/*
		Admin login Attempt.
    */

	public function attemptLogin(Request $request)
	{	
		try
		{
			$this->validate($request, ['email' => 'required|email', 
										'password' => 'required|min:3'
										]);
			$admin_data = array('email' => $request->email, 
								'password' => $request->password);
			if(Auth::guard('admin')->attempt($admin_data))
			{
				$request->session()->regenerate();
				#toastr()->success('You are successfully Login.');
				return redirect('/admin/dashboard');
			}else
			{
				return back()->with('alert', 'Wrong Credentials');
			}
		}catch(\Exception $ex)
		{
		    return back()->with('error', $ex->getMessage());
		}
	}

	/*
		Admin logout Attempt.
    */

	public function logout(Request $request)
	{
	  	Auth::guard('admin')->logout();
	  	return redirect('/');
	}

	/*
	* Display edit profile page
	*/ 
	public function editProfile()
	{
		# Get auth details
		$loginId = Auth::guard('admin')->user()->id;

		# get admin detail
		$admin  = $this->admin->findOrFail($loginId);

		# display the edit profile page
		return view('admin.my_profile', compact('admin'));
	}


	/*
	* Update login user profile
	*/ 
	public function updateProfile(Request $request, $id)
	{
		$this->validate($request,[
			'username' 	=> 'required',
			'email' 	=> 'required',
			'mobile' 	=> 'required',
			'region' 	=> 'required',
			'address' 	=> 'required',
			'country_id'=> 'required'
		],[
			'username.required' => ' The user name field is required.',
			'email.required' 	=> ' The email field is required.',
			'mobile.required' 	=> ' The mobile field is required.',
			'region.required' 	=> ' The region field is required.',
			'address.required'  => ' The address field is required.',
			'country_id.required'  => ' The country field is required.',
		]);

		
		# Gather the form data
		$profileData = [
			'username' 	=> $request->get('username'),
			'email' 	=> $request->get('email'),
			'mobile' 	=> $request->get('mobile'),
			'region' 	=> $request->get('region'),
			'address' 	=> $request->get('address'),
			'country_id'=> $request->get('country_id'),
			'city_id' 	=> $request->get('city_id'),
		];

		# update the profile 
		$this->admin->findOrFail($id)->update($profileData);

		# Display the profile page
		return redirect()->back()->with(['success_message' => 'Profile Update Successfully']);
	}
}
