<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Models\PopularPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PopularPlaceController extends Controller
{
    # Bind the popularPlace Model
    protected $popularPlaces;

    # Bind the Company Model
    protected $companies;

    # Bind the view path of company
    protected $view = 'admin.popular_places.';

    /**
     * Define the constructor of controller
     *
     * @return bind models
     */
    public function __construct(
                                Company $companies,
                                PopularPlace $popularPlaces
                                )
    {
        $this->companies  = $companies;
        $this->popularPlaces  = $popularPlaces;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Gather the filter input
        $countryName    = $request->get('country_name');
        $stateName      = $request->get('state_name');
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');

        # Get the all company
        $companies = $this->companies
                        ->with(['companyParking.parkingSetupSpace'])
                        ->searchFilter($countryName, $stateName, $fromDate, $todate)
                        ->get();

        # display a listing of popular-place.
        return view($this->view.'.index', compact(
                                                    'companies',
                                                    'countryName', 
                                                    'stateName', 
                                                    'fromDate',
                                                    'todate'
                                                ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # Get the all company
        $companies = $this->companies
                        ->get();

        # display a listing of popular-place.
        return view($this->view.'.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        # define relation array
        $relations =    [
                            'companyParking', 
                            'companyParking.parkingSetupSpace', 
                            'companyParking.parkingFeature',
                            'popularPlaces'
                        ];

        # Get the all company
        $company = $this->companies
                        ->with($relations)
                        ->findOrFail($id);

        # display a listing of popular-place.
        return view($this->view.'.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # define relation array
        $relations =    [
                            'companyParking', 
                            'companyParking.parkingSetupSpace', 
                            'companyParking.parkingFeature',
                            'popularPlaces'
                        ];

        # Get the all company
        $company = $this->companies
                        ->with($relations)
                        ->findOrFail($id);

        # display a listing of popular-place.
        return view($this->view.'.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # Gather the form data
        $popularPlacesData = $request->get('parking_place');

        # define relation array
        $relations =    [
                            'popularPlaces'
                        ];

        # Get the all company
        $companyPopularSpaces = $this->popularPlaces
                                    ->onlyCompany($id)
                                    ->get()
                                    ->toArray();

        if (!empty($popularPlacesData)) {
            foreach ($popularPlacesData as $popularPlace) {
                # check popular place selected or not
                if (isset($popularPlace['popular_place_id'])) {
                    # check selected popular place already in DB or not.
                    if (!in_array($popularPlace['popular_place_id'], array_column($companyPopularSpaces, 'parking_id'))) {

                        # set the popular place array
                        $popularPlaceData = [
                            'company_id' => $id,
                            'parking_id' => $popularPlace['popular_place_id'],
                            'selected_popular_place' => true
                        ];

                        # create new popularplace.
                        $this->popularPlaces->create($popularPlaceData);
                    }
                }
            }
        }

        # delete unselected popular place
        foreach ($companyPopularSpaces as $key => $companyPopularSpace) {
            # check unselected data for delete
            if (!in_array($companyPopularSpace['parking_id'], 
                array_column($popularPlacesData, 'popular_place_id'))) {
                $this->popularPlaces->findOrFail($companyPopularSpace['id'])->delete();
            }
        }

        # redirect back with success message
        return redirect()->back()->with(['status' => 200, 'success_message' => 'Popular Place Assigned Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
