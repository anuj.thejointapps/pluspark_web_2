<?php

namespace App\Http\Controllers\Admin;

use Mail;
use Validate;
use App\Models\Company;
use App\Utils\CommonUtil;
use App\Models\CompanyImage;
use Illuminate\Http\Request;
use App\Models\CompanyParking;
use App\Models\WasherAllotCompany;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
    # Bind the util 
    protected $commonUtil;

    # Bind the Company Model
    protected $companies;

    # Bind the CompanyParking Model
    protected $companyParkings;

    # Bind the WasherAllotCompany.
    protected $washerAllotCompany;

    # Bind the CompanyImage Model
    protected $companyImages;

    # Bind the view path of company
    protected $view = 'admin.company.';

    # Bind the type for create sequence
    protected $type = 'company';

    # Bind the pagination 
    protected $pagination = 10;

    /**
     * Define the constructor of controller
     *
     * @return bind models
     */
    public function __construct(
                                Company $companies, 
                                CompanyImage $companyImages, 
                                CompanyParking $companyParkings,
                                CommonUtil $commonUtil,
                                WasherAllotCompany $washerAllotCompany
                                )
    {
        $this->companies      = $companies;
        $this->commonUtil     = $commonUtil;
        $this->companyImages  = $companyImages;
        $this->companyParkings    = $companyParkings;
        $this->washerAllotCompany = $washerAllotCompany;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Gather the filter input
        $countryName    = $request->get('country_name');
        $stateName      = $request->get('state_name');
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');

        # Gather the all companies
        $companies =    $this->companies
                            ->searchFilter($countryName, $stateName, $fromDate, $todate)
                            ->paginate($this->pagination);

        # display the company list page.
        return view($this->view.'index', compact(
                                                'companies',
                                                'countryName', 
                                                'stateName', 
                                                'fromDate',
                                                'todate'
                                                ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # get company last refcount
        $refCount = $this->commonUtil->lastReferenceCount($this->type);

        # create company unique id and get company sequence.
        $companySequence = $this->commonUtil->genrateSequences($refCount, $this->type);

        # display the company model pop.
        return view($this->view.'create', compact('companySequence'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # gather the form data
        $companyData = [
            "company_auto_id"       => $request->get('company_auto_id'),
            "name"                  => $request->get('name'),
            "contact_person_name"   => $request->get('contact_person_name'),
            "contact_person_mobile" => $request->get('contact_person_mobile'),
            "date_of_join"          => date('Y-m-d', strtotime($request->get('date_of_join'))),
            "email"                 => $request->get('email'),
            "country_code"          => $request->get('country_code'),
            "number_of_parking"     => $request->get('no_of_parking'),
            "country"               => $request->get('country_name'),
            "city"                  => $request->get('city_name'),
            "address"               => $request->get('address'),
            "notes"                 => $request->get('notes'),
            "currency_id"           => $request->get('currency_id'),
        ];

        # create a company
        $company = $this->companies->create($companyData);

        # update ref. count of company
        $refCount = $this->commonUtil->referenceCount($this->type);

        # upload file
        if(Input::hasFile('company_images')) {
            foreach ($request->file('company_images') as $key => $image) {
                $filename        = $image->getClientOriginalName();
                $destinationpath = base_path() . '/public/assets/images/company_image/';

                $today_date     = date('d-m-Y');
                $random_number  = rand(1111, 9999);

                $filenameData = $today_date.'_'.$random_number.$filename;
                $movefilename = $image->move($destinationpath, $filenameData);

                if ($movefilename) {
                    $companyImageData = [
                                            'image_path' => 'assets/images/company_image/'.$filenameData,
                                            'company_id' => $company->id
                                        ];

                    # create company image
                    $this->companyImages->create($companyImageData);
                } else {
                   $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
                }
            }
        } else {
            $output = ['error' => 100, 'message' => 'File Not Found. Please Check !'];
        }

        # return with response and redirect to add parking page.
        return redirect()->route('company.generate-credential', [$company->id]);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        # Gather the company info.
        $company  = $this->companies
                        ->with([
                                'companyImages', 
                                'allotWasherOfCompany',
                                'companyParking',
                                'currency',
                                'allotWasherOfCompany.washer'
                                ])
                        ->findorFail($id);

        # display the company show page.
        return view($this->view.'show', compact('company'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCompanyParkingSpace($pakringId)
    {
        # get parking 
        $parking = $this->companyParkings
                        ->with([
                                    'parkingDriveFeatures',
                                    'parkingWasherFeatures',
                                    'parkingBookingFeature',
                                    'parkingSubscriptionFeature',
                                    'parkingFeature',
                                    'parkingSetupSpace',
                                    'parkingSetupSpace.parkingSpaceImages',
                                    'parkingSetupSpace.spaceType',
                                    'parkingDescription',
                                    'parkingMarkOut',
                                    'parkingPayoutSetting',
                                    'parkingAdvanceSetting'
                                ])
                        ->findOrFail($pakringId);

        # Display company parking space.
        return view($this->view.'company_parking_space', compact('parking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # Gather the company info.
        $company  = $this->companies->findorFail($id);

        # display the company edit page.
        return view($this->view.'edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # gather the form data
        $companyData = [
            "contact_person_name"   => $request->get('contact_person_name'),
            "name"                  => $request->get('name'),
            "contact_person_mobile" => $request->get('contact_person_mobile'),
            "date_of_join"          => date('Y-m-d', strtotime($request->get('date_of_join'))),
            "email"                 => $request->get('email'),
            "number_of_parking"     => $request->get('no_of_parking'),
            "country_code"          => $request->get('country_code'),
            "country"               => $request->get('country_name'),
            "city"                  => $request->get('city_name'),
            "address"               => $request->get('address'),
            "notes"                 => $request->get('notes'),
            "currency_id"           => $request->get('currency_id'),
        ];

        # update a company
        $company = $this->companies->findOrFail($id)->update($companyData);

        # upload file
        if(Input::hasFile('company_images')) {
            foreach ($request->file('company_images') as $key => $image) {
                $filename        = $image->getClientOriginalName();
                $destinationpath = base_path() . '/public/assets/images/company_image/';

                $today_date     = date('d-m-Y');
                $random_number  = rand(1111, 9999);

                $filenameData = $today_date.'_'.$random_number.$filename;
                $movefilename = $image->move($destinationpath, $filenameData);

                if ($movefilename) {
                    $companyImageData = [
                                            'image_path' => 'assets/images/company_image/'.$filenameData,
                                            'company_id' => $id
                                        ];

                    # create company image
                    $this->companyImages->create($companyImageData);
                } else {
                   $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
                }
            }
        } else {
            $output = ['error' => 100, 'message' => 'File Not Found. Please Check !'];
        }

        # set success output
        $output = ['status' => 200, 'success_message' => 'Company Updated Successfully!'];

        # return with response 
        return response()->json($output);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function companyEnableDisabled(Request $request)
    {
        # get company details
        $company = $this->companies->findOrFail($request->get('company_id'));

        # define $company data as an array
        $companyData = [];
        $message = '';

        # check company status
        if ($company->status == '1') {
            $companyData['status']  = '0';
            $message = "Company Inactive Successfully!";
        } else {
            $companyData['status']  = '1';
            $message = "Company Active Successfully!";
        }

        # update brand status
        $company->update($companyData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Generate company credential
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generateCompanyCredential($id)
    {
        // Get company.
        $company = $this->companies->findOrFail($id);

        # display the company credential page.
        return view($this->view.'company_credential', compact('company'));
    }

    /**
     * Generate company credential and mail send
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generateCredential(Request $request, $id)
    {
        # Gather form data and set data for update
        $email = $request->get('email');
        $companyData = [
            'password'   => Hash::make($request->get('password')),
        ];

        # update password
        $this->companies->findOrFail($id)->update($companyData);

        $company = [
            'email' => $email,
            'password' => $request->get('password'),
        ];

        # send mail with usename password
        $mail =     Mail::send('admin.forgot-password.company_credential_send_on_mail', $company, 
                    function($message) use($email) {
                        $message->to($email, 'Plus Spark')
                        ->subject('Company Credential');
                        $message->from('nishikant.thejointapps@gmail.com','Nishikant Tyagi');
                    });

        # redirect to company list
        return redirect()->route('company.index')->with(['success_message' => 'Company credential Updated Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * User Check Already is in record or not.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function emailCheck(Request $request)
    {
        $email = $request->input('email');

        $count = $this->companies->where('email', $email)->count();
        if ($count == 0) {
            echo "true";
            exit;
        } else {
            echo "false";
            exit;
        }
    }
}
