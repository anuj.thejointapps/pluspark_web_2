<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    # Base view
    protected $view = "admin.user_management.";

    # Bind the User Model
    protected $users;

    # Bind the pagination 
    protected $pagination = 10;

    /**
    * @method constructor for Controller
    * @return 
    * @param
    */
    public function __construct(User $users)
    {
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Gather the filter input
        $countryName    = $request->get('country_name');
        $stateName      = $request->get('state_name');
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');
        $month          = $request->get('month');

        # Get all users
        $users = $this->users
                        ->with(['country'])
                        ->searchFilter($countryName, $stateName, $fromDate, $todate, $month)
                        ->paginate($this->pagination);

        # Diaplay user list page
        return view($this->view.'index', compact(
                                                'users', 
                                                'countryName', 
                                                'stateName', 
                                                'fromDate',
                                                'todate',
                                                'month'
                                            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        # get user details
        $user = $this->users->findOrFail($id);

        # display detail page of user
        return view($this->view.'show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # get user details
        $user = $this->users->findOrFail($id);

        # display detail page of user
        return view($this->view.'edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // 'gender'        =>  'required',
            'name'          =>  'required',
            // 'country_name'  =>  'required',
            // 'state_name'    =>  'required'
        ]);

        # get user details
        $user = $this->users->findOrFail($id);

        # Gather the form data
        $userData = [
            'name'          => $request->get('name'),
            'country_name'  => $request->get('country_name'),
            'state_name'    => $request->get('state_name'),
            'gender'        => $request->get('gender'),
            'address'       => $request->get('address'),
        ];

        # update user
        $user->update($userData);

        # redirect back to user edit page wuth message
        return redirect()->back()->with(['message_success' => 'User Updated Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userEnableDisabled(Request $request)
    {
        # get user details
        $user = $this->users->findOrFail($request->get('userId'));

        # define $user data as an array
        $userData = [];
        $message = '';

        # check user status
        if ($user->status == '1') {
            $userData['status']  = '0';
            $message = "Model Inactive Successfully!";
        } else {
            $userData['status']  = '1';
            $message = "Model Active Successfully!";
        }

        # update user status
        $user->update($userData);

        # redirect back to listing page of user with message_success
        return response()->json(['status' => 200, 'message_success' => $message]);
    }

}
