<?php

namespace App\Http\Controllers\Admin;

#Models
use App\Models\Admin;
use App\Models\AdminPasswordReset;

use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    #Admin
    protected $admin;

    #adminPasswordReset
    protected $adminPasswordReset;

    # Base View
    protected $view = 'admin.forgot-password.';

    /**
    * @method constructor for Controller
    * @return 
    * @param
    */
    public function __construct(Admin $admin, AdminPasswordReset $adminPasswordReset)
    {
      $this->admin              = $admin;
      $this->adminPasswordReset = $adminPasswordReset;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #return to view
        return view($this->view.'index');
    }

    /**
     * @method to send reset password link on email
     * @return json
     * @param Request $request
     */
    public function sendEmail(Request $request)
    {
        #fetch Email
        $email = $request->get('email') ?? '';

        #Validate Email
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) { 
            return response()->json(['status' => 401, 'msg' => 'Email is Invalid']);
        }

        #Fetch Admin Data
        $admin = $this->admin->where('email', $email)->get();

        #Validate Admin
        if($admin->isEmpty()) {
            return response()->json(['status' => 401, 'msg' => 'Admin not found on email']);
        }

        #Set data for admin Passowrd Reset
        $data = [
            'email'  => $email,
            'token'  => str_random(60),
        ];

        #Create token for Email
        $adminEmailToken = $this->adminPasswordReset->create($data);

        # Send link to Email
        $link = Url('/') . '/password-reset/' . $adminEmailToken->token . '?email=' . urlencode($email);
        
        #mailData
        $mailData = ['admin' => $admin->first(), 'link' => $link];

        #Send Email on Admin Email
        $mail = Mail::send('admin.forgot-password.email', $mailData, function($message) use($email) {
                 $message->to($email, 'Plus Spark')->subject
                    ('Password Reset Link is');
                 $message->from('nishikant.thejointapps@gmail.com','Nishikant Tyagi');
              });

        #Validate Mail Send
        if(count(Mail::failures()) > 0) {
            return response()->json(['status' => 401, 'msg' => 'Something went wrong']); 
        }
        
        #Mail Sent
        return response()->json(['status' => 200, 'msg' => 'Reset Password link sent on email.']);
    }

    /**
     * @method to reset Pssword
     * @return to view
     * @param 
     */
    public function resetPassword($token, Request $request)
    {
        #Fetch Data
        $email = $request->email;

        #Create token for Email
        $adminEmailToken = $this->adminPasswordReset
                                ->where('email', $email)
                                ->where('token', $token)
                                ->get();

        #Fetch Admin
        $admin = $this->admin->where('email', $email)->get();

        # validate if not Found
        if($adminEmailToken->isEmpty() OR $admin->isEmpty()) {
            return view($this->view.'reset-password')->with(['status' => 401, 'msg' => 'Sorry! User Not found in records']);
        }

        #Fetch last Token Model
        $adminEmailToken = $adminEmailToken->last();

        #Validate token Expiry
        if($adminEmailToken->created_at->diffInMinutes(Carbon::now()) > 160) {
            return view($this->view.'reset-password')->with(['status' => 401, 'msg' => 'Sorry! Link expired Please try again']);
        }

        #return to view
        return view($this->view.'reset-password')->with(['status' => 200, 'msg' => '', 'admin' => $admin->first()]);
    }

    /**
     * @method to reset the admin password
     * @return json
     * @param Request $request
     */
    public function resetAdminPassword(Request $request)
    {
        #Fetch Data
        $data = [
            'password' => Hash::make($request->password),
        ];

        #Fetch Admin
        $admin = $this->admin->find($request->adminId);

        #Update Admin Password
        $admin->update($data);

        #return to login
        return response()->json(['status' => 200, 'msg' => 'Password Updated Successfully']);
    }
}

