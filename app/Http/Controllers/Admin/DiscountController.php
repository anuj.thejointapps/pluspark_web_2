<?php

namespace App\Http\Controllers\Admin;

#Models
use App\Models\Company;
use App\Models\Discount;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountController extends Controller
{
    # Bind the Company Model
    protected $company;

    # Bind the Discount Model
    protected $discount;

    # Bind the pagination 
    protected $pagination = 10;

    # Base View
    protected $view = 'admin.discount.';
    
    /**
     * @method Define the constructor of controller.
     *  @param Discount $discount
     * @return
     */    
    public function __construct(Company $company, Discount $discount)
    {
        $this->company  = $company;
        $this->discount = $discount;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Gather the filter input
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');
        $month          = $request->get('month');

        # get the all colors
        $discounts =    $this->discount
                            ->with(['company'])
                            ->searchFilter($fromDate, $todate, $month)
                            ->paginate($this->pagination);

        # display the listing of color
        return view($this->view.'index', compact(
                                                    'discounts',
                                                    'fromDate',
                                                    'todate',
                                                    'month'
                                                ));
    }

    /**
     * @method to fetch company Id
     * @return json
     * @param Request $request
     */
    public function fetchCompanyId(Request $request)
    {
        #Fetch CompanyId
        $companyId = $request->id;

        #Fetch Company
        $company = $this->company->find($companyId);

        #Fetch Company Auto generate id
        $companyId = $company->company_auto_id ?? '';

        #return response
        return response()->json(['status' => 200, 'id' => $companyId]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # Display the create color page
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # Fetch Data
        $startDate  = $request->start_date.' 00:00:00';
        $endDate    = $request->end_date.' 00:00:00';

        # Fetch Discount 
        $discount = $this->discount
                        ->whereDate('start_date', '>=', $startDate)
                        ->whereDate('end_date', '<=', $endDate)
                        ->first();

        if (!empty($discount->id)) {
            redirect()->back()->with(['success_message' => 'Discount already added on this start date and end date!']);
        }

        #Set Date to Save
        $data = [
            'company_id'        => 0,
            'title'             => $request->name ?? '',
            'description'       => $request->description ?? '',
            'percent_discount'  => $request->discount_percent ?? '',
            'on_booking'        => isset($request->booking) ? true : false,
            'on_subscription'   => isset($request->subscription) ? true : false,
            'on_wallet'         => isset($request->valet) ? true : false,
            'start_date'        => $startDate,
            'end_date'          => $endDate,
            'status'            => true,
            'added_by'          => 'admin',
        ];

        #Cerate Discount
        $this->discount->create($data);

        #return redirect
        return redirect()->route('discount.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Code
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # get the color details
        $color = $this->colors->findOrFail($id);

        # Display the edit color page
        return view($this->view.'edit', compact('color'));
    }

    /**
     * @method to open update Modal
     * @param Request $request
     * @return json
     */
    public function openDiscountUpdate(Request $request)
    {
        #Fetch id
        $discountId = $request->id;

        #Fetch Discount 
        $discount = $this->discount
                         ->with(['company'])
                         ->find($discountId);
        
        # return to Modal View
        $html = view($this->view.'update')->with(['discount' => $discount])->render();

        # return json
        return response()->json(['status' => 200, 'html' => $html]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discount $discount)
    {
        #Fetch Data
        $startDate  = $request->start_date.' 00:00:00';
        $endDate    = $request->end_date.' 00:00:00';

        # Fetch Discount 
        $discountExist = $this->discount
                        ->whereDate('start_date', '>=', $startDate)
                        ->whereDate('end_date', '<=', $endDate)
                        ->where('id', '!=', $discount->id)
                        ->first();

        if (!empty($discountExist->id)) {
            redirect()->back()->with(['success_message' => 'Discount already added on this start date and end date!']);
        }

        #Set Date to Save
        $data = [
            'company_id'        => 0,
            'title'             => $request->name ?? '',
            'description'       => $request->description ?? '',
            'percent_discount'  => $request->discount_percent ?? '',
            'on_booking'        => isset($request->booking) ? true : false,
            'on_subscription'   => isset($request->subscription) ? true : false,
            'on_wallet'         => isset($request->valet) ? true : false,
            'start_date'        => $startDate,
            'end_date'          => $endDate,
            'status'            => true,
        ];

        #Cerate Discount
        $discount->update($data);

        #return redirect
        return redirect()->route('discount.index');
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request)
    {
        # get discount details
        $discount = $this->discount->findOrFail($request->id);

        # define $discount data as an array
        $discountData = [];
        $message = '';

        # check color status
        if ($discount->status) {
            $discountData['status']  = false;
            $message = "Discount Inactive Successfully!";
        } else {
            $discountData['status']  = true;
            $message = "Discount Active Successfully!";
        }

        # update discount status
        $discount->update($discountData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
