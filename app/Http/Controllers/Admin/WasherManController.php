<?php

namespace App\Http\Controllers\Admin;

use Mail;
use App\Utils\CommonUtil;
use App\Models\Washerman;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class WasherManController extends Controller
{
    # Bind the util 
    protected $commonUtil;

    # Bind the Washerman Model
    protected $washermens;

    # Bind the view path of company
    protected $view = 'admin.washerman.';

    # Bind the type for create sequence
    protected $type = 'washer';

    # Bind the pagination 
    protected $pagination = 10;

    /**
     * Define the constructor of controller
     *
     * @return bind models
     */
    public function __construct(Washerman $washermens, CommonUtil $commonUtil)
    {
        $this->washermens = $washermens;
        $this->commonUtil = $commonUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Gather the filter input
        $countryName    = $request->get('country_name');
        $stateName      = $request->get('state_name');
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');
        $month          = $request->get('month');

        # Get all washerman
        $washermens = $this->washermens
                            ->onlyWasher()
                            ->searchFilter($countryName, $stateName, $fromDate, $todate, $month)
                            ->paginate($this->pagination);

        # return view.
        return view($this->view.'index', compact(
                                                    'washermens',
                                                    'countryName', 
                                                    'stateName', 
                                                    'fromDate',
                                                    'todate',
                                                    'month'
                                                ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # get washer last refcount
        $refCount = $this->commonUtil->lastReferenceCount($this->type);

        # create washer unique id and get washer sequence.
        $washerSequence = $this->commonUtil->genrateSequences($refCount, $this->type);

        # display the washer model pop.
        return view($this->view.'create', compact('washerSequence'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # check address enter or not
        $addressData = [];
        if (!empty($request->get('address'))) {
            # get lat long and pincode from google api
            $addressData = $this->commonUtil->geocode($request->get('address'));
        }

        # Generate Random password.
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomNumber = substr(str_shuffle($str_result), 0, 8);
        $password = $randomNumber;
        $email = $request->get('email');

        # Gather the form data
        $washerData = [
            'washer_auto_id'    => $request->get('washer_auto_id'),
            'name'              => $request->get('name'),
            'job_type_id'       => 3,
            'email'             => $email,
            'password'          => Hash::make($password),
            'country_code'      => $request->get('country_code'),
            'mobile'            => $request->get('mobile'),
            'city'              => $request->get('city_name'),
            'country'           => $request->get('country_name'),
            'gender'            => $request->get('gender'),
            'date_of_join'      => date('Y-m-d'),
            'address'           => $request->get('address'),
            'latitude'          => !empty($addressData[0]) ? $addressData[0] : '',
            'longitude'         => !empty($addressData[1]) ? $addressData[1] : '',
            'pincode'           => !empty($addressData[2]) ? $addressData[2] : Null,
        ];

        # upload file
        if(Input::hasFile('washer_image')) {
            #Fetch File
            $filename        = $request->file('washer_image')
                                       ->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/washer_image/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('washer_image')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $washerData['image_path'] = 'assets/images/washer_image/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               return response()->json($output);
            }
        }

        # create a washer.
        $this->washermens->create($washerData);

        # update ref. count of washer
        $refCount = $this->commonUtil->referenceCount($this->type);

        # send washer credential into mail
        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        # send mail with usename password
        $mail =     Mail::send('admin.forgot-password.staff_credential_send_into_mail', $credentials, 
                        function($message) use($email) {
                            $message->to($email, 'Plus Spark')
                            ->subject('Company Credential');
                            $message->from('nishikant.thejointapps@gmail.com','Nishikant Tyagi');
                        }
                    );

        # return response json
        return response()->json(['status' => 200, 'success_message' => 'Washer Added Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        # Gather the washer info.
        $washer  = $this->washermens->findorFail($id);

        # display the washer show page.
        return view($this->view.'show', compact('washer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # Gather the washer info.
        $washer  = $this->washermens->findorFail($id);

        # display the washer edit page.
        return view($this->view.'edit', compact('washer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # check address enter or not
        $addressData = [];
        if (!empty($request->get('address'))) {
            # get lat long and pincode from google api
            $addressData = $this->commonUtil->geocode($request->get('address'));
        }

        # Gather the form data
        $washerData = [
            'name'              => $request->get('name'),
            'email'             => $request->get('email'),
            'mobile'            => $request->get('mobile'),
            'country_code'      => $request->get('country_code'),
            'city'              => $request->get('city_name'),
            'country'           => $request->get('country_name'),
            'gender'            => $request->get('gender'),
            'address'           => $request->get('address'),
            'latitude'          => !empty($addressData[0]) ? $addressData[0] : '',
            'longitude'         => !empty($addressData[1]) ? $addressData[1] : '',
            'pincode'           => !empty($addressData[2]) ? $addressData[2] : Null,
        ];

        # upload file
        if(Input::hasFile('washer_image')) {
            $filename        = $request->file('washer_image')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/washer_image/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('washer_image')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $washerData['image_path'] = 'assets/images/washer_image/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               return response()->json($output);
            }
        }

        # create a washer.
        $this->washermens->findOrFail($id)->update($washerData);

        # return response json
        return response()->json(['status' => 200, 'success_message' => 'Washer Updated Successfully!']);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function washerEnableDisabled(Request $request)
    {
        # get washer details
        $washer = $this->washermens->findOrFail($request->get('washer_id'));

        # define $washer data as an array
        $washerData = [];
        $message = '';

        # check washer status
        if ($washer->status == '1') {
            $washerData['status']  = '0';
            $message = "Washer Inactive Successfully!";
        } else {
            $washerData['status']  = '1';
            $message = "Washer Active Successfully!";
        }

        # update brand status
        $washer->update($washerData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * User Check Already is in record or not.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function emailCheck(Request $request)
    {
        $email = $request->input('email');

        $count = $this->washermens->where('email', $email)->count();
        if ($count == 0) {
            echo "true";
            exit;
        } else {
            echo "false";
            exit;
        }
    }

    /**
     * User Check Already is in record or not.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mobileCheck(Request $request)
    {
        $mobile = $request->input('mobile');

        $count = $this->washermens->where('mobile', $mobile)->count();
        if ($count == 0) {
            echo "true";
            exit;
        } else {
            echo "false";
            exit;
        }
    }
}
