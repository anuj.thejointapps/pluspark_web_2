<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;

class CountryController extends Controller
{
    # Bind the view
    protected $view = "admin.country.";
    
    # Bind the Country model
    protected $countries;

    /*
    * Define the constructor of model
    */ 
    public function __construct(Country $countries)
    {
        $this->countries = $countries;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Gather all countries
        $countries = $this->countries->get();

        # display listing of resource
        return view($this->view.'index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # display listing of resource
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>  'required',
            'country_code'  =>  'required'
        ]);

        # Gather the form data
        $formData = [
            'name'  => $request->get('name'),
            'country_code'  => $request->get('country_code')
        ];

        # update state
        $this->countries->create($formData);

        # redirect back to update page
        return redirect()->back()->with(['success_message' => 'Country Created Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # Get countries
        $country = $this->countries->findOrFail($id);

        # display listing of resource
        return view($this->view.'edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'   =>  'required',
            'country_code'  =>  'required'
        ]);

        # Gather the form data
        $formData = [
            'name'  => $request->get('name'),
            'country_code'  => $request->get('country_code')
        ];

        # update state
        $this->countries->findOrFail($id)->update($formData);

        # redirect back to update page
        return redirect()->back()->with(['success_message' => 'Country Updated Successfully!']);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function countryEnableDisabled(Request $request)
    {
        # get country details
        $country = $this->countries->findOrFail($request->get('country_id'));

        # define $country data as an array
        $countryData = [];
        $message = '';

        # check country status
        if ($country->status == '1') {
            $countryData['status']  = '0';
            $message = "Country Inactive Successfully!";
        } else {
            $countryData['status']  = '1';
            $message = "Country Active Successfully!";
        }

        # update country status
        $country->update($countryData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        # Get countries
        $country = $this->countries->findOrFail($id)->delete();

        # redirect back to list
        return redirect()->back()->with(['success_message' => 'Country Deleted Successfully!']);
    }
}
