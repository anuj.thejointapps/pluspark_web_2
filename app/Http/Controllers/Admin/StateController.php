<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\Country;

class StateController extends Controller
{
    # Bind the view
    protected $view = "admin.state.";
    
    # Bind the State model
    protected $states;

    # Bind the Country model
    protected $countries;

    /*
    * Define the constructor of model
    */ 
    public function __construct(State $states, Country $countries)
    {
        $this->states       = $states;
        $this->countries    = $countries;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Gather all states
        $states = $this->states->with(['country'])->get();

        # display listing of resource
        return view($this->view.'index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # display listing of resource
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country_id'  =>  'required',
            'name'        =>  'required'
        ]);

        # Gather the form data
        $formData = [
            'country_id'  => $request->get('country_id'),
            'name'        => $request->get('name')
        ];

        # update state
        $this->states->create($formData);

        # redirect back to update page
        return redirect()->back()->with(['success_message' => 'City Created Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       # Get states
        $state = $this->states->findOrFail($id);

        # display listing of resource
        return view($this->view.'edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'country_id'  =>  'required',
            'name'        =>  'required'
        ]);

        # Gather the form data
        $formData = [
            'country_id'  => $request->get('country_id'),
            'name'        => $request->get('name')
        ];

        # update state
        $this->states->findOrFail($id)->update($formData);

        # redirect back to update page
        return redirect()->back()->with(['success_message' => 'City Updated Successfully!']);
    }

    /**
     * Get states by country id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getStatesByCountry(Request $request)
    {
        # Gether the country id
        $countryId = $request->get('country_id');

        # get all state of country
        $states = $this->states->isCountry($countryId)->get();

        # redirect response.
        return response()->json(['status' => 200, 'message' => 'Get All Country', 'data' => $states]);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function stateEnableDisabled(Request $request)
    {
        # get state details
        $state = $this->states->findOrFail($request->get('state_id'));

        # define $state data as an array
        $stateData = [];
        $message = '';

        # check state status
        if ($state->status == '1') {
            $stateData['status']  = '0';
            $message = "City Inactive Successfully!";
        } else {
            $stateData['status']  = '1';
            $message = "City Active Successfully!";
        }

        # update state status
        $state->update($stateData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        # Get states
        $state = $this->states->findOrFail($id)->delete();

        # redirect back to list
        return redirect()->back()->with(['success_message' => 'City Deleted Successfully!']);
    }
}
