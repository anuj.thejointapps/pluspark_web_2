<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Models\Washerman;
use Illuminate\Http\Request;
use App\Models\WasherAllotCompany;
use App\Http\Controllers\Controller;

class WasherCompanyAllotController extends Controller
{
    # Bind the Company Model
    protected $companies;

    # Bind the Washerman Model
    protected $washermens;

    # Bind the WasherAllotCompany.
    protected $washerAllotCompany;

    # Bind the view path of company
    protected $view = 'admin.washer_company_allot.';

    # Bind the type for create sequence
    protected $type = 'company';

    /**
     * Define the constructor of controller
     *
     * @return bind models
     */
    public function __construct(
                                Company $companies, 
                                Washerman $washermens, 
                                WasherAllotCompany $washerAllotCompany
                            )
    {
        $this->companies            = $companies;
        $this->washermens           = $washermens;
        $this->washerAllotCompany   = $washerAllotCompany;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Gather the filter input
        $countryName    = $request->get('country_name');
        $stateName      = $request->get('state_name');
        $fromDate       = $request->get('start_date');
        $todate         = $request->get('end_date');

        # Gather the company.
        $companies = $this->companies
                            ->searchFilter($countryName, $stateName, $fromDate, $todate)
                            ->get();

        # display the company list for washer allot
        return view($this->view.'index', compact(
                                                    'companies',
                                                    'countryName', 
                                                    'stateName', 
                                                    'fromDate',
                                                    'todate'
                                                ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # get company id
        $company_id = $id;

        # Get all washer of alloted on company.
        $allotedWasher  = $this->washerAllotCompany
                                ->select(['washer_id', 'id'])
                                ->onlyCompany($company_id)
                                ->get()
                                ->toArray();

        # Get all washer.
        $washers = $this->washermens->onlyWasher()->get();

        # display the company allot.
        return view($this->view.'washer_allot_to_company', compact(
                                                                    'washers', 
                                                                    'company_id', 
                                                                    'allotedWasher'
                                                                ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # check validation
        $request->validate([
            'washers' => 'required'
        ]);

        // dd($request->all());

        # Gather the all washer
        $washers    = $request->get('washers');
        $companyId  = $request->get('company_id');

        # Get all washer of alloted on company.
        $allotedWasher  = $this->washerAllotCompany
                                ->select(['washer_id', 'id'])
                                ->onlyCompany($companyId)
                                ->get()
                                ->toArray();

        if (!empty($washers)) {
            foreach ($washers as $washer) {
                if (!in_array($washer['create_id'], array_column($allotedWasher, 'id'))) {
                    # Gather the form data.
                    $allotWasherData = [
                        'company_id'  => $companyId,
                        'washer_id'  => $washer['create_id'],
                    ];

                    # create a washer.
                    $this->washerAllotCompany->create($allotWasherData);
                }
            }

            foreach ($allotedWasher as $altWasher) {
                if (!in_array($altWasher['id'], array_column($washers, 'create_id'))) {
                    # delete a washer.
                    $this->washerAllotCompany->findOrFail($altWasher['id'])->delete();
                }
            }
        }

        # redirect back to on washer allot to company.
        return redirect()->back()->with([ 'status' => 200, 'success_message' => 'Washer Alloted to company Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
