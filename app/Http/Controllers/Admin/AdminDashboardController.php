<?php

namespace App\Http\Controllers\Admin;

# Models

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{
  #Base view
  protected $view = "admin.dashboard.";

  /**
   * @method constructor for Controller
   * @return 
   * @param
  */
  public function __construct()
  {
    # code...
  }

  /**
   * @method index for class
   * @param
   * @return to view
  */
  public function index()
  {
    #return view
    return view($this->view.'index');
  }
}






