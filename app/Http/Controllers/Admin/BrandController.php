<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    # Bind the Brand Model
    protected $brands;

    # Bind the view of Brand
    protected $view = 'admin.brands.';

    /**
     * Define the constructor of controller.
     *
     * @return Bind the Brand Model
     */    
    public function __construct(Brand $brands)
    {
        $this->brands = $brands;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # get the all brands
        $brands = $this->brands->all();

        # display the listing of color
        return view($this->view.'index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # Display the create brand page
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # check validation
        $request->validate([
            'name' => 'required'
        ]);

        # Gather the form data
        $brandData  = [
            'name' => $request->get('name')
        ];

        # create new brand
        $this->brands->create($brandData);

        # redirect back to create brand with message.
        return redirect()->back()->with(['success_message' => 'Brand Created Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Code
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # get the brand details
        $brand = $this->brands->findOrFail($id);

        # Display the edit brand page
        return view($this->view.'edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # check validation
        $request->validate([
            'name' => 'required'
        ]);

        # Gather the form data
        $brandData  = [
            'name' => $request->get('name')
        ];

        # update brand
        $this->brands->findOrFail($id)->update($brandData);

        # redirect back to edit page with message.
        return redirect()->back()->with(['success_message' => 'Brand Updated Successfully!']);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function brandEnableDisabled(Request $request)
    {
        # get brand details
        $brand = $this->brands->findOrFail($request->get('brand_id'));

        # define $brand data as an array
        $brandData = [];
        $message = '';

        # check brand status
        if ($brand->status == '1') {
            $brandData['status']  = '0';
            $message = "Brand Inactive Successfully!";
        } else {
            $brandData['status']  = '1';
            $message = "Brand Active Successfully!";
        }

        # update brand status
        $brand->update($brandData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
