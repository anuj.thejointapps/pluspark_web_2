<?php

namespace App\Http\Controllers\Admin;

use App\Models\Modal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModalController extends Controller
{
    # Bind the modal 
    protected $modals;

    # Bind the view
    protected $view = 'admin.modals.';

    /**
     * Define the constructor of controller.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Modal $modals)
    {
        $this->modals = $modals;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Get all modals
        $modals = $this->modals->with(['brand'])->get();

        # display the list of modal
        return view($this->view.'index', compact('modals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # display the create page of modal
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # check validation
        $request->validate([
            'brand_id' => 'required',
            'name' => 'required'
        ]);

        # Gather the form data
        $modalData = [
            'brand_id'  => $request->get('brand_id'),
            'name'  => $request->get('name')
        ];

        # create new modal
        $this->modals->create($modalData);

        # redirect back to create page with message
        return redirect()->back()->with(['success_message' => 'Model created Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # Get modal
        $modal = $this->modals->with(['brand'])->findOrFail($id);

        # show edit page of modal
        return view($this->view.'edit', compact('modal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # check validation
        $request->validate([
            'brand_id' => 'required',
            'name' => 'required'
        ]);

        # Gather the form data
        $modalData = [
            'brand_id'  => $request->get('brand_id'),
            'name'  => $request->get('name')
        ];

        # update modal
        $this->modals->findOrFail($id)->update($modalData);

        # redirect back to edit page with message
        return redirect()->back()->with(['success_message' => 'Model updated Successfully!']);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modelEnableDisabled(Request $request)
    {
        # get modal details
        $modal = $this->modals->findOrFail($request->get('model_id'));

        # define $modal data as an array
        $modalData = [];
        $message = '';

        # check modal status
        if ($modal->status == '1') {
            $modalData['status']  = '0';
            $message = "Model Inactive Successfully!";
        } else {
            $modalData['status']  = '1';
            $message = "Model Active Successfully!";
        }

        # update modal status
        $modal->update($modalData);

        # redirect back to listing page of modal with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
