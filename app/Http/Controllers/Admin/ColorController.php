<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{
    # Bind the Color Model
    protected $colors;

    # Bind the view of Color
    protected $view = 'admin.colors.';
    
    /**
     * Define the constructor of controller.
     *
     * @return Bind the Color Model
     */    
    public function __construct(Color $colors)
    {
        $this->colors = $colors;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # get the all colors
        $colors = $this->colors->all();

        # display the listing of color
        return view($this->view.'index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # Display the create color page
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # check validation
        $request->validate([
            'name' => 'required'
        ]);

        # Gather the form data
        $colorData  = [
            'name' => $request->get('name')
        ];

        # create color
        $this->colors->create($colorData);

        # redirect back to create page with message.
        return redirect()->back()->with(['success_message' => 'Color Created Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Code
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # get the color details
        $color = $this->colors->findOrFail($id);

        # Display the edit color page
        return view($this->view.'edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        # check validation
        $request->validate([
            'name' => 'required'
        ]);

        # Gather the form data
        $colorData  = [
            'name' => $request->get('name')
        ];

        # update color
        $this->colors->findOrFail($id)->update($colorData);

        # redirect back to edit page with message.
        return redirect()->back()->with(['success_message' => 'Color Updated Successfully!']);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function colorEnableDisabled(Request $request)
    {
        # get color details
        $color = $this->colors->findOrFail($request->get('color_id'));

        # define $color data as an array
        $colorData = [];
        $message = '';

        # check color status
        if ($color->status == '1') {
            $colorData['status']  = '0';
            $message = "Color Inactive Successfully!";
        } else {
            $colorData['status']  = '1';
            $message = "Color Active Successfully!";
        }

        # update color status
        $color->update($colorData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
