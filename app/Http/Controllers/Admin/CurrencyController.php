<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Support\Facades\Input;

class CurrencyController extends Controller
{
    # Bind the view
    protected $view = "admin.currency.";
    
    # Bind the Currency model
    protected $currencies;

    /*
    * Define the constructor of model
    */ 
    public function __construct(Currency $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Gather all currencies
        $currencies = $this->currencies->get();

        # display listing of resource
        return view($this->view.'index', compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # display listing of resource
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>  'required',
            'currency_code'  =>  'required',
            'symbol'  =>  'required'
        ]);

        # Gather the form data
        $formData = [
                'name'             => $request->get('name'),
                'currency_code'    => $request->get('currency_code'),
                'country_id'       => $request->get('country_id'),
        ];

        # upload file
        if(Input::hasFile('symbol')) {
            $filename        = $request->file('symbol')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/symbol/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('symbol')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $formData['symbol'] = 'assets/images/symbol/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               return response()->json($output);
            }
        }

        # update state
        $this->currencies->create($formData);

        # redirect back to update page
        return redirect()->back()->with(['success_message' => 'Currency Created Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        # Get currency
        $currency = $this->currencies->findOrFail($id);

        # display listing of resource
        return view($this->view.'edit', compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  =>  'required',
            'currency_code'  =>  'required',
        ]);

        # Gather the form data
        $formData = [
                'name'             => $request->get('name'),
                'currency_code'    => $request->get('currency_code'),
                'country_id'       => $request->get('country_id')
        ];

        # upload file
        if(Input::hasFile('symbol')) {
            $filename        = $request->file('symbol')->getClientOriginalName();
            $destinationpath = base_path() . '/public/assets/images/symbol/';

            $today_date     = date('d-m-Y');
            $random_number  = rand(1111, 9999);

            $filenameData = $today_date.'_'.$random_number.$filename;
            $movefilename = $request->file('symbol')->move($destinationpath, $filenameData);

            if ($movefilename) {
                $formData['symbol'] = 'assets/images/symbol/'.$filenameData;
            } else {
               $output = ['error' => 100, 'message' => 'File Not Uploaded. Please Check !', 'image_name' => $filename];
               return response()->json($output);
            }
        }

        # update currency
        $this->currencies->findOrFail($id)->update($formData);

        # redirect back to update page
        return redirect()->back()->with(['success_message' => 'Currency Updated Successfully!']);
    }

    /**
     * Update Status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function currencyEnableDisabled(Request $request)
    {
        # get currency details
        $currency = $this->currencies->findOrFail($request->get('currency_id'));

        # define $currency data as an array
        $currencyData = [];
        $message = '';

        # check currency status
        if ($currency->status == '1') {
            $currencyData['status']  = '0';
            $message = "Currency Inactive Successfully!";
        } else {
            $currencyData['status']  = '1';
            $message = "Currency Active Successfully!";
        }

        # update currency status
        $currency->update($currencyData);

        # redirect back to listing page of state with message_success
        return response()->json(['status' => 200, 'success_message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
