<?php

namespace App\Http\Controllers\Admin;

#Models
use App\Models\Company;
use App\Models\DriverCommission;
use App\Models\WasherCommission;

#Base Files
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommissionController extends Controller
{
    # Bind the Company Model
    protected $company;

    # Bind the driverCommission Model
    protected $driverCommission;

    # Bind the washerCommission Model
    protected $washerCommission;

    # Bind the view path of company
    protected $view = 'admin.commission-management.';

    /**
     * Define the constructor of controller
     *
     * @return bind models
     */
    public function __construct(
            Company $company, DriverCommission $driverCommission,
            WasherCommission $washerCommission 
    ) {
        $this->company              = $company;
        $this->driverCommission     = $driverCommission;
        $this->washerCommission     = $washerCommission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Fetch the company
        $companies = $this->company
                          ->with(['driverCommission', 'washerCommission'])
                          ->get();

        #return to view
        return view($this->view.'index')->with(['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company, Request $request)
    {
        #Fetch Company

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function OpenEditCommission(Request $request)
    {
        #Fetch Company Id
        $company = $this->company
                        ->with(['driverCommission', 'washerCommission'])
                        ->find($request->id);
        
        # return to Modal View
        $html = view($this->view.'edit')->with([
            'company'           => $company, 
            'driverCommission'  => $company->driverCommission, 
            'washerCommission'  => $company->washerCommission, 
            'type' => $request->type
        ])->render();

        # return json
        return response()->json(['status' => 200, 'html' => $html]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        #Fetch Company
        #Fetch Company Id
        $company = $this->company
                        ->with(['driverCommission', 'washerCommission'])
                        ->find($request->company_id);

        #fetch Driver Commisiion
        $driverCommiss = $company->driverCommission;
        $washerCommiss = $company->washerCommission;

        #Fetch Data for 
        if($request->type == 'driver') {
            $data = [
                'company_id'            => $company->id,
                'company_cash_comm'     => $request->company_cash,
                'company_card_comm'     => $request->company_visa_master,
                'pls_park_cash_comm'    => $request->plus_park_cash,
                'pls_park_card_comm'    => $request->plus_park_visa_master,
                'thrd_party_cash_comm'  => $request->third_party_cash,
                'thrd_party_card_comm'  => $request->third_visa_master,
            ];

            #Validate
            if($driverCommiss == '') {
                $this->driverCommission->create($data);
            } else {
                $driverCommiss->update($data);
            }
        } else {
            $data = [
                'company_id'            => $company->id,
                'company_cash_comm'     => $request->company_cash,
                'company_card_comm'     => $request->company_visa_master,
                'pls_park_cash_comm'    => $request->plus_park_cash,
                'pls_park_card_comm'    => $request->plus_park_visa_master,
                'thrd_party_cash_comm'  => $request->third_party_cash,
                'thrd_party_card_comm'  => $request->third_visa_master,
            ];

            #Validate
            if($washerCommiss == '') {
                $this->washerCommission->create($data);
            } else {
                $washerCommiss->update($data);
            }
        }

        #return route
        return redirect()->route('commission.index')
                         ->with(['success_message' => 'Commission Updated successfully!']);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
