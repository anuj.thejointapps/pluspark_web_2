<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\DiscountPercent;

class DiscountPercentComposer 
{
    /**
     * The Brand repository implementation.
     *
     * @var Bind DiscountPercent model
     */
    protected $discountPercent;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'discountPercents';

    /**
     * Create a new profile composer.
     *
     * @param  DiscountPercent  $discountPercent
     * @return void
     */
    public function __construct(DiscountPercent $discountPercent)
    {
        // Dependencies automatically resolved by service container...
        $this->discountPercent   = 	$discountPercent;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\DiscountPercent $discountPercent
     * @return Collection
     */
    protected function populate()
    {
        return $this->discountPercent->active()->get();
    }
}