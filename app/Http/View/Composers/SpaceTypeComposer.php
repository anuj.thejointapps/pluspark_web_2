<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\SpaceType;

class SpaceTypeComposer 
{
    /**
     * The SpaceType repository implementation.
     *
     * @var Bind SpaceType model
     */
    protected $spaceTypes;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'spaceTypes';

    /**
     * Create a new profile composer.
     *
     * @param  SpaceType  $SpaceTypes
     * @return void
     */
    public function __construct(SpaceType $spaceTypes)
    {
        // Dependencies automatically resolved by service container...
        $this->spaceTypes   = 	$spaceTypes;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\SpaceType $spaceTypes
     * @return Collection
     */
    protected function populate()
    {
        return $this->spaceTypes->get();
    }
}