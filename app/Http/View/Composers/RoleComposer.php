<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class RoleComposer 
{
    /**
     * The Role repository implementation.
     *
     * @var Bind Role model
     */
    protected $roles;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'roles';

    /**
     * Create a new profile composer.
     *
     * @param  Role  $roles
     * @return void
     */
    public function __construct(Role $roles)
    {
        // Dependencies automatically resolved by service container...
        $this->roles   = 	$roles;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\Role $roles
     * @return Collection
     */
    protected function populate()
    {
        return $this->roles->where('is_admin', 0)->get();
    }
}