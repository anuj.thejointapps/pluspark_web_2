<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\PlanManagement;

class PlanComposer 
{
    /**
     * The PlanManagement repository implementation.
     *
     * @var Bind PlanManagement model
     */
    protected $plans;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'plans';

    /**
     * Create a new profile composer.
     *
     * @param  PlanManagement  $plans
     * @return void
     */
    public function __construct(PlanManagement $plans)
    {
        // Dependencies automatically resolved by service container...
        $this->plans   = 	$plans;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\PlanManagement $plans
     * @return Collection
     */
    protected function populate()
    {
        return $this->plans->where('is_deleted', 0)->get();
    }
}