<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Country;

class CountryComposer 
{
    /**
     * The Country repository implementation.
     *
     * @var Bind Country model
     */
    protected $countries;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'countries';

    /**
     * Create a new profile composer.
     *
     * @param  Country  $countries
     * @return void
     */
    public function __construct(Country $countries)
    {
        // Dependencies automatically resolved by service container...
        $this->countries   = 	$countries;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\Coountry $countries
     * @return Collection
     */
    protected function populate()
    {
        return $this->countries->all();
    }
}