<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Company;

class CompanyComposer 
{
    /**
     * The Company repository implementation.
     *
     * @var Bind Company model
     */
    protected $company;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'companies';

    /**
     * Create a new profile composer.
     *
     * @param  State  $companies
     * @return void
     */
    public function __construct(Company $company)
    {
        // Dependencies automatically resolved by service container...
        $this->company   = 	$company;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\Coountry $states
     * @return Collection
     */
    protected function populate()
    {
        return $this->company->all()->where('email','<>', 'admin@pluspark.com');
    }
}