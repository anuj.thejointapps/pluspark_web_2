<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Currency;

class CurrencyComposer 
{
    /**
     * The Currency repository implementation.
     *
     * @var Bind Currency model
     */
    protected $currencies;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'currencies';

    /**
     * Create a new profile composer.
     *
     * @param  Currency  $currencies
     * @return void
     */
    public function __construct(Currency $currencies)
    {
        // Dependencies automatically resolved by service container...
        $this->currencies   = 	$currencies;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\Coountry $currencies
     * @return Collection
     */
    protected function populate()
    {
        return $this->currencies->where('status', 1)->get();
    }
}