<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\JobType;

class JobTypeComposer 
{
    /**
     * The JobType repository implementation.
     *
     * @var Bind JobType model
     */
    protected $jobTypes;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'jobTypes';

    /**
     * Create a new profile composer.
     *
     * @param  State  $jobTypes
     * @return void
     */
    public function __construct(JobType $jobTypes)
    {
        // Dependencies automatically resolved by service container...
        $this->jobTypes   = 	$jobTypes;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\Coountry $states
     * @return Collection
     */
    protected function populate()
    {
        return $this->jobTypes->all()->where('id','!=', 3);
    }
}