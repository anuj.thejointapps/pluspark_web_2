<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\State;

class StateComposer 
{
    /**
     * The State repository implementation.
     *
     * @var Bind State model
     */
    protected $states;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'states';

    /**
     * Create a new profile composer.
     *
     * @param  State  $states
     * @return void
     */
    public function __construct(State $states)
    {
        // Dependencies automatically resolved by service container...
        $this->states   = 	$states;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\Coountry $states
     * @return Collection
     */
    protected function populate()
    {
        return $this->states->all();
    }
}