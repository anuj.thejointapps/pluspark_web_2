<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Brand;

class BrandComposer 
{
    /**
     * The Brand repository implementation.
     *
     * @var Bind Brand model
     */
    protected $brands;

    /**
     * Dropdown variable name associated with class
     *
     * @var string
     */
    protected $name = 'brands';

    /**
     * Create a new profile composer.
     *
     * @param  Brand  $brands
     * @return void
     */
    public function __construct(Brand $brands)
    {
        // Dependencies automatically resolved by service container...
        $this->brands   = 	$brands;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $view->with($this->name, $this->populate());
    }

    /**
     * Resolve Data from the class
     *
     * @param App\Models\Coountry $brands
     * @return Collection
     */
    protected function populate()
    {
        return $this->brands->all();
    }
}