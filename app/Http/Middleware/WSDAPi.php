<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Washerman;

# Interface
use App\Interfaces\ApiStatusInterface;

class WSDAPi implements ApiStatusInterface
{
    /**  
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        # fetch The wsdId
        $headers = $request->header();

        #check authorization
        if(!isset($headers['authorization'])) {
            $response = [
                'statusCode' => ApiStatusInterface::OK,
                'message'    => 'Token is required.',
                'success'    => false,
                'data'       => null
            ];

            #return the Response
            return response()->json([$response]);
        }

        #Fetch token 
        $token = str_replace('Bearer ', '', $headers['authorization'][0]);

        # get the user Token
        $wsdUser = Washerman::where('token', $token)->get()->first();

        #put wsduser in sesion
        if($wsdUser != '') {
            \Session::put('wsdUser', $wsdUser);
        }

        # get Request Token
        #$authorizationToken = $request->header('Authorization');
        
        # Chek Api Token for vallidation
        if($wsdUser != ''){
          return $next($request);
        }

        return response()->json([
            'statusCode' => ApiStatusInterface::UNAUTHORIZED,
            'message'    => 'Unauthenticated user.',
            'success'    => false,
            'data'       => null
        ]);

        return $next($request);
    }
}
