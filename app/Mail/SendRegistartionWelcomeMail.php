<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRegistartionWelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    # Otp
    protected $userName; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userName)
    {
        $this->userName = $userName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('UserName for Login')->view('emailTemplates.registartion-welcome')->with(['userName' => $this->userName]);
    }
}
