<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtpMail extends Mailable
{
    use Queueable, SerializesModels;

    # Otp
    protected $otp; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($otp)
    {
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Otp for Password Reset')->view('emailTemplates.otp-email')->with(['otp' => $this->otp]);
    }
}
