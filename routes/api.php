<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

#Route regsiter and login user
Route::get('countries', 'Api\Common\CommomApiController@fetchCountries');
Route::post('states', 'Api\Common\CommomApiController@fetchStates');
Route::post('user/register', 'Api\User\RegisterController@register');
Route::post('user/login', 'Api\User\RegisterController@login');

#Route for forgot password
Route::post('user/sendOtp', 'Api\User\RegisterController@generateOtp');
Route::post('user/verifyOtp', 'Api\User\RegisterController@verifyOtp');
Route::post('user/updatePassword', 'Api\User\RegisterController@updatePassword');
Route::post('user/availability', 'Api\User\RegisterController@availability');
Route::post('user/availabilityOnEmailOrMobile', 'Api\User\RegisterController@checkUserAvailabilityOnEmailOrPhone');

#Route to delete booking from all
Route::post('booking/delete', 'Api\Common\CommomApiController@deleteBooking');
Route::post('sendNotification', 'Api\Common\CommomApiController@sendNotification');
#Route::get('login', 'Api\User\RegisterController@login')->name('login');


# User Authenticated Routes
Route::group(['middleware' => 'auth:api', 'prefix' => 'user', 'namespace' => 'Api\User'], function() {
    Route::post('updateProfile', 'ProfileController@updateProfile');
    Route::post('changePassword', 'ProfileController@changePassword');

    #Vehicles Related Apis
    Route::get('brands/fetch', 'VehicleController@fetchBrands');
    Route::post('brands/fetchModals', 'VehicleController@fetchModals');
    Route::get('fetchColors', 'VehicleController@fetchColors');
    Route::post('vehicle/save', 'VehicleController@saveVehicle');
    Route::post('vehicle/fetch', 'VehicleController@fetchVehicles');

    #Route for Search places
    Route::post('searchPlace/save', 'ParkingController@saveSearchPlace');
    Route::get('searchPlace/fetch', 'ParkingController@fetchSearchPlace');

    #Route for parking Space
    Route::post('parkingSpace/fetch', 'ParkingController@fetchParkingSpace');
    Route::post('parking/detail', 'ParkingController@fetchParkingDetails');
    Route::post('parking/book', 'ParkingController@bookParking');
    Route::get('parking/recent', 'ParkingController@recentParkingList');
    Route::post('switchScreenStatus', 'ParkingController@switchScreenStatus');
    Route::post('booking/status', 'ParkingController@bookingStatus');
    Route::post('booking/updateWashStatus', 'ParkingController@bookingWashStatusUpdate');
    Route::post('getMyCar', 'ParkingController@getMyCar');
    Route::post('fetchWashCost', 'ParkingController@fetchWashCost');
    Route::post('booking/savePaymentMode', 'ParkingController@savePaymentMode');
    Route::post('booking/fetchBill', 'ParkingController@fetchBookingBill');
    Route::post('booking/payment', 'ParkingController@payment');
    Route::post('booking/paymentForScanorPrivate', 'ParkingController@paymentForScanorPrivate');
    Route::post('booking/allotParkingNo', 'ParkingController@allotParkingNo');
    Route::post('booking/scanMe', 'ParkingController@scanMe');
    Route::post('booking/assignSlotNumber', 'ParkingController@assignSlotNumber');
    Route::post('booking/detail', 'ParkingController@bookingDetail');
    Route::post('booking/pass/qrScan', 'ParkingController@scanBookingPassQr');
    Route::post('booking/checkForPaymentRequest', 'ParkingController@checkForPaymentRequest');
   
    #Delete Pass Api
    Route::get('booking/pass/fetch', 'ParkingController@fetchPasses');
    Route::post('booking/pass/delete', 'ParkingController@deletePass');
    Route::post('booking/pass/vehicle/fetch', 'ParkingController@fetchPassVehicle');

    #Routefor Booking Status Update
    Route::post('booking/updateStatus', 'ParkingController@updateBookingStatus');

    #Routes for Events
    Route::get('events/fetch', 'EventController@fetchEvents');

    #Routes for promocodes
    Route::post('promocode/fetch', 'PromocodeController@fetchPromocode');
    Route::post('promocode/validate', 'PromocodeController@validatePromocode');
});

#W S D(Washer, Supervisor, Driver) unauthenticated Routes
Route::post('wsd/sendOtp', 'Api\WSD\LoginController@generateOtp');
Route::post('wsd/login', 'Api\WSD\LoginController@login');

# WSD Authenticated Routes
Route::group(['middleware' => 'wsd', 'prefix' => 'wsd', 'namespace' => 'Api\WSD'], function() {
    #Supervisor Routes
    Route::get('supervisor/setBookings', 'Supervisor\BookingController@fetchSetBookings');
    Route::post('supervisor/booking', 'Supervisor\BookingController@booking');
    Route::get('supervisor/getBookings', 'Supervisor\BookingController@getBookings');
    Route::post('supervisor/booking/billDetail', 'Supervisor\BookingController@bookingBillDetail');
    Route::get('supervisor/booking/todayCheckin', 'Supervisor\BookingController@todayCheckin');
    Route::post('supervisor/booking/search', 'Supervisor\BookingController@searchTodaysCheckin');
    Route::post('supervisor/booking/scan', 'Supervisor\BookingController@scanForBookingCheck');
    Route::post('supervisor/booking/updateStatus', 'Supervisor\BookingController@updateBookingStatus');
    Route::get('supervisor/fetchAllParkings', 'Supervisor\BookingController@fetchAllParkings');
    Route::post('supervisor/updateCurrentParking', 'Supervisor\BookingController@updateCurrentParking');
    Route::post('supervisor/createUserBooking', 'Supervisor\BookingController@createUserBooking');
    Route::post('supervisor/createUserBookingOnClose', 'Supervisor\BookingController@createUserBookingOnClose');
    Route::post('supervisor/updateSlots', 'Supervisor\BookingController@updateSlots');
    Route::get('supervisor/todaysBookingList', 'Supervisor\BookingController@todaysBookingList');
    Route::get('supervisor/futureBookingList', 'Supervisor\BookingController@futureBookingList');
    Route::post('supervisor/booking/futuresearch', 'Supervisor\BookingController@searchfutureBookingList');
    Route::post('supervisor/booking/pick', 'Supervisor\BookingController@pickTheGetBooking');
    Route::get('supervisor/booking/fetchCurrentBookingStatus', 'Supervisor\BookingController@fetchCurrentBookingStatus');
    Route::post('supervisor/booking/markAsPaid', 'Supervisor\BookingController@markAsPaid');
    Route::post('supervisor/booking/detail', 'Supervisor\BookingController@bookingDetail');
    Route::post('supervisor/booking/checkedIn', 'Supervisor\BookingController@checkedInBooking');
    Route::post('supervisor/booking/checkout', 'Supervisor\BookingController@checkout');
    Route::post('supervisor/booking/paymentRequested', 'Supervisor\BookingController@requestForPayment');
    Route::post('supervisor/booking/savePaymentMode', 'Supervisor\BookingController@savePaymentMode');

    #Washer Routes
    Route::get('washer/pendingBookings', 'Washer\BookingController@fetchSetBookings');
    Route::post('washer/booking', 'Washer\BookingController@booking');
    Route::get('washer/inProgressBookings', 'Washer\BookingController@getBookings');
    Route::get('washer/booking/todayCheckin', 'Washer\BookingController@todayCheckin');
    Route::post('washer/booking/search', 'Washer\BookingController@searchTodaysCheckin');
    Route::post('washer/booking/updateStatus', 'Washer\BookingController@updateBookingStatus');

    #Driver Routes
    Route::get('driver/setBookings', 'Driver\BookingController@fetchSetBookings');
    Route::post('driver/booking', 'Driver\BookingController@booking');
    Route::get('driver/getBookings', 'Driver\BookingController@getBookings');
    Route::post('driver/booking/billDetail', 'Driver\BookingController@bookingBillDetail');
    Route::get('driver/booking/todayCheckin', 'Driver\BookingController@todayCheckin');
    Route::post('driver/booking/search', 'Driver\BookingController@searchTodaysCheckin');
    Route::post('driver/booking/scan', 'Driver\BookingController@scanForBookingCheck');
    Route::post('driver/booking/updateStatus', 'Driver\BookingController@updateBookingStatus');
    Route::get('driver/fetchAllParkings', 'Driver\BookingController@fetchAllParkings');
    Route::post('driver/updateCurrentParking', 'Driver\BookingController@updateCurrentParking');
    Route::post('driver/createUserBooking', 'Driver\BookingController@createUserBooking');
    Route::post('driver/createUserBookingOnClose', 'Driver\BookingController@createUserBookingOnClose');
    Route::get('driver/todaysBookingList', 'Driver\BookingController@todaysBookingList');
    Route::get('driver/futureBookingList', 'Driver\BookingController@futureBookingList');
    Route::post('driver/booking/futuresearch', 'Driver\BookingController@searchfutureBookingList');
    Route::post('driver/booking/pick', 'Driver\BookingController@pickTheGetBooking');
    Route::get('driver/booking/fetchCurrentBookingStatus', 'Driver\BookingController@fetchCurrentBookingStatus');
    Route::post('driver/booking/markAsPaid', 'Driver\BookingController@markAsPaid');
    Route::post('driver/booking/detail', 'Driver\BookingController@bookingDetail');
    Route::post('driver/booking/checkedIn', 'Driver\BookingController@checkedInBooking');
    Route::post('driver/booking/checkout', 'Driver\BookingController@checkout');
    Route::post('driver/booking/paymentRequested', 'Driver\BookingController@requestForPayment');
    Route::post('driver/booking/savePaymentMode', 'Driver\BookingController@savePaymentMode');
});