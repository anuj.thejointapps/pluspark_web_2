<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::get('/barcode', function() {
	return view('admin.barcode');
});
Route::get('/', 'Admin\AdminLoginController@index');
Route::post('/admin/login', 'Admin\AdminLoginController@attemptLogin');
Route::resource('/forgot-password', 'Admin\ForgotPasswordController');
Route::any('send-email', 'Admin\ForgotPasswordController@sendEmail');
Route::get('password-reset/{token}', 'Admin\ForgotPasswordController@resetPassword');
Route::any('resetPassword', 'Admin\ForgotPasswordController@resetAdminPassword');
Route::post('/states/country', 'Admin\StateController@getStatesByCountry')->name('states.country');

#Admin authenticated route
Route::group(['middleware' => ['admin'], 'prefix'=>'admin', 'namespace' => 'Admin'], function() {
	Route::get('/dashboard', 'AdminDashboardController@index')->name('admin.dashboard');
	Route::get('/my-profile', 'AdminLoginController@editProfile')->name('admin.my.profile');
	Route::post('/my/profile/{id}', 'AdminLoginController@updateProfile')->name('admin.myprofile');
	Route::get('/logout', 'AdminLoginController@logout');

	#Country Routes
	Route::resource('/countries', 'CountryController');
	Route::post('/countries/status-update', 'CountryController@countryEnableDisabled')->name('countries.status');

	# currency Routes
	Route::resource('/currencies', 'CurrencyController');
	Route::post('/currencies/status-update', 'CurrencyController@currencyEnableDisabled')->name('currencies.status');

	#State Routes
	Route::resource('/states', 'StateController');
	Route::post('/states/status-update', 'StateController@stateEnableDisabled')->name('states.status');

	#User Routes
	Route::resource('/users', 'UserController');
	Route::post('/users/status-update', 'UserController@userEnableDisabled')->name('users.status');

	#Brand Routes
	Route::resource('/brands', 'BrandController');
	Route::post('/brands/status-update', 'BrandController@brandEnableDisabled')->name('brands.status');

	#Color Routes
	Route::resource('/colors', 'ColorController');
	Route::post('/colors/status-update', 'ColorController@colorEnableDisabled')->name('colors.status');

	#Models Routes
	Route::resource('/models', 'ModalController');
	Route::post('/models/status-update', 'ModalController@modelEnableDisabled')->name('models.status');

	#Company Routes
	Route::resource('/company', 'CompanyController');
	Route::post('/company/status-update', 'CompanyController@companyEnableDisabled')->name('company.status');
	Route::post('/company/email-check', 'CompanyController@emailCheck')->name('company.email-check');
	Route::get('/company-parking/{parkingId}/details', 'CompanyController@showCompanyParkingSpace')->name('company-parkings.show-details');
	// Route::resource('/company-parking', 'CompanyParkingController');
	// Route::get('/company-parking/{id}/add', 'CompanyParkingController@addParking')->name('company-parking.add');

	#Washer Routes
	Route::resource('/washer', 'WasherManController');
	Route::post('/washer/email-check', 'WasherManController@emailCheck')->name('washer.email-check');
	Route::post('/washer/mobile-check', 'WasherManController@mobileCheck')->name('washer.mobile-check');
	Route::post('/washer/status-update', 'WasherManController@washerEnableDisabled')->name('washer.status');
	Route::resource('/washer-company-allot', 'WasherCompanyAllotController');

	# credential generate
	Route::get('company/{id}/generate-credential', 'CompanyController@generateCompanyCredential')->name('company.generate-credential');
	Route::post('company/{id}/generate-credential', 'CompanyController@generateCredential')->name('company.generate-credential');
	
	#Discount Routes
	Route::resource('discount', 'DiscountController');
	Route::any('fetchCompanyId', 'DiscountController@fetchCompanyId');
	Route::any('openDiscounUpdate', 'DiscountController@openDiscountUpdate');
	Route::post('/discount/status-update', 'DiscountController@statusUpdate')->name('discount.status');

	#Promocodes Routes
	Route::resource('promocode', 'PromocodeController');
	// Route::any('promocode/fetchCompanyId', 'PromocodeController@fetchCompanyId');
	Route::any('openPromocodeUpdate', 'PromocodeController@openPromocodeUpdate');
	Route::post('/promocode/status-update', 'PromocodeController@statusUpdate')->name('promocode.status');

	# popularspace Routes
	Route::resource('popular-places', 'PopularPlaceController');

	#Routes for Commission
	Route::resource('commission', 'CommissionController');
	Route::any('OpenEditCommission', 'CommissionController@OpenEditCommission');
});

Route::group(['prefix' => 'company', 'namespace' => 'Company'], function() {
	Route::get('/', 'CompanyLoginController@index')->name('company.login');
	Route::post('/login', 'CompanyLoginController@attemptLogin')->name('company.attempt.login');
	Route::get('/forgot-password', 'ForgotPasswordController@index')->name('company.forgot-password.index');
	Route::any('send-email', 'ForgotPasswordController@sendEmail')->name('company.send-email');
	Route::get('password-reset/{token}', 'ForgotPasswordController@resetPassword');
	Route::any('resetPassword', 'ForgotPasswordController@resetCompanyPassword');

	Route::group(['middleware' => 'company'], function() {
		Route::get('/logout', 'CompanyLoginController@logout')->name('company.logout');
		Route::get('dashboard', 'DashboardController@index')->name('company.dashboard.index');

		# Company Parking Route
		Route::resource('company-parkings', 'CompanyParkingController');
		Route::post('/parking/status-update', 'CompanyParkingController@companyParkingEnableDisabled')->name('company-parking.status');

		# Company Discount Route
		Route::resource('company-discount', 'DiscountController');
		Route::any('openDiscounUpdate', 'DiscountController@openDiscountUpdate');
		Route::post('/discount/status-update', 'DiscountController@statusUpdate')->name('company-discount.status');

		# Company Promocode Route
		Route::resource('company-promocodes', 'PromoCodeController');
		Route::any('promocode/{id}/openDiscounUpdate', 'PromoCodeController@openPromocodeUpdate')->name('promocode.openPromocodeUpdate');
		Route::post('/promocode/status-update', 'PromoCodeController@statusUpdate')->name('company-promocode.status');

		# Event Routes
		Route::resource('events', 'EventController');
		Route::post('/events/status-update', 'EventController@eventEnableDisabled')->name('events.status');

		# Employee Routes
		Route::resource('/employees', 'EmployeeController');
		Route::post('/employees/email-check', 'EmployeeController@emailCheck')->name('employees.email-check');
		Route::post('/employees/mobile-check', 'EmployeeController@mobileCheck')->name('employees.mobile-check');
		Route::post('/employees/status-update', 'EmployeeController@employeeEnableDisabled')->name('employees.status');
		Route::get('/employees/{id}/parking-allot', 'EmployeeController@employeeParkingAllot')->name('employees.parking-allot');
		Route::post('/employees/{id}/parking-allocate', 'EmployeeController@employeeParkingAllocate')->name('employees.parking-allocate');

		# Company parking Space Route
		Route::get('company-parkings/{parking_id}/parking-space', 'CompanyParkingController@createSpaceOfParking')->name('parking-space.create');
		Route::post('parking-space/set-up-space', 'CompanyParkingController@setUpSpace')->name('parking-space.setup-space');
		Route::post('parking-space/space-description', 'CompanyParkingController@spaceDescription')->name('parking.space-description');
		Route::post('parking-space/space-feature', 'CompanyParkingController@parkingFeature')->name('parking.space-feature');
		Route::post('parking-space/mark-out-space', 'CompanyParkingController@markOutSpace')->name('parking-space.mark-out-space');
		Route::post('parking-space/payout-setting', 'CompanyParkingController@payoutSetting')->name('parking-space.payout-setting');
		Route::post('parking-space/advance-space-setting', 'CompanyParkingController@advanceSpaceSetting')->name('parking-space.advance-space-setting');
		Route::post('parking-space/parking-driver-feature', 'CompanyParkingController@parkingDriverFeature')->name('parking-space.parking-driver-feature');
		Route::post('parking-space/parking-washer-feature', 'CompanyParkingController@parkingWasherFeature')->name('parking-space.parking-washer-feature');
		Route::post('parking-space/parking-booking-feature', 'CompanyParkingController@parkingBookingFeature')->name('parking-space.parking-booking-feature');
		Route::post('parking-space/parking-subscription-feature', 'CompanyParkingController@parkingSubscriptionFeature')->name('parking-space.parking-subscription-feature');

		#Booking management Routes
		Route::resource('onGoingbooking', 'BookingController');
		Route::get('/onGoingbooking/show/{id}/{type}','BookingController@show')->name('onGoingbooking.show');
		Route::post('/onGoingbooking/export','BookingController@exportdata')->name('onGoingbooking.export');
	});
});