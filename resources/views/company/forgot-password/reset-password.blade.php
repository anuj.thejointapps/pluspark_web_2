<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PlusPark Company</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('assets/css/demo_1/style.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
          <div class="row flex-grow">
            <div class="col-lg-6 d-flex align-items-center justify-content-center">
              <div class="auth-form-transparent text-left p-3">
                <div class="brand-logo">
                  <img src="{{asset('assets/images/logo.svg')}}" alt="logo">
                </div>
                <h4>Reset Password</h4>
                <!-- <h6 class="font-weight-light">Happy to see you again!</h6> -->
                @if($status == 200)
                  <form class="pt-3">
                    <input type="hidden" name="company_id" value="{{ $company->id }}" id="company_id">
                    <div class="form-group">
                      <label>Enter New Password</label>
                      <div class="input-group">
                        <div class="input-group-prepend bg-transparent">
                          <span class="input-group-text bg-transparent border-right-0">
                            <i class="mdi mdi-lock-outline text-primary"></i>
                          </span>
                        </div>
                        <input type="password" class="form-control form-control-lg border-left-0" placeholder="Enter New Password" required="required" id="password">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Confirm New Password</label>
                      <div class="input-group">
                        <div class="input-group-prepend bg-transparent">
                          <span class="input-group-text bg-transparent border-right-0">
                            <i class="mdi mdi-lock-outline text-primary"></i>
                          </span>
                        </div>
                        <input type="password" class="form-control form-control-lg border-left-0" placeholder="Confirm New Password" required="required" id="confirm_password">
                      </div>
                    </div>
                    <div class="my-3">
                      <button type="button" id="reset_password_button" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn">Reset</button>
                    </div>
                  </form>
                @else
                  <h4>{{$msg ?? ''}}</h4>
                @endif
              </div>
            </div>
            <div class="col-lg-6 login-half-bg d-flex flex-row">
              <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2021 All rights reserved.</p>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('assets/vendors/js/vendor_bundle_base.js')}}"></script>
    <script src="{{asset('assets/vendors/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/alerts.js')}}"></script>
    
    <script>
      $("button#reset_password_button").on('click', function(e) {
        e.preventDefault();
        let password = $('input#password').val();
        let confirmPassword = $('input#confirm_password').val();
        let companyId = $('input#company_id').val();

        //Validate passwords
        if(password == '' || confirmPassword == '') {
          swal({
            title: "",
            text: "Password and Confirm Password is required.",
            icon: "warning",
          });

          return false;
        }

        //Validate Password Length
        if(password.length < 8 || password.length > 16) {
          swal({
            title: "",
            text: "Password must be atleast 8 character or less than 16 character.",
            icon: "warning",
          });
          return false;
        }

        //validate both password
        if(password != confirmPassword) {
          swal({
            title: "",
            text: "Password and Confirm password not match.",
            icon: "warning",
          });
          return false;
        }

        //Send to Controller
        $.ajax({
            url: "{{action('Company\ForgotPasswordController@resetCompanyPassword')}}",
            method: "GET",
            dataType: "json",
            data:{'companyId':companyId, 'password':password, 'confirmPassword':confirmPassword},
            success: function(response) {
              if (response.status == 200) {
                setTimeout(function() {
                  swal({
                      title: "",
                      text: response.msg,
                      type: "success"
                  }).then (function() {
                      const url = "{{url('/')}}"
                      window.location.href = url+"/company";
                  });
                }, 1000);
              } else {
                swal({
                  title: "",
                  text: response.msg,
                  icon: "warning",
                });
              }
            }
          }); 
      }); 
    </script>
  </body>
</html>