@extends('company.layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title">
          <span class="page-title-icon bg-gradient-success text-white mr-2">
            <i class="mdi mdi-account"></i>
          </span> Employee Management <i class="mdi mdi-chevron-right"></i> View Employee
      </h3>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="form-group text-center">
          <div class="avatar-pic mx-auto mb-3 gal-result" style="background-image: url({{ asset($employee->image_path) }});">
            <!-- <img src="{{ asset($employee->image_path) }}"> -->
          </div>
          <h6>Employee ID: {{ $employee->washer_auto_id ?? '' }}</h6>
          <p>D.O.J. : {{ $employee->date_of_join ?? '' }}</p>
        </div>
        <ul class="nav nav-tabs">
          <li class="nav-item"><a class="nav-link active" href="#Basic_Details" data-toggle="tab">Basic Details</a></li>
        </ul>
        
        <div class="tab-content">
          <div id="Basic_Details" class="tab-pane fade show active">
            <!-- Basic Details Tab -->
            <div class="card mb-3">
                <div class="container card-body">
                  <div class="row">
                    <div class="col-sm-9"><h5>Basic Details</h5></div>
                    <div class="col-sm-3">
                    <button type="button" class="btn btn-md btn-gradient-success btn-modal" data-href="{{ route('employees.edit', [$employee->id ?? '' ]) }}" 
                    data-container=".employee_edit">Edit </button>
                    </div>
                    <div class="col-sm-6">
                      <label>Name</label>
                      <p>{{ $employee->name ?? '' }}</p> 
                      <label>Mobile</label>
                      <p>+{{ $employee->country_code ?? '' }}-{{ $employee->mobile ?? '' }}</p> 
                      <label>Email</label>
                      <p>{{ $employee->email ?? '' }}</p>
                      <label>Gender</label>
                      <p>{{ $employee->gender ?? '' }}</p>
                    </div>
                    <div class="col-sm-6"> 
                      <label>Country</label>
                      <p>{{ $employee->country ?? '' }}</p> 
                      <label>City</label>
                      <p>{{ $employee->city ?? '' }}</p> 
                      <label>Address</label>
                      <p>{{ $employee->address ?? '' }}</p> 
                      <label>Alloted To Company</label>
                      <p>Data</p>
                    </div>
                  </div>
                </div>
            </div>
            <!-- Basic Details Tab -->
          </div>
        </div>
      </div>
    </div>
</div>
<div class="modal fade employee_edit" data-keyboard="false" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel" data-backdrop="static" data-keyboard="false">
</div>
<!-- content-wrapper ends -->
@stop

@section('js-content')
  <script type="text/javascript">

      $(document).on('submit', 'form#employee_edit', function(e) {
        e.preventDefault();
        var data = new FormData(this);
        $('.emp_edit_submit').attr('disabled','disabled');

        $.ajax({
            cache:false,
            contentType: false,
            processData: false,
            url: $(this).attr("action"),
            method: $(this).attr("method"),
            dataType: "json",
            data: data,
            success: function(response) {
              $('.emp_edit_submit').removeAttr('disabled','disabled');
              if (response.status == 200) {
                  swal(response.success_message);
                  $('div.employee_edit').modal('hide');
                  location.reload(true);
              }
            }
        }); 
      });
  </script>
@stop