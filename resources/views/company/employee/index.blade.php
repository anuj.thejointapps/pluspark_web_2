@extends('company.layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-water-pump"></i>
      </span> Employee List
    </h3>
  </div>

  <form method="get" action="{{ route('employees.index') }}">
    <div class="row gutter-sm">
    <!-- 
      <div class="col-12 col-sm-3 col-md-3 form-group">
          <select class="form-control" name="country_name" id="country_name">
            <option value="" data-id="">Select a-z</option>
            <option value="a-z">A-Z</option>
            <option value="a-z">a-z</option>
          </select>
      </div> -->

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <select class="form-control" name="country_name" id="country_name">
            <option value="" data-id="">Select Country</option>
            @foreach($countries as $country)
              <option  data-id="{{ $country->id ?? '' }}" @if($countryName == $country->name) selected="selected" @endif value="{{ $country->name ?? ''}}">
                {{ $country->name ?? '' }}</option>
            @endforeach
          </select>
      </div>

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <select class="form-control" name="state_name" id="state_name">
            <option value="">Select City</option>
            @foreach($states as $state)
              <option @if($stateName == $state->name) selected="selected" @endif value="{{ $state->name ?? ''}}">
                {{ $state->name ?? '' }}</option>
            @endforeach
          </select>
      </div>
      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" autocomplete="off" class="form-control" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="From Date" id="start_date" />
      </div>
      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" autocomplete="off"  class="form-control" name="end_date" value="{{ $todate ?? '' }}" placeholder="To Date" id="end_date" />
      </div>
      <div class="col-12 col-sm-3 col-md-3 form-group">
          <select class="form-control" name="month">
            <option value="">Select Month</option>
            <option value="1" @if($month == '1') selected="selected" @endif>1 Month</option>
            <option value="3" @if($month == '3') selected="selected" @endif>3 Months</option>
            <option value="6" @if($month == '6') selected="selected" @endif>6 Months</option>
          </select>
      </div>

      <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
        <button type="submit" class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> Filter</button>
      </div>
    </div>
  </form>
  <div class="row gutter-sm">
    <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
      <button type="button" class="btn btn-md btn-gradient-success btn-modal" data-href="{{ route('employees.create') }}" 
        data-container=".employee_create">ADD </button>
    </div>
  </div>
 
  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable-employee">
              <thead>
                <th>EMP Id.</th>
                <th>Employee Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Joining Date</th>
                <th>Country</th>
                <th>City</th>
                <th>Job Type</th>
                <th>Action</th>
              </thead>
              <tbody>

                @foreach($employees as $employee)
                  <tr>
                    <td>{{ $employee->washer_auto_id ?? '' }}</td>
                    <td>{{ $employee->name ?? '' }}</td>
                    <td>{{ $employee->mobile ?? '' }}</td>
                    <td>{{ $employee->email ?? '' }}</td>
                    <td>{{ $employee->date_of_join ?? '' }}</td>
                    <td>{{ $employee->country ?? '' }}</td>
                    <td>{{ $employee->city ?? '' }}</td>
                    <td>{{ $employee->jobType ?? '' }}</td>
                    <td>
                      <a href="{{ route('employees.show', [$employee->id ?? '']) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-eye"></i></a>

                      <a href="{{ route('employees.parking-allot', [$employee->id ?? '']) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-eye">Allot</i></a>
                      <label class="togglebox toggleStatus ml-1" data-employee-id="{{ $employee->id ?? ''}}">
                        <input type="checkbox" @if($employee->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                        <span class="label-text"></span>
                      </label>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {{ $employees->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade employee_create" data-keyboard="false" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel" data-backdrop="static" data-keyboard="false">
</div>
@stop

@section('js-content')
  <script type="text/javascript">

    $('.datatable-employee').DataTable({
          dom: 'Bfrtip',
          buttons: [
           {
              extend: 'copy',
              exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
              }
           },
           {
              extend: 'csv',
              exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
              }
           },
           {
              extend: 'excel',
              exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
              }
           },
           {
              extend: 'print',
              exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
              }
           }
          ]
     });

    $(document).ready(function(){

      $("#start_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
        });
    })

    $(document).on('submit', 'form#employee_create', function(e) {
      e.preventDefault();
      var data = new FormData(this);
      $('.emp_create_submit').attr('disabled','disabled');

      $.ajax({
          cache:false,
          contentType: false,
          processData: false,
          url: $(this).attr("action"),
          method: $(this).attr("method"),
          dataType: "json",
          data: data,
          success: function(response) {
            $('.emp_create_submit').removeAttr('disabled','disabled');
            if (response.status == 200) {
                swal(response.success_message);
                $('div.employee_create').modal('hide');
                location.reload(true);
            }
          }
      }); 
    });

    $(document).on('change', 'label.toggleStatus', function() {
      var employeeId = $(this).data('employee-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('employees.status') }}",
        data: {employee_id: employeeId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
            location.reload(true);
          }
        }
      })
    })

  </script>
@stop