<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form method="post" action="{{ route('employees.store') }}" enctype="multipart/form-data" id="employee_create" name="employee_create">
        @csrf
        <h5>Add Basic Details</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" name="employee_auto_id" readonly="readonly" class="form-control" placeholder="Employee ID" value="{{ $employeeSequence ?? '' }}" />
            </div>

            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="Email"/>
            </div>

            <div class="form-group">
              <input type="text" name="city_name" class="form-control" placeholder="City"/>
            </div>

            <div class="form-group">
              <input type="text" name="date_of_join" autocomplete="off" class="form-control" placeholder="Date Of Join" id="date_of_join">
            </div>

            <div class="form-group">
              <input type="text" name="gender" class="form-control" placeholder="Gender"/>
            </div>
          </div>
          
          <div class="col-md-6">

            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Employee Name"/>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <input type="number" name="country_code" class="form-control" placeholder="Country code"/>
                </div>
              </div>

              <div class="col-md-8">
                  <input type="text" onkeypress="return numbersonly(event);" maxlength="10" name="mobile" class="form-control" placeholder="Mobile"/>
              </div>
            </div>

            <div class="form-group">
              <input type="text" name="country_name" class="form-control" placeholder="Country"/>
            </div>

            <div class="form-group">
              <select class="form-control" name="job_type_id" id="job_type_id">
                <option value="">Select JobType</option>
                @foreach($jobTypes as $jobType)
                  <option value="{{ $jobType->id ?? '' }}">{{ $jobType->name ?? '' }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <textarea rows="3" name="address" class="form-control" placeholder="Address"></textarea>
            </div>

            <div class="form-group">
              <input type="file" name="employee_image">
            </div>
          </div>
        </div>

        <div class="text-center">
          <button type="button" class="btn btn-gradient-success btn-md float-right ml-3" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-gradient-danger btn-md float-right emp_create_submit">Save & Add</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#date_of_join").datepicker({
        startDate : '-0m',
        format: "yyyy-mm-dd",
        orientation: 'bottom',
        autoclose: true
    });
  });
  
  $(function() {
    $("form[name='employee_create']").validate({
      rules: {
        name: "required",
        country_code: "required",
        mobile: {
          required: true,
          minlength: 10,
          remote: {
              url: "{{ route('employees.mobile-check') }}",
              type: "post"
           }
        },
        date_of_join: {
          required : true
        },
        job_type_id: {
          required: true
        },
        email: {
          required: true,
          minlength: 5,
          remote: {
              url: "{{ route('employees.email-check') }}",
              type: "post"
           }
        }
      },
      messages: {
        name: "Please Enter Name",
        country_code: "Please Enter Country Code",
        date_of_join: {
          required: "Please select date of join"
        },
        job_type_id: {
          required: 'Please select job type'
        },
        mobile: {
          required: "Please enter your Mobile Number",
          minlength: "Your mobile must be at least 10 characters long",
          remote : 'Mobile Number already in record'
        },
        email: {
          required: "Email is required",
          minlength: "Your Email should be at atleast 5 character",
          remote: "Email already in record"
        }
      }
    });
  });
</script>