<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form method="post" action="{{ route('employees.update', [$employee->id ?? '']) }}" enctype="multipart/form-data" id="employee_edit" name="employee_edit">
        @csrf
        @method('patch')
        <h5>Edit Basic Details</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" name="employee_auto_id" readonly="readonly" class="form-control" placeholder="Employee ID" value="{{ $employee->washer_auto_id ?? '' }}" />
            </div>

            <div class="form-group">
              <input type="email" readonly="readonly" name="email" class="form-control" placeholder="Email" value="{{ $employee->email ?? '' }}" />
            </div>

            <div class="form-group">
              <input type="text" name="city_name" class="form-control" placeholder="City" value="{{ $employee->city ?? '' }}"/>
            </div>

            <div class="form-group">
              <input type="text" name="date_of_join" autocomplete="off" class="form-control" placeholder="Date Of Join" id="date_of_join" value="{{ $employee->date_of_join ?? '' }}">
            </div>

            <div class="form-group">
              <input type="text" name="gender" class="form-control" placeholder="Gender" value="{{ $employee->gender ?? '' }}"/>
            </div>
          </div>
          
          <div class="col-md-6">

            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Employee Name" value="{{ $employee->name ?? '' }}"/>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <input type="number" name="country_code" class="form-control" placeholder="Country code" value="{{ $employee->country_code ?? '' }}" />
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" readonly="readonly" onkeypress="return numbersonly(event);" maxlength="10" name="mobile" class="form-control" placeholder="Mobile" value="{{ $employee->mobile ?? '' }}"/>
                </div>
              </div>
            </div>


            <div class="form-group">
              <input type="text" name="country_name" class="form-control" placeholder="Country" value="{{ $employee->country ?? '' }}"/>
            </div>

            <div class="form-group">
              <select class="form-control" name="job_type_id" id="job_type_id">
                <option value="">Select JobType</option>
                @foreach($jobTypes as $jobType)
                  <option value="{{ $jobType->id ?? '' }}" {{ ($jobType->id==$employee->job_type_id) ? 'selected="selected"' : '' }} >{{ $jobType->name ?? '' }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <textarea rows="3" name="address" class="form-control" placeholder="Address">{{ $employee->address ?? '' }}</textarea>
            </div>

            <div class="form-group">
              <input type="file" name="employee_image">
              <img src="{{ asset($employee->image_path ?? '') }}" class="wpx-50 hpx-50 mb-1 mr-1">
            </div>
          </div>
        </div>

        <div class="text-center">
          <button type="button" class="btn btn-gradient-success btn-md float-right ml-3" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-gradient-danger btn-md float-right emp_edit_submit">Save & Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).ready(function(){
    $("#date_of_join").datepicker({
        startDate : '-0m',
        format: "yyyy-mm-dd",
        orientation: 'bottom',
        autoclose: true
    });
  });

  $(function() {
    $("form[name='employee_edit']").validate({
      rules: {
        name: "required",
        country_code: "required",
        email: "required",
        date_of_join: {
          required : true
        },
        job_type_id: {
          required: true
        },
        mobile: {
          required: true,
          minlength: 10
        }
      },
      messages: {
        name: "Please Enter Name",
        country_code: "Please Enter Country Code",
        email: "Please Enter Email",
        date_of_join: {
          required: "Please select date of join"
        },
        job_type_id: {
          required: 'Please select job type'
        },
        mobile: {
          required: "Please enter your Mobile Number",
          minlength: "Your mobile must be at least 10 characters long"
        }
      }
    });
  });
</script>