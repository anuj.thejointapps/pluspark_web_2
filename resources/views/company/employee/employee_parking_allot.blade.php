@extends('company.layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Employee Management </h3>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('company.dashboard.index') }}"><i class="mdi mdi-home"></i></a></li>
				<li class="breadcrumb-item"><a href="{{ route('employees.index') }}">Allot Employee</a></li>
			</ol>
		</nav>
	</div>

	<!-- Card Start -->
	<div class="card">
        <div class="card-body">
			<div class="row">
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
		          	<form method="post" action="{{ route('employees.parking-allocate', [$employeeId ?? 0]) }}">
		          		@csrf
		          		<h6>Parkings</h6>
		          		@foreach($parkings as $parking)
				            <div class="form-check m-0">
		                        <label class="form-check-label text-muted">
		                          <input type="checkbox" 
		                          	@if(in_array($parking->id, array_column($employeeAllot, 'parking_id')))
		                          		checked="checked"
		                          	@endif
		                          	name="parkings[][create_id]" class="form-check-input" value="{{ $parking->id ?? '' }}">
		                          	<i class="input-helper"></i> {{ $parking->name ?? '' }}
		                        </label>
	                      	</div>
	                    @endforeach
		                <span class="text-danger">{{ $errors->first('parkings') }}</span>

	                    <input type="hidden" name="employee_id" value="{{ $employeeId ?? 0 }}">

			            <div class="text-center">
			              	<button type="submit" class="btn btn-gradient-success btn-md">Save & Submit</button>
			            </div>
		          	</form>
			    </div>
			</div>
        </div>
	</div>
	<!-- End Card -->
</div>
<!-- content-wrapper ends -->
@stop
