@extends('company.layouts.main-layout')
@section('content')
<style type="text/css">
/*
*
* ==========================================
* CUSTOM UTIL CLASSES
* ==========================================
*/
.nav-pills-custom .nav-link {
  color: #aaa;
  background: #fff;
  position: relative;
}

.nav-pills-custom .nav-link.active {
  color: #45b649;
  background: #fff;
}

/* Add indicator arrow for the active tab */
@media (min-width: 992px) {
  .nav-pills-custom .nav-link::before {
    content: '';
    display: block;
    border-top: 8px solid transparent;
    border-left: 10px solid #fff;
    border-bottom: 8px solid transparent;
    position: absolute;
    top: 50%;
    right: -10px;
    transform: translateY(-50%);
    opacity: 0;
  }
}

.nav-pills-custom .nav-link.active::before {
  opacity: 1;
}

/*
*
* ==========================================
* FOR DEMO PURPOSE
* ==========================================
*/
body {
  min-height: 100vh;
  background: linear-gradient(to left, #dce35b, #45b649);
}

</style>

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> Parking Management </h3>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('company.dashboard.index') }}"><i class="mdi mdi-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{ route('company-parkings.index') }}">Parkings</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add Parking</li>
      </ol>
    </nav>
  </div>

  <div class="card">
    <div class="card-body">
      <!-- Demo header-->
      <section class="py-5 header">
        <div class="container py-4">
          <div class="row">
            <h3 class="col-12 text-center">Add Parking Space</h3>
            <div class="col-md-3">
              <input type="hidden" name="parking_id" id="parking_id" value="{{ $parking->id ?? ''}}">
              <!-- Tabs nav -->
              <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link mb-3 p-3 shadow active" id="v-pills-setup-space-tab">
                  <i class="fa fa-user-circle-o mr-2"></i>
                  <span class="font-weight-bold small text-uppercase">Setup Your Space</span><br>
                  <small>Essential Space Configuration including location, title, photos</small>
                </a>

                <a class="nav-link mb-3 p-3 shadow" id="v-pills-space-description-tab">
                  <i class="fa fa-calendar-minus-o mr-2"></i>
                  <span class="font-weight-bold small text-uppercase">Space Description</span><br>
                  <small>Add description, suggestion, features, and more info to help promote your space</small>
                </a>

                <a class="nav-link mb-3 p-3 shadow" id="v-pills-parking-features-tab">
                  <i class="fa fa-star mr-2"></i>
                  <span class="font-weight-bold small text-uppercase">Parking Features</span><br>
                  <small>Essential Space Configuration including location, title, photos</small>
                </a>

                <a class="nav-link mb-3 p-3 shadow" id="v-pills-mark-parking-tab">
                  <i class="fa fa-star mr-2"></i>
                  <span class="font-weight-bold small text-uppercase">Mark Out Your Parking Space</span><br>
                  <small>Lorem ipsum dolor sit omel consectetur, adipisicing elit, sed do</small>
                </a>

                <a class="nav-link mb-3 p-3 shadow" id="v-pills-payout-setting-tab">
                  <i class="fa fa-star mr-2"></i>
                  <span class="font-weight-bold small text-uppercase">Your Payout Setting</span><br>
                  <small>Lorem ipsum dolor sit omel consectetur, adipisicing elit, sed do</small>
                </a>

                <a class="nav-link mb-3 p-3 shadow" id="v-pills-advance-setting-tab">
                  <i class="fa fa-check mr-2"></i>
                  <span class="font-weight-bold small text-uppercase">Advance Space Setting</span><br>
                  <small>Lorem ipsum dolor sit omel consectetur, adipisicing elit, sed do</small>
                </a>
              </div>
            </div>

            <div class="col-md-9">
              <!-- Tabs content -->
              <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade shadow rounded bg-white show active p-5 setUpSpaceForm" role="tabpanel" aria-labelledby="v-pills-setup-space-tab">
                  <h4 class="font-italic mb-4">Setup your Space</h4>
                  <!-- include setup your space file -->
                  @include('company.company_parking.partials.setup_your_space')
                </div>
                
                <div class="tab-pane fade shadow rounded bg-white p-5 spaceDescriptionForm" role="tabpanel" aria-labelledby="v-pills-space-description-tab">
                  <h4 class="font-italic mb-4">Space Description</h4>
                  <!-- include space description -->
                  @include('company.company_parking.partials.space_description')
                </div>
                
                <div class="tab-pane fade shadow rounded bg-white p-5 parkingFeatureForm" role="tabpanel" aria-labelledby="v-pills-parking-features-tab">
                  <h4 class="font-italic mb-4">Parking Features</h4>
                  <!-- include parking features -->
                  @include('company.company_parking.partials.parking_feature')
                </div>
                
                <div class="tab-pane fade shadow rounded bg-white p-5 markOutParkingSpaceForm" role="tabpanel" aria-labelledby="v-pills-mark-parking-tab">
                  <h4 class="font-italic mb-4">Mark Out Your Parking Space</h4>
                  <!-- include mark out parking space -->
                  @include('company.company_parking.partials.mark_out_parking_space')
                </div>
                
                <div class="tab-pane fade shadow rounded bg-white p-5 payoutSettingForm" role="tabpanel" aria-labelledby="v-pills-payout-setting-tab">
                  <h4 class="font-italic mb-4">Your Payout Settings</h4>
                  <!-- include mark out parking space -->
                  @include('company.company_parking.partials.payout_setting')
                </div>

                <div class="tab-pane fade shadow rounded bg-white p-5 advanceSpaceForm" role="tabpanel" aria-labelledby="v-pills-advance-setting-tab">
                  <h4 class="font-italic mb-4">Advance Space Settings</h4>
                  <!-- include mark out parking space -->
                  @include('company.company_parking.partials.advance_space_setting')
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

{{--- include driver feature of pop up ---}}
@include('company.company_parking.partials.driver_feature_pop_up')

{{--- include driver wahser of pop up ---}}
@include('company.company_parking.partials.washer_feature_pop_up')

{{--- include driver booking of pop up ---}}
@include('company.company_parking.partials.booking_feature_pop_up')

{{--- include driver subscription of pop up ---}}
@include('company.company_parking.partials.subscription_feature_pop_up')


<!-- content-wrapper ends -->
@stop

@section('js-content')
<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD4LaNErtgQJDO9lyPwWlfEOhPCMmJrmE&libraries=places&callback=initMap"></script>
<script type="text/javascript">

  $(document).ready(function(){
    if ($('#washer_service_available').is(':checked')) {
      $('#set_valet_washing_price').show();
      $('#washer_company_cash').attr('required', 'required')
      $('#washer_company_visa_master').attr('required', 'required')
      $('#washer_plus_park_cost_of_full_wash').attr('required', 'required')
      $('#wahser_plus_park_cost_of_exterior_wash').attr('required', 'required')
      $('#washer_plus_park_cost_of_interior_wash').attr('required', 'required')
      $('#washer_plus_park_cash').attr('required', 'required')
      $('#washer_plus_park_visa_master').attr('required', 'required')
      $('#washer_third_cost_of_full_wash').attr('required', 'required')
      $('#washer_third_cost_of_exterior_wash').attr('required', 'required')
      $('#washer_third_cost_of_interior_wash').attr('required', 'required')
      $('#washer_third_cash').attr('required', 'required')
      $('#washer_third_visa_master').attr('required', 'required')
    } else {
      $('#set_valet_washing_price').hide()
      $('#washer_company_cash').removeAttr('required', 'required')
      $('#washer_company_visa_master').removeAttr('required', 'required')
      $('#washer_plus_park_cost_of_full_wash').removeAttr('required', 'required')
      $('#wahser_plus_park_cost_of_exterior_wash').removeAttr('required', 'required')
      $('#washer_plus_park_cost_of_interior_wash').removeAttr('required', 'required')
      $('#washer_plus_park_cash').removeAttr('required', 'required')
      $('#washer_plus_park_visa_master').removeAttr('required', 'required')
      $('#washer_third_cost_of_full_wash').removeAttr('required', 'required')
      $('#washer_third_cost_of_exterior_wash').removeAttr('required', 'required')
      $('#washer_third_cost_of_interior_wash').removeAttr('required', 'required')
      $('#washer_third_cash').removeAttr('required', 'required')
      $('#washer_third_visa_master').removeAttr('required', 'required')
    }
    
    if ($('#valet_service_available').is(':checked')) {
      $('#set_valet_price').show()
      $('#driver_company_cash').attr('required', 'required')
      $('#driver_company_visa_master').attr('required', 'required')
      $('#driver_plus_park_cost_of_valet').attr('required', 'required')
      $('#driver_plus_park_cash').attr('required', 'required')
      $('#driver_plus_park_visa_master').attr('required', 'required')
      $('#driver_third_cost_of_valet').attr('required', 'required')
      $('#driver_third_cost').attr('required', 'required')
      $('#driver_third_visa_master').attr('required', 'required')
    } else {
      $('#set_valet_price').hide()
      $('#driver_company_cash').removeAttr('required', 'required')
      $('#driver_company_visa_master').removeAttr('required', 'required')
      $('#driver_plus_park_cost_of_valet').removeAttr('required', 'required')
      $('#driver_plus_park_cash').removeAttr('required', 'required')
      $('#driver_plus_park_visa_master').removeAttr('required', 'required')
      $('#driver_third_cost_of_valet').removeAttr('required', 'required')
      $('#driver_third_cost').removeAttr('required', 'required')
      $('#driver_third_visa_master').removeAttr('required', 'required')
    }
  })

  $('#washer_service_available').change(function() {
     if ($(this).is(':checked')) {
        $('#set_valet_washing_price').show()
     } else {
        $('#set_valet_washing_price').hide()
     }
  })

  $('#valet_service_available').change(function() {
     if ($(this).is(':checked')) {
        $('#set_valet_price').show()
     } else {
        $('#set_valet_price').hide()
     }
  })

  $('#country_id').on('change', function() {
    const countryId = $(this).val()
    if (countryId != '') {
      $('#show-error-message-country-code').text('')
    } else {
      $('#show-error-message-country-code').text('Please Select Country')
    }
    initMap();
  })

  // Search Google API
  function initMap() {
    const input = document.getElementById("location-map");
    const countryId =  $('#country_id').val();
    const countryCode =  $('#country_id').find('option:selected').attr('country-code');

    if (countryId == '') {
      $('#show-error-message-country-code').text('Please Select Country')
      return false;
    }

    const autocomplete = new google.maps.places.Autocomplete(input);
    // Set initial restrict to the greater list of countries.
    autocomplete.setComponentRestrictions({'country': [countryCode]});
    const infowindow = new google.maps.InfoWindow();
    const marker = new google.maps.Marker({
      anchorPoint: new google.maps.Point(0, -29),
    });

    var latitude = '';
    var longitude = '';
    var postalCode = '';
    var county = '';
    var fullStreetAddress = '';
    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      console.log(place)
      for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
          if (place.address_components[i].types[j] == "postal_code") {
             postalCode = place.address_components[i].long_name;
          }

          // if (place.address_components[i].types[j] == "administrative_area_level_1") {
          //    stateName = place.address_components[i].long_name;
          // }

          // if (place.address_components[i].types[j] == "locality") {
          //    mailingAddress = place.address_components[i].long_name;
          //    city = place.address_components[i].long_name;
          // }

          if (place.address_components[i].types[j] == "administrative_area_level_2") {
             county = place.address_components[i].long_name;
          }
        }
      }

      // //Set street Address Full
      // streetAddress = postalCode +' '+ place.name + ' ' + route + ' ' + city;

      // //Set the Data
      $('#latitude').val(place.geometry['location'].lat());
      $('#longitude').val(place.geometry['location'].lng());
      // $('#mailing_state').val(stateName);
      // $('#mailing_city').val(city);
      // $('#mailing_county').val(county);
      $('#pincode').val(postalCode);
      // $('#mailing_address_line_1').val(place.name);

      // // --------- show lat and long ---------------
      // $("#lat_area").removeClass("d-none");
      // $("#long_area").removeClass("d-none");
    });
  }

  //Set the datepicker date Style to Y-m-D on Edit
  $("#mark_specific_day_avaiable" ).datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
  });

  $("#date_of_birth" ).datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
  });

  $('input#feature-driver').on('change', function(e){
    if(e.target.checked){
      $('#featuredriver').show().modal();
    }
  });

  $('input#feature-washer').on('change', function(e){
    if(e.target.checked){
      $('#featurewasher').show().modal();
    }
  });

  $('input#feature-booking').on('change', function(e){
    if(e.target.checked){
      $('#featurebooking').show().modal();
    }
  });

  $('input#feature-subscription').on('change', function(e){
    if(e.target.checked){
      $('#featuresubscription').show().modal();
    }
  });

  $(document).ready(function(){
    $("#mark_specific_day_avaiable").datepicker({
        orientation: 'bottom',
        autoclose: true
    });
  });

  $(document).ready(function(){
    $("#date_of_birth").datepicker({
        orientation: 'bottom',
        autoclose: true
    });
  });

  $(function() {
    $("form[name='mark_out_space']").validate({
        rules: {
          set_booking_price: "required"
        },
        messages: {
          set_booking_price: "Please Enter Set Booking Price"
        }
    });
  });

  $(document).on('submit', 'form#set_up_space', function(e){
    e.preventDefault();
    var data = new FormData(this);

    $.ajax({
      cache:false,
      contentType: false,
      processData: false,
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:data,
      success: function(response) {
        if(response.status == 200) {
          $('.setUpSpaceForm').removeClass('hide');
          $('.setUpSpaceForm').removeClass('active');
          $('#v-pills-setup-space-tab').removeClass('active');
          $('#v-pills-space-description-tab').addClass('active');
          $('.spaceDescriptionForm').addClass('show');
          $('.spaceDescriptionForm').addClass('active');
          // swal(response.success_message);
          toastr.success(response.success_message);
        }
      }
    }); 
  });

  $(document).on('submit', 'form#space_description', function(e){
    e.preventDefault();
    var data = $(this).serialize();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:data,
      success: function(response) {
        if(response.status == 200) {
          $('.spaceDescriptionForm').removeClass('hide');
          $('.spaceDescriptionForm').removeClass('active');
          $('#v-pills-space-description-tab').removeClass('active');
          $('#v-pills-parking-features-tab').addClass('active');
          $('.parkingFeatureForm').addClass('show');
          $('.parkingFeatureForm').addClass('active');
          // swal(response.success_message);
          toastr.success(response.success_message, 'Success');
        }
      }
    }); 
  });

  $(document).on('submit', 'form#space_feature', function(e){
    e.preventDefault();
    var parking_id = $('#parking_id').val();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:$(this).serialize()+"&parking_id="+parking_id,
      success: function(response) {
        if(response.status == 200) {
          $('.parkingFeatureForm').removeClass('hide');
          $('.parkingFeatureForm').removeClass('active');
          $('#v-pills-parking-features-tab').removeClass('active');
          $('#v-pills-mark-parking-tab').addClass('active');
          $('.markOutParkingSpaceForm').addClass('show');
          $('.markOutParkingSpaceForm').addClass('active');
          toastr.success(response.success_message, 'Success');
          // swal(response.success_message);
        }
      }
    }); 
  });

  $(document).on('submit', 'form#mark_out_space', function(e){
    e.preventDefault();
    var data = $(this).serialize();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:data,
      success: function(response) {
        if(response.status == 200) {
          $('.markOutParkingSpaceForm').removeClass('hide');
          $('.markOutParkingSpaceForm').removeClass('active');
          $('#v-pills-mark-parking-tab').removeClass('active');
          $('#v-pills-payout-setting-tab').addClass('active');
          $('.payoutSettingForm').addClass('show');
          $('.payoutSettingForm').addClass('active');
          toastr.success(response.success_message, 'Success');
          // swal(response.success_message);
        }
      }
    }); 
  });

  $(document).on('submit', 'form#payout_setting', function(e){
    e.preventDefault();
    var data = new FormData(this);

    $.ajax({
      cache:false,
      contentType: false,
      processData: false,
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:data,
      success: function(response) {
        if(response.status == 200) {
          $('.payoutSettingForm').removeClass('hide');
          $('.payoutSettingForm').removeClass('active');
          $('#v-pills-payout-setting-tab').removeClass('active');
          $('#v-pills-advance-setting-tab').addClass('active');
          $('.advanceSpaceForm').addClass('show');
          $('.advanceSpaceForm').addClass('active');
          toastr.success(response.success_message, 'Success');
          // swal(response.success_message);
        }
      }
    }); 
  });

  $(document).on('submit', 'form#advance_space_setting', function(e){
    e.preventDefault();
    var data = $(this).serialize();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:data,
      success: function(response) {
        if(response.status == 200) {
          toastr.success(response.success_message, 'Success');
          // swal(response.success_message);
          window.location.href = "{{ route('company-parkings.index')}}";
        }
      }
    }); 
  });

  $(document).on('submit', 'form#driver_feature', function(e){
    e.preventDefault();
    var parking_id = $('#parking_id').val();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:$(this).serialize()+"&parking_id="+parking_id,
      success: function(response) {
        if(response.status == 200) {
          swal(response.success_message);
          $('#featuredriver').modal('hide');
        }
      }
    }); 
  });

  $(document).on('submit', 'form#washer_feature', function(e){
    e.preventDefault();
    var parking_id = $('#parking_id').val();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:$(this).serialize()+"&parking_id="+parking_id,
      success: function(response) {
        if(response.status == 200) {
          swal(response.success_message);
          $('#featurewasher').modal('hide');
        }
      }
    }); 
  });

  $(document).on('submit', 'form#booking_feature', function(e){
    e.preventDefault();
    var parking_id = $('#parking_id').val();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:$(this).serialize()+"&parking_id="+parking_id,
      success: function(response) {
        if(response.status == 200) {
          swal(response.success_message);
          $('#featurebooking').modal('hide');
        }
      }
    }); 
  });

  $(document).on('submit', 'form#subscription_feature', function(e){
    e.preventDefault();
    var parking_id = $('#parking_id').val();

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      dataType: "json",
      data:$(this).serialize()+"&parking_id="+parking_id,
      success: function(response) {
        if(response.status == 200) {
          swal(response.success_message);
          $('#featuresubscription').modal('hide');
        }
      }
    }); 
  });

  //On change of Parking pass Type
  $(document).on('change', 'input.pass_type_radio', function(e) {
    var value = $(this).val();
    var privateCodeValue = $('input#private_code_for_parking').val();
    
    //Validate if private code than show input
    if(value == 'private'){
      $('input#private_code_for_parking').show();
      $('#scan_me_image_div').hide();

      //Validate if private code is empty
      if(privateCodeValue == '') {
        var privateCode = randomString(8, 'aA#!');
        $('input#private_code_for_parking').val(privateCode);
      }
    } else if (value == 'qr_code') {
      $('input#private_code_for_parking').hide();
      $('#scan_me_image_div').hide();
    } else {
      $('input#private_code_for_parking').hide();
      $('#scan_me_image_div').show();
    }
  });

  //function to generate random String
  function randomString(length, chars) {
    var mask = '';
    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.indexOf('#') > -1) mask += '0123456789';
    if (chars.indexOf('!') > -1) mask += '.#';
    var result = '';
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
  }
</script>
@stop