@extends('company.layouts.main-layout')
@section('content')
  <!-- content-wrapper start -->
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title">
        <span class="page-title-icon bg-gradient-success text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> Parking Management <i class="mdi mdi-chevron-right"></i> View
      </h3>
    </div>

    <!-- <form method="get" action="#"> -->
    <div class="row gutter-sm">

      <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group ml-3">
        @if(Auth::guard('company')->user()->number_of_parking > $companyParkings->count())
          <a href="{{ route('company-parkings.create') }}" class="btn btn-md btn-gradient-success btn-modal">Add Parking</a>
        @endif
      </div>
    </div>
    <!-- </form> -->

    <div class="row">
      <!-- <div class="col-md-12"> -->
        <!-- <div class="tab-content"> -->
          <!-- Manage Parking Tab -->
          <!-- <div id="Manage_Parking" class="tab-pane fade"> -->
            <div class="container">
              <div class="row">
                @foreach($companyParkings as $companyParking)
                  <div class="col-sm-6 col-md-4 mb-3">
                    <div class="card">
                      <div class="card-body text-center">
                        <div class="bg-img-60 mb-3" style="background-image: url({{ asset($companyParking->image_path ?? '') }});"></div>
                        <h6>{{ $companyParking->name ?? '' }}</h6>
                       <!--  <button type="button" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-book"></i></button> -->
                        <a href="{{ route('company-parkings.show', [$companyParking->id ?? 0]) }}" title="History" class="btn btn-gradient-info btn-sm"><i class="mdi mdi-book-open-page-variant"></i></a>
                        <a href="{{ route('company-parkings.edit', [$companyParking->id ?? 0]) }}" title="Edit" class="btn btn-gradient-danger btn-sm"><i class="mdi mdi-border-color"></i></a>

                        <a href="{{ route('parking-space.create', [$companyParking->id ?? 0]) }}" title="Bookings" class="btn btn-gradient-danger btn-sm"><i class="mdi mdi-book"></i></a>

                      <label class="togglebox toggleStatus ml-1" data-companyparking-id="{{ $companyParking->id ?? ''}}">
                        <input type="checkbox" @if($companyParking->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                        <span class="label-text"></span>
                      </label>

                       <!--  <button type="button" class="btn btn-gradient-dark btn-sm"><i class="mdi mdi-dots-horizontal"></i></button> -->
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          <!-- </div> -->
          <!-- End Manage Parking Tab -->
        <!-- </div> -->
      <!-- </div> -->
    </div>
  </div>
@stop

@section('js-content')
  <script type="text/javascript">
    
    $(document).on('change', 'label.toggleStatus', function() {
      var companyParkingId = $(this).data('companyparking-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('company-parking.status') }}",
        data: {company_parking_id: companyParkingId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
            location.reload(true);
          }
        }
      })
    })

  </script>
@stop