@extends('company.layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> Parking Management </h3>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('company.dashboard.index') }}"><i class="mdi mdi-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{ route('company-parkings.index') }}">Parkings</a></li>
        <li class="breadcrumb-item active" aria-current="page">Parking</li>
      </ol>
    </nav>
  </div>

  <div class="card">
    <div class="card-body">
        <div class="card mb-3">
          <div class="container card-body">
            <div class="row">
              <div class="col-sm-9"><h5>Basic Details</h5></div>
              <div class="col-sm-3">
                <a class="btn btn-md btn-gradient-success btn-modal" href="{{ route('company-parkings.edit', [$parking->id ?? '']) }}" >EDIT </a>
              </div>
              <div class="col-sm-6">
                <label>Parking ID</label>
                <p>{{ $parking->parking_auto_id ?? '' }}</p> 
                <label>Parking Type</label>
                <p>
                  @if($parking->parking_type == 1)
                    Home
                  @elseif($parking->parking_type == 2)
                    Company
                  @elseif($parking->parking_type == 3)
                    Establishment
                  @endif
                </p>

                <label>City</label>
                <p>{{ $parking->city ?? '' }}</p> 
              </div>

              <div class="col-sm-6"> 
                <label>Country</label>
                <p>{{ $parking->country ?? '' }}</p> 
                <label>Location</label>
                <p>{{ $parking->location ?? '' }}</p>
                <p>
                  <img src="{{ asset($parking->image_path ?? '') }}" class="wpx-50 hpx-50 mb-1 mr-1">
                </p>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>

</div>
<!-- content-wrapper ends -->
@stop