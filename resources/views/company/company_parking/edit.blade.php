@extends('company.layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> Parking Management </h3>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('company.dashboard.index') }}"><i class="mdi mdi-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{ route('company-parkings.index') }}">Parkings</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit Parking</li>
      </ol>
    </nav>
  </div>

  <div class="card">
    <div class="card-body">
      <form class="cmxform" id="signupForm" method="post" action="{{ route('company-parkings.update', [$parking->id ?? '']) }}" enctype="multipart/form-data">
        @csrf
        @method('patch')
        <div class="row">
          <div class="col-12 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label>Parking ID</label>
              <input class="form-control" readonly="readonly" name="parking_auto_id" type="text" value="{{ $parking->parking_auto_id ?? '' }}">
            </div>

            <div class="form-group">
              <label>Parking Name</label>
              <input class="form-control" name="parking_name" type="text" value="{{ $parking->name ?? '' }}" >
              <span class="text-danger">{{ $errors->first('parking_name') }}</span>
            </div>

            <div class="form-group">
              <label>Country</label>
              <input class="form-control" name="country" type="text" value="{{ $parking->country ?? '' }}">
            </div>

            <div class="form-group">
              <label>Parking Image</label>
              <input class="form-control" name="parking_image" type="file">                  
              <img src="{{ asset($parking->image_path ?? '') }}" class="wpx-50 hpx-50 mt-2 mb-1 mr-1">
            </div>

          </div>

          <div class="col-12 col-sm-6 col-md-6 col-lg-6">

            <div class="form-group">
              <label>Parking Type</label>
              <select name="parking_type" class="form-control">
                <option value="">Select Parking Type</option>
                <option value="1" @if($parking->parking_type == 1) selected="selected" @endif>Home</option>
                <option value="2" @if($parking->parking_type == 2) selected="selected" @endif>Company</option>
                <option value="3" @if($parking->parking_type == 3) selected="selected" @endif>Establishment</option>
              </select>
              <span class="text-danger">{{ $errors->first('parking_type') }}</span>
            </div>

            <div class="form-group">
              <label>City</label>
              <input class="form-control" name="city" type="text" value="{{ $parking->city ?? '' }}">
            </div>

            <div class="form-group">
              <label>Location</label>
              <input class="form-control" name="location" type="text" value="{{ $parking->location ?? '' }}">
            </div>
          </div>

          <div class="col-12 text-center">
            <button type="submit" class="btn btn-gradient-success float-right">Update & Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

</div>
<!-- content-wrapper ends -->
@stop