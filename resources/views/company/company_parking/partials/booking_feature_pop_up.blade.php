<div class="modal fade" id="featurebooking" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <form method="post" action="{{ route('parking-space.parking-booking-feature') }}" id="booking_feature">
          <input type="hidden" name="booking_feature_id" value="{{ !empty($parking->parkingBookingFeature) ? $parking->parkingBookingFeature->id : '' }}">
          <h5>
            <div class="form-check">
              <label class="form-check-label text-success">
                <input type="checkbox" checked="" disabled="" class="form-check-input">
                <i class="input-helper"></i>
              </label>
            </div>
            <span class="pl-4">Booking Service Commission</span>
          </h5>
          <h6>Commission of Plus Park</h6>
          <div class="form-group">
            <label>Cash (%)</label>
            <img src="{{ asset(Auth::guard('company')->user()->currency ?Auth::guard('company')->user()->currency->symbol : '') }}" width="25px" height="25px">
            <input type="text" value="{{ !empty($parking->parkingBookingFeature) ? $parking->parkingBookingFeature->cash : '' }}" name="cash" class="form-control" placeholder="Cash" />
          </div>
          <div class="form-group">
            <label>Visa/Mastercard (%)</label>
            <input type="text" value="{{ !empty($parking->parkingBookingFeature) ? $parking->parkingBookingFeature->visa_master : '' }}" name="visa_master" class="form-control" placeholder="Visa/Mastercard" />
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-gradient-success btn-md">Add & Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>