
  <form class="cmxform" name="mark_out_space" id="mark_out_space" method="post" action="{{ route('parking-space.mark-out-space') }}">
    @csrf
    <input type="hidden" name="parking_id" value="{{ $parking_id ?? '' }}">
    <input type="hidden" name="mark_out_space_id" value="{{ !empty($markOutSpace) ? $markOutSpace->id : '' }}">

    @php
      $set_available_time_am_pm = !empty($markOutSpace) ? $markOutSpace->set_available_time_am_pm : '';
      $set_available_time = !empty($markOutSpace) ? $markOutSpace->set_available_time : '';
      $start_time = !empty($markOutSpace) ? $markOutSpace->start_time : '';
      $end_time   = !empty($markOutSpace) ? $markOutSpace->end_time : '';
      $start_time_am_pm = !empty($markOutSpace) ? $markOutSpace->start_time_am_pm : '';
      $end_time_am_pm   = !empty($markOutSpace) ? $markOutSpace->end_time_am_pm : '';
      $method_of_payment   = !empty($markOutSpace) ? json_decode($markOutSpace->method_of_payment) : [];
      $days_space_available   = !empty($markOutSpace) ? json_decode($markOutSpace->days_space_available) : [];
    @endphp

    <div class="row">
      <div class="col-12 ">
        <div class="form-group">
          <label>Parking Space Label</label>
          <input class="form-control" name="parking_space_label" value="{{ !empty($markOutSpace) ? $markOutSpace->parking_space_label : '' }}" type="text" placeholder="Parking Space Label">
        </div>

        <div class="form-group">
          <label>Special Access Instructions</label>
          <textarea class="form-control" name="special_access_instruction" placeholder="Access Instructions">{{ !empty($markOutSpace) ? $markOutSpace->special_access_instructions : '' }}</textarea>
        </div>

        <h6>Set Avaiable Time</h6>
        <div class="form-group">
          <div class="form-check">
            <label class="form-check-label text-muted">
              <input type="radio" @if($set_available_time == '24') checked="checked" @endif name="set_available_time" class="form-check-input" value="24">
                <i class="input-helper"></i> 24 hrs
            </label>
          </div>

          {{-- <select name="set_available_time_24_hour" class="form-control">
            <option value="am" @if($set_available_time_am_pm == 'am') selected='selected' @endif >AM</option>
            <option value="pm" @if($set_available_time_am_pm == 'pm') selected='selected' @endif>PM</option>
          </select> --}}
        </div>
        <div class="form-group">
          <div class="form-check">
            <label class="form-check-label text-muted">
              <input type="radio"  @if($set_available_time == 'customize') checked="checked" @endif name="set_available_time" class="form-check-input" value="customize">
                <i class="input-helper"></i> Customize
            </label>
          </div>
        </div>
        {{-- <div class="form-group">
          <label>Start Time</label>
          <div class="row">
            <div class="col-6">
              <select name="start_time" class="form-control">
                <option  @if($start_time == '12:00') selected='selected' @endif value="12:00">12:00</option>
                <option  @if($start_time == '1:00') selected='selected' @endif value="1:00">1:00</option>
                <option  @if($start_time == '2:00') selected='selected' @endif value="2:00">2:00</option>
                <option  @if($start_time == '3:00') selected='selected' @endif value="3:00">3:00</option>
                <option  @if($start_time == '4:00') selected='selected' @endif value="4:00">4:00</option>
                <option  @if($start_time == '5:00') selected='selected' @endif value="5:00">5:00</option>
                <option  @if($start_time == '6:00') selected='selected' @endif value="6:00">6:00</option>
                <option  @if($start_time == '7:00') selected='selected' @endif value="7:00">7:00</option>
                <option  @if($start_time == '8:00') selected='selected' @endif value="8:00">8:00</option>
                <option  @if($start_time == '9:00') selected='selected' @endif value="9:00">9:00</option>
                <option  @if($start_time == '10:00') selected='selected' @endif value="10:00">10:00</option>
              </select>
            </div>
            <div class="col-6">
              <select name="start_time_am_pm" class="form-control">
                <option  @if($start_time_am_pm == 'am') selected='selected' @endif value="am">AM</option>
                <option  @if($start_time_am_pm == 'pm') selected='selected' @endif value="pm">PM</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>End Time</label>
          <div class="row">
            <div class="col-6">
              <select class="form-control" name="end_time">
                <option  @if($end_time == '12:00') selected='selected' @endif value="12:00">12:00</option>
                <option  @if($end_time == '1:00') selected='selected' @endif value="1:00">1:00</option>
                <option  @if($end_time == '2:00') selected='selected' @endif value="2:00">2:00</option>
                <option  @if($end_time == '3:00') selected='selected' @endif value="3:00">3:00</option>
                <option  @if($end_time == '4:00') selected='selected' @endif value="4:00">4:00</option>
                <option  @if($end_time == '5:00') selected='selected' @endif value="5:00">5:00</option>
                <option  @if($end_time == '6:00') selected='selected' @endif value="6:00">6:00</option>
                <option  @if($end_time == '7:00') selected='selected' @endif value="7:00">7:00</option>
                <option  @if($end_time == '8:00') selected='selected' @endif value="8:00">8:00</option>
                <option  @if($end_time == '9:00') selected='selected' @endif value="9:00">9:00</option>
                <option  @if($end_time == '10:00') selected='selected' @endif value="10:00">10:00</option>
              </select>
            </div>
            <div class="col-6">
              <select class="form-control" name="end_time_am_pm">
                <option  @if($end_time_am_pm == 'am') selected='selected' @endif value="am">AM</option>
                <option  @if($end_time_am_pm == 'pm') selected='selected' @endif value="pm">PM</option>
              </select>
            </div>
          </div>
        </div> --}}

        <div class="row">
          <div class="col-6 form-group">
            <label>Start Time</label>
            <input type="text" name="start_time" autocomplete="off" id="start_time" value="{{ $start_time ?? '' }} {{ $start_time_am_pm ?? '' }}" class="form-control timepicker-time"/>
          </div>
          <div class="col-6 form-group">
            <label>End Time</label>
            <input type="text" name="end_time" value="{{  $end_time ?? ''  }} {{ $end_time_am_pm ?? '' }}" autocomplete="off" id="end_time" class="form-control timepicker-time"/>
          </div>
        </div>
        <div class="form-group">
          <label>About Extended Time</label>
          <textarea class="form-control" name="about_extended_time" placeholder="About Extended Time">{{ !empty($markOutSpace) ? $markOutSpace->about_extended_time : '' }}</textarea>
        </div>

        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label>Set Booking Price</label>
              <input type="text" name="set_booking_price" value="{{ !empty($markOutSpace) ? $markOutSpace->set_book_price : '' }}" class="form-control" placeholder="Set Booking Price">
            </div>
          </div>

          <div class="col-6">
            <!-- <div class="form-group" id="set_valet_price"> -->
            <div class="form-group" id="set_valet_price_new">
              <label>Set Valet Price</label>
              <input type="text" name="set_valet_price" onkeypress="return numbersonly(event);"  required="required" value="{{ !empty($markOutSpace) ? $markOutSpace->set_valet_price : '' }}" class="form-control" placeholder="Set Valet Price">
            </div>
          </div>  
        </div>

        <div class="row">
          <div class="col-4">
            <div class="form-group" id="full_wash_price">
              <label>Full Wash Price</label>
              <input type="text" name="full_wash_price" required="required" onkeypress="return numbersonly(event);"  value="{{ !empty($markOutSpace) ? $markOutSpace->full_wash_cost : '' }}" class="form-control" placeholder="Set Full Washing Price">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group" id="interior_wash_price">
              <label>Interior Wash Price</label>
              <input type="text" name="interior_wash_price" required="required" onkeypress="return numbersonly(event);"  value="{{ !empty($markOutSpace) ? $markOutSpace->interior_wash_cost : '' }}" class="form-control" placeholder="Set Interior Wash Price">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group" id="exterior_wash_price">
              <label>Exterior Wash Price</label>
              <input type="text" name="exterior_wash_price" required="required" onkeypress="return numbersonly(event);"  value="{{ !empty($markOutSpace) ? $markOutSpace->exterior_wash_cost : '' }}" class="form-control" placeholder="Set Exterior Wash Price">
            </div>
          </div>
        </div>

        <h6>Method Of Payment</h6>
        <div class="row">
          <div class="form-group ml-5">
            <div class="form-check">
              <label class="form-check-label text-muted">
                <input type="checkbox" @if(in_array( 'cash', $method_of_payment)) checked="checked" @endif name="method_of_payment[]" value="cash" class="form-check-input">
                  <i class="input-helper"></i> Cash
              </label>
            </div>
          </div>

          <div class="form-group ml-5">
            <div class="form-check">
              <label class="form-check-label text-muted">
                <input type="checkbox" @if(in_array( 'debit_card', $method_of_payment)) checked="checked" @endif name="method_of_payment[]" value="debit_card" class="form-check-input">
                  <i class="input-helper"></i> Debit Card
              </label>
            </div>
          </div>

          <div class="form-group ml-5">
            <div class="form-check">
              <label class="form-check-label text-muted">
                <input type="checkbox" @if(in_array( 'credit_card', $method_of_payment))checked="checked" @endif name="method_of_payment[]" value="credit_card" class="form-check-input">
                  <i class="input-helper"></i> Credit Card
              </label>
            </div>
          </div>
        </div>

        <h6>Select Which Days your space id available</h6>
        <div class="btn-group" role="group" aria-label="Basic checkbox toggle button group">
          <input type="checkbox" class="btn-check" name="week_days[]" @if(in_array('monday', $days_space_available)) checked="checked" @endif value="monday" id="btncheck1" autocomplete="off" hidden="">
          <label class="btn btn-sm btn-outline-success" for="btncheck1">M</label>

          <input type="checkbox" class="btn-check" name="week_days[]" @if(in_array('tuesday', $days_space_available)) checked="checked" @endif value="tuesday" id="btncheck2" autocomplete="off" hidden="">
          <label class="btn btn-sm btn-outline-success" for="btncheck2">T</label>

          <input type="checkbox" class="btn-check" name="week_days[]" @if(in_array('wednesday', $days_space_available)) checked="checked" @endif value="wednesday" id="btncheck3" autocomplete="off" hidden="">
          <label class="btn btn-sm btn-outline-success" for="btncheck3">W</label>

          <input type="checkbox" class="btn-check" name="week_days[]" @if(in_array('thursday', $days_space_available)) checked="checked" @endif value="thursday" id="btncheck4" autocomplete="off" hidden="">
          <label class="btn btn-sm btn-outline-success" for="btncheck4">T</label>

          <input type="checkbox" class="btn-check" name="week_days[]" @if(in_array('friday', $days_space_available)) checked="checked" @endif value="friday" id="btncheck5" autocomplete="off" hidden="">
          <label class="btn btn-sm btn-outline-success" for="btncheck5">F</label>

          <input type="checkbox" class="btn-check" name="week_days[]" @if(in_array('saturday', $days_space_available)) checked="checked" @endif value="saturday" id="btncheck6" autocomplete="off" hidden="">
          <label class="btn btn-sm btn-outline-success" for="btncheck6">S</label>

          <input type="checkbox" class="btn-check" name="week_days[]" @if(in_array('sunday', $days_space_available)) checked="checked" @endif value="sunday" id="btncheck7" autocomplete="off" hidden="">
          <label class="btn btn-sm btn-outline-success" for="btncheck7">S</label>
        </div>

        <div class="form-group">
          <label>Mark Specific days available</label>
          <input type="text" class="form-control" id="mark_specific_day_avaiable"  value="{{ !empty($markOutSpace->days_mark_space_unavailable) ? $markOutSpace->days_mark_space_unavailable : '' }}" name="mark_specific_day_avaiable" placeholder="Mark Specific days available" autocomplete="off">
        </div>

        <div class="col-12 text-center">
          <button type="submit" class="btn btn-gradient-success float-right">Save & Next</button>
        </div>
      </div>
    </div>
  </form>
