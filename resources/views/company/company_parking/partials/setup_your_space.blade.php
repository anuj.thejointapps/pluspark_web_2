  <form id="set_up_space" method="post" action="{{ route('parking-space.setup-space') }}"  enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="parking_id" value="{{ $parking_id ?? '' }}">
    <input type="hidden" name="parking_setup_space_id" value="{{ !empty($setUpSpace) ? $setUpSpace->id : '' }}">
    <div class="form-group">
      <label>Space Headline</label>
      <input class="form-control" name="space_headline" type="text" value="{{ !empty($setUpSpace) ? $setUpSpace->space_headline : '' }}" placeholder="Space Headline">
    </div>

    @php
      $setupSpaceType     = !empty($setUpSpace) ? $setUpSpace->space_type : '';
      $parker_book_space  = !empty($setUpSpace) ? $setUpSpace->parker_book_space : '';
      $washer_service_available = !empty($setUpSpace) ? $setUpSpace->washer_service_available : '';
      $valet_service_available  = !empty($setUpSpace) ? $setUpSpace->valet_service_available : '';
      $pass_type  = !empty($setUpSpace) ? $setUpSpace->pass_type : '';
    @endphp

    <div class="form-group">
      <label>Space Type</label>
      <select name="space_type" class="form-control">
        <option value="">Select Space Type</option>
        @foreach($spaceTypes as $spaceType)
          <option value="{{ $spaceType->id ?? '' }}" @if($setupSpaceType == $spaceType->id) selected="selected" @endif>{{ $spaceType->name ?? '' }}</option>
        @endforeach
      </select>
    </div>

    <h6>How should parkers book your space</h6>
    <div class="form-group mr-4">
      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" @if($parker_book_space == 'daily') checked="checked" @endif name="daily" class="form-check-input">
            <i class="input-helper"></i> Daily
        </label>
      </div>
    </div>
    <br>

    <h6>Pass Type</h6>
    <div class="form-group">
      <div class="form-check d-inline-block mr-4">
        <label class="form-check-label text-muted">
          <input type="radio" class="pass_type_radio" name="pass_type" @if($pass_type == 'scan_me') checked="checked" @endif value="scan_me" class="form-check-input">
            <i class="input-helper"></i> Scan Me
        </label>
      </div>
      <div class="form-check d-inline-block mr-4">
        <label class="form-check-label text-muted">
          <input type="radio" class="pass_type_radio" name="pass_type" @if($pass_type == 'qr_code') checked="checked" @endif value="qr_code" class="form-check-input">
          <i class="input-helper"></i> QR Code
        </label>
      </div>
      <div class="form-check d-inline-block mr-4">
        <label class="form-check-label text-muted">
          <input type="radio" class="pass_type_radio" name="pass_type" @if($pass_type == 'private') checked="checked" @endif value="private" class="form-check-input">
            <i class="input-helper"></i> Private
        </label>
      </div>
    </div>
    @if($setUpSpace != '')
      <div class="form-group">
        <input class="form-control" id="private_code_for_parking" name="private_code" value="{{ $setUpSpace != '' ?  $setUpSpace->private_code : '' }}" type="text" placeholder="Private Code..." @if(($setUpSpace != '') AND ($setUpSpace->pass_type != 'private')) ? style="display:none;" : '' @endif readonly="readonly">
      </div>
    @endif

    @if(($setUpSpace != '') AND ($setUpSpace->pass_type == 'scan_me') AND ($setUpSpace->scan_me_image != ''))
      <div class="form-group" id="scan_me_image_div">
        <img src="{{ asset($setUpSpace->scan_me_image) }}">
      </div>
    @endif

    <div class="form-group">
      <label>Country</label>
      <select class="form-control" name="country_id" id="country_id">
        <option value="">Select Country</option>
        @foreach($countries as $country)
          <option value="{{ $country->id ?? '' }}" {{ ($setUpSpace != '' AND $country->id==$setUpSpace->country_id) ? 'selected="Selected"' : '' }} country-code="{{ $country->country_code ?? '' }}">{{ $country->name ?? '' }}</option>
        @endforeach
      </select>
      <span class="error" id="show-error-message-country-code"></span>
    </div>

    <div class="form-group">
      <label>Location</label>
      <input class="form-control" id="location-map" name="location" value="{{ !empty($setUpSpace) ? $setUpSpace->location : '' }}" type="text" placeholder="Location">
    </div>

    <div class="form-group">
      <label>Pincode</label>
      <input class="form-control" id="pincode" name="pincode" value="{{ !empty($setUpSpace) ? $setUpSpace->pincode : '' }}" type="text" placeholder="Pincode">
    </div>
    <input type="hidden" name="latitude" id="latitude" value="{{ !empty($setUpSpace) ? $setUpSpace->latitude : '' }}">
    <input type="hidden" name="longitude" id="longitude" value="{{ !empty($setUpSpace) ? $setUpSpace->longitude : '' }}">

    <div class="form-group">
      <label>Total Slot</label>
      <input class="form-control" name="total_slot" value="{{ !empty($setUpSpace) ? $setUpSpace->total_slot : '' }}" type="text" onkeypress="return numbersonly(event);" placeholder="Total Slot">
    </div>
    <div class="form-group">
      <label>Booking Confirm Time(In minutes)</label>
      <input class="form-control" name="confirm_time" value="{{ !empty($setUpSpace) ? $setUpSpace->booking_confirm_time : '' }}" type="text" onkeypress="return numbersonly(event);" placeholder="Booking Confirm Time(In minutes)" required="required">
    </div>
    <div class="form-group">
      <label>Booking Edit/Cancel Time(In minutes)</label>
      <input class="form-control" name="edit_or_cancel_time" value="{{ !empty($setUpSpace) ? $setUpSpace->booking_cancel_edit_time : '' }}" type="text" onkeypress="return numbersonly(event);" placeholder="Booking Edit/Cancel Time(In minutes)" required="required">
    </div>

    <div class="form-group mr-4">
      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox"  @if($valet_service_available) checked="checked" @endif name="valet_service_available" id="valet_service_available" class="form-check-input">
            <i class="input-helper"></i> Valet Service Available
        </label>
      </div>
    </div>

    <div class="form-group mr-4">
      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox"  @if($washer_service_available) checked="checked" @endif name="washer_service_available" id="washer_service_available" class="form-check-input">
            <i class="input-helper"></i> Washer Service Available
        </label>
      </div>
    </div>

    <div class="form-group">
      <label>Photos</label>
      <input class="form-control" name="space_photos[]" multiple="multiple" type="file">
      @if(!empty(($setUpSpace->parkingSpaceImages)))
        @foreach($setUpSpace->parkingSpaceImages as $parkingSpaceImage)
          <img src="{{ asset($parkingSpaceImage->image_path) }}" class="wpx-50 hpx-50 mb-1 mr-1">
        @endforeach
      @endif
    </div>

    <div class="form-group mt-3">
      <button type="submit" class="btn btn-gradient-success float-right">Next</button>
    </div>
  </form>
