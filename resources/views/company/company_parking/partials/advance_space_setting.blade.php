.
  <form class="cmxform" id="advance_space_setting" method="post" action="{{ route('parking-space.advance-space-setting') }}">
    @csrf
    <input type="hidden" name="parking_id" value="{{ $parking_id ?? '' }}">
    <input type="hidden" name="advance_space_setting_id" value="{{ !empty($advanceSpaceSetting) ? $advanceSpaceSetting->id : '' }}">

    @php
      $discount         = !empty($advanceSpaceSetting) ? $advanceSpaceSetting->discounts : false;
      $private_space    = !empty($advanceSpaceSetting) ? $advanceSpaceSetting->private_space : false;
      $hidden_space     = !empty($advanceSpaceSetting) ? $advanceSpaceSetting->hidden_space : false;
      $customer_t_and_c = !empty($advanceSpaceSetting) ? $advanceSpaceSetting->customer_t_and_c : false;
    @endphp
    <div class="row">
      <div class="col-12 ">
        <div class="form-group">
          <label>Discounts</label>
          <label class="togglebox toggleStatus ml-1 float-right" data-user-id="{{ $user->id ?? ''}}">
              @if($discount) ON @else OFF @endif
            <input type="checkbox" @if($discount) checked="checked" @endif name="discount" hidden=""/>
            <span class="label-text"></span>
          </label>
          <p>To provide discount to this parking, turn on Discount'</p>
        </div>

        <div class="form-group">
          <label>Private Space</label>
            <label class="togglebox toggleStatus ml-1 float-right" data-user-id="{{ $user->id ?? ''}}">@if($private_space) ON @else OFF @endif
            <input type="checkbox" @if($private_space) checked="checked" @endif name="private_space_check" hidden=""/>
            <span class="label-text"></span>
          </label>
          <input type="text" class="form-control" placeholder="Private Space" value="{{ !empty($advanceSpaceSetting) ? $advanceSpaceSetting->private_space_link : '' }}" name="private_space">
          <p>Add private space details in above given text area</p>
        </div>

        <div class="form-group">
          <label>Hidden Space</label>
            <label class="togglebox toggleStatus ml-1 float-right" data-user-id="{{ $user->id ?? ''}}">@if($hidden_space) ON @else OFF @endif
            <input type="checkbox" @if($hidden_space) checked="checked" @endif name="hidden_space_check" hidden=""/>
            <span class="label-text"></span>
          </label>    
          <input type="text" class="form-control" placeholder="Hidden Space" value="{{ !empty($advanceSpaceSetting) ? $advanceSpaceSetting->hidden_space_link : '' }}" name="hidden_space">
          <p>Add hidden space details in above given text area</p>
        </div>

        <div class="form-group">
          <label>Custom Terms & Conditions</label>
          <label class="togglebox toggleStatus ml-1 float-right" data-user-id="{{ $user->id ?? ''}}">
            @if($customer_t_and_c) ON @else OFF @endif
            <input type="checkbox" @if($customer_t_and_c) checked="checked" @endif name="customer_t_check" hidden=""/>
            <span class="label-text"></span>
          </label>
          <textarea class="form-control" rows="5" placeholder="Custom term and conditions" name="custom_t_and_c_text">{{ !empty($advanceSpaceSetting)? $advanceSpaceSetting->custom_t_and_c_text : '' }}</textarea>
        </div>

        <div class="col-12 text-center">
          <button type="submit" class="btn btn-gradient-success float-right">Save & Finish</button>
        </div>
      </div>
    </div>
  </form>
