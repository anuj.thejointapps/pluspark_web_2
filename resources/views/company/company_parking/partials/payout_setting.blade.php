
  <form class="cmxform" id="payout_setting" method="post" action="{{ route('parking-space.payout-setting') }}" enctype="multipart/form-data">

    <input type="hidden" name="parking_id" value="{{ $parking_id ?? '' }}">
    <input type="hidden" name="payout_setting_id" value="{{ !empty($payoutSetting) ? $payoutSetting->id : '' }}">

    @csrf
    <div class="row">
      <div class="col-12 ">
        <div class="form-group">
          <label>Street Address</label>
          <input class="form-control" name="street_address" type="text" placeholder="Street Address" value="{{ !empty($payoutSetting) ? $payoutSetting->address : '' }}">
        </div>

        <div class="form-group">
          <label>District/State</label>
          <input class="form-control" name="district_state" type="text" placeholder="District/State" value="{{ !empty($payoutSetting) ? $payoutSetting->country : '' }}">
        </div>

        <div class="form-group">
          <label>Pincode</label>
          <input class="form-control" name="pincode" type="text" placeholder="Pincode" value="{{ !empty($payoutSetting) ? $payoutSetting->pincode : '' }}">
        </div>

        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <div class="form-group">
                <label>First Name</label>
                <input class="form-control" name="first_name" type="text" placeholder="First Name" value="{{ !empty($payoutSetting) ? $payoutSetting->first_name : '' }}">
              </div>
            </div>
          </div>

          <div class="col-6">
            <div class="form-group">
              <div class="form-group">
                <label>Last Name</label>
                <input class="form-control" name="last_name" type="text" placeholder="Last Name" value="{{ !empty($payoutSetting) ? $payoutSetting->last_name : '' }}">
              </div>
            </div>
          </div>
        </div>
        <br>

        <div class="form-group">
          <label>Date of birth</label>
          <input class="form-control" autocomplete="off" name="date_of_birth" id="date_of_birth" type="text" placeholder="Date of birth" value="{{ !empty($payoutSetting->date_of_birth) ? $payoutSetting->date_of_birth : '' }}">
        </div>

        <div class="form-group">
          <label>Identification</label>
          <input class="form-control" name="identification" type="text" placeholder="Identification" value="{{ !empty($payoutSetting) ? $payoutSetting->identification : '' }}">
        </div>

        <div class="form-group">
          <label>Identification Pic</label>
          <input class="form-control" name="identification_pic" type="file" placeholder="Identification">
          <img src="{{ !empty($payoutSetting) ? asset($payoutSetting->identification_photo) : '' }}" class="wpx-50 hpx-50 mt-2 mb-1 mr-1">
        </div>

        <div class="text-right">
          <button type="submit" class="btn btn-gradient-success float-right">Save & Next</button>
        </div>
      </div>
    </div>
  </form>
