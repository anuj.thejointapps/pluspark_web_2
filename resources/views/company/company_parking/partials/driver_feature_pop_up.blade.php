<div class="modal fade" id="featuredriver" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
            <!-- <h5>
              <div class="form-check">
                <label class="form-check-label text-success">
                  <input type="checkbox" checked="" disabled="" class="form-check-input">
                  <i class="input-helper"></i>
                </label>
              </div>
            </h5> -->

            @php
              $drivecompany    = '';
              $drivethirdParty = '';
              $driveplusPark   = '';
              $driverFeature = $parking->parkingDriveFeatures->isNotEmpty() ? 
                               $parking->parkingDriveFeatures->first() : '';
              foreach ($parking->parkingDriveFeatures as $parkingDriveFeat) {
                  if ($parkingDriveFeat->type == 'company') {
                      $drivecompany = $parkingDriveFeat;
                  } else if ($parkingDriveFeat->type == 'third_party') {
                      $drivethirdParty = $parkingDriveFeat;
                  } else if ($parkingDriveFeat->type == 'plus_park') {
                      $driveplusPark = $parkingDriveFeat;
                  }
              }
            @endphp

            <ul class="nav nav-tabs">
              <!-- <li class="nav-item"><a data-toggle="tab" href="#company" class="nav-link active">Company</a></li> -->
              <li class="nav-item">
                <a data-toggle="tab" href="#plus_park" class="nav-link active" style="width: 440px;text-align: center;">
                  Driver/Valet Cost
                </a>
              </li>
              <!-- <li class="nav-item"><a data-toggle="tab" href="#third_party" class="nav-link">Third Party</a></li> -->
            </ul>
            <div class="tab-content">

              <!-- <div id="company" class="tab-pane fade show active">
                <form method="post" action="{{ route('parking-space.parking-driver-feature') }}" id="driver_feature">
                    <h6>Commission of Company</h6>
                    <input type="hidden" name="type" value="company">
                    <input type="hidden" name="driver_feature_id" value="{{ !empty($drivecompany) ? $drivecompany->id : '' }}">

                    <div class="form-group">
                      <label>Cash (%)</label>
                      <input type="text" class="form-control" id="driver_company_cash" placeholder="Cash" name="cash" value="{{ !empty($drivecompany) ? $drivecompany->cash : '' }}" />
                    </div>

                    <div class="form-group">
                      <label>Visa/Mastercard (%)</label>
                      <input type="text" name="visa_master" id="driver_company_visa_master" class="form-control" value="{{ !empty($drivecompany) ? $drivecompany->visa_master : '' }}" placeholder="Visa/Mastercard" />
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-gradient-success btn-md">Add & Save</button>
                    </div>
                  </form>
              </div> -->

              <div id="plus_park" class="tab-pane fade show active">
                <form method="post" action="{{ route('parking-space.parking-driver-feature') }}" id="driver_feature">
                  <input type="hidden" name="type" value="plus_park">
                  <input type="hidden" name="driver_feature_id" value="{{ !empty($driveplusPark) ? $driveplusPark->id : '' }}">

                  <div class="form-group">
                    <label>Cost of Valet</label>
                    <input type="text" name="cost_of_valet" id="driver_plus_park_cost_of_valet" class="form-control" value="{{ $driverFeature != '' ? $driverFeature->cost_of_valet : '' }}" placeholder="Cost Of Valet" />
                  </div>
                  <div class="form-check d-inline-block mr-4">
                    <label class="form-check-label text-muted">
                      <input type="radio" name="valet_type" @if($driverFeature != '' AND $driverFeature->type == 'company') checked="checked" @endif value="company" class="form-check-input" required="required">
                        <i class="input-helper"></i> Company
                    </label>
                  </div>
                  <div class="form-check d-inline-block mr-4">
                    <label class="form-check-label text-muted">
                      <input type="radio" name="valet_type" value="plus_park" class="form-check-input" @if($driverFeature != '' AND $driverFeature->type == 'plus_park') checked="checked" @endif>
                      <i class="input-helper"></i> Plus Parks
                    </label>
                  </div>
                  <div class="form-check d-inline-block mr-4">
                    <label class="form-check-label text-muted">
                      <input type="radio" name="valet_type" value="third_party" class="form-check-input" @if($driverFeature != '' AND $driverFeature->type == 'third_party') checked="checked" @endif>
                        <i class="input-helper"></i> Third party
                    </label>
                  </div>
                  <!-- <h6>Commission of Plus Park</h6>
                  <div class="form-group">
                    <label>Cash (%)</label>
                    <input type="text" name="cash" id="driver_plus_park_cash" class="form-control" value="{{ !empty($driveplusPark) ? $driveplusPark->cash : '' }}" placeholder="Cash (%)" />
                  </div>
                  <div class="form-group">
                    <label>Visa/Mastercard (%)</label>
                    <input type="text" name="visa_master" id="driver_plus_park_visa_master" class="form-control" value="{{ !empty($driveplusPark) ? $driveplusPark->visa_master : '' }}" placeholder="Visa/Mastercard (%)" />
                  </div> -->

                  <div class="text-center">
                    <button type="submit" class="btn btn-gradient-success btn-md">Add & Save</button>
                  </div>
                </form>
              </div>

              <!-- <div id="third_party" class="tab-pane fade">
                <form method="post" action="{{ route('parking-space.parking-driver-feature') }}" id="driver_feature">
                  <input type="hidden" name="type" value="third_party">
                  <input type="hidden" name="driver_feature_id" value="{{ !empty($drivethirdParty) ? $drivethirdParty->id : '' }}">

                  <div class="form-group">
                    <label>Cost of Valet</label>
                    <input type="text" name="cost_of_valet" id="driver_third_cost_of_valet" value="{{ !empty($drivethirdParty) ? $drivethirdParty->cost_of_valet : '' }}" class="form-control" placeholder="Cost Of Valet" />
                  </div>
                  <h6>Commission of Third Party</h6>
                  <div class="form-group">
                    <label>Cash (%)</label>
                    <input type="text" name="cash" id="driver_third_cost" value="{{ !empty($drivethirdParty) ? $drivethirdParty->cash : '' }}" class="form-control" placeholder="Cash (%)" />
                  </div>
                  <div class="form-group">
                    <label>Visa/Mastercard (%)</label>
                    <input type="text" name="visa_master" id="driver_third_visa_master" value="{{ !empty($drivethirdParty) ? $drivethirdParty->visa_master : '' }}" class="form-control" placeholder="Visa/Mastercard (%)" />
                  </div>

                  <div class="text-center">
                    <button type="submit" class="btn btn-gradient-success btn-md">Add & Save</button>
                  </div>
                </form>
              </div> -->
            </div>
        </div>
      </div>
    </div>
</div>