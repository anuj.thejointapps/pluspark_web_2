<div class="modal fade" id="featurewasher" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <!-- <h5>
            <div class="form-check">
              <label class="form-check-label text-success">
                <input type="checkbox" checked="" disabled="" class="form-check-input">
                <i class="input-helper"></i>
              </label>
            </div>
            <span class="pl-4">Washer Cost</span>
          </h5> -->

          @php
            $washercompany    = '';
            $washerthirdParty = '';
            $washerplusPark   = '';
            $washerFeature = $parking->parkingWasherFeatures->isNotEmpty() ? 
                               $parking->parkingWasherFeatures->first() : '';
            foreach ($parking->parkingWasherFeatures as $parkingWasherFeat) {
                if ($parkingWasherFeat->type == 'company') {
                    $washercompany = $parkingWasherFeat;
                } else if ($parkingWasherFeat->type == 'third_party') {
                    $washerthirdParty = $parkingWasherFeat;
                } else if ($parkingWasherFeat->type == 'plus_park') {
                    $washerplusPark = $parkingWasherFeat;
                }
            }
          @endphp

          <ul class="nav nav-tabs">
            <!-- <li class="nav-item"><a data-toggle="tab" href="#washer_company" class="nav-link active">Company</a></li> -->
            <li class="nav-item"><a data-toggle="tab" href="#washer_plus_park" class="nav-link active" style="width: 440px;text-align: center;">Washer Cost</a></li>
            <!-- <li class="nav-item"><a data-toggle="tab" href="#washer_third_party" class="nav-link">Third Party</a></li> -->
          </ul>
          <div class="tab-content">

            <!-- <div id="washer_company" class="tab-pane fade show active">
              <form method="post" action="{{ route('parking-space.parking-washer-feature') }}" id="washer_feature">
                <h6>Commission of Company</h6>

                <input type="hidden" name="type" value="company">
                <input type="hidden" name="washer_feature_id" value="{{ !empty($washercompany) ? $washercompany->id : '' }}">

                <div class="form-group">
                  <label>Cash (%)</label>
                  <input type="text" name="cash" id="washer_company_cash" class="form-control" value="{{ !empty($washercompany) ? $washercompany->cash : '' }}" placeholder="Cash" />
                </div>
                <div class="form-group">
                  <label>Visa/Mastercard (%)</label>
                  <input type="text" name="visa_master" id="washer_company_visa_master" class="form-control" value="{{ !empty($washercompany) ? $washercompany->visa_master : '' }}" placeholder="Visa/Mastercard (%)" />
                </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-gradient-success btn-md">Add & Save</button>
                  </div>
              </form>
            </div> -->

            <div id="washer_plus_park" class="tab-pane fade show active">
              <form method="post" action="{{ route('parking-space.parking-washer-feature') }}" id="washer_feature">
                <input type="hidden" name="type" value="plus_park">
                <input type="hidden" name="washer_feature_id" value="{{ !empty($washerplusPark) ? $washerplusPark->id : '' }}" required="required">
                <div class="form-group">
                  <label>Cost of Full Wash</label>
                  <input type="text" class="form-control" id="washer_plus_park_cost_of_full_wash" value="{{ $washerFeature->cost_of_full_wash ?? '' }}" name="cost_of_full_wash" placeholder="Cost Of Full Wash" required="required" />
                </div>
                <div class="form-group">
                  <label>Cost of Exterior Wash Only</label>
                  <input type="text" name="cost_of_exterior_wash" id="wahser_plus_park_cost_of_exterior_wash" value="{{ $washerFeature->cost_of_exterior_wash ?? '' }}" placeholder="Cost Of Exterior Wash Only" class="form-control"required="required" />
                </div>
                <div class="form-group">
                  <label>Cost of Interior Wash Only</label>
                  <input type="text" name="cost_of_interior_wash" id="washer_plus_park_cost_of_interior_wash" value="{{ $washerFeature->cost_of_interior_wash ?? '' }}" class="form-control" placeholder="Cost Of Interior Wash Only" required="required" />
                </div>
                <div class="form-check d-inline-block mr-4">
                  <label class="form-check-label text-muted">
                    <input type="radio" name="washer_type" @if($washerFeature != '' AND $washerFeature->type == 'company') checked="checked" @endif value="company" class="form-check-input" required="required">
                      <i class="input-helper"></i> Company
                  </label>
                </div>
                <div class="form-check d-inline-block mr-4">
                  <label class="form-check-label text-muted">
                    <input type="radio" name="washer_type" value="plus_park" class="form-check-input" @if($washerFeature != '' AND $washerFeature->type == 'plus_park') checked="checked" @endif>
                    <i class="input-helper"></i> Plus Parks
                  </label>
                </div>
                <div class="form-check d-inline-block mr-4">
                  <label class="form-check-label text-muted">
                    <input type="radio" name="washer_type" value="third_party" class="form-check-input" @if($washerFeature != '' AND $washerFeature->type == 'third_party') checked="checked" @endif>
                      <i class="input-helper"></i> Third party
                  </label>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-gradient-success btn-md">Add & Save</button>
                </div>
              </form>
            </div>

            <!-- <div id="washer_third_party" class="tab-pane fade">

              <form method="post" action="{{ route('parking-space.parking-washer-feature') }}" id="washer_feature">
                
                <input type="hidden" name="type" value="third_party">
                <input type="hidden" name="washer_feature_id" value="{{ !empty($washerthirdParty) ? $washerthirdParty->id : '' }}">
                <div class="form-group">
                  <label>Cost of Full Wash</label>
                  <input type="text" name="cost_of_full_wash" id="washer_third_cost_of_full_wash" value="{{ !empty($washerthirdParty) ? $washerthirdParty->cost_of_full_wash : '' }}" placeholder="Cost Of Full Wash" class="form-control"/>
                </div>
                <div class="form-group">
                  <label>Cost of Exterior Wash Only</label>
                  <input type="text" name="cost_of_exterior_wash" id="washer_third_cost_of_exterior_wash" value="{{ !empty($washerthirdParty) ? $washerthirdParty->cost_of_exterior_wash : '' }}" class="form-control" placeholder="Cost of Exterior Wash Only" />
                </div>
                <div class="form-group">
                  <label>Cost of Interior Wash Only</label>
                  <input type="text" name="cost_of_interior_wash" id="washer_third_cost_of_interior_wash" value="{{ !empty($washerthirdParty) ? $washerthirdParty->cost_of_interior_wash : '' }}" class="form-control" placeholder="Cost Of Interior Wash" />
                </div>
                <h6>Commission of Third Party</h6>
                <div class="form-group">
                  <label>Cash (%)</label>
                  <input type="text" name="cash" id="washer_third_cash" value="{{ !empty($washerthirdParty) ? $washerthirdParty->cash : '' }}" class="form-control" placeholder="Cash (%)" />
                </div>
                <div class="form-group">
                  <label>Visa/Mastercard (%)</label>
                  <input type="text" name="visa_master" id="washer_third_visa_master" value="{{ !empty($washerthirdParty) ? $washerthirdParty->visa_master : '' }}" placeholder="Visa/Mastercard (%)" class="form-control"/>
                </div>

                  <div class="text-center">
                    <button type="submit" class="btn btn-gradient-success btn-md">Add & Save</button>
                  </div>
              </form>
            </div> -->
          </div>
      </div>
    </div>
  </div>
</div>