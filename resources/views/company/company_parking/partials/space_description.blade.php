
<form class="cmxform" id="space_description" method="post" action="{{ route('parking.space-description') }}">
  <div class="row">
    <div class="col-12">
      @csrf
      <input type="hidden" name="parking_id" value="{{ $parking_id ?? '' }}">
      <input type="hidden" name="space_description_id" value="{{ !empty($spaceDescription) ? $spaceDescription->id : '' }}">

      @php
        $select_space_features = !empty($spaceDescription) ? json_decode($spaceDescription->select_space_features) : [];
        $select_vehicle_type = !empty($spaceDescription) ? json_decode($spaceDescription->select_vehicle_type) : [];
      @endphp

      <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="description" placeholder="Description">{{ !empty($spaceDescription) ? $spaceDescription->description : '' }}</textarea>
      </div>

      <div class="form-group">
        <label>Ideal for people who</label>
        <textarea class="form-control" name="ideal_for_people_who" placeholder="Ideal For People who">{{!empty($spaceDescription) ? $spaceDescription->idle_page_for_people : ''}}</textarea>
      </div>

      <h6 class="mb-1">Select Vehicle Type</h6>
      <div class="btn-group mb-3" role="group" aria-label="Basic checkbox toggle button group">
        <label>
          <input type="checkbox" class="btn-check" name="vehicle_type[]" @if(in_array('car', $select_vehicle_type)) checked="checked" @endif value="car" autocomplete="off" hidden="">
            <span class="btn btn-sm btn-outline-success">Car</span>
        </label>

        <label>
          <input type="checkbox" class="btn-check" name="vehicle_type[]" @if(in_array('truck', $select_vehicle_type)) checked="checked" @endif value="truck" autocomplete="off" hidden="">
            <span class="btn btn-sm btn-outline-success">Truck</span>
        </label>

        <label>
          <input type="checkbox" class="btn-check" name="vehicle_type[]" @if(in_array('boat', $select_vehicle_type)) checked="checked" @endif value="boat" autocomplete="off" hidden="">
            <span class="btn btn-sm btn-outline-success">Boat</span>
        </label>

       <!--  <label>
          <input type="checkbox" class="btn-check" name="vehicle_type[]" @if(in_array('bulb', $select_vehicle_type)) checked="checked" @endif value="bulb" autocomplete="off" hidden="">
            <span class="btn btn-sm btn-outline-success">Bulb</span>
        </label> -->

        <label>
          <input type="checkbox" class="btn-check" name="vehicle_type[]" @if(in_array('bike', $select_vehicle_type)) checked="checked" @endif value="bike" autocomplete="off" hidden="">
            <span class="btn btn-sm btn-outline-success">Bike</span>
        </label>

        <label>
          <input type="checkbox" class="btn-check" name="vehicle_type[]" @if(in_array('scooter', $select_vehicle_type))  checked="checked" @endif value="scooter" autocomplete="off" hidden="">
            <span class="btn btn-sm btn-outline-success">Scooter</span>
        </label>
      </div>

      <h6 class="mb-1">Select Space Features</h6>
    </div>
    <div class="col-12 col-sm-4 col-md-3">
      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array('24_hour', $select_space_features)) checked="checked" @endif value="24_hour" class="form-check-input">
          <i class="input-helper"></i> 24 X 7
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'disabled', $select_space_features)) checked="checked" @endif value="disabled" class="form-check-input">
          <i class="input-helper"></i> Disabled
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'large', $select_space_features)) checked="checked" @endif value="large" class="form-check-input">
          <i class="input-helper"></i> Large
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'driver', $select_space_features)) checked="checked" @endif value="driver" class="form-check-input">
          <i class="input-helper"></i> Driver
        </label>
      </div>
    </div>

    <div class="col-12 col-sm-4 col-md-3">
      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'street', $select_space_features)) checked="checked" @endif value="street" class="form-check-input">
          <i class="input-helper"></i> Street
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'covered', $select_space_features)) checked="checked" @endif value="covered" class="form-check-input">
          <i class="input-helper"></i> Covered
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'drive_way', $select_space_features)) checked="checked" @endif value="drive_way" class="form-check-input">
          <i class="input-helper"></i> Driveway
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'washer', $select_space_features)) checked="checked" @endif value="washer" class="form-check-input">
          <i class="input-helper"></i> Washer
        </label>
      </div>
    </div>

    <div class="col-12 col-sm-4 col-md-3">
      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'guard', $select_space_features)) checked="checked" @endif value="guard" class="form-check-input">
          <i class="input-helper"></i> Guard
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'lock_up', $select_space_features)) checked="checked" @endif value="lock_up" class="form-check-input">
          <i class="input-helper"></i> Lock Up
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'gate', $select_space_features)) checked="checked" @endif value="gate" class="form-check-input">
          <i class="input-helper"></i> Gate
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'time_extendable', $select_space_features)) checked="checked" @endif value="time_extendable" class="form-check-input">
          <i class="input-helper"></i> Time Extendable
        </label>
      </div>
    </div>  

    <div class="col-12 col-sm-4 col-md-3">
      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'yard', $select_space_features)) checked="checked" @endif value="yard" class="form-check-input">
          <i class="input-helper"></i> Yard
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'cctv', $select_space_features)) checked="checked" @endif value="cctv" class="form-check-input">
          <i class="input-helper"></i> CCTV
        </label>
      </div>

      <div class="form-check">
        <label class="form-check-label text-muted">
          <input type="checkbox" name="space_features[]" @if(in_array( 'field', $select_space_features)) checked="checked" @endif value="field" class="form-check-input">
          <i class="input-helper"></i> Field
        </label>
      </div>
    </div>  

    <div class="col-12 mt-3">
      <button type="submit" class="btn btn-gradient-success float-right">Save & Next</button>
    </div>
  </div>
</form>
