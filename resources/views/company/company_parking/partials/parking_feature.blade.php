<form class="cmxform" id="space_feature" method="post" action="{{ route('parking.space-feature') }}" enctype="multipart/form-data">
  <input type="hidden" name="space_feature_id" value="{{ !empty($parking->parkingFeature) ? $parking->parkingFeature->id : '' }}">
  @php
    $selected_feature = !empty($parking->parkingFeature) ? json_decode($parking->parkingFeature->selected_feature) : [];
    $driverFeature = $parking->parkingDriveFeatures->isNotEmpty() ? 
                               $parking->parkingDriveFeatures->first() : '';
  @endphp
  <div class="form-group">
    <div class="form-check d-inline-block mr-4">
      <label class="form-check-label text-muted">
        <input type="checkbox" @if($driverFeature != '') checked="checked" @endif id="feature-driver" name="features[]" value="driver" class="form-check-input">
          <i class="input-helper"></i> Driver
      </label>
    </div>

    <div class="form-check d-inline-block mr-4">
      <label class="form-check-label text-muted">
        <input type="checkbox" @if(in_array('washer', $selected_feature)) checked="checked" @endif id="feature-washer" name="features[]" value="washer" class="form-check-input">
          <i class="input-helper"></i> Washer
      </label>
    </div>

    <div class="form-check d-inline-block mr-4">
      <label class="form-check-label text-muted">
        <input type="checkbox" @if(in_array('booking', $selected_feature)) checked="checked" @endif id="feature-booking" name="features[]" value="booking" class="form-check-input">
          <i class="input-helper"></i> Booking
      </label>
    </div>

    <div class="form-check d-inline-block mr-4">
      <label class="form-check-label text-muted">
        <input type="checkbox" @if(in_array('subscription', $selected_feature)) checked="checked" @endif id="feature-subscription" name="features[]" value="subscription" class="form-check-input">
          <i class="input-helper"></i> Subscription
      </label>
    </div>
  </div>

  <div class="form-group mt-3">
    <button type="submit" class="btn btn-gradient-success float-right">Save & Next</button>
  </div>
</form>
