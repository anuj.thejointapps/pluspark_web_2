@extends('company.layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> Show Event
    </h3>
  </div>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <label>Event ID</label>
              <p>{{ $event->event_auto_id ?? '' }}</p>
              <label>Event Name</label>
              <p>{{ $event->name ?? '' }}</p>
              <label>Latitude</label>
              <p>{{ $event->latitude ?? '' }}</p>
              <label>Longitude</label>
              <p>{{ $event->longitude ?? '' }}</p>
              <label>Pincode</label>
              <p>{{ $event->pincode ?? '' }}</p>
            </div>

            <div class="col-6">
              <label>Start date</label>
              <p>{{ $event->start_date ?? '' }}</p> 
              <label>End Date</label>
              <p>{{ $event->end_date ?? '' }}</p> 
              <label>Start Time</label>
              <p>{{ $event->start_time ?? '' }}</p> 
              <label>End Time</label>
              <p>{{ $event->end_time ?? '' }}</p> 
            </div>
        </div>
      </div>
    </div>
  </div>

  <h3>Parking That Are Near Event Location</h3>
  <div class="row">
    @if($company->companyParking->isNotEmpty())
      @foreach($company->companyParking as $key => $companyParking)
        @if(in_array($companyParking->id, 
              array_column($event->eventPopularPlaces->toArray(), 'parking_id')))
          <div class="col-md-12 stretch-card grid-margin">
            <div class="card card-img-holder">
              <div class="card-body">
                <div class="row">
                  <div class="col-4">
                    <h5>{{ $companyParking->name ?? '' }}</h5>
                    <br><br>
                    <h5>Parking Slots : <br> </h5>
                    <p class="text-danger">
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->total_slot : 0 }} SLOT
                    </p>
                  </div>

                  <div class="col-4">
                    <h5>Location</h5>
                    <br>
                    <p>
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->location : '' }}
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->pincode : '' }}
                    </p>
                  </div>

                  <div class="col-4">
                    @php
                      $parkingFeatures  = !empty($companyParking->parkingFeature) ? json_decode($companyParking->parkingFeature->selected_feature) : [];
                    @endphp

                    <div class="row">
                      @foreach($parkingFeatures as $parkingFeature)
                        <div class="col-md-12 mb-2">{{ $parkingFeature }}</div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endif
      @endforeach
    @else

      <div class="col-md-12">
        <h2>No Parking Space Available on company</h2>
      </div>
    @endif
  </div>

</div>

@stop