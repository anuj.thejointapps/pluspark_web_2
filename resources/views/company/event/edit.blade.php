@extends('company.layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> Edit Event
    </h3>
  </div>

  <form method="post" action="{{ route('events.update', [$event->id ?? '']) }}">
    <div class="row">
      @csrf
      @method('patch')
      <div class="col-md-12 stretch-card grid-margin">
        <div class="card card-img-holder">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Event Name</label>
                  <input type="text" name="name" id="name" class="form-control" placeholder="Event Name" value="{{ $event->name ?? '' }}" />
                  <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Location</label>
                  <textarea class="form-control" name="location" rows="6" cols="4" placeholder="Location">{{ $event->location ?? '' }}</textarea>
                  <span class="text-danger">{{ $errors->first('location') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Start Date</label>
                  <input type="text" name="start_date" autocomplete="off" id="start_date" class="form-control" placeholder="Start Date" value="{{ $event->start_date ?? '' }}"/>
                  <span class="text-danger">{{ $errors->first('start_date') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>End Date</label>
                  <input type="text" autocomplete="off" name="end_date" autocomplete="off" id="end_date" class="form-control" placeholder="End Date" value="{{ $event->end_date ?? '' }}"/>
                  <span class="text-danger">{{ $errors->first('end_date') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Start Time</label>
                  <input type="text" autocomplete="off" name="start_time" id="start_time" value="{{ $event->start_time ?? '' }}" class="form-control timepicker-time" placeholder="Start Time"/>
                  <span class="text-danger">{{ $errors->first('start_time') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>End Time</label>
                  <input type="text" name="end_time" id="end_time" class="form-control timepicker-time" placeholder="End Time" value="{{ $event->end_time ?? '' }}" />
                  <span class="text-danger">{{ $errors->first('end_time') }}</span>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <h3>Add Parking That Are Near Event Location</h3>
    <div class="row">
      @if($company->companyParking->isNotEmpty())
        @foreach($company->companyParking as $key => $companyParking)
          <div class="col-md-12 stretch-card grid-margin">
            <div class="card card-img-holder">
              <div class="card-body">
                <div class="row">
                  <div class="col-3">
                    <h5>{{ $companyParking->name ?? '' }}</h5>
                    <br><br>
                    <h5>Parking Slots : <br> </h5>
                    <p class="text-danger">
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->total_slot : 0 }} SLOT
                    </p>
                  </div>

                  <div class="col-3">
                    <h5>Location</h5>
                    <br>
                    <p>
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->location : '' }}
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->pincode : '' }}
                    </p>
                    @php $countValetPrice = false; @endphp
                    @foreach($event->eventPopularPlaces as $eventPopularPlaceData)
                      @if($eventPopularPlaceData->parking_id == $companyParking->id)
                        @php $countValetPrice = true; @endphp
                        <input type="number" value="{{ $eventPopularPlaceData->valet_price ?? '' }}" class="form-control" name="parking_place[{{$key}}][valet_price]" placeholder="Valet Price">

                        <input type="hidden" value="{{ $eventPopularPlaceData->id ?? '' }}" class="form-control" name="parking_place[{{$key}}][event_popular_id]" placeholder="Valet Price">
                      @endif
                    @endforeach
                    @if(!$countValetPrice)
                      <input type="number" value="" class="form-control" name="parking_place[{{$key}}][valet_price]" placeholder="Valet Price">
                    @endif
                  </div>

                  <div class="col-3">
                    @php
                      $parkingFeatures  = !empty($companyParking->parkingFeature) ? json_decode($companyParking->parkingFeature->selected_feature) : [];
                    @endphp

                    <div class="row">
                      @foreach($parkingFeatures as $parkingFeature)
                        <div class="col-md-12 mb-2">{{ $parkingFeature }}</div>
                      @endforeach
                    </div>
                  </div>

                  <div class="col-3">
                    <input type="checkbox" name="parking_place[{{$key}}][popular_place_id]" class="form-control" @if(in_array($companyParking->id, 
                array_column($event->eventPopularPlaces->toArray(), 'parking_id'))) checked='checked' @endif  value="{{ $companyParking->id ?? '' }}">
                  </div>
                </div>

              </div>
            </div>
          </div>
        @endforeach
      @else
        <div class="col-md-12">
          <h2>No Parking Space Available on company</h2>
        </div>
      @endif
      <div class="ml-4 text-center">
        <input type="submit" class="btn btn-gradient-success" value="Update & Save">
      </div>
    </div>
  </form>
</div>

@stop

@section('js-content')

  <script type="text/javascript">
    $(document).ready(function(){

    // $('#start_time').datetimepicker();
      $("#start_date").datepicker({
          format: "yyyy-mm-dd",
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          format: "yyyy-mm-dd",
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
        });
    })

  </script>
@stop