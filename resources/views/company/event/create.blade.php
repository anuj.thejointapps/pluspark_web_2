@extends('company.layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> Add Event
    </h3>
  </div>

  <form method="post" action="{{ route('events.store') }}">
    <div class="row">
      @csrf
      <div class="col-md-12 stretch-card grid-margin">
        <div class="card card-img-holder">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Event Name</label>
                  <input type="text" name="name" id="name" class="form-control" placeholder="Event Name"/>
                  <span class="text-danger">{{ $errors->first('name') }}</span>

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Location</label>
                  <textarea class="form-control" name="location" rows="6" cols="4" placeholder="Location"></textarea>
                  <span class="text-danger">{{ $errors->first('location') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Start Date</label>
                  <input type="text" name="start_date" id="start_date" autocomplete="off" class="form-control" placeholder="Start Date"/>
                  <span class="text-danger">{{ $errors->first('start_date') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>End Date</label>
                  <input type="text" name="end_date" id="end_date" autocomplete="off" class="form-control" placeholder="End Date"/>
                  <span class="text-danger">{{ $errors->first('end_date') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Start Time</label>
                  <input type="text" autocomplete="off" name="start_time" id="start_time" class="form-control timepicker-time" placeholder="Start Time"/>
                  <span class="text-danger">{{ $errors->first('start_time') }}</span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>End Time</label>
                  <input type="text" autocomplete="off" name="end_time" id="end_time" class="form-control timepicker-time" placeholder="End Time"/>
                  <span class="text-danger">{{ $errors->first('end_time') }}</span>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <h3>Add Parking That Are Near Event Location</h3>
    <div class="row">
      @if($company->companyParking->isNotEmpty())
        @foreach($company->companyParking as $key => $companyParking)
          <div class="col-md-12 stretch-card grid-margin">
            <div class="card card-img-holder">
              <div class="card-body">
                <div class="row">
                  <div class="col-3">
                    <h5>{{ $companyParking->name ?? '' }}</h5>
                    <br><br>
                    <h5>Parking Slots : <br> </h5>
                    <p class="text-danger">
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->total_slot : 0 }} SLOT
                    </p>
                  </div>

                  <div class="col-3">
                    <h5>Location</h5>
                    <br>
                    <p>
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->location : '' }}
                      {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->pincode : '' }}
                    </p>
                    <input type="number" class="form-control" name="parking_place[{{$key}}][valet_price]" placeholder="Valet Price">
                  </div>

                  <div class="col-3">
                    @php
                      $parkingFeatures  = !empty($companyParking->parkingFeature) ? json_decode($companyParking->parkingFeature->selected_feature) : [];
                    @endphp

                    <div class="row">
                      @foreach($parkingFeatures as $parkingFeature)
                        <div class="col-md-12 mb-2">{{ $parkingFeature }}</div>
                      @endforeach
                    </div>
                  </div>

                  <div class="col-3">
                    <input type="checkbox" name="parking_place[{{$key}}][popular_place_id]" class="form-control" value="{{ $companyParking->id ?? '' }}">
                  </div>
                </div>

              </div>
            </div>
          </div>
        @endforeach
      @else
        <div class="col-md-12">
          <h2>No Parking Space Available on company</h2>
        </div>
      @endif
      <div class="ml-4 text-center">
        <input type="submit" class="btn btn-gradient-success" value="Add & Save">
      </div>
    </div>
  </form>
</div>

@stop

@section('js-content')

  <script type="text/javascript">

    $(document).ready(function(){

      $("#start_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
      });

      
    })

  </script>
@stop