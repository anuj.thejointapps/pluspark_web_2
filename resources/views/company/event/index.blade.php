@extends('company.layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> Event Management 
    </h3>
  </div>
  <form method="get" action="{{ route('events.index') }}">
    <div class="row gutter-sm">

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" class="form-control" autocomplete="off" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="From Date" id="start_date" />
      </div>

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" class="form-control" autocomplete="off" name="end_date" value="{{ $todate ?? '' }}" placeholder="To Date" id="end_date" />
      </div>

      
      <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
        <button type="submit" class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> Filter</button>
        <a href="{{ route('events.create') }}" class="btn btn-md btn-gradient-success">ADD </a>
      </div>
    </div>
  </form>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable-event">
              <thead>
                <th>Event ID</th>
                <th>Event Name</th>
                <th>Event Location</th>
                <th>Parkings (Near That Location)</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($events as $event)
                  <tr>
                    <td>{{ $event->event_auto_id ?? '' }}</td>
                    <td>{{ $event->name ?? '' }}</td>
                    <td>{{ $event->location ?? '' }}</td>
                    <td>
                      @if(!empty($event->eventPopularPlaces))
                        {{ $event->eventPopularPlaces->count() ?? 0 }}
                      @endif                    
                    </td>
                    <td>{{ $event->start_date ?? '' }}</td>
                    <td>{{ $event->end_date ?? '' }}</td>
                    <td>{{ date('h:i a', strtotime($event->start_time)) }}</td>
                    <td>{{ date('h:i a', strtotime($event->end_time)) }}</td>
                    <td>
                      <a href="{{ route('events.edit', [$event->id ?? '']) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-pencil"></i></a>
                      
                      <a href="{{ route('events.show', [$event->id ?? '']) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-eye"></i></a>

                      <label class="togglebox toggleStatus ml-1" data-event-id="{{ $event->id ?? ''}}">
                        <input type="checkbox" @if($event->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                        <span class="label-text"></span>
                      </label>

                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {{ $events->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop

@section('js-content')

  <script type="text/javascript">

    $('.datatable-event').DataTable({
          // paging: true,
          // destroy: true,
          // searching: false,
          // lengthChange: true,
          // pageLength: 10,
          // info: true, 
          dom: 'Bfrtip',
          buttons: [
           {
              extend: 'copy',
              exportOptions: {
                columns: [0,1,2,3,4,5,6]
              }
           },
           {
              extend: 'csv',
              exportOptions: {
                columns: [0,1,2,3,4,5,6]
              }
           },
           {
              extend: 'excel',
              exportOptions: {
                columns: [0,1,2,3,4,5,6]
              }
           },
           {
              extend: 'print',
              exportOptions: {
                columns: [0,1,2,3,4,5,6]
              }
           }
          ]
     });

    $(document).ready(function(){

      $("#start_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
        });
    })

    $(document).on('change', 'label.toggleStatus', function() {
      var eventId = $(this).data('event-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('events.status') }}",
        data: {event_id: eventId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })
  </script>
@stop