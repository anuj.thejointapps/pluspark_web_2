@extends('company.layouts.main-layout')
@section('content')
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-book-open-variant"></i>
      </span> Ongoing Booking Management
    </h3>
    <!-- <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">
          <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
        </li>
      </ul>
    </nav> -->
  </div>
  <div class="row gutter-sm" id="start_date_validate" style="color:red; display: none;">
      <span >*Please choose Start Date.</span>
  </div>
  <div class="row gutter-sm" id="end_date_validate" style="color:red; display: none;">
      <span >*Please choose End Date.</span>
  </div>
  <div class="row gutter-sm">
    <!-- <div class="col-12 col-sm-3 col-md-3 form-group">
      <select class="form-control">
        <option hidden="">Country</option>
        <option value="India">India</option>
        <option value="USA">USA</option>
      </select>
    </div>
    <div class="col-12 col-sm-3 col-md-3 form-group">
      <select class="form-control">
        <option hidden="">City</option>
        <option value="Delhi">Delhi</option>
        <option value="Mumbai">Mumbai</option>
      </select>
    </div> -->
    <div class="col-12 col-sm-3 col-md-4 col-lg-2 form-group">
      <input type="text" class="form-control" placeholder="From Date" id="start_date" />
    </div><br/>
    
    <div class="col-12 col-sm-3 col-md-4 col-lg-2 form-group">
      <input type="text" class="form-control" placeholder="To Date" id="end_date" />
    </div>
    <!-- <div class="col-12 col-sm-3 col-md-4 col-lg-2 form-group">
      <select class="form-control">
        <option value="" hidden="">Duration</option>
        <option value="1 Month">1 Month</option>
        <option value="2 Month">2 Month</option>
      </select>
    </div> -->
    <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
      <button class="btn btn-md btn-gradient-success" id="export_button"> 
        <i class="mdi mdi-file-excel"></i> Export
      </button>
      <!-- <button class="btn btn-md btn-gradient-info" data-toggle="modal" data-target="#add-booking">Add</button> -->
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>
                <th>Id.</th>
                <th>Arrival Date</th>
                <th>Arrival Time</th>
                <th>Parking</th>
                <th>User ID</th>
                <th>User Name</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($bookings as $booking)
                  <tr>
                    <td>{{ $booking->order_number ?? '' }}</td>
                    <td>{{ $booking->created_date ?? '' }}</td>
                    <td>{{ $booking->created_time ?? '' }}</td>
                    <td>{{ $booking->parking->name ?? '' }}</td>
                    <td>{{ $booking->user->id ?? '' }}</td>
                    <td>{{ $booking->user->name ?? '' }}</td>
                    <td>{{ $booking->booking_price ?? '' }}</td>
                    <td>{{ $booking->fetch_booking_status ?? '' }}</td>
                    <td>
                       <a href="{{ route('onGoingbooking.show', [$booking->id, $booking->getTable()]) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-eye"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('js-content')
  <script type="text/javascript">
    $("button#export_button").on('click', function(e) {
      var startDate  = $("input#start_date").val();
      var endDate    = $("input#end_date").val();

      //Validate Start Date
      if(startDate == '') {
        $("div#start_date_validate").show();
      } else {
        $("div#start_date_validate").hide();
      }

      //Validate End date
      if(endDate == '') {
        $("div#end_date_validate").show();
      } else {
        $("div#end_date_validate").hide();
      }

      //Call Ajax fro Export Excel
      if(startDate != '' && endDate != '') {
        $.ajax({
          method: 'POST',
          url: "{{ route('onGoingbooking.export') }}",
          data: {startDate: startDate, endDate:endDate},
          dataType: 'json',
          success: function(response) {
            if (response.status == 200) {
              swal(response.success_message)
              location.reload(true);
            }
          }
        });
      }
    });
  </script>
@stop