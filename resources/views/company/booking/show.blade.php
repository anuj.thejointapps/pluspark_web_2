@extends('company.layouts.main-layout')
@section('content')
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title">
        <span class="page-title-icon bg-gradient-success text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> Ongoing Booking Management <i class="mdi mdi-chevron-right"></i> View Booking
      </h3>
      <!-- <nav aria-label="breadcrumb">
        <ul class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">
            Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
          </li>
        </ul>
      </nav> -->
    </div>
    <div class="row">
      <div class="col-md-12">
        <!--  -->
        <div class="card mb-3">
          <div class="container card-body">
            <div class="row">
              <div class="col-sm-6">
                <h5>Booking Details</h5>
                <label>Booking ID</label>
                <p>{{ $booking->order_number ?? '' }}</p> 
                <label>Booking Date</label>
                <p>{{ $booking->created_date ?? '' }}</p> 
                <label>Parking</label>
                <p>{{ $booking->parking->name ?? '' }}</p>
                <label>User ID</label>
                <p>{{ $booking->user->id ?? '' }}</p>
                <label>User Name</label>
                <p>{{ $booking->user->name ?? '' }}</p>
                <label>Charge (Per Hr)</label>
                <p>Data</p>
                <label>Payment Method</label>
                <p>{{ ucfirst($booking->payment_method) ?? '' }}</p>
                <label>Payment Amount</label>
                <p>{{ $booking->booking_price ?? '' }}</p>
              </div>
              <div class="col-sm-6"> 
                <h5>Service Taken</h5>
                <label>Driver</label>
                @if($driver != '')
                  <p>Id: {{ $driver->id ?? '' }}</p> 
                  <p>Name: {{ $driver->name ?? '' }}</p> 
                  <p>Email: {{ $driver->email ?? '' }}</p> 
                  <p>Mobile: {{ $driver->mobile ?? '' }}</p> 
                @else
                  <p>N/A</p>
                @endif
                <label>Washer</label>
                @if($washer != '')
                  <p>Id: {{ $washer->id ?? '' }}</p> 
                  <p>Name: {{ $washer->name ?? '' }}</p> 
                  <p>Email: {{ $washer->email ?? '' }}</p> 
                  <p>Mobile: {{ $washer->mobile ?? '' }}</p> 
                @else
                  <p>N/A</p>
                @endif

                <h5>Vehicles</h5>
                <label>Vehicle Brand</label>
                <p>{{ $booking->vehicle->full_name ?? '' }}</p> 
                <label>Vehicle Model</label>
                <p>{{ $booking->vehicle->modal_name ?? '' }}</p> 
                <label>Vehicle Plate Number</label>
                <p>{{ $booking->vehicle->plate_number ?? '' }}</p> 
              </div>
            </div>
          </div>
        </div>
        <!--  -->
      </div>
    </div>
  </div>
@stop