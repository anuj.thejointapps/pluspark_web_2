@extends('company.layouts.main-layout')
@section('content')

  <!-- content-wrapper start -->
  <div class="content-wrapper">
    <div class="page-header">
      <!-- <h3 class="page-title"> Country </h3> -->
      <h3 class="page-title"> Promocode Management</h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('company.dashboard.index') }}"><i class="mdi mdi-home"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">Promocode</li>
        </ol>
      </nav>
    </div>

    <form method="get" action="{{ route('company-promocodes.index') }}">
      <div class="row gutter-sm">

        <div class="col-12 col-sm-3 col-md-3 form-group">
            <input type="text" autocomplete="off" class="form-control" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="From Date" id="start_date" />
        </div>
        <div class="col-12 col-sm-3 col-md-3 form-group">
            <input type="text" autocomplete="off"  class="form-control" name="end_date" value="{{ $todate ?? '' }}" placeholder="To Date" id="end_date" />
        </div>
        <div class="col-12 col-sm-3 col-md-3 form-group">
            <select class="form-control" name="month">
              <option value="">Select Month</option>
              <option value="1" @if($month == '1') selected="selected" @endif>1 Month</option>
              <option value="3" @if($month == '3') selected="selected" @endif>3 Months</option>
              <option value="6" @if($month == '6') selected="selected" @endif>6 Months</option>
            </select>
        </div>

        <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
          <button type="submit" class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> Filter</button>
        </div>
      </div>
    </form>

    <div class="row">
      <div class="col-md-12 stretch-card grid-margin">
        <div class="card card-img-holder">
          <div class="card-body">
            <div class="table-responsive">
              <a href="#" class="btn btn-sm float-right btn-md btn-gradient-primary mb-2" id="add_discount_button" data-toggle="modal" data-target="#myModal">Add</a>
              <table class="table table-bordered datatable-promocode">
                <thead>
                  <tr>
                    <th>Promocode Id</th>
                    <th>Code</th>
                    <th>Offer Type</th>
                    <th>Discount</th>
                    <th>validity</th>
                    <th>Start date</th>
                    <th>Expiry date</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($promocodes as $promocode)
                    <tr>
                      <td>{{ $promocode->unique_id ?? '' }}</td>
                      <td>{{ $promocode->code ?? '' }}</td>
                      <td>{{ strtoupper($promocode->offer_type) ?? '' }}</td>
                      <td>{{ $promocode->discount ?? '' }}</td>
                      <td>{{ $promocode->validity_in_month ?? '' }}</td>
                      <td>{{ $promocode->start_date_formatted ?? '' }}</td>
                      <td>{{ $promocode->end_date_formatted ?? '' }}</td>
                      <td>
                        <!--   <a href="#" class="btn btn-md btn-gradient-primary mb-3" data-id="{{ $promocode->id }}" id="edit_promocode">Edit</a> -->

                        <button type="button" class="btn btn-sm btn-gradient-primary mb-3 btn-modal" data-href="{{ route('promocode.openPromocodeUpdate', [$promocode->id ?? '']) }}" data-container=".promocode_edit">Edit </button>

                        <label class="togglebox toggleStatus ml-1" data-id="{{ $promocode->id ?? ''}}">
                          <input type="checkbox" @if($promocode->status) checked="checked" @endif name="block_unblock" hidden=""/>
                          <span class="label-text"></span>
                        </label>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $promocodes->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Add Modal -->
  <form action="{{route('company-promocodes.store')}}" method="POST">
      @csrf
    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <form method="post" action="#">
              <h5>Add Promocode</h5>
              <div class="form-group">
                <label>Start Date</label><span class="required" style="color: red;">*</span>
                <input type="text" autocomplete="off" name="start_date" class="form-control start_date_formatted" id="start_date1" required="required"/>
              </div>
              <div class="form-group">
                <label>End Date</label><span class="required" style="color: red;">*</span>
                <input type="text" autocomplete="off" name="end_date" class="form-control end_date_formatted" id="end_date1" required="required"/>
              </div>
            <!--   <div class="form-group">
                <label>Company Name</label><span class="required" style="color: red;">*</span>
                <select class="form-control" name="company_id" id="company_name" required="required">
                  <option hidden="" value="0">Choose Company</option>
                  @foreach($companies as $company)
                    <option value="{{ $company->id }}">{{ $company->name ?? '' }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Company ID</label><span class="required" style="color: red;">*</span>
                <input type="text" name="" class="form-control" id="company_id" />
              </div> -->
              <div class="form-group">
                <label>Promocode ID</label><span class="required" style="color: red;">*</span>
                <input type="text" name="promocode_id" class="form-control" id="promocode_id" value="{{ $nextPromoCodeUniqueId }}" />
              </div>
              <div class="form-group">
                <label>Code</label><span class="required" style="color: red;">*</span>
                <input type="text" name="code" class="form-control" required="required"/>
              </div>
              <div class="form-group">
                <label>Discount Type</label><span class="required" style="color: red;">*</span>
                <select class="form-control" name="discount_type" id="discount_type" required="required">
                  <option value="percent">Percent Discount</option>
                  <option value="flat">Flat Discount</option>
                </select>
              </div>
              <div class="form-group">
                <label>Discount</label><span class="required" style="color: red;">*</span>
                <input type="text" name="discount" class="form-control"  onkeypress="return numbersonly(event);"  required="required"/>
              </div>
              <div class="form-group">
                <label>Usage Allowed</label><span class="required" style="color: red;">*</span>
                <input type="text" name="usage_allowed" class="form-control"  onkeypress="return numbersonly(event);"  required="required"/>
              </div>
              <h6>Service</h6>
              <div class="row">
                <div class="form-group ml-5">
                  <label class="form-check-label text-dark">
                    <input type="checkbox" name="booking" value="booking" class="form-check-input">
                      <i class="input-helper"></i> Booking
                  </label>
                </div>

                <div class="form-group ml-5">
                  <label class="form-check-label text-dark">
                    <input type="checkbox" name="subscription"value="subscription" class="form-check-input">
                      <i class="input-helper"></i> Subscription
                  </label>
                </div>

                <div class="form-group ml-5">
                  <label class="form-check-label text-dark">
                    <input type="checkbox" name="valet"value="valet" class="form-check-input">
                      <i class="input-helper"></i> Valet
                  </label>
                </div>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-gradient-success">Submit</button>
                 <button type="button" class="btn btn-gradient-danger" data-dismiss="modal">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </form>

  <!-- <div class="modal fade updatePromocodeModal" id="updatePromocodeModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="updateSplashImage" aria-labelledby="gridSystemModalLabel">
  </div> -->

  <div class="modal fade promocode_edit" data-keyboard="false" tabindex="-1" role="dialog" 
      aria-labelledby="gridSystemModalLabel" data-backdrop="static" data-keyboard="false">
  </div>
@stop

@section('js-content')
  <script type="text/javascript">

    $('.datatable-promocode').DataTable({
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'copy',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
        },
        {
          extend: 'csv',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
        },
        {
          extend: 'excel',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
        },
        {
          extend: 'print',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
        }
      ]
     });
    var nowDate = new Date();
    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
    //Set the datepicker date Style to Y-m-D
    $( ".start_date_formatted" ).datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      startDate: today,
    });

    //Set the datepicker date Style to Y-m-D
    $( ".end_date_formatted" ).datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      startDate: today, 
    });


    //Toggle status
    $(document).on('change', 'label.toggleStatus', function() {
      var promocodeId = $(this).attr('data-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('company-promocode.status') }}",
        data: {id: promocodeId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })

    // Number Only.
    function numbersonly(e) {
      var unicode=e.charCode? e.charCode : e.keyCode
      if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
          if (unicode<48||unicode>57) //if not a number
            return false //disable key press
      }
    }
  </script>
@stop