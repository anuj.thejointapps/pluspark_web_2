<!-- footer -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- footer -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <script src="{{asset('assets/vendors/js/vendor_bundle_base.js')}}"></script>
    <script src="{{asset('assets/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/vendors/sweetalert/sweetalert.min.js')}}"></script>

    <script src="{{ asset('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net/jquery.dataTables.js') }}"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    <script src="{{ asset('assets/js/jquery.multiselect.js')}}"></script>
    <script src="{{ asset('assets/js/iEdit.js')}}"></script>
    <!-- <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/hoverable-collapse.js"></script>
    <script src="assets/js/misc.js"></script>
    <script src="assets/js/settings.js"></script>
    <script src="assets/js/todolist.js"></script> -->
    <script src="{{asset('assets/js/alerts.js')}}"></script>

  <script type="text/javascript" src="{{ asset('assets/vendors/datatable-button/vfs_fonts.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/vendors/datatable-button/pdfmake.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/vendors/datatable-button/jszip.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/vendors/datatable-button/datatables.buttons.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/vendors/datatable-button/buttons.html5.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/vendors/datatable-button/buttons.print.min.js') }}"></script>
  <!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script> -->
  <!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script> -->
  <!-- Validation js -->
  <script type="text/javascript" src="{{ asset('assets/vendors/jquery-validation/jquery.validate.min.js') }}"></script>
    
    <script src="{{asset('assets/js/dashboard.js')}}"></script>
    <script src="{{asset('assets/js/global.js')}}"></script>
    <!-- <script src="assets/js/form-validation.js"></script> -->
    <script type="text/javascript">
      $("select#languageForLang").on('change', function(e) {
        alert(1111111);
      });
    </script>
          
