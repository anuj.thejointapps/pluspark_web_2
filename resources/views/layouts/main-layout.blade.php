<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Plus Spark Admin</title>
    <!-- css -->
    <link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" >
    <link rel="stylesheet" href="{{asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('assets/css/demo_1/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/globle.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/jquery.multiselect.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/iEdit.css')}}"/>
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
    <style type="text/css">
      .error {
        color: red;
      }
    </style>
  </head>
  <body>
    <div class="container-scroller">
	 @if(session()->has('success') || session()->has('error')) 
    @endif
  <!-----------------modal--------------------->
  	@include('layouts.header')
		@yield('content')
		@include('layouts.footer')
	@yield('js-content')

	<script type="text/javascript">
		$(document).ready(function() {
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		});
	
	  // btn-modal.
	  $(document).on( 'click', 'button.btn-modal', function(e){
	      e.preventDefault();
	      var container = $(this).data("container");

	      $.ajax({
	          url: $(this).data("href"),
	          dataType: "html",
	          success: function(result){
	              $(container).html(result).modal('show');
	          }
	      });
	  });

    // Link Modal.
	  $(document).on('click', 'a.link-modal', function(e) {
	    e.preventDefault();
	      var container = $(this).data("container");

	      $.ajax({
	          url: $(this).attr("href"),
	          dataType: "html",
	          success: function(result){
	              $('div.'+container).html(result).modal('show');
	          }
	      });
	  });
	      
    // $(document).ready(function(){
    //   $('.datatable').DataTable({
    //         paging: false,
    //           destroy: true,
    //        dom: 'Bfrtip',
    //           buttons: [
              
    //              {extend: "copy", className: "buttonsToHide"},
    //              {extend: "csv", className: "buttonsToHide"},
    //              {extend: "excel", className: "buttonsToHide"},
    //              {extend: "print", className: "buttonsToHide"},
    //               // 'copy', 'csv', 'excel', 'pdf', 'print'
    //           ]
    //    });
    //  });

    $('#country_name').on('change',function(){
      var countryID = $('option:selected', this).attr('data-id');
        
      $('#state_name').html(''); 
      if(countryID){
        $.ajax({
          type : 'post',
          url  : '{{ route("states.country") }}',
          data : {country_id: countryID},
          dataType : 'json',
          success:function(response) {
            $('#state_name').html('<option value="">Select City</option>'); 
            if (response.status == 200) {
              response.data.map((state, key) => {
                $('#state_name').append('<option value="'+state.name+'">'+state.name+'</option>'); 
              })
            }
          }
        }); 
      } else {
        $('#state_name').html('<option value="">Select City</option>'); 
      }
    })

    $('#country_id').on('change',function(){
      var countryID = $('option:selected', this).attr('data-id');
        
      $('#state_id').html(''); 
      if(countryID){
        $.ajax({
          type : 'post',
          url  : '{{ route("states.country") }}',
          data : {country_id: countryID},
          dataType : 'json',
          success:function(response) {
            $('#state_id').html('<option value="">Select City</option>'); 
            if (response.status == 200) {
              response.data.map((state, key) => {
                $('#state_id').append('<option value="'+state.id+'">'+state.name+'</option>'); 
              })
            }
          }
        }); 
      } else {
        $('#state_id').html('<option value="">Select City</option>'); 
      }
    })
    
    // Number Only.
    function numbersonly(e) {
      var unicode=e.charCode? e.charCode : e.keyCode
      if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
          if (unicode<48||unicode>57) //if not a number
            return false //disable key press
      }
    }
	</script>
	@if(Session::has('success_message'))
      	<script>
        	swal("{{ session('success_message') }}");
      	</script>
	@endif
</body>
</html>