
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PlusPark Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/demo_1/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
          <div class="row flex-grow">
            <div class="col-lg-6 d-flex align-items-center justify-content-center">
              <div class="auth-form-transparent text-left p-3">
                <div class="brand-logo">
                  <img src="assets/images/logo.svg" alt="logo">
                </div>
                <h4>Forgot Password</h4>
                <!-- <h6 class="font-weight-light">Happy to see you again!</h6> -->
                <form class="pt-3">
                  <div class="form-group">
                    <label for="exampleInputEmail">Email Id</label>
                    <div class="input-group">
                      <div class="input-group-prepend bg-transparent">
                        <span class="input-group-text bg-transparent border-right-0">
                          <i class="mdi mdi-account-outline text-primary"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control form-control-lg border-left-0" id="email_id" placeholder="Email Id" required="required">
                    </div>
                  </div>
                  <div class="my-3">
                    <button type="button" id="send_link_button" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn">SEND LINK</button>
                  </div>
                  <div class="text-center mt-4 font-weight-light"> Back to <a href="{{url('/')}}" class="text-primary">Login</a></div>
                </form>
              </div>
            </div>
            <div class="col-lg-6 login-half-bg d-flex flex-row">
              <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2021 All rights reserved.</p>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->    
    <script src="{{asset('assets/vendors/js/vendor_bundle_base.js')}}"></script>
    <script src="assets/vendors/sweetalert/sweetalert.min.js"></script>
    <script src="assets/js/alerts.js"></script>
    
    <script type="text/javascript">
      $("button#send_link_button").on('click', function(e) {
        e.preventDefault();
        let emailId = $('input#email_id').val();

        //Validate emailId
        if(emailId == '') {
          return false;
        }
        
        //Send to Controller
        $.ajax({
            url: "{{action('Admin\ForgotPasswordController@sendEmail')}}",
            method: "GET",
            dataType: "json",
            data:{'email':emailId},
            success: function(response) {
              if (response.status == 200) {
                swal({
                  title: "",
                  text: response.msg,
                  icon: "success",
                });
              } else {
                swal({
                  title: "",
                  text: response.msg,
                  icon: "warning",
                });
              }
            }
          }); 
      });
    </script>
  </body>
</html>