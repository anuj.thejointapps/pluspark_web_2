@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> Edit Profile </h3>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="dashboard.php"><i class="mdi mdi-home"></i></a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
      </ol>
    </nav>
  </div>
  @if(Session::has('success_message'))
    <span class="alert alert-success">{{ Session::get('success_message') }}</span>
  @endif
  <div class="row">
    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
      <div class="card">
        <div class="card-body">
            

          <form class="cmxform" id="signupForm" method="post" action="{{ route('admin.myprofile', [$admin->id]) }}">
            @csrf
            <fieldset>
              <div class="form-group">
                <label for="username">User Name</label>
                <input id="username" class="form-control" placeholder="User Name" value="{{ $admin->name ?? '' }}" name="username" type="text">
                <span class="text-danger">{{ $errors->first('firstname') }}</span>
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input id="email" class="form-control" value="{{ $admin->email ?? '' }}" placeholder="Email" name="email" type="email">
                <span class="text-danger">{{ $errors->first('email') }}</span>
              </div>
              <div class="form-group">
                <label for="mobile">Mobile</label>
                <input id="mobile" class="form-control" value="{{ $admin->mobile ?? '' }}" placeholder="Mobile" name="mobile" type="text">
                <span class="text-danger">{{ $errors->first('mobile') }}</span>
              </div>
              <div class="form-group">
                <label for="region">Region</label>
                <input id="region" class="form-control" value="{{ $admin->region ?? '' }}" placeholder="Region" name="region" type="text">
                <span class="text-danger">{{ $errors->first('region') }}</span>
              </div>
              <div class="form-group">
                  <label for="country_id">Country</label>
                  <select class="form-control" name="country_id" id="country_id">
                    <option value="" data-id="">Select Country</option>
                    @foreach($countries as $country)
                      <option  data-id="{{ $country->id ?? '' }}" @if($admin->country_id == $country->id) selected="selected" @endif value="{{ $country->id ?? ''}}">
                        {{ $country->name ?? '' }}</option>
                  @endforeach
                  </select>
                <span class="text-danger">{{ $errors->first('country_id') }}</span>
              </div>
              <div class="form-group">
                  <label for="city_id">City</label>
                  <select class="form-control" name="city_id" id="state_id">
                    <option value="">Select City</option>
                    @foreach($states as $city)
                      <option @if($admin->city_id == $city->id) selected="selected" @endif value="{{ $city->id ?? ''}}">
                        {{ $city->name ?? '' }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group">
                <label for="address">Address</label>
                <input id="address" class="form-control" value="{{ $admin->address ?? '' }}" placeholder="Address" name="address" type="text">
                <span class="text-danger">{{ $errors->first('address') }}</span>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop