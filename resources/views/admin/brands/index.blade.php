@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <!-- <h3 class="page-title"> Brand </h3> -->
    

    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> Brands 
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">Brands</li>
        </ol>
    </nav>
  </div>
  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <a href="{{ route('brands.create') }}" class="btn float-left mr-1 btn-sm btn-gradient-primary mb-3">Add Brand</a>
            <table class="table table-bordered datatable-brand">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($brands as $brand)
                <tr>
                  <td>{{ $brand->name ?? '' }}</td>
                  <td>
                    <a href="{{ route('brands.edit', [$brand->id]) }}" class="btn btn-sm btn-gradient-primary mb-3">Edit</a>
                    <label class="togglebox toggleStatus ml-1" data-brand-id="{{ $brand->id ?? ''}}">
                      <input type="checkbox" @if($brand->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                      <span class="label-text"></span>
                    </label>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('js-content')

  <script type="text/javascript">
    
    $('.datatable-brand').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'copy',
          exportOptions: {
            columns: [0]
          }
       },
       {
          extend: 'csv',
          exportOptions: {
            columns: [0]
          }
       },
       {
          extend: 'excel',  
          exportOptions: {
            columns: [0]
          }
       },
       {
          extend: 'print',
          exportOptions: {
            columns: [0]
          }
       }
      ]
    });

    $(document).on('change', 'label.toggleStatus', function() {
      var brandId = $(this).data('brand-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('brands.status') }}",
        data: {brand_id: brandId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })
  </script>
@stop