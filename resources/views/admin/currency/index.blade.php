@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">

    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-account"></i>
      </span> {{__('currency.currencies')}} 
    </h3>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i></a></li>
        <li class="breadcrumb-item active" aria-current="page">{{__('currency.currency')}} </li>
      </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <a href="{{ route('currencies.create') }}" class="btn btn-sm float-left mr-1 btn-gradient-primary mb-3">{{__('basicbuttons.add')}}  {{__('currency.currency')}}</a>
            <table class="table table-bordered datatable-currency">
              <thead>
                <tr>
                  <th>{{__('country.name')}} </th>
                  <th>{{__('currency.symbol')}} </th>
                  <th>{{__('currency.code')}}</th>
                  <th>{{__('country.country')}} {{__('country.name')}}</th>
                  <th class="wpx-90">{{__('basicbuttons.action')}} </th>
                </tr>
              </thead>
              <tbody>
                @foreach($currencies as $currency)
                <tr>
                  <td>{{ $currency->name ?? '' }}</td>
                  <td><img src="{{ asset($currency->symbol ?? '') }}" width="50px" height="50px"></td>
                  <td>{{ $currency->currency_code ?? '' }}</td>
                  <td>{{ $currency->country ? $currency->country->name : '' }}</td>
                  <td>
                    <a href="{{ route('currencies.edit', [$currency->id]) }}" class="btn btn-sm btn-gradient-primary">{{__('basicbuttons.edit')}} </a>
                    <label class="togglebox toggleStatus ml-1" data-currency-id="{{ $currency->id ?? ''}}">
                      <input type="checkbox" @if($currency->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                      <span class="label-text"></span>
                    </label>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('js-content')

  <script type="text/javascript">
    
    $('.datatable-currency').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'copy',
          text:'{{ __('datatablebuttons.copy') }}',
          exportOptions: {
            columns: [0,1,2,3]
          }
       },
       {
          extend: 'csv',
          text:'{{ __('datatablebuttons.csv') }}',
          exportOptions: {
            columns: [0,1,2,3]
          }
       },
       {
          extend: 'excel',  
          text:'{{ __('datatablebuttons.excel') }}',
          exportOptions: {
            columns: [0,1,2,3]
          }
       },
       {
          extend: 'print',
          text:'{{ __('datatablebuttons.print') }}',
          exportOptions: {
            columns: [0,1,2,3]
          }
       }
      ]
    });

    $(document).on('change', 'label.toggleStatus', function() {
      var currencyId = $(this).data('currency-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('currencies.status') }}",
        data: {currency_id: currencyId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })
  </script>
@stop