@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-account"></i>
      </span> {{__('currency.currency')}} {{__('basicbuttons.edit')}}  
    </h3>

    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">
          <a href="{{ route('currencies.index') }}">
            <i class="mdi mdi-arrow-left icon-sm text-primary align-middle mr-1"></i> {{__('basicbuttons.back')}} 
          </a>
        </li>
      </ul>
    </nav>
  </div>
  <!-- row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <form class="cmxform" id="signupForm" method="post" enctype="multipart/form-data" action="{{ route('currencies.update', [$currency->id]) }}">
            @csrf
            @method('patch')
            <fieldset>
              <div class="form-group">
                <label for="name">{{__('currency.currency')}} {{__('country.name')}}</label>
                <input id="name" class="form-control" value="{{ $currency->name ?? '' }}" placeholder="{{__('currency.currency')}} {{__('country.name')}}" name="name" type="text">
                <span class="text-danger">{{ $errors->first('name') }}</span>
              </div>

              <div class="form-group">
                <label for="currency_code">{{__('currency.currency')}} {{__('currency.code')}}</label>
                <input id="currency_code" class="form-control" value="{{ $currency->currency_code ?? '' }}" placeholder="{{__('currency.currency')}} Code" name="currency_code" type="text">
                <span class="text-danger">{{ $errors->first('currency_code') }}</span>
              </div>

              <div class="form-group">
                <label for="country_id">{{__('country.country')}} {{__('country.name')}}</label>
                <select class="form-control" name="country_id">
                  @foreach($countries as $country)
                    <option value="{{ $country->id ?? '' }}" {{ ($country->id==$currency->country_id) ? 'selected="selected"' : '' }}>{{ $country->name ?? '' }}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label for="username">{{__('currency.currency')}} {{__('currency.symbol')}}</label>
                <input id="username" class="form-control" name="symbol" type="file">
                <img src="{{ asset($currency->symbol ?? '') }}" width="50px" height="50px">
              </div>

              <button type="submit" class="btn btn-primary">{{__('basicbuttons.update')}} </button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- End row -->
</div>
@stop
