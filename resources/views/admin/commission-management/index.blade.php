@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-water-pump"></i>
      </span> Company List
    </h3>
  </div>
  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>
                <th>C ID</th>
                <th>Company Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Parking Capacity</th>
                <th>Country</th>
                <th>City</th>
                <th>Driver Commission</th>
                <th>Washer Commission</th>
              </thead>
              <tbody>
                @foreach($companies as $company)
                  <tr>
                    <td>{{ $company->company_auto_id ?? '' }}</td>
                    <td>{{ $company->contact_person_name ?? '' }}</td>
                    <td>{{ $company->contact_person_mobile ?? '' }}</td>
                    <td>{{ $company->email ?? '' }}</td>
                    <td>{{ $company->number_of_parking ?? '' }}</td>
                    <td>{{ $company->country ?? '' }}</td>
                    <td>{{ $company->city ?? '' }}</td>
                    <td>
                      <a href="#" 
                        class="btn btn-sm btn-gradient-primary mb-3"
                        data-id="{{ $company->id }}" 
                        data-type="driver" 
                        id="edit_driver">
                         Edit
                      </a>
                    </td>
                    <td>
                      <a href="#" 
                        class="btn btn-sm btn-gradient-primary mb-3"
                        data-id="{{ $company->id }}" 
                        data-type="washer" 
                        id="edit_washer">
                         Edit
                      </a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<!-- content-wrapper ends -->
<div class="modal fade updateDriverWasher" id="updateDriverWasher" data-backdrop="static" data-keyboard="false" tabindex="-1" role="updateSplashImage" aria-labelledby="gridSystemModalLabel">
</div>
@stop

@section('js-content')
  <script type="text/javascript">
    //On Edit of Driver Commission
    $('a#edit_driver').on('click', function(e) {
     var companyId = $(this).attr('data-id');
     $.ajax({
        url: "{{action('Admin\CommissionController@OpenEditCommission')}}",
        method: "GET",
        dataType: "json",
        data:{'id':companyId, 'type' : 'driver'},
        success: function(response) {
          if (response.status == 200) {
            $('#updateDriverWasher').html(response.html);
            $('.updateDriverWasher').modal('show');
          } 
        }
      }); 
    });

    //On Edit of Washer Commission
    $('a#edit_washer').on('click', function(e) {
     var companyId = $(this).attr('data-id');
     $.ajax({
        url: "{{action('Admin\CommissionController@OpenEditCommission')}}",
        method: "GET",
        dataType: "json",
        data:{'id':companyId, 'type' : 'washer'},
        success: function(response) {
          if (response.status == 200) {
            $('#updateDriverWasher').html(response.html);
            $('.updateDriverWasher').modal('show');
          } 
        }
      }); 
    });

    // Number Only.
    function numbersonly(e) {
      var unicode=e.charCode? e.charCode : e.keyCode
      if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
          if (unicode<48||unicode>57) //if not a number
            return false //disable key press
      }
    }

  </script>
@stop