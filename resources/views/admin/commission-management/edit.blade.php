<form action="{{ route('commission.update', $company->id) }}" method="post" enctype="multipart/form-data" onsubmit="return Validate(this);">
  <input type="hidden" name="company_id" value="{{$company->id}}">
  @csrf
  {{ method_field('PUT') }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h3 style="text-align:center;" >Commission of @if($type == 'driver') Driver @else Washer @endif </h3>
        @if($type == 'driver')
          <input type="hidden" name="type" value="driver">
          <h6>Commission of Company</h6>
          <div class="form-group">
            <label>Cash (%)</label>
            <input type="text" class="form-control" id="company_cash" placeholder="Cash" name="company_cash" value="{{ $driverCommission != '' ? $driverCommission->company_cash_comm : '' }}" required="required" />
          </div>
          <div class="form-group">
            <label>Visa/Mastercard (%)</label>
            <input type="text" name="company_visa_master" id="company_visa_master" class="form-control" value="{{ $driverCommission != '' ? $driverCommission->company_card_comm : '' }}" placeholder="Visa/Mastercard" required="required" onkeypress="return numbersonly(event);"/>
          </div>
          <h6>Commission of Plus Park</h6>
          <div class="form-group">
            <label>Cash (%)</label>
            <input type="text" name="plus_park_cash" id="plus_park_cash" class="form-control" value="{{ $driverCommission != '' ? $driverCommission->pls_park_cash_comm : '' }}" placeholder="Cash (%)" required="required" onkeypress="return numbersonly(event);"/>
          </div>
          <div class="form-group">
            <label>Visa/Mastercard (%)</label>
            <input type="text" name="plus_park_visa_master" id="plus_park_visa_master" class="form-control" value="{{ $driverCommission != '' ? $driverCommission->pls_park_card_comm : '' }}" placeholder="Visa/Mastercard (%)"  onkeypress="return numbersonly(event);"/>
          </div>
          <h6>Commission of Third Party</h6>
          <div class="form-group">
            <label>Cash (%)</label>
            <input type="text" name="third_party_cash" id="third_cost" value="{{ $driverCommission != '' ? $driverCommission->thrd_party_cash_comm : '' }}" class="form-control" placeholder="Cash (%)" required="required" onkeypress="return numbersonly(event);"/>
          </div>
          <div class="form-group">
            <label>Visa/Mastercard (%)</label>
            <input type="text" name="third_visa_master" id="third_visa_master" value="{{ $driverCommission != '' ? $driverCommission->thrd_party_card_comm : '' }}" class="form-control" placeholder="Visa/Mastercard (%)" required="required" onkeypress="return numbersonly(event);"/>
          </div>
        @else
          <input type="hidden" name="type" value="washer">
          <h6>Commission of Company</h6>
          <div class="form-group">
            <label>Cash (%)</label>
            <input type="text" class="form-control" id="company_cash" placeholder="Cash" name="company_cash" value="{{ $washerCommission != '' ? $washerCommission->company_cash_comm : '' }}" required="required" />
          </div>
          <div class="form-group">
            <label>Visa/Mastercard (%)</label>
            <input type="text" name="company_visa_master" id="company_visa_master" class="form-control" value="{{ $washerCommission != '' ? $washerCommission->company_card_comm : '' }}" placeholder="Visa/Mastercard" required="required" onkeypress="return numbersonly(event);"/>
          </div>
          <h6>Commission of Plus Park</h6>
          <div class="form-group">
            <label>Cash (%)</label>
            <input type="text" name="plus_park_cash" id="plus_park_cash" class="form-control" value="{{ $washerCommission != '' ? $washerCommission->pls_park_cash_comm : '' }}" placeholder="Cash (%)" required="required" onkeypress="return numbersonly(event);"/>
          </div>
          <div class="form-group">
            <label>Visa/Mastercard (%)</label>
            <input type="text" name="plus_park_visa_master" id="plus_park_visa_master" class="form-control" value="{{ $washerCommission != '' ? $washerCommission->pls_park_card_comm : '' }}" placeholder="Visa/Mastercard (%)"  onkeypress="return numbersonly(event);"/>
          </div>
          <h6>Commission of Third Party</h6>
          <div class="form-group">
            <label>Cash (%)</label>
            <input type="text" name="third_party_cash" id="third_cost" value="{{ $washerCommission != '' ? $washerCommission->thrd_party_cash_comm : '' }}" class="form-control" placeholder="Cash (%)" required="required" onkeypress="return numbersonly(event);"/>
          </div>
          <div class="form-group">
            <label>Visa/Mastercard (%)</label>
            <input type="text" name="third_visa_master" id="third_visa_master" value="{{ $washerCommission != '' ? $washerCommission->thrd_party_card_comm : '' }}" class="form-control" placeholder="Visa/Mastercard (%)" required="required" onkeypress="return numbersonly(event);"/>
          </div>
        @endif
        <div class="text-center">
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script>
  //Set the datepicker date Style to Y-m-D on Edit
  $( "input.edit_start_date_formatted" ).datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: today,
  });

  
  //Set the datepicker date Style to Y-m-D
  $( "input.edit_end_date_formatted" ).datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: today, 
  });

</script>
