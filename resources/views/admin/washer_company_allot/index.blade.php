@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-water-pump"></i>
      </span> Company List
    </h3>
  </div>
    <form method="get" action="{{ route('washer-company-allot.index') }}">
      <div class="row gutter-sm">
        <div class="col-12 col-sm-3 col-md-3 form-group">
            <select class="form-control" name="country_name" id="country_name">
              <option value="" data-id="">Select Country</option>
              @foreach($countries as $country)
                <option  data-id="{{ $country->id ?? '' }}" @if($countryName == $country->name) selected="selected" @endif value="{{ $country->name ?? ''}}">
                  {{ $country->name ?? '' }}</option>
            @endforeach
            </select>
        </div>

        <div class="col-12 col-sm-3 col-md-3 form-group">
            <select class="form-control" name="state_name" id="state_name">
              <option value="">Select City</option>
              @foreach($states as $state)
                <option @if($stateName == $state->name) selected="selected" @endif value="{{ $state->name ?? ''}}">
                  {{ $state->name ?? '' }}</option>
            @endforeach
            </select>
        </div>
        <div class="col-12 col-sm-3 col-md-3 form-group">
            <input type="text" autocomplete="off" class="form-control" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="From Date" id="start_date" />
        </div>
        <div class="col-12 col-sm-3 col-md-3 form-group">
            <input type="text" autocomplete="off"  class="form-control" name="end_date" value="{{ $todate ?? '' }}" placeholder="To Date" id="end_date" />
        </div>

        <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
          <button type="submit" class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> Filter</button>
        </div>
      </div>
    </form>
  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>
                <th>C ID</th>
                <th>Company Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Parking Capacity</th>
                <th>Country</th>
                <th>City</th>
                <th>Washer Needed</th>
              </thead>
              <tbody>
                @foreach($companies as $company)
                  <tr>
                    <td>{{ $company->company_auto_id ?? '' }}</td>
                    <td>{{ $company->contact_person_name ?? '' }}</td>
                    <td>{{ $company->contact_person_mobile ?? '' }}</td>
                    <td>{{ $company->email ?? '' }}</td>
                    <td>{{ $company->number_of_parking ?? '' }}</td>
                    <td>{{ $company->country ?? '' }}</td>
                    <td>{{ $company->city ?? '' }}</td>
                    <td>
                      <a href="{{ route('washer-company-allot.edit', [$company->id ?? '']) }}" class="btn btn-gradient-success btn-sm">Allot</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<!-- content-wrapper ends -->
<div class="modal fade washer_create" data-keyboard="false" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel">
</div>
@stop

@section('js-content')
  <script type="text/javascript">


    $(document).ready(function(){

      $("#start_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
        });
    })

    $(document).on('change', 'label.toggleStatus', function() {
      var washerId = $(this).data('washer-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('washer.status') }}",
        data: {washer_id: washerId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })
  </script>
@stop