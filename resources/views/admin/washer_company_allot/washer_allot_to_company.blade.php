@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Washer Management </h3>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i></a></li>
				<li class="breadcrumb-item"><a href="company-management.php">Allot Washer</a></li>
			</ol>
		</nav>
	</div>

	<div class="row gutter-sm">
		<div class="col-12 col-sm-3 col-md-3 form-group">
		  	<select class="form-control">
		    	<option hidden="">Sort By</option>
		    	<option value="India">a-z</option>
		    	<option value="USA">A-Z</option>
		  	</select>
		</div>

		<div class="col-12 col-sm-3 col-md-3 form-group">
			<select class="form-control">
				<option hidden="">Country</option>
				@foreach($countries as $country)
					<option value="{{ $country->id ?? '' }}">{{ $country->name ?? '' }}</option>
				@endforeach
			</select>
		</div>

		<div class="col-12 col-sm-3 col-md-3 form-group">
			<select class="form-control">
				<option hidden="">City</option>
				@foreach($states as $city)
					<option value="{{ $city->id ?? '' }}">{{ $city->name ?? '' }}</option>
				@endforeach
			</select>
		</div>
		
		<div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
		  	<button class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> Filter</button>
		  	<!-- <button class="btn btn-md btn-gradient-info" data-toggle="modal" data-target="#add-company">Add</button> -->
		</div>
	</div>

	<!-- Card Start -->
	<div class="card">
        <div class="card-body">
			<div class="row">
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
		          	<form method="post" action="{{ route('washer-company-allot.update', [$company_id ?? 0]) }}">
		          		@csrf
		          		@method('patch')
		          		<h6>Washer</h6>
		          		@foreach($washers as $washer)
				            <div class="form-check m-0">
		                        <label class="form-check-label text-muted">
		                          <input type="checkbox" 
		                          	@if(in_array($washer->id, array_column($allotedWasher, 'washer_id')))
		                          		checked="checked"
		                          	@endif
		                          	name="washers[][create_id]" class="form-check-input" value="{{ $washer->id ?? '' }}">
		                          	<i class="input-helper"></i> {{ $washer->name ?? '' }}
		                        </label>
	                      	</div>
	                    @endforeach
		                <span class="text-danger">{{ $errors->first('washers') }}</span>

	                    <input type="hidden" name="company_id" value="{{ $company_id ?? 0 }}">

			            <div class="text-center">
			              	<button type="submit" class="btn btn-gradient-success btn-md">Save & Submit</button>
			            </div>
		          	</form>
			    </div>
			</div>
        </div>
	</div>
	<!-- End Card -->
</div>
<!-- content-wrapper ends -->
@stop
