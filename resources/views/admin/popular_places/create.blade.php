@extends('layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> Add Company to Popular Places
    </h3>
  </div>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>
                <th>Company Name</th>
                <th>Country</th>
                <th>City</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($companies as $company)
                  <tr>
                    <td>{{ $company->name ?? '' }}</td>
                    <td>{{ $company->country ?? '' }}</td>
                    <td>{{ $company->city ?? '' }}</td>
                    <td>
                      <a href="{{ route('popular-places.edit', [$company->id ?? '']) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-eye"></i>View Parkings</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
