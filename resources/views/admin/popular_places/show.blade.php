@extends('layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> View Parking from selected company
    </h3>
  </div>

    <div class="row">
      <h3 class="ml-4">{{ $company->name ?? '' }}</h3>

      @if($company->companyParking->isNotEmpty())
        @foreach($company->companyParking as $key => $companyParking)
          @if(in_array($companyParking->id, 
                array_column($company->popularPlaces->toArray(), 'parking_id')))
            <div class="col-md-12 stretch-card grid-margin">
              <div class="card card-img-holder">
                <div class="card-body">
                  <div class="row">
                    <div class="col-4">
                      <h5>{{ $companyParking->name ?? '' }}</h5>
                      <br><br>
                      <h5>Parking Slots : <br> </h5>
                      <p class="text-danger">
                        {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->total_slot : 0 }} SLOT
                      </p>
                    </div>

                    <div class="col-4">
                      <h5>Location</h5>
                      <br>
                      <p>
                        {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->location : '' }}
                        {{ ($companyParking->parkingSetupSpace) ? $companyParking->parkingSetupSpace->pincode : '' }}
                      </p>
                    </div>

                    <div class="col-4">
                      @php
                        $parkingFeatures  = !empty($companyParking->parkingFeature) ? json_decode($companyParking->parkingFeature->selected_feature) : [];
                      @endphp

                      <div class="row">
                        @foreach($parkingFeatures as $parkingFeature)
                          <div class="col-md-12 mb-2">{{ $parkingFeature }}</div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif
        @endforeach
      @else

        <div class="col-md-12">
          <h2>No Parking Space Available on company</h2>
        </div>
      @endif

    </div>
</div>

@stop

@section('js-content')

  <script type="text/javascript">

    $(document).ready(function(){
      $("#start_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
        });
    })

  </script>
@stop