@extends('layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> Company Management 
    </h3>
  </div>
  <form method="get" action="{{ route('popular-places.index') }}">
    <div class="row gutter-sm">

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" class="form-control" autocomplete="off" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="From Date" id="start_date" />
      </div>

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" class="form-control" autocomplete="off" name="end_date" value="{{ $todate ?? '' }}" placeholder="To Date" id="end_date" />
      </div>

      <div class="col-12 col-sm-3 col-md-3 form-group">
        <select class="form-control" name="country_name" id="country_name">
          <option value="" data-id="">Select Country</option>
          @foreach($countries as $country)
            <option  data-id="{{ $country->id ?? '' }}" @if($countryName == $country->name) selected="selected" @endif value="{{ $country->name ?? ''}}">
              {{ $country->name ?? '' }}</option>
          @endforeach
        </select>
      </div>
      
      <div class="col-12 col-sm-3 col-md-3 form-group">
        <select class="form-control" name="state_name" id="state_name">
          <option value="">Select City</option>
          @foreach($states as $state)
            <option @if($stateName == $state->name) selected="selected" @endif value="{{ $state->name ?? ''}}">
              {{ $state->name ?? '' }}</option>
        @endforeach
        </select>
      </div>
      
      <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
        <button type="submit" class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> Filter</button>
        <a href="{{ route('popular-places.create') }}" class="btn btn-md btn-gradient-success">ADD </a>
        <!-- <a href="password-change-request.php" class="btn btn-md btn-gradient-success">Password Change Request</a> -->
      </div>
    </div>
  </form>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable-popular-space">
              <thead>
                <th>Company Id.</th>
                <th>Company Name</th>
                <th>Parkings Lots (Under the company)</th>
                <th>Country</th>
                <th>City</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($companies as $company)
                  <tr>
                    <td>{{ $company->company_auto_id ?? '' }}</td>
                    <td>{{ $company->name ?? '' }}</td>
                    <td>
                      @php $parkingLotsCount = 0; @endphp
                      @if(!empty($company->companyParking))
                        @foreach($company->companyParking as $companyParking)
                          @if(!empty($companyParking->parkingSetupSpace))
                              @php 
                                $parkingLotsCount += !empty($companyParking->parkingSetupSpace->total_slot) ? (int)($companyParking->parkingSetupSpace->total_slot) : 0
                              @endphp
                          @endif
                        @endforeach
                      @endif
                      {{ $parkingLotsCount }}
                    </td>
                    <td>{{ $company->country ?? '' }}</td>
                    <td>{{ $company->city ?? '' }}</td>
                    <td>
                      <a href="{{ route('popular-places.show', [$company->id ?? '']) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-eye"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop

@section('js-content')

  <script type="text/javascript">

      $('.datatable-popular-space').DataTable({
        dom: 'Bfrtip',
        buttons: [
         {
            extend: 'copy',
            exportOptions: {
              columns: [0,1,2,3]
            }
         },
         {
            extend: 'csv',
            exportOptions: {
              columns: [0,1,2,3]
            }
         },
         {
            extend: 'excel',  
            exportOptions: {
              columns: [0,1,2,3]
            }
         },
         {
            extend: 'print',
            exportOptions: {
              columns: [0,1,2,3]
            }
         }
        ]
      });
    $(document).ready(function(){

      $("#start_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
        });
    })

  </script>
@stop