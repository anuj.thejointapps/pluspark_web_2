@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  	<div class="page-header">
	    <h3 class="page-title">
	      	<span class="page-title-icon bg-gradient-success text-white mr-2">
	        	<i class="mdi mdi-account"></i>
	     	</span> User Management View
	    </h3>
	    <nav aria-label="breadcrumb">
	      	<ul class="breadcrumb">
		        <li class="breadcrumb-item active" aria-current="page">
		        	<a href="{{ route('users.index') }}">
			           <i class="mdi mdi-arrow-left icon-sm text-primary align-middle mr-1"></i> Back
		        	</a>
		        </li>
	      	</ul>
	    </nav>
  	</div>
  	<div class="row">
	    <div class="col-md-12">
	      	<ul class="nav nav-tabs">
		        <li class="nav-item"><a class="nav-link active" href="#Basic_Details" data-toggle="tab">Basic Details</a></li>
		        <li class="nav-item"><a class="nav-link" href="#Booking_History" data-toggle="tab">Booking History</a></li>
		        <li class="nav-item"><a class="nav-link" href="#Subscription_History" data-toggle="tab">Subscription History</a></li>
	      	</ul>
	      	<div class="tab-content">
		        <div id="Basic_Details" class="tab-pane fade show active">
		          	<!--  -->
		          	<div class="card mb-3">
			            <div class="container card-body">
			              	<div class="row">
				                <div class="col-sm-9">
				                	<h5>Personal Details</h5>
				                </div>
				                <div class="col-sm-3">
				                	<a href="{{ route('users.edit', [$user->id ?? '']) }}" class="btn btn-md btn-gradient-primary mb-3">Edit</a>
				                </div>
				                <div class="col-sm-6">
									<label>Name</label>
									<p>{{ $user->name ?? '' }}</p> 
									<label>Email</label>
									<p>{{ $user->email ?? '' }}</p> 
									<label>Mobile</label>
									<p>{{ $user->mobile ?? '' }}</p>
									<label>Gender</label>
									<p>{{ $user->gender ?? '' }}</p> 
				                </div>
				                <div class="col-sm-6">
									<label>Address</label>
									<p>{{ $user->address ?? '' }}</p>
									<label>Country</label>
									<p>{{ $user->country_name ?? '' }}</p>
									<label>City</label>
									<p>{{ $user->state_name ?? '' }}</p>
									<label>NO Shows</label>
									<p>N/A</p> 
									<label>Coins</label>
									<p>N/A</p> 
				                </div>
			              	</div>
			            </div>
		          	</div>
		          	<!--  -->
		          	<div class="card mb-3">
			            <div class="container card-body">
			              	<div class="row">
				                <div class="col-sm-12">
				                	<h5>Vehicle Details</h5>
				                </div>
				                <div class="col-sm-6">
									<label>Total Vehicles Added</label>
									<p>55</p> 
									<label>Vehicles ID</label>
									<p>#HHKJ6578870</p> 
									<label>Vehicles Color</label>
									<p>Red</p> 
									<label>Vehicles Model</label>
									<p>2021</p> 
									<label>Plate Number</label>
									<p>DL AA 1111</p> 
				                </div>
				                <div class="col-sm-6">
				                  	<label>Vehicles Type</label>
				                  	<p>Bike</p> 
				                  	<label>Vehicles Image</label>
				                  	<p>
					                    <img src="assets/images/1.jpg" class="wpx-50 hpx-50 mb-1 mr-1">
					                    <img src="assets/images/2.jpg" class="wpx-50 hpx-50 mb-1 mr-1">
					                    <img src="assets/images/3.jpg" class="wpx-50 hpx-50 mb-1 mr-1">
				                  	</p>
				                </div>
			              	</div>
			            </div>
		          	</div>

		          	<!--  -->
		          	<div class="card mb-3">
			            <div class="container card-body">
			              	<div class="row">
				                <div class="col-sm-12">
				                	<h5>User Stats</h5>
				                </div>
				                <div class="col-sm-6">
									<label>Total Appointments Made</label>
									<p>5</p> 
									<label>Total Appointments Completed</label>
									<p>3</p> 
									<label>Total Noshows(Lifetime)</label>
									<p>2</p> 
									<label>Total Cancellations</label>
									<p>2</p> 
				                </div>
				                <div class="col-sm-6">
									<label>Preffered Business</label>
									<p>Data</p> 
									<label>Preffered Service</label>
									<p>Dara</p>
									<label>Preffered Service Provider</label>
									<p>Data</p>
									<label>Total Points Redeemed</label>
									<p>Data</p>
				                </div>
			              	</div>
			            </div>
		          	</div>
		          	<!--  -->
		        </div>

		        <div id="Booking_History" class="tab-pane fade">
		          	<div class="card">
			            <div class="card-body">
			              	<div class="table-responsive">
				                <table class="table table-striped datatable">
				                  	<thead>
					                    <tr>
											<th>Booking ID</th>
											<th>Date</th>
											<th>Parking</th>
											<th>Status</th>
					                    </tr>
				                  	</thead>
				                  	<tbody>
					                    <tr>
											<td>ABS1233</td>
											<td>23 Jan 2021</td>
											<td>Location</td>
											<td><span class="text-success">Completed</span></td>
					                    </tr>
					                    <tr>
											<td>ABS1233</td>
											<td>23 Jan 2021</td>
											<td>Location</td>
											<td><span class="text-danger">Completed</span></td>
					                    </tr>
				                  	</tbody>
				                </table>
			              	</div>
			            </div>
		          	</div>
		        </div>
		        <div id="Subscription_History" class="tab-pane fade">
		          	<div class="card">
			            <div class="card-body">
			              	<div class="table-responsive">
				                <table class="table table-striped datatable">
				                  	<thead>
					                    <tr>
											<th>Booking ID</th>
											<th>Date</th>
											<th>Parking</th>
											<th>Status</th>
					                    </tr>
				                  	</thead>
				                  	<tbody>
					                    <tr>
											<td>ABS1233</td>
											<td>23 Jan 2021</td>
											<td>Location</td>
											<td><span class="text-success">Completed</span></td>
					                    </tr>
					                    <tr>
											<td>ABS1233</td>
											<td>23 Jan 2021</td>
											<td>Location</td>
											<td><span class="text-danger">Completed</span></td>
					                    </tr>
				                  	</tbody>
				                </table>
			              	</div>
			            </div>
		          	</div>
		        </div>
	      	</div>
	    </div>
  	</div>
</div>
<!-- content-wrapper ends -->
@stop