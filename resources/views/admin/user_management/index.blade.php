@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  	<div class="page-header">
	    <h3 class="page-title">
	      	<span class="page-title-icon bg-gradient-primary text-white mr-2">
	        	<i class="mdi mdi-account"></i>
	      	</span> User Management 
	    </h3>
	    <!-- <nav aria-label="breadcrumb">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item active" aria-current="page">
	          <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
	        </li>
	      </ul>
	    </nav> -->
  	</div>

  	<form method="get" action="{{ route('users.index') }}">
	  	<div class="row gutter-sm">
		    <div class="col-12 col-sm-3 col-md-3 form-group">
		      	<select class="form-control" name="country_name" id="country_name">
			        <option value="" data-id="">Select Country</option>
			        @foreach($countries as $country)
				        <option  data-id="{{ $country->id ?? '' }}" @if($countryName == $country->name) selected="selected" @endif value="{{ $country->name ?? ''}}">
				        	{{ $country->name ?? '' }}</option>
				    @endforeach
		      	</select>
		    </div>
		    <div class="col-12 col-sm-3 col-md-3 form-group">
		      	<select class="form-control" name="state_name" id="state_name">
			        <option value="">Select City</option>
			        @foreach($states as $state)
				        <option @if($stateName == $state->name) selected="selected" @endif value="{{ $state->name ?? ''}}">
				        	{{ $state->name ?? '' }}</option>
				    @endforeach
		      	</select>
		    </div>
		    <div class="col-12 col-sm-3 col-md-3 form-group">
		      	<input type="text" autocomplete="off" class="form-control" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="From Date" id="start_date" />
		    </div>
		    <div class="col-12 col-sm-3 col-md-3 form-group">
		      	<input type="text" autocomplete="off" class="form-control" name="end_date" value="{{ $todate ?? '' }}" placeholder="To Date" id="end_date" />
		    </div>
		    <div class="col-12 col-sm-3 col-md-3 form-group">
		      	<select class="form-control" name="month">
		      		<option value="">Select Month</option>
			        <option value="1" @if($month == '1') selected="selected" @endif>1 Month</option>
			        <option value="3" @if($month == '3') selected="selected" @endif>3 Months</option>
			        <option value="6" @if($month == '6') selected="selected" @endif>6 Months</option>
		      	</select>
		    </div>
		    <div class="col-12 col-sm-9 col-md-9 form-group">
		      	<button class="btn btn-md btn-gradient-primary">Search</button>
		      	<!-- <button class="btn btn-md btn-gradient-primary" name="export" value="Export"><i class="mdi mdi-file-excel"></i>Export</button> -->
		      	<!-- <button class="btn btn-md btn-gradient-info">Add New</button> -->
		    </div>
	  	</div>
  	</form>
  	<div class="row">
	    <div class="col-md-12 stretch-card grid-margin">
	      	<div class="card card-img-holder">
		        <div class="card-body">
		          	<div class="table-responsive">
			            <table class="table table-striped datatable-user">
			              	<thead>
				                <th>User Id.</th>
				                <th>User Name</th>
				                <th>Mobile</th>
				                <th>Email</th>
				                <th>Joining Date</th>
				                <th>Country</th>
				                <th>City</th>
				                <th>Action</th>
			              	</thead>
			              	<tbody>
			              		@if(!empty($users))
			              		@foreach($users as $user)
					                <tr>
					                  <td>#{{ $user->id ?? '' }}</td>
					                  <td>{{ $user->username ?? '' }}</td>
					                  <td>{{ $user->mobile ?? '' }}</td>
					                  <td>{{ $user->email ?? '' }}</td>
					                  <td>{{ $user->created_at ? date('Y-m-d', strtotime($user->created_at)) : '' }}</td>
					                  <td>{{ $user->country_name ?? 'N/A' }}</td>
					                  <td>{{ $user->state_name ?? 'N/A' }}</td>
					                  <td>
					                    <a href="{{ route('users.show', [$user->id ?? 0]) }}" class="btn btn-gradient-info btn-sm">
					                    	<i class="mdi mdi-eye"></i>
					                    </a>
					                    <label class="togglebox toggleStatus ml-1" data-user-id="{{ $user->id ?? ''}}">
					                      <input type="checkbox" @if($user->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
					                      <span class="label-text"></span>
					                    </label>
					                  </td>
					                </tr>
				                @endforeach
				                @endif
			              	</tbody>
			            </table>
			            {{ $users->links() }}
		          	</div>
		        </div>
	      	</div>
	    </div>
  	</div>
</div>
<!-- content-wrapper ends -->
@stop

@if (Session::has('message_success'))
	@section('js-content')
	  	<script>
	    	swal("{{ session('message_success') }}");
	  	</script>
	@stop
@endif

@section('js-content')

	<script type="text/javascript">

	    $('.datatable-user').DataTable({
	      dom: 'Bfrtip',
	      buttons: [
	       {
	          extend: 'copy',
	          exportOptions: {
	            columns: [0,1,2,3,4,5,6]
	          }
	       },
	       {
	          extend: 'csv',
	          exportOptions: {
	            columns: [0,1,2,3,4,5,6]
	          }
	       },
	       {
	          extend: 'excel',	
	          exportOptions: {
	            columns: [0,1,2,3,4,5,6]
	          }
	       },
	       {
	          extend: 'print',
	          exportOptions: {
	            columns: [0,1,2,3,4,5,6]
	          }
	       }
	      ]
	    });
		
		$(document).ready(function(){

			$("#start_date").datepicker({
			    orientation: 'bottom',
			    autoclose: true
			}).on('changeDate', function(selected) {
			    var startDate = new Date(selected.date.valueOf());
			    $('#end_date').datepicker('setStartDate', startDate);
			});
			  
			$("#end_date").datepicker({
			    orientation: 'bottom',
			    autoclose: true
			}).on('changeDate', function(selected) {
			    var startDate = new Date(selected.date.valueOf());
			    $('#start_date').datepicker('setEndDate', startDate);
		  	});
		})

		$(document).on('change', 'label.toggleStatus', function() {
			var userId = $(this).data('user-id')

			$.ajax({
				method: 'POST',
				url: "{{ route('users.status') }}",
				data: {userId: userId},
				dataType: 'json',
				success: function(response) {
					if (response.status == 200) {
						swal(response.message_success)
					}
				}
			})
		})
	
	</script>

@stop