@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  	<div class="page-header">
	    <h3 class="page-title">
	      	<span class="page-title-icon bg-gradient-success text-white mr-2">
	        	<i class="mdi mdi-account"></i>
	      	</span> User Management Edit
	    </h3>
	    <nav aria-label="breadcrumb">
	      	<ul class="breadcrumb">
		        <li class="breadcrumb-item active" aria-current="page">
		        	<a href="{{ route('users.show', [$user->id ?? '']) }}">
			           <i class="mdi mdi-arrow-left icon-sm text-primary align-middle mr-1"></i> Back
		        	</a>
		        </li>
	      	</ul>
	    </nav>
  	</div>
  	<form method="post" action="{{ route('users.update', [$user->id ?? '' ]) }}">
  		@csrf
  		@method('patch')
	  	<div class="row">
		    <div class="col-md-12">
		      	<!--  -->
		      	<div class="card mb-3">
			        <div class="container card-body">
			          	<div class="row">
				            <div class="col-sm-12">
				            	<h5>Personal Details</h5>
				            </div>
				            <div class="col-sm-6">
				              	<div class="form-group">
					                <label>Name</label>
					                <input type="text" value="{{ $user->name ?? '' }}" name="name" class="form-control" />
	                                <span class="text-danger">{{ $errors->first('name') }}</span>
				              	</div>
				              	<div class="form-group">
					                <label>Email</label>
					                <input type="email" value="{{ $user->email ?? '' }}" disabled="disabled" class="form-control" /> 
				              	</div>
				              	<div class="form-group">
					                <label>Mobile</label>
					                <input type="tel" value="{{ $user->mobile ?? '' }}" disabled="disabled" class="form-control" /> 
								</div>
								<div class="form-group">
					                <label>Gender</label>
					                <div>
					                  	<label class="radiobox mr-3">
						                    <input type="radio" hidden="" value="Male" @if($user->gender == "Male") checked="checked" @endif name="gender" />
						                    <span class="label-text"></span>Male
					                  	</label>
					                  	<label class="radiobox mr-3">
						                    <input type="radio" hidden="" @if($user->gender == "Female") checked="checked" @endif value="Female" name="gender" />
						                    <span class="label-text"></span>Female
					                  	</label>
					                </div>
				              	</div>
				            </div>
				            <div class="col-sm-6">
								<label>Address</label>
								<input type="text" value="{{ $user->address ?? '' }}" name="address" class="form-control" /> 
								<label>Country</label>
								<input type="text" value="{{ $user->country_name ?? '' }}" name="country_name" placeholder="Country Name" class="form-control" /> 
								<label>City</label>
								<input type="text" value="{{ $user->state_name ?? '' }}" name="state_name" placeholder="State Name" class="form-control" /> 
								<label>NO Shows</label>
								<input type="number" value="2" class="form-control" /> 
								<label>Coins</label>
								<input type="number" value="2" class="form-control" /> 
				            </div>
			          	</div>
			        </div>
		      	</div>
			    <!--  -->
			    <div class="card mb-3">
			        <div class="container card-body">
			          	<div class="row">
				            <div class="col-sm-12">
				            	<h5>Vehicle Details</h5>
				            </div>
				            <div class="col-sm-6">
				              	<div class="form-group">
					                <label>Total Vehicles Added</label>
					                <input type="number" class="form-control" value="55">
				              	</div>
				              	<div class="form-group">
					                <label>Vehicles ID</label>
					                <input type="text" class="form-control" value="HHKJ6578870">
				              	</div>
				              	<div class="form-group">
					                <label>Vehicles Color</label>
					                <input type="text" class="form-control" value="Red">
				              	</div>
				              	<div class="form-group">
					                <label>Vehicles Model</label>
					                <input type="text" class="form-control" value="2021">
				              	</div>
				              	<div class="form-group">
					                <label>Plate Number</label>
					                <input type="text" class="form-control" value="DL AA 1111">
				              	</div>
				            </div>
			            	<div class="col-sm-6">
				              	<label>Vehicles Type</label>
				              	<p>Bike</p> 
				              	<label>Vehicles Image</label>
								<p>
									<label class=""></label>
									<img src="assets/images/1.jpg" class="wpx-50 hpx-50 mb-1 mr-1">
									<img src="assets/images/2.jpg" class="wpx-50 hpx-50 mb-1 mr-1">
									<img src="assets/images/3.jpg" class="wpx-50 hpx-50 mb-1 mr-1">
								</p>
			            	</div>
			          	</div>
			        </div>
			    </div>
		      	<!--  -->
		      	<div class="card mb-3">
			        <div class="container card-body">
			          	<div class="row">
			            	<div class="col-sm-12">
			            		<h5>User Stats</h5>
			            	</div>
				            <div class="col-sm-6">
				              	<div class="form-group">
					                <label>Total Appointments Made</label>
					                <input type="number" value="5" class="form-control"/>
				              	</div>
				              	<div class="form-group">
					                <label>Total Appointments Completed</label>
					                <input type="number" value="3" class="form-control"/>
				              	</div>
				              	<div class="form-group">
					                <label>Total Noshows(Lifetime)</label>
					                <input type="number" value="2" class="form-control"/>
				              	</div>
				              	<div class="form-group">
					                <label>Total Cancellations</label>
					                <input type="number" value="2" class="form-control"/>
				              	</div>
				            </div>
				            <div class="col-sm-6">
				              	<div class="form-group">
					                <label>Preffered Business</label>
					                <input type="text" value="Data" class="form-control"/>
				              	</div>
				              	<div class="form-group">
					                <label>Preffered Service</label>
					                <input type="text" value="Dara" class="form-control"/>
				              	</div>
				              	<div class="form-group">
					                <label>Preffered Service Provider</label>
					                <input type="text" value="Data" class="form-control"/>
				              	</div>
				              	<div class="form-group">
					                <label>Total Points Redeemed</label>
					                <input type="number" value="2" class="form-control"/>
				              	</div>
				              	<div class="form-group">
				              		<input type="submit" class="btn btn-md btn-gradient-primary mb-3" value="Update User">
				              	</div>
				            </div>
			          	</div>
			        </div>
		      	</div>
		      	<!--  -->
		    </div>
	  	</div>
  	</form>
</div>
<!-- content-wrapper ends -->
@stop

@if(Session::has('message_success'))
	@section('js-content')
	  	<script>
	    	swal("{{ session('message_success') }}");
	  	</script>
	@stop
@endif