<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form method="post" action="{{ route('washer.update', [$washer->id ?? '']) }}" enctype="multipart/form-data" id="washer_edit" name="washer_edit">
        @csrf
        @method('patch')
        <h5>Edit Basic Details</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" name="washer_auto_id" readonly="readonly" class="form-control" placeholder="washer ID" value="{{ $washer->washer_auto_id ?? '' }}" />
            </div>

            <div class="form-group">
              <input type="email" readonly="readonly" name="email" class="form-control" placeholder="Email" value="{{ $washer->email ?? '' }}" />
            </div>

            <div class="form-group">
              <input type="text" name="city_name" class="form-control" placeholder="City" value="{{ $washer->city ?? '' }}"/>
            </div>

            <div class="form-group">
              <input type="text" name="gender" class="form-control" placeholder="Gender" value="{{ $washer->gender ?? '' }}"/>
            </div>
          </div>
          
          <div class="col-md-6">

            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Washer Name" value="{{ $washer->name ?? '' }}"/>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <input type="number" name="country_code" class="form-control" placeholder="Mobile" value="{{ $washer->country_code ?? '' }}"/>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" onkeypress="return numbersonly(event);" maxlength="10" readonly="readonly" name="mobile" class="form-control" placeholder="Mobile" value="{{ $washer->mobile ?? '' }}"/>
                </div>
              </div>
            </div>

            <div class="form-group">

            </div>

            <div class="form-group">
              <input type="text" name="country_name" class="form-control" placeholder="Country" value="{{ $washer->country ?? '' }}"/>
            </div>

            <div class="form-group">
              <textarea rows="3" name="address" class="form-control" placeholder="Address">{{ $washer->address ?? '' }}</textarea>
            </div>

            <div class="form-group">
              <input type="file" name="washer_image">
              <img src="{{ asset($washer->image_path ?? '') }}" class="wpx-50 hpx-50 mb-1 mr-1">
            </div>
          </div>
        </div>

        <div class="text-center">
          <button type="button" class="btn btn-gradient-success btn-md float-right ml-3" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-gradient-success btn-md float-right washer_edit_submit">Save & Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $("form[name='washer_edit']").validate({
      rules: {
        name: "required",
        country_code: "required",
        email: "required",
        mobile: {
          required: true,
          minlength: 10
        }
      },
      messages: {
        name: "Please Enter Name",
        country_code: "Please Enter Country Code",
        email: "Please Enter Email",
        mobile: {
          required: "Please enter your Mobile Number",
          minlength: "Your mobile must be at least 10 characters long"
        }
      }
    });
  });
</script>