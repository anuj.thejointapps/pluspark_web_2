@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  	<div class="page-header">
	    <h3 class="page-title">
	      	<span class="page-title-icon bg-gradient-success text-white mr-2">
	        	<i class="mdi mdi-account"></i>
	      	</span> Washer Management <i class="mdi mdi-chevron-right"></i> View Washer
	    </h3>
  	</div>

  	<div class="row">
	    <div class="col-md-12">
	      	<div class="form-group text-center">
	        	<div class="avatar-pic mx-auto mb-3 gal-result" style="background-image: url({{ asset($washer->image_path) }});">
	        		<!-- <img src="{{ asset($washer->image_path) }}"> -->
	        	</div>
	        	<h6>Washer ID: {{ $washer->washer_auto_id ?? '' }}</h6>
	        	<p>D.O.J. : {{ $washer->date_of_join ?? '' }}</p>
	      	</div>
	      	<ul class="nav nav-tabs">
	        	<li class="nav-item"><a class="nav-link active" href="#Basic_Details" data-toggle="tab">Basic Details</a></li>
	        	<li class="nav-item"><a class="nav-link" href="#Washer_History" data-toggle="tab">Washer History</a></li>
	      	</ul>
	      	
	      	<div class="tab-content">
		        <div id="Basic_Details" class="tab-pane fade show active">
			        <!-- Basic Details Tab -->
			        <div class="card mb-3">
			            <div class="container card-body">
			              <div class="row">
			                <div class="col-sm-9"><h5>Basic Details</h5></div>
			                <div class="col-sm-3">
								<button type="button" class="btn btn-md btn-gradient-success btn-modal" data-href="{{ route('washer.edit', [$washer->id ?? '' ]) }}" 
								data-container=".washer_edit">Edit </button>
			                </div>
			                <div class="col-sm-6">
			                  <label>Name</label>
			                  <p>{{ $washer->name ?? '' }}</p> 
			                  <label>Mobile</label>
			                  <p>+{{ $washer->country_code ?? '' }}-{{ $washer->mobile ?? '' }}</p> 
			                  <label>Email</label>
			                  <p>{{ $washer->email ?? '' }}</p>
			                  <label>Gender</label>
			                  <p>{{ $washer->gender ?? '' }}</p>
			                </div>
			                <div class="col-sm-6"> 
			                  <label>Country</label>
			                  <p>{{ $washer->country ?? '' }}</p> 
			                  <label>City</label>
			                  <p>{{ $washer->city ?? '' }}</p> 
			                  <label>Address</label>
			                  <p>{{ $washer->address ?? '' }}</p> 
			                  <label>Alloted To Company</label>
			                  <p>Data</p>
			                </div>
			              </div>
			            </div>
			        </div>
			        <!-- Basic Details Tab -->
		        </div>

		        <div id="Washer_History" class="tab-pane fade">
					<div class="card">
			            <div class="card-body">
			              <div class="row gutter-sm">
			                <div class="col-12 col-sm-12 col-md-12"><h6>Washer Stats</h6></div>
			                <div class="col-12 col-sm-4 col-md-4">
			                  <label>Total Jobs Given</label>
			                  <p>20</p>
			                </div>
			                <div class="col-12 col-sm-4 col-md-4">
			                  <label>Total Jobs Completed</label>
			                  <p>20</p>
			                </div>
			                <div class="col-12 col-sm-4 col-md-4">
			                  <label>Admin Commission</label>
			                  <p>10%</p>
			                </div>
			                <div class="col-12 col-sm-3 col-md-3 form-group">
			                  <select class="form-control">
			                    <option hidden="">Filter</option>
			                    <option value="Not Started">Not Started</option>
			                    <option value="Started">Started</option>
			                    <option value="Completed">Completed</option>
			                    <option value="Cancelled">Cancelled</option>
			                  </select>
			                </div>
			                <div class="col-12 col-sm-3 col-md-3 form-group">
			                  <input type="text" class="form-control" placeholder="From Date" id="start_date" />
			                </div>
			                <div class="col-12 col-sm-3 col-md-3 form-group">
			                  <input type="text" class="form-control" placeholder="To Date" id="end_date" />
			                </div>
			                <div class="col-12 col-sm-3 col-md-3 form-group">
			                  <select class="form-control">
			                    <option hidden="">Country</option>
			                    <option value="India">India</option>
			                    <option value="USA">USA</option>
			                  </select>
			                </div>
			                <div class="col-12 col-sm-3 col-md-3 form-group">
			                  <select class="form-control">
			                    <option hidden="">City</option>
			                    <option value="Delhi">Delhi</option>
			                    <option value="Mumbai">Mumbai</option>
			                  </select>
			                </div>
			                <div class="col-12 col-sm-3 col-md-3 form-group">
			                  <select class="form-control">
			                    <option value="" hidden="">Duration</option>
			                    <option value="1 Month">1 Month</option>
			                    <option value="3 Months">3 Months</option>
			                    <option value="6 MOnths">6 MOnths</option>
			                  </select>
			                </div>
			                <div class="col-12 col-sm-6 col-md-6 form-group">
			                  <button class="btn btn-md btn-gradient-primary"><i class="mdi mdi-file-excel"></i> Export</button>
			                  <!-- <button class="btn btn-md btn-gradient-info">Add New</button> -->
			                </div>
			                <div class="col-12 col-sm-12">
			                  <div class="table-responsive">
			                    <table class="table table-striped datatable">
			                      <thead>
			                        <tr>
			                          <th>Date</th>
			                          <th>Model</th>
			                          <th>Number Plate</th>
			                          <th>Time</th>
			                          <th class="wpx-80">Status</th>
			                        </tr>
			                      </thead>
			                      <tbody>
			                        <tr>
			                          <td>21/2/2021</td>
			                          <td>Volvo</td>
			                          <td>ASD1234</td>
			                          <td>10:00AM</td>
			                          <td><span class="text-danger">Cancelled</span></td>
			                        </tr>
			                        <tr>
			                          <td>21/2/2021</td>
			                          <td>Volvo</td>
			                          <td>ASD1234</td>
			                          <td>10:00AM</td>
			                          <td><span class="text-success">Completed</span></td>
			                        </tr>
			                        <tr>
			                          <td>21/2/2021</td>
			                          <td>Volvo</td>
			                          <td>ASD1234</td>
			                          <td>10:00AM</td>
			                          <td><span class="text-info">Started</span></td>
			                        </tr>
			                        <tr>
			                          <td>21/2/2021</td>
			                          <td>Volvo</td>
			                          <td>ASD1234</td>
			                          <td>10:00AM</td>
			                          <td><span class="text-dark">Not Started</span></td>
			                        </tr>
			                      </tbody>
			                    </table>
			                  </div>
			                </div>
			              </div>
			            </div>
					</div>
		        </div>
	      	</div>
	    </div>
  	</div>
</div>
<div class="modal fade washer_edit" data-keyboard="false" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel" data-backdrop="static" data-keyboard="false">
</div>
<!-- content-wrapper ends -->
@stop

@section('js-content')
	<script type="text/javascript">

	    $(document).on('submit', 'form#washer_edit', function(e) {
	      	e.preventDefault();
	      	var data = new FormData(this);
		    $('.washer_edit_submit').attr('disabled','disabled');

	      	$.ajax({
	          cache:false,
	          contentType: false,
	          processData: false,
	          url: $(this).attr("action"),
	          method: $(this).attr("method"),
	          dataType: "json",
	          data: data,
	          success: function(response) {
	            $('.washer_edit_submit').removeAttr('disabled','disabled');
	            if (response.status == 200) {
	                swal(response.success_message);
	                $('div.washer_edit').modal('hide');
	                location.reload(true);
	            }
	          }
	      	}); 
	    });
	</script>
@stop