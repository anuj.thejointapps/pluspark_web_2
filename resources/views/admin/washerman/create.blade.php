<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form method="post" action="{{ route('washer.store') }}" enctype="multipart/form-data" id="washer_create" name="washer_create">
        @csrf
        <h5>Add Basic Details</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" name="washer_auto_id" readonly="readonly" class="form-control" placeholder="washer ID" value="{{ $washerSequence ?? '' }}" />
            </div>

            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="Email"/>
            </div>

            <div class="form-group">
              <input type="text" name="city_name" class="form-control" placeholder="City"/>
            </div>

            <div class="form-group">
              <input type="text" name="gender" class="form-control" placeholder="Gender"/>
            </div>
          </div>
          
          <div class="col-md-6">

            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Washer Name"/>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <input type="number" name="country_code" class="form-control" placeholder="Country code"/>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" onkeypress="return numbersonly(event);" maxlength="10"  name="mobile" class="form-control" placeholder="Mobile"/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <input type="text" name="country_name" class="form-control" placeholder="Country"/>
            </div>

            <div class="form-group">
              <textarea rows="3" name="address" class="form-control" placeholder="Address"></textarea>
            </div>

            <div class="form-group">
              <input type="file" name="washer_image">
            </div>
          </div>
        </div>

        <div class="text-center">
          <button type="button" class="btn btn-gradient-success btn-md float-right ml-3" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-gradient-success btn-md float-right washer_create_submit">Save & Add</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $("form[name='washer_create']").validate({
      rules: {
        name: "required",
        country_code: "required",
        mobile: {
          required: true,
          minlength: 10,
          remote: {
              url: "{{ route('washer.mobile-check') }}",
              type: "post"
           }
        },
        email: {
          required: true,
          minlength: 5,
          remote: {
              url: "{{ route('washer.email-check') }}",
              type: "post"
           }
        }
      },
      messages: {
        name: "Please Enter Name",
        country_code: "Please Enter Country Code",
        mobile: {
          required: "Please enter your Mobile Number",
          minlength: "Your mobile must be at least 10 characters long",
          remote : 'Mobile Number already in record'
        },
        email: {
          required: "Email is required",
          minlength: "Your Email should be at atleast 5 character",
          remote: "Email already in record"
        }
      }
    });
  });
</script>