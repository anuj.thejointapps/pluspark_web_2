@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <!-- <h3 class="page-title"> Brand </h3> -->
    

    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> Colors 
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">Colors</li>
        </ol>
    </nav>
  </div>
  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <a href="{{ route('colors.create') }}" class="btn float-left btn-sm mr-1 btn-gradient-primary mb-3">Add Color</a>
            <table class="table table-bordered datatable-color">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($colors as $color)
                <tr>
                  <td>{{ $color->name ?? '' }}</td>
                  <td>
                    <a href="{{ route('colors.edit', [$color->id]) }}" class="btn btn-sm btn-gradient-primary mb-3">Edit</a>
                    <label class="togglebox toggleStatus ml-1" data-color-id="{{ $color->id ?? ''}}">
                      <input type="checkbox" @if($color->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                      <span class="label-text"></span>
                    </label>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('js-content')

  <script type="text/javascript">
    
    $('.datatable-color').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'copy',
          exportOptions: {
            columns: [0]
          }
       },
       {
          extend: 'csv',
          exportOptions: {
            columns: [0]
          }
       },
       {
          extend: 'excel',  
          exportOptions: {
            columns: [0]
          }
       },
       {
          extend: 'print',
          exportOptions: {
            columns: [0]
          }
       }
      ]
    });
    $(document).on('change', 'label.toggleStatus', function() {
      var colorId = $(this).data('color-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('colors.status') }}",
        data: {color_id: colorId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })
  </script>
@stop