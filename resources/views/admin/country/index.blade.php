
@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <!-- <h3 class="page-title"> Country </h3> -->
    

    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> {{__('country.countries')}} 
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('country.country')}} </li>
        </ol>
    </nav>
  </div>
  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <a href="{{ route('countries.create') }}" class="btn btn-sm float-left mr-1 btn-gradient-primary mb-3">{{__('country.add_country')}} </a>
            <table class="table table-bordered datatable-country">
              <thead>
                <tr>
                  <th>{{__('country.name')}} </th>
                  <th>{{__('country.country_code')}} </th>
                  <th>{{__('country.cities')}} </th>
                  <th class="wpx-90">{{__('country.action')}} </th>
                </tr>
              </thead>
              <tbody>
                @foreach($countries as $country)
                <tr>
                  <td>{{ $country->name ?? '' }}</td>
                  <td>{{ $country->country_code ?? '' }}</td>
                  <td>
                    @if(!empty($country->states))
                      @foreach($country->states as $city)
                          {{ $city->name ?? '' }},
                      @endforeach
                    @endif
                  </td>
                  <td>
                    <a href="{{ route('countries.edit', [$country->id]) }}" class="btn btn-sm btn-gradient-primary">{{__('country.edit')}} </a>
                    <label class="togglebox toggleStatus ml-1" data-country-id="{{ $country->id ?? ''}}">
                      <input type="checkbox" @if($country->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                      <span class="label-text"></span>
                    </label>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('js-content')

  <script type="text/javascript">
    
    $('.datatable-country').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'copy',
          text:'{{ __('datatablebuttons.copy') }}',
          exportOptions: {
            columns: [0,1,2]
          }
       },
       {
          extend: 'csv',
          text:'{{ __('datatablebuttons.csv') }}',
          exportOptions: {
            columns: [0,1,2]
          }
       },
       {
          extend: 'excel',  
          text:'{{ __('datatablebuttons.excel') }}',
          exportOptions: {
            columns: [0,1,2]
          }
       },
       {
          extend: 'print',
          text:'{{ __('datatablebuttons.print') }}',
          exportOptions: {
            columns: [0,1,2]
          }
       }
      ]
    });

    $(document).on('change', 'label.toggleStatus', function() {
      var countryId = $(this).data('country-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('countries.status') }}",
        data: {country_id: countryId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })
  </script>
@stop