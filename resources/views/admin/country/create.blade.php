@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <!-- <h3 class="page-title"> Country Create </h3> -->
    <!-- <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="dashboard.php"><i class="mdi mdi-home"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">Country Create</li>
        </ol>
    </nav>
 -->

    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> {{__('country.country')}} {{__('country.create')}} 
    </h3>
    <nav aria-label="breadcrumb">
        <ul class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ route('countries.index') }}">
               <i class="mdi mdi-arrow-left icon-sm text-primary align-middle mr-1"></i> {{__('basicbuttons.back')}}
            </a>
          </li>
        </ul>
    </nav>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
            

          <form class="cmxform" id="signupForm" method="post" action="{{ route('countries.store') }}">
            @csrf
            <fieldset>
              <div class="form-group">
                <label for="username">{{__('country.name')}}</label>
                <input id="username" class="form-control" placeholder="{{__('country.name')}}" name="name" type="text">
                <span class="text-danger">{{ $errors->first('name') }}</span>
              </div>

              <div class="form-group">
                <label for="username">{{__('country.country_code')}}</label>
                <input id="username" class="form-control" placeholder="{{__('country.country_code')}}" name="country_code" type="text">
                <span class="text-danger">{{ $errors->first('country_code') }}</span>
              </div>
              <button type="submit" class="btn btn-primary">{{__('country.create')}}</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
