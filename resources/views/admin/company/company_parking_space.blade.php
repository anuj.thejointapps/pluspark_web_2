@extends('layouts.main-layout')
@section('content')
  <!-- content-wrapper start -->
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title">
        <span class="page-title-icon bg-gradient-success text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> Company Management <i class="mdi mdi-chevron-right"></i> Parking Space View
      </h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="tab-content">
          <!-- Company Details Tab -->
          <!-- <div id="Company_Details" class="tab-pane fade show active"> -->
            <div class="card mb-3">
              <div class="container card-body">
                <div class="row">
                  <div class="col-sm-9"><h5>Company Parking Setup Space</h5></div>
                  <div class="col-sm-6">
                    <label>Space Headline</label>
                    <p>{{ !empty($parking->parkingSetupSpace) ? $parking->parkingSetupSpace->space_headline : '' }}</p> 
                    <label>Space Type</label>
                    <p>{{ !empty($parking->parkingSetupSpace) ? (!empty($parking->parkingSetupSpace->spaceType) ? $parking->parkingSetupSpace->spaceType->name : '') : '' }}</p> 
                    <label>Pass Type</label>
                    <p>{{ !empty($parking->parkingSetupSpace) ? $parking->parkingSetupSpace->pass_types : '' }}</p> 
                    <label>Location</label>
                    <p>{{ !empty($parking->parkingSetupSpace) ? $parking->parkingSetupSpace->location : '' }}</p> 
                    <label>Pincode</label>
                    <p>{{ !empty($parking->parkingSetupSpace) ? $parking->parkingSetupSpace->pincode : '' }}</p>
                  </div>

                  <div class="col-sm-6">  
                    <label>Valet Service Available</label>
                    <p>{{ !empty($parking->parkingSetupSpace) ? $parking->parkingSetupSpace->valet_services_available : '' }}</p> 
                    <label>Washer Service Available</label>
                    <p>{{ !empty($parking->parkingSetupSpace) ? $parking->parkingSetupSpace->washer_services_available : '' }}</p> 
                    <label>Parking Image</label>
                    <p>
                      @foreach($parking->parkingSetupSpace->images as $image)
                        <img src="{{ asset($image->image_path) }}" width="75px" height="75px">
                      @endforeach
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div class="card mb-3">
              <div class="container card-body">
                <div class="row">
                  <div class="col-sm-9"><h5>Company Parking Space Description</h5></div>
                  <div class="col-sm-6">
                    <label>Space Description</label>
                    <p>{{ !empty($parking->parkingDescription) ? $parking->parkingDescription->description : '' }}</p> 
                    <label>Ideal For People Who</label>
                    <p>{{ !empty($parking->parkingDescription) ? $parking->parkingDescription->idle_page_for_people : '' }}</p>
                    <label>Select Vechile Type</label>
                    @php
                      $select_vehicle_type = !empty($parking->parkingDescription) ? $parking->parkingDescription->select_vehicle_type : '';
                      $select_space_features = !empty($parking->parkingDescription) ? json_decode($parking->parkingDescription->select_space_features) : [];
                    @endphp
                    <p>
                      @if($select_vehicle_type == 'airplane')
                        Airplane
                      @elseif($select_vehicle_type == 'truck')
                        Truck
                      @elseif($select_vehicle_type == 'car')
                        Car
                      @elseif($select_vehicle_type == 'bulb')
                        Bulb
                      @elseif($select_vehicle_type == 'bike')
                        Bike
                      @elseif($select_vehicle_type == 'bus')
                        Bus
                      @endif
                    </p>
                  </div>

                  <div class="col-sm-6">
                    <label>Select Space Features</label>
                    <p>
                      @foreach($select_space_features as $select_space_features_value)
                          <b>{{ $loop->iteration }}</b> .
                          @if($select_space_features_value == '24_hour')
                            24 X 7 <br>
                          @elseif($select_space_features_value == 'disabled')
                            Disabled <br>
                          @elseif($select_space_features_value == 'large')
                            Larger <br>
                          @elseif($select_space_features_value == 'driver')
                            Driver <br>
                          @elseif($select_space_features_value == 'street')
                            Street <br>
                          @elseif($select_space_features_value == 'covered')
                            Covered <br>
                          @elseif($select_space_features_value == 'drive_way')
                            Drive Way <br>
                          @elseif($select_space_features_value == 'washer')
                            Washer <br>
                          @elseif($select_space_features_value == 'guard')
                            Guard <br>
                          @elseif($select_space_features_value == 'lock_up')
                            Lock up <br>
                          @elseif($select_space_features_value == 'gate')
                            Gate <br>
                          @elseif($select_space_features_value == 'time_extendable')
                            Time Extendable <br>
                          @elseif($select_space_features_value == 'yard')
                            Yard <br>
                          @elseif($select_space_features_value == 'cctv')
                            CCTV <br>
                          @elseif($select_space_features_value == 'field')
                            Field <br>
                          @endif
                      @endforeach
                    </p> 
                  </div>
                </div>
              </div>
            </div>

            <div class="card mb-3">
              <div class="container card-body">
                <div class="row">
                  <div class="col-sm-9"><h5>Company Parking Space Features</h5></div>
                  @php
                    $selected_features = !empty($parking->parkingFeature) ? json_decode($parking->parkingFeature->selected_feature) : [];
                  @endphp
                  <div class="col-sm-6">
                    <label>Select Space Features</label>
                    <p>
                      @foreach($selected_features as $selected_feature_value)
                        <b>{{ $loop->iteration }}</b> .
                        @if($selected_feature_value == 'driver')
                          Driver <br>
                          <div class="row">
                            <div class="col-md-12">
                              <p>
                                @foreach($parking->parkingDriveFeatures as $parkingDriveFeature)
                                  <div class="row">
                                    <div class="col-md-12">
                                      <label>Type</label>
                                      <p> 
                                        @if($parkingDriveFeature->type == 'company')
                                          Comapny <br>

                                        @elseif($parkingDriveFeature->type == 'plus_park')
                                          Plus Park <br>

                                        @elseif($parkingDriveFeature->type == 'third_party')
                                          Third Party <br>

                                        @endif
                                      </p>

                                      <label>Cash</label>
                                      <p>{{ $parkingDriveFeature->cash ?? '' }}</p>

                                      <label>Visa Master</label>
                                      <p>{{ $parkingDriveFeature->visa_master ?? '' }}</p>

                                      <label>Cost Of Valet</label>
                                      <p>{{ $parkingDriveFeature->cost_of_valet ?? '' }}</p>
                                    </div>
                                  </div>
                                @endforeach
                              </p>
                            </div>
                          </div>
                        @elseif($selected_feature_value == 'washer')
                          Washer <br>
                          <div class="row">
                            <div class="col-md-12">
                              <p>
                                @foreach($parking->parkingWasherFeatures as $parkingWasherFeature)
                                  <div class="row">
                                    <div class="col-md-12">
                                      <label>Type</label>
                                      <p>
                                        @if($parkingWasherFeature->type == 'company')
                                          Comapny <br>
                                        @elseif($parkingWasherFeature->type == 'plus_park')
                                          Plus Park <br>
                                        @elseif($parkingWasherFeature->type == 'third_party')
                                          Third Party <br>
                                        @endif
                                      </p>

                                      <label>Cash</label>
                                      <p>{{ $parkingWasherFeature->cash ?? '' }}</p>

                                      <label>Visa Master</label>
                                      <p>{{ $parkingWasherFeature->visa_master ?? '' }}</p>

                                      <label>Cost Of Full Wash</label>
                                      <p>{{ $parkingWasherFeature->cost_of_full_wash ?? '' }}</p>

                                      <label>Cost Of Exterior Wash</label>
                                      <p>{{ $parkingWasherFeature->cost_of_exterior_wash ?? '' }}</p>

                                      <label>Cost Of interior Wash</label>
                                      <p>{{ $parkingWasherFeature->cost_of_interior_wash ?? '' }}</p>
                                    </div>
                                  </div>
                                @endforeach
                              </p>
                            </div>
                          </div>
                        @elseif($selected_feature_value == 'booking')
                          Booking <br>
                          <p>
                            <div class="row">
                              <div class="col-md-12">
                                <label>Cash</label>
                                <p>{{ !empty($parking->parkingBookingFeature) ? $parking->parkingBookingFeature->cash : '' }}</p>

                                <label>Visa Master</label>
                                <p>{{ !empty($parking->parkingBookingFeature) ? $parking->parkingBookingFeature->visa_master : '' }}</p>
                              </div>
                            </div>
                          </p>
                        @elseif($selected_feature_value == 'subscription')
                          Subscirpiton <br>
                          <p>
                            <div class="row">
                              <div class="col-md-12">
                                <label>Cash</label>
                                <p>{{ !empty($parking->parkingSubscriptionFeature) ? $parking->parkingSubscriptionFeature->cash : '' }}</p>

                                <label>Visa Master</label>
                                <p>{{ !empty($parking->parkingSubscriptionFeature) ? $parking->parkingSubscriptionFeature->visa_master : '' }}</p>
                              </div>
                            </div>
                          </p>
                        @endif
                      @endforeach
                    </p> 
                  </div>

                </div>
              </div>
            </div>

            <div class="card mb-3">
              <div class="container card-body">
                <div class="row">
                  <div class="col-sm-9"><h5>Company Parking Space Mark Out</h5></div>
                  <div class="col-sm-6">
                    <label>Parking Space Label</label>
                    <p>{{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->parking_space_label : '' }}</p> 
                    <label>Special Access Instructions</label>
                    <p>{{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->special_access_instructions : '' }}</p>
                    <label>Set Avaiable Time</label>
                    <p>
                      @if($parking->set_available_time == 'customize')
                        Customize
                      @else
                        24
                      @endif
                    </p>

                    <label>Start Time</label>
                    <p>
                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->start_time : '' }}

                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->start_time_am_pm : '' }}
                    </p> 

                    <label>End Time</label>
                    <p>
                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->end_time : '' }}

                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->end_time_am_pm : '' }}
                    </p>

                    <label>About Extended Time</label>
                    <p>
                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->about_extended_time : '' }}
                    </p> 

                    <label>Set Booking Price</label>
                    <p>
                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->set_book_price : '' }}
                    </p>

                  </div>

                  <div class="col-sm-6">

                    <label>Set Valet Price</label>
                    <p>
                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->set_valet_price : '' }}
                    </p> 

                    <label>Set Valet & Washing Price</label>
                    <p>
                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->set_valet_and_washing_price : '' }}
                    </p> 

                    @php
                      $method_of_payments = !empty($parking->parkingMarkOut) ? json_decode($parking->parkingMarkOut->method_of_payment) : [];     
                      $days_mark_space_unavailables = !empty($parking->parkingMarkOut) ? json_decode($parking->parkingMarkOut->days_space_available) : [];
                    @endphp
                    <label>Method Of Payment</label>
                    <p>
                      @foreach($method_of_payments as $method_of_payment_value)
                        @if($method_of_payment_value == 'cash')
                          Cash
                        @elseif($method_of_payment_value == 'credit_card')
                          Credit Card
                        @elseif($method_of_payment_value == 'debit_card')
                          Debit Card
                        @endif
                      @endforeach
                    </p>

                    <label>Day Mark Space Unavailable</label>
                    <p>
                      @foreach($days_mark_space_unavailables as $days_mark_space_unavailable_value)
                        @if($days_mark_space_unavailable_value == 'monday')
                          Monday <br>
                        @elseif($days_mark_space_unavailable_value == 'tuesday')
                          Tuesday <br>
                        @elseif($days_mark_space_unavailable_value == 'wednesday')
                          Wednesday <br>
                        @elseif($days_mark_space_unavailable_value == 'thursday')
                          Thursday <br>
                        @elseif($days_mark_space_unavailable_value == 'friday')
                          Friday <br>
                        @elseif($days_mark_space_unavailable_value == 'saturday')
                          Saturday <br>
                        @elseif($days_mark_space_unavailable_value == 'sunday')
                          Sunday
                        @endif
                      @endforeach
                    </p>

                    <label>Mark Specific days available</label>
                    <p>
                      {{ !empty($parking->parkingMarkOut) ? $parking->parkingMarkOut->days_mark_space_unavailable : '' }}
                    </p>

                  </div>
                </div>
              </div>
            </div>

            <div class="card mb-3">
              <div class="container card-body">
                <div class="row">
                  <div class="col-sm-9"><h5>Company Parking Payout Settings</h5></div>

                  <div class="col-sm-6">
                    <label>Space Description</label>
                    <p>{{ !empty($parking->parkingPayoutSetting) ? $parking->parkingPayoutSetting->address : '' }}</p> 
                    <label>District/State</label>
                    <p>{{ !empty($parking->parkingPayoutSetting) ? $parking->parkingPayoutSetting->country : '' }}</p>
                    <label>Pincode</label>
                    <p>{{ !empty($parking->parkingPayoutSetting) ? $parking->parkingPayoutSetting->pincode : '' }}</p>
                    <label>First name</label>
                    <p>{{ !empty($parking->parkingPayoutSetting) ? $parking->parkingPayoutSetting->first_name : '' }}</p>
                    <label>Last Name</label>
                    <p>{{ !empty($parking->parkingPayoutSetting) ? $parking->parkingPayoutSetting->last_name : '' }}</p>
                  </div>

                  <div class="col-sm-6">
                    <label>Date Of Birth</label>
                    <p>{{ !empty($parking->parkingPayoutSetting) ? $parking->parkingPayoutSetting->date_of_birth : '' }}</p>
                    <label>Identification</label>
                    <p>{{ !empty($parking->parkingPayoutSetting) ? $parking->parkingPayoutSetting->identification : '' }}</p>
                    <label>Identification Photo</label>
                    <p><img src="{{ asset($parking->parkingPayoutSetting->identification_photo) }}" width="50px" height="50px"></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="card mb-3">
              <div class="container card-body">
                <div class="row">
                  <div class="col-sm-9"><h5>Company Parking Advance Setting</h5></div>

                  <div class="col-sm-6">
                    <label>Discount</label>
                    <p>
                      @if(!empty($parking->parkingAdvanceSetting) AND $parking->parkingAdvanceSetting->discounts)
                        ON
                      @else
                        OFF
                      @endif
                    </p> 
                    <label>Private Space Link</label>
                    <p>
                      @if(!empty($parking->parkingAdvanceSetting) AND $parking->parkingAdvanceSetting->private_space)
                        ON
                      @else
                        OFF
                      @endif
                    </p> 
                    <label>Private Space</label>
                    <p>
                      {{ $parking->parkingAdvanceSetting ? $parking->parkingAdvanceSetting->private_space_link : '' }}
                    </p> 
                  </div>

                  <div class="col-sm-6">
                    <label>Hidden Space Link</label>
                    <p>
                      @if(!empty($parking->parkingAdvanceSetting) AND $parking->parkingAdvanceSetting->hidden_space)
                        ON
                      @else
                        OFF
                      @endif
                    </p> 
                    <label>Hidden Space</label>
                    <p>
                      {{ $parking->parkingAdvanceSetting ? $parking->parkingAdvanceSetting->hidden_space_link : '' }}
                    </p> 

                    <label>Customer Term And Condition Setting</label>
                    <p>
                      @if(!empty($parking->parkingAdvanceSetting) AND $parking->parkingAdvanceSetting->customer_t_and_c)
                        ON
                      @else
                        OFF
                      @endif
                    </p> 
                    <label>Customer Term And Condition</label>
                    <p>
                      {{ $parking->parkingAdvanceSetting ? $parking->parkingAdvanceSetting->custom_t_and_c_text : '' }}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          <!-- </div> -->
          <!-- End Company Details Tab -->
        </div>
      </div>
    </div>
  </div>

<div class="modal fade company_edit" data-keyboard="false" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel" data-backdrop="static" data-keyboard="false">
</div>

  <div class="modal fade" id="delete-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <h2><i class="mdi mdi-delete text-danger"></i></h2>
          <h6>Are you sure you want to delete this washer?</h6>
          <button type="button" class="btn btn-gradient-success btn-md" data-dismiss="modal">Yes</button>
          <button type="button" class="btn btn-gradient-danger btn-md" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
@stop

@section('js-content')
<script type="text/javascript">
  
    $(document).on('submit', 'form#company_edit', function(e) {
      e.preventDefault();
      var data = new FormData(this);

      $.ajax({
          cache:false,
          contentType: false,
          processData: false,
          url: $(this).attr("action"),
          method: $(this).attr("method"),
          dataType: "json",
          data: data,
          success: function(response) {
            if (response.status == 200) {
                swal(response.success_message);
                $('div.company_edit').modal('hide');                  
                location.reload(true);

            }
          }
      }); 
    });

</script>
@stop