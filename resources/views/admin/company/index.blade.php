@extends('layouts.main-layout')
@section('content')

<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home-modern"></i>
      </span> {{__('company_management.company')}}  {{__('company_management.management')}} 
    </h3>
  </div>
  <form method="get" action="{{ route('company.index') }}">
    <div class="row gutter-sm">

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" class="form-control" autocomplete="off" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="{{__('basicbuttons.from_date')}} " id="start_date" />
      </div>

      <div class="col-12 col-sm-3 col-md-3 form-group">
          <input type="text" class="form-control" autocomplete="off" name="end_date" value="{{ $todate ?? '' }}" placeholder="{{__('basicbuttons.to_date')}} " id="end_date" />
      </div>

      <div class="col-12 col-sm-3 col-md-3 form-group">
        <select class="form-control" name="country_name" id="country_name">
          <option value="" data-id="">{{__('basicbuttons.select')}} {{__('country.country')}}</option>
          @foreach($countries as $country)
            <option  data-id="{{ $country->id ?? '' }}" @if($countryName == $country->name) selected="selected" @endif value="{{ $country->name ?? ''}}">
              {{ $country->name ?? '' }}</option>
          @endforeach
        </select>
      </div>
      
      <div class="col-12 col-sm-3 col-md-3 form-group">
        <select class="form-control" name="state_name" id="state_name">
          <option value="">{{__('basicbuttons.select')}} {{__('city.city')}}</option>
          @foreach($states as $state)
            <option @if($stateName == $state->name) selected="selected" @endif value="{{ $state->name ?? ''}}">
              {{ $state->name ?? '' }}</option>
        @endforeach
        </select>
      </div>
      
      <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
        <button type="submit" class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> {{__('basicbuttons.filter')}}</button>
        <button type="button" class="btn btn-md btn-gradient-success btn-modal" data-href="{{ route('company.create') }}"   data-container=".company_create">{{__('basicbuttons.add')}} </button>
        <!-- <a href="password-change-request.php" class="btn btn-md btn-gradient-success">Password Change Request</a> -->
      </div>
    </div>
  </form>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable-company">
              <thead>
                <th>{{__('company_management.company')}} {{__('basicText.id')}}.</th>
                <th>{{__('company_management.company')}} {{__('country.name')}}</th>
                <th>{{__('basicText.mobile')}}</th>
                <th>{{__('basicText.email')}}</th>
                <th>{{__('basicText.joining')}} {{__('basicText.date')}}</th>
                <th>{{__('country.country')}}</th>
                <th>{{__('city.city')}}</th>
                <th>{{__('basicText.parkings')}} {{__('basicText.owned')}}</th>
                <th>{{__('basicbuttons.action')}}</th>
              </thead>
              <tbody>
                @foreach($companies as $company)
                  <tr>
                    <td>{{ $company->company_auto_id ?? '' }}</td>
                    <td>{{ $company->name ?? '' }}</td>
                    <td>{{ $company->contact_person_mobile ?? '' }}</td>
                    <td>{{ $company->email ?? '' }}</td>
                    <td>{{ $company->date_of_join ?? '' }}</td>
                    <td>{{ $company->country ?? '' }}</td>
                    <td>{{ $company->city ?? '' }}</td>
                    <td>{{ $company->number_of_parking ?? '' }}</td>
                    <td>
                      <a href="{{ route('company.show', [$company->id ?? '']) }}" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-eye"></i></a>
                      <label class="togglebox toggleStatus ml-1" data-company-id="{{ $company->id ?? ''}}">
                        <input type="checkbox" @if($company->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                        <span class="label-text"></span>
                      </label>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {{ $companies->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade company_create" data-keyboard="false" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel" data-backdrop="static" data-keyboard="false">
</div>
@stop

@section('js-content')

  <script type="text/javascript">

    $('.datatable-company').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'copy',
          text:'{{ __('datatablebuttons.copy') }}',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
       },
       {
          extend: 'csv',
          text:'{{ __('datatablebuttons.csv') }}',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
       },
       {
          extend: 'excel',  
          text:'{{ __('datatablebuttons.excel') }}',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
       },
       {
          extend: 'print',
          text:'{{ __('datatablebuttons.print') }}',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
          }
       }
      ]
    });

    $(document).ready(function(){

      $("#start_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker('setStartDate', startDate);
      });
        
      $("#end_date").datepicker({
          orientation: 'bottom',
          autoclose: true
      }).on('changeDate', function(selected) {
          var startDate = new Date(selected.date.valueOf());
          $('#start_date').datepicker('setEndDate', startDate);
        });
    })

    $(document).on('change', 'label.toggleStatus', function() {
      var companyId = $(this).data('company-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('company.status') }}",
        data: {company_id: companyId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })

  </script>
@stop