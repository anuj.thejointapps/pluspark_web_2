<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form method="post" action="{{ route('company.update', [$company->id ?? '']) }}" enctype="multipart/form-data" id="company_edit" name="company_edit">
        @csrf
        @method('patch')
        <h5>Edit Basic Details</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" name="company_auto_id" readonly="readonly" class="form-control" placeholder="Company ID" value="{{ $company->company_auto_id ?? ''}}" />
            </div>

            <div class="form-group">
              <input type="text" name="name" id="name" class="form-control" placeholder="Company Name" value="{{ $company->name ?? ''}}"/>
            </div>

            <div class="form-group">
              <input type="text" name="contact_person_name" id="contact_person_name" class="form-control" placeholder="Contact Person Name" value="{{ $company->contact_person_name ?? ''}}"/>
            </div>

            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="Email" readonly="readonly" value="{{ $company->email ?? ''}}"/>
            </div>

            <div class="form-group">
              <input type="text" name="city_name" class="form-control" placeholder="City" value="{{ $company->city ?? ''}}"/>
            </div>

            <div class="form-group">
              <textarea rows="3" name="address" class="form-control" placeholder="Address">{{ $company->address ?? ''}}</textarea>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" id="date_of_join" autocomplete="off" name="date_of_join" class="form-control" placeholder="D.O.J." value="{{ $company->date_of_join ?? '' }}"/>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <input type="number" name="country_code" class="form-control" placeholder="Country code"  value="{{ $company->country_code ?? ''}}"/>
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" onkeypress="return numbersonly(event);" maxlength="10" name="contact_person_mobile" class="form-control" placeholder="Contact Person Mobile" value="{{ $company->contact_person_mobile ?? ''}}"/>
                </div>

              </div>
            </div>

            <div class="form-group">
              <input type="text" name="no_of_parking" class="form-control" placeholder="Number Of Parking" value="{{ $company->number_of_parking ?? ''}}"/>
            </div>

            <div class="form-group">
              <input type="text" name="country_name" class="form-control" placeholder="Country" value="{{ $company->country ?? ''}}"/>
            </div>

            <div class="form-group">
              <select class="form-control" name="currency_id">
                <option value="">Select Currency</option>
                @foreach($currencies as $currency)
                   <img src="{{ asset($currency->symbol ?? '') }}" width="25px" height="25px">
                   <option value="{{ $currency->id ?? '' }}" {{ ($currency->id==$company->currency_id) ? 'selected="selected"' : '' }}>{{ $currency->name ?? '' }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <textarea rows="3" name="notes" class="form-control" placeholder="Notes">{{ $company->notes ?? ''}}</textarea>
            </div>

            <div class="form-group">
              <input type="file" name="company_images[]" multiple="multiple">

              @if(!empty($company->companyImages))
                @foreach($company->companyImages as $companyImage)
                  <img src="{{ asset($companyImage->image_path ?? '') }}" class="wpx-50 hpx-50 mb-1 mr-1">
                @endforeach
              @endif
            </div>
          </div>
        </div>

        <div class="text-center">
          <button type="button" class="btn btn-gradient-success btn-md float-right ml-3" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-gradient-success btn-md float-right">Save & Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#date_of_join").datepicker({
        startDate : '-0m',
        format: "yyyy-mm-dd",
        orientation: 'bottom',
        autoclose: true
    });
  });

  $(function() {
    $("form[name='company_edit']").validate({
      rules: {
        contact_person_name: "required",
        country_code: "required",
        name: "required",
        email: "required",
        date_of_join: "required",
        no_of_parking: "required",
        contact_person_mobile: {
          required: true,
          minlength: 10
        }
      },
      messages: {
        contact_person_name: "Please Enter Person Name",
        country_code: "Please Enter Country Code",
        name: "Please Enter Company Name",
        email: "Please Enter Email",
        date_of_join: "Please Enter Date Of Join",
        no_of_parking: "Please Enter Number Of Parking",
        contact_person_mobile: {
          required: "Please enter your Mobile Number",
          minlength: "Your mobile must be at least 10 characters long"
        }
      }
    });
  });

</script>