@extends('layouts.main-layout')
@section('content')
  <!-- content-wrapper start -->
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title">
        <span class="page-title-icon bg-gradient-success text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> {{__('basicText.company')}} {{__('basicText.management')}}  <i class="mdi mdi-chevron-right"></i> {{__('basicbuttons.view')}}
      </h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs">
          <li class="nav-item"><a class="nav-link active" href="#Company_Details" data-toggle="tab">{{__('basicText.company')}} {{__('basicText.details')}}</a></li>
          <li class="nav-item"><a class="nav-link" href="#Washer_Alloted" data-toggle="tab">{{__('basicText.washer')}}  {{__('basicText.alloted_to')}}  {{__('basicText.company')}} </a></li>
          <li class="nav-item"><a class="nav-link" href="#Manage_Parking" data-toggle="tab">{{__('basicText.manage')}}  {{__('basicText.parking')}}</a></li>
        </ul>
        <div class="tab-content">
          <!-- Company Details Tab -->
          <div id="Company_Details" class="tab-pane fade show active">
            <div class="card mb-3">
              <div class="container card-body">
                <div class="row">
                  <div class="col-sm-9"><h5>Basic Details</h5></div>
                  <div class="col-sm-3">
                    <button type="button" class="btn btn-md btn-gradient-success btn-modal" data-href="{{ route('company.edit', [$company->id ?? '']) }}" 
                      data-container=".company_edit">EDIT </button>
                  </div>
                  <div class="col-sm-6">
                    <label>{{__('basicText.company')}} {{__('basicText.id')}}</label>
                    <p>{{ $company->company_auto_id ?? '' }}</p> 
                    <label>{{__('basicText.d_o_j')}}</label>
                    <p>{{ $company->date_of_join ?? '' }}</p> 
                    <label>{{__('basicText.number')}}  {{__('basicText.of')}}  {{__('basicText.parking')}} </label>
                    <p>{{ $company->number_of_parking ?? '' }}</p> 
                    <label>{{__('basicText.contact')}} {{__('basicText.person')}} {{__('basicText.name')}}</label>
                    <p>{{ $company->contact_person_name ?? '' }}</p> 
                    <label>{{__('basicText.contact')}} {{__('basicText.person')}} {{__('basicText.mobile')}}</label>
                    <p>+{{ $company->country_code ?? '' }}-{{ $company->contact_person_mobile ?? '' }}</p> 
                    <label>{{__('basicText.email')}}</label>
                    <p>{{ $company->email ?? '' }}</p> 
                  </div>

                  <div class="col-sm-6"> 
                    <label>{{__('country.country')}}</label>
                    <p>{{ $company->country ?? '' }}</p> 
                    <label>{{__('city.city')}}</label>
                    <p>{{ $company->city ?? '' }}</p> 

                    <label>{{__('currency.currency')}}</label>
                    <p><img width="25px" height="25px" src="{{ asset($company->currency ? $company->currency->symbol  : '') }}">{{ $company->currency ? $company->currency->name  : '' }}</p> 
                    <label>{{__('basicText.address')}}</label>
                    <p>{{ $company->address ?? '' }}</p> 
                    <label>{{__('basicText.notes')}} </label>
                    <p>{{ $company->notes ?? '' }}</p> 
                    <p>
                      @if(!empty($company->companyImages))
                        @foreach($company->companyImages as $companyImage)
                          <img src="{{ asset($companyImage->image_path ?? '') }}" class="wpx-50 hpx-50 mb-1 mr-1">
                        @endforeach
                      @endif
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- End Company Details Tab -->

          <!-- Washer Alloted Tab -->
          <div id="Washer_Alloted" class="tab-pane fade">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped datatable">
                    <thead>
                      <tr>
                        <th>{{__('basicText.washer')}}  {{__('basicText.name')}} </th>
                        <th>{{__('basicText.region')}} </th>
                        <!-- <th class="wpx-80">Action</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      @if(!empty($company->allotWasherOfCompany))
                        @foreach($company->allotWasherOfCompany as $allotWasherCompany)
                          <tr>
                            <td>{{ ($allotWasherCompany->washer) ? $allotWasherCompany->washer->name : '' }}</td>
                            <td>{{ ($allotWasherCompany->washer) ? $allotWasherCompany->washer->country : '' }}</td>
                            <!-- <td>
                              <label class="togglebox ml-1">
                                <input type="checkbox" name="block_unblock" hidden=""/>
                                <span class="label-text"></span>
                              </label>
                              <button type="button" data-toggle="modal" data-target="#delete-modal" class="ml-2 btn btn-gradient-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                            </td> -->
                          </tr>
                        @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- End Washer Alloted Tab -->
          
          <!-- Manage Parking Tab -->
          <div id="Manage_Parking" class="tab-pane fade">
            <div class="container">
              <div class="row">

                @foreach($company->companyParking as $parking)
                  <div class="col-sm-6 col-md-4 mb-3">
                    <div class="card">
                      <div class="card-body text-center">
                        <div class="bg-img-60 mb-3" style="background-image: url({{ asset($parking->image_path ?? '') }});"></div>
                        <h6>{{ $parking->name ?? '' }}</h6>

                        <a href="{{ route('company-parkings.show-details', [$parking->id ?? 0]) }}" class="btn btn-gradient-info btn-sm"><i class="mdi mdi-book-open-page-variant"></i></a>
                        <!-- <button type="button" class="btn btn-gradient-success btn-sm"><i class="mdi mdi-book"></i></button>
                        <a href="{{ route('company-parkings.show', [$parking->id ?? 0]) }}" class="btn btn-gradient-info btn-sm"><i class="mdi mdi-book-open-page-variant"></i></a>
                        <a href="{{ route('company-parkings.edit', [$parking->id ?? 0]) }}" class="btn btn-gradient-danger btn-sm"><i class="mdi mdi-border-color"></i></a>

                        <a href="{{ route('parking-space.create', [$parking->id ?? 0]) }}" class="btn btn-gradient-danger btn-sm"><i class="mdi mdi-book"></i></a>

                        <button type="button" class="btn btn-gradient-dark btn-sm"><i class="mdi mdi-dots-horizontal"></i></button> -->
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
          <!-- End Manage Parking Tab -->
        </div>
      </div>
    </div>
  </div>

<div class="modal fade company_edit" data-keyboard="false" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel" data-backdrop="static" data-keyboard="false">
</div>

  <div class="modal fade" id="delete-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <h2><i class="mdi mdi-delete text-danger"></i></h2>
          <h6>{{__('basicText.delete_confirm')}} {{__('basicText.this')}}  {{__('basicText.washer')}} ?</h6>
          <button type="button" class="btn btn-gradient-success btn-md" data-dismiss="modal">{{__('basicbuttons.yes')}}</button>
          <button type="button" class="btn btn-gradient-danger btn-md" data-dismiss="modal">{{__('basicbuttons.no')}}</button>
        </div>
      </div>
    </div>
  </div>
@stop

@section('js-content')
<script type="text/javascript">
  
    $(document).on('submit', 'form#company_edit', function(e) {
      e.preventDefault();
      var data = new FormData(this);

      $.ajax({
          cache:false,
          contentType: false,
          processData: false,
          url: $(this).attr("action"),
          method: $(this).attr("method"),
          dataType: "json",
          data: data,
          success: function(response) {
            if (response.status == 200) {
                swal(response.success_message);
                $('div.company_edit').modal('hide');                  
                location.reload(true);

            }
          }
      }); 
    });

</script>
@stop