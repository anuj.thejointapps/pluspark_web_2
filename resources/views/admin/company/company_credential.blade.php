@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  	<div class="page-header">
	    <h3 class="page-title">
	      	<span class="page-title-icon bg-gradient-success text-white mr-2">
	        	<i class="mdi mdi-account"></i>
	      	</span> Generate Company Credential
	    </h3>
	    <nav aria-label="breadcrumb">
	      	<ul class="breadcrumb">
		        <li class="breadcrumb-item active" aria-current="page">
		        	<a href="{{ route('company.index') }}">
			           <i class="mdi mdi-arrow-left icon-sm text-primary align-middle mr-1"></i> Back
		        	</a>
		        </li>
	      	</ul>
	    </nav>
  	</div>
  	<form method="post" action="{{ route('company.generate-credential', [$company->id ?? '' ]) }}">
  		@csrf
	  	<div class="row">
		    <div class="col-md-12">
		      	<!--  -->
		      	<div class="card mb-3">
			        <div class="container card-body">
			          	<div class="row">
				            <div class="col-sm-12">
				            	<h5>Generate Credentials</h5>
				            </div>
				            <div class="col-sm-6">
				              	<div class="form-group">
					                <label>Email</label>
					                <input type="text" name="email" value="{{ $company->email ?? '' }}" readonly="readonly" class="form-control" /> 
				              	</div>
				              	<div class="form-group">
					                <label>Password</label>
					                <input type="text" required="required" value="" name="password" class="form-control" placeholder="******" /> 
								</div>
				            </div>
				            <div class="col-md-12">
				            	<input type="submit" class="btn btn-success float-left" value="Generate Credential">
				            </div>
			          	</div>
			        </div>
		      	</div>
		    </div>
	  	</div>
  	</form>
</div>
<!-- content-wrapper ends -->
@stop