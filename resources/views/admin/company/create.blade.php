<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form method="post" action="{{ route('company.store') }}" enctype="multipart/form-data" id="company_create" name="company_create">
        @csrf
        <h5>{{__('basicbuttons.add')}}  {{__('basicText.basic')}} {{__('basicText.details')}}</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" name="company_auto_id" readonly="readonly" class="form-control" placeholder="{{__('basicText.company')}} {{__('basicText.id')}}" value="{{ $companySequence ?? '' }}" />
            </div>

            <div class="form-group">
              <input type="text" name="name" id="name" class="form-control" placeholder="{{__('basicText.company')}} {{__('basicText.name')}}"/>
            </div>

            <div class="form-group">
              <input type="text" name="contact_person_name" id="contact_person_name" class="form-control" placeholder="{{__('basicText.contact')}} {{__('basicText.person')}} {{__('basicText.name')}}"/>
            </div>

            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="{{__('basicText.email')}}"/>
            </div>

            <div class="form-group">
              <input type="text" name="city_name" class="form-control" placeholder="{{__('city.city')}}"/>
            </div>

            <div class="form-group">
              <textarea rows="3" name="address" class="form-control" placeholder="{{__('basicText.address')}}"></textarea>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" id="date_of_join" name="date_of_join" class="form-control" placeholder="{{__('basicText.d_o_j')}}" autocomplete="off" />
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <input type="number" name="country_code" class="form-control" placeholder="{{__('country.country_code')}}"/>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" onkeypress="return numbersonly(event);" maxlength="10" name="contact_person_mobile" class="form-control" placeholder="{{__('basicText.contact')}} {{__('basicText.person')}} {{__('basicText.mobile')}}"/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <input type="text" name="no_of_parking" class="form-control" placeholder="{{__('basicText.no_of_parking')}} "/>
            </div>

            <div class="form-group">
              <input type="text" name="country_name" class="form-control" placeholder="{{__('country.country')}} "/>
            </div>

            <div class="form-group">
              <select class="form-control" name="currency_id">
                <option value="">{{__('basicbuttons.select')}} {{__('currency.currency')}}</option>
                @foreach($currencies as $currency)
                   <img src="{{ asset($currency->symbol ?? '') }}" width="25px" height="25px">
                   <option value="{{ $currency->id ?? '' }}" >{{ $currency->name ?? '' }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <textarea rows="3" name="notes" class="form-control" placeholder="{{__('basicText.notes')}} "></textarea>
            </div>

            <div class="form-group">
              <input type="file" name="company_images[]" multiple="multiple">
            </div>
          </div>
        </div>

        <div class="text-center">
          <button type="button" class="btn btn-gradient-success btn-md float-right ml-3" data-dismiss="modal">{{__('basicbuttons.close')}}</button>
          <button type="submit" class="btn btn-gradient-success btn-md float-right">{{__('basicbuttons.save')}}</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#date_of_join").datepicker({
        startDate : '-0m',
        format: "yyyy-mm-dd",
        orientation: 'bottom',
        autoclose: true
    });
  });

  $(function() {
    $("form[name='company_create']").validate({
        rules: {
          name: "required",
          country_code: "required",
          contact_person_name: "required",
          date_of_join: "required",
          no_of_parking: "required",
          contact_person_mobile: {
            required: true,
            minlength: 10
          },
          email: {
            required: true,
            minlength: 5,
            remote: {
                url: "{{ route('company.email-check') }}",
                type: "post"
             }
          }
        },
        messages: {
          contact_person_name: "{{__('basicText.please_enter')}}  {{__('basicText.person')}} {{__('basicText.name')}}",
          name: "{{__('basicText.please_enter')}}  {{__('basicText.company')}} {{__('basicText.name')}}",
          country_code: "{{__('basicText.please_enter')}}  {{__('basicText.company')}} {{__('basicText.code')}} ",
          date_of_join: "{{__('basicText.please_enter')}}  {{__('basicText.date')}} {{__('basicText.of')}} {{__('basicText.joining')}}",
          no_of_parking: "{{__('basicText.please_enter')}}  {{__('basicText.number')}} {{__('basicText.of')}} {{__('basicText.parkings')}}",
          contact_person_mobile: {
            required: "{{__('basicText.please_enter')}}  {{__('basicText.your')}}  {{__('basicText.mobile')}}  {{__('basicText.number')}} ",
            minlength: "{{__('basicText.atleast_10_char_mobile')}} "
          },
          email: {
            required: "{{__('basicText.email')}} {{__('basicText.is_required')}} ",
            minlength: "{{__('basicText.your')}}  {{__('basicText.email')}}  {{__('basicText.atleast_5_char')}} ",
            remote: "{{__('basicText.email')}} {{__('basicText.already_in_record')}} "
          }
        }
    });
  });

</script>