<form action="{{ route('discount.update', $discount->id) }}" method="post" enctype="multipart/form-data" onsubmit="return Validate(this);">
  <input type="hidden" name="discount_id" value="{{$discount->id}}">
  @csrf
  {{ method_field('PUT') }}
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <form method="post" action="#">
            <h5>Update Discount</h5>
            <div class="form-group">
              <label>Start Date</label><span class="required" style="color: red;">*</span>
              <input type="text" autocomplete="off" name="start_date" class="form-control edit_start_date_formatted" id="edit_start_date_formatted" required="required" value="{{ $discount->start_date_formatted ?? '' }}" />
            </div>
            <div class="form-group">
              <label>End Date</label><span class="required" style="color: red;">*</span>
              <input type="text" autocomplete="off" name="end_date" class="form-control edit_end_date_formatted" id="edit_end_date_formatted" required="required" value="{{ $discount->end_date_formatted ?? '' }}"/>
            </div>

            <div class="form-group">
              <label>Discount Name</label><span class="required" style="color: red;">*</span>
              <input type="text" name="name" class="form-control" value="{{ $discount->title }}" required="required"/>
            </div>
            <div class="form-group">
              <label>Discount Description</label>
              <input type="text" name="description" value="{{ $discount->description }}" class="form-control"/>
            </div>
            <div class="form-group">
              <label>Discount (%)</label><span class="required" style="color: red;">*</span>
              <input type="text" name="discount_percent" class="form-control" value="{{ $discount->percent_discount }}"  onkeypress="return numbersonly(event);"  required="required"/>
            </div>
            <h6>Service</h6>
              <div class="row">
                <div class="form-group ml-5">
                  <label class="form-check-label text-muted">
                    <input type="checkbox" name="booking" value="booking" class="form-check-input" {{ $discount->on_booking ? 'checked' : '' }}>
                      <i class="input-helper"></i> Booking
                  </label>
                </div>

                <div class="form-group ml-5">
                  <label class="form-check-label text-muted">
                    <input type="checkbox" name="subscription"value="subscription" class="form-check-input" {{ $discount->on_subscription ? 'checked' : '' }}>
                      <i class="input-helper"></i> Subscription
                  </label>
                </div>

                <div class="form-group ml-5">
                  <label class="form-check-label text-muted">
                    <input type="checkbox" name="valet"value="valet" class="form-check-input" {{ $discount->on_wallet ? 'checked' : '' }}>
                      <i class="input-helper"></i> Valet
                  </label>
                </div>
              </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary">Update</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
  </div>
</form>
<script>
  //Set the datepicker date Style to Y-m-D on Edit
  $( "input.edit_start_date_formatted" ).datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: today,
  });

  
  //Set the datepicker date Style to Y-m-D
  $( "input.edit_end_date_formatted" ).datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: today, 
  });

</script>
