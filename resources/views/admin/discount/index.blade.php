@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <!-- <h3 class="page-title"> Country </h3> -->
    

    <h3 class="page-title">
        </span> Discount Management
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">Discount</li>
        </ol>
    </nav>
  </div>

    <form method="get" action="{{ route('discount.index') }}">
      <div class="row gutter-sm">

        <div class="col-12 col-sm-3 col-md-3 form-group">
            <input type="text" autocomplete="off" class="form-control" value="{{ $fromDate ?? ''}}" name="start_date" placeholder="From Date" id="start_date" />
        </div>
        <div class="col-12 col-sm-3 col-md-3 form-group">
            <input type="text" autocomplete="off"  class="form-control" name="end_date" value="{{ $todate ?? '' }}" placeholder="To Date" id="end_date" />
        </div>
        <div class="col-12 col-sm-3 col-md-3 form-group">
            <select class="form-control" name="month">
              <option value="">Select Month</option>
              <option value="1" @if($month == '1') selected="selected" @endif>1 Month</option>
              <option value="3" @if($month == '3') selected="selected" @endif>3 Months</option>
              <option value="6" @if($month == '6') selected="selected" @endif>6 Months</option>
            </select>
        </div>

        <div class="col-12 col-sm-9 col-md-12 col-lg-6 form-group">
          <button type="submit" class="btn btn-md btn-gradient-success"><i class="mdi mdi-file-excel"></i> Filter</button>
        </div>
      </div>
    </form>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <a href="#" class="btn btn-sm float-left mr-1 btn-md btn-gradient-primary mb-2" id="add_discount_button" data-toggle="modal" data-target="#myModal">Add</a>
            <table class="table table-bordered datatable-discount">
              <thead>
                <tr>
                  <!-- <th>Company Id</th>
                  <th>Company Name</th> -->
                  <th>Title</th>
                  <th>Discount(%)</th>
                  <th>Start date</th>
                  <th>Expiry date</th>
                  <th width="90px">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($discounts as $discount)
                <tr>
                  <!-- <td>{{ $discount->company->company_auto_id ?? '' }}</td> -->
                  <!-- <td>{{ $discount->company->name ?? '' }}</td> -->
                  <td>{{ $discount->title ?? '' }}</td>
                  <td>{{ $discount->percent_discount ?? '' }}</td>
                  <td>{{ $discount->start_date_formatted ?? '' }}</td>
                  <td>{{ $discount->end_date_formatted ?? '' }}</td>
                  <td>
                    <a href="#" class="btn btn-sm btn-gradient-primary mb-3" data-id="{{ $discount->id }}" id="edit_discount">Edit</a>
                    <label class="togglebox toggleStatus ml-1" data-id="{{ $discount->id ?? ''}}">
                      <input type="checkbox" @if($discount->status) checked="checked" @endif name="block_unblock" hidden=""/>
                      <span class="label-text"></span>
                    </label>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            {{ $discounts->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Add Modal -->
<form action="{{route('discount.store')}}" method="POST">
    @csrf
  <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <form method="post" action="#">
            <h5>Add Discount</h5>
            <div class="form-group">
              <label>Start Date</label><span class="required" style="color: red;">*</span>
              <input type="text" autocomplete="off" name="start_date" class="form-control start_date_formatted" id="start_date1" required="required"/>
            </div>
            <div class="form-group">
              <label>End Date</label><span class="required" style="color: red;">*</span>
              <input type="text" autocomplete="off" name="end_date" class="form-control end_date_formatted" id="end_date1" required="required"/>
            </div>
           <!--  <div class="form-group">
              <label>Company Name</label><span class="required" style="color: red;">*</span>
              <select class="form-control" name="company_id" id="company_name" required="required">
                <option hidden="" value="0">Choose Company</option>
                @foreach($companies as $company)
                  <option value="{{ $company->id }}">{{ $company->name ?? '' }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Company ID</label><span class="required" style="color: red;">*</span>
              <input type="text" name="" class="form-control" id="company_id" />
            </div> -->
            <div class="form-group">
              <label>Discount Name</label><span class="required" style="color: red;">*</span>
              <input type="text" name="name" class="form-control" required="required"/>
            </div>
            <div class="form-group">
              <label>Discount Description</label>
              <input type="text" name="description" class="form-control"/>
            </div>
            <div class="form-group">
              <label>Discount (%)</label><span class="required" style="color: red;">*</span>
              <input type="text" name="discount_percent" class="form-control"  onkeypress="return numbersonly(event);"  required="required"/>
            </div>
            <h6>Service</h6>
              <div class="row">
                <div class="form-group ml-5">
                  <label class="form-check-label text-muted">
                    <input type="checkbox" name="booking" value="booking" class="form-check-input">
                      <i class="input-helper"></i> Booking
                  </label>
                </div>

                <div class="form-group ml-5">
                  <label class="form-check-label text-muted">
                    <input type="checkbox" name="subscription"value="subscription" class="form-check-input">
                      <i class="input-helper"></i> Subscription
                  </label>
                </div>

                <div class="form-group ml-5">
                  <label class="form-check-label text-muted">
                    <input type="checkbox" name="valet"value="valet" class="form-check-input">
                      <i class="input-helper"></i> Valet
                  </label>
                </div>
              </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
               <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="modal fade updateDiscountModal" id="updateDiscountModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="updateSplashImage" aria-labelledby="gridSystemModalLabel">
</div>
@stop

@section('js-content')
  <script type="text/javascript">

    $('.datatable-discount').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'copy',
          exportOptions: {
            columns: [0,1,2,3,4,5]
          }
       },
       {
          extend: 'csv',
          exportOptions: {
            columns: [0,1,2,3,4,5]
          }
       },
       {
          extend: 'excel',
          exportOptions: {
            columns: [0,1,2,3,4,5]
          }
       },
       {
          extend: 'print',
          exportOptions: {
            columns: [0,1,2,3,4,5]
          }
       }
      ]
    });

    var nowDate = new Date();
    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
    //Set the datepicker date Style to Y-m-D
    $( ".start_date_formatted" ).datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      startDate: today,
    });

    //Set the datepicker date Style to Y-m-D
    $( ".end_date_formatted" ).datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      startDate: today, 
    });

   // On change Company Name Set Company Id
    $('select#company_name').on('change', function(e) {
      var companyId = $(this).val();
      $.ajax({
        url: "{{action('Admin\DiscountController@fetchCompanyId')}}",
        method: "GET",
        dataType: "json",
        data:{'id':companyId},
        success: function(response) {
          if (response.status == 200) {
            $('input#company_id').val(response.id);
          } 
        }
      });
    });

    //On Edit of Discount
    $('a#edit_discount').on('click', function(e) {
     var discountId = $(this).attr('data-id');
     $.ajax({
        url: "{{action('Admin\DiscountController@openDiscountUpdate')}}",
        method: "GET",
        dataType: "json",
        data:{'id':discountId},
        success: function(response) {
          if (response.status == 200) {
            $('#updateDiscountModal').html(response.html);
            $('.updateDiscountModal').modal('show');
          } 
        }
      }); 
    });

    //Toggle status
    $(document).on('change', 'label.toggleStatus', function() {
      var discountId = $(this).attr('data-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('discount.status') }}",
        data: {id: discountId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })

    // Number Only.
    function numbersonly(e) {
      var unicode=e.charCode? e.charCode : e.keyCode
      if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
          if (unicode<48||unicode>57) //if not a number
            return false //disable key press
      }
    }
  </script>
@stop