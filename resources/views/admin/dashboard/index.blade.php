@extends('layouts.main-layout')
@section('content')
<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-home"></i>
      </span> Dashboard </h3>
    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">
          <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
        </li>
      </ul>
    </nav>
  </div>
  <div class="row gutter-sm">
    <div class="col-12 col-sm-3 col-md-3 form-group">
      <select class="form-control">
        <option value="City">City</option>
        <option value="State">State</option>
        <option value="Country">Country</option>
      </select>
    </div>
    <div class="col-12 col-sm-3 col-md-3 form-group">
      <input type="text" class="form-control" placeholder="From Date" id="start_date" />
    </div>
    <div class="col-12 col-sm-3 col-md-3 form-group">
      <input type="text" class="form-control" placeholder="To Date" id="end_date" />
    </div>
    <div class="col-12 col-sm-3 col-md-3 form-group">
      <select class="form-control">
        <option value="This Week">This Week</option>
        <option value="This Month">This Month</option>
        <option value="This Year">This Year</option>
      </select>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 stretch-card grid-margin">
      <div class="card bg-gradient-danger card-img-holder text-white">
        <div class="card-body">
          <img src="{{asset('assets/images/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
          <h4 class="font-weight-normal mb-3">Total Companies <i class="mdi mdi-play mdi-24px float-right"></i></h4>
          <h2 class="mb-0">150</h2>
          <h6 class="card-text">Application</h6>
          <div class="progress mt-3">
            <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 stretch-card grid-margin">
      <div class="card bg-gradient-info card-img-holder text-white">
        <div class="card-body">
          <img src="{{ asset('assets/images/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
          <h4 class="font-weight-normal mb-3">Total Active Users <i class="mdi mdi-book-open-variant mdi-24px float-right"></i></h4>
          <h2 class="mb-0">334</h2>
          <h6 class="card-text">Users</h6>
          <div class="progress mt-3">
            <div class="progress-bar bg-gradient-warning" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 stretch-card grid-margin">
      <div class="card bg-gradient-primary card-img-holder text-white">
        <div class="card-body">
          <img src="{{ asset('assets/images/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
          <h4 class="font-weight-normal mb-3">Total Number of Active Parking Spacing <i class="mdi mdi-car mdi-24px float-right"></i></h4>
          <h2 class="mb-0">741</h2>
          <div class="progress mt-3">
            <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 stretch-card grid-margin">
      <div class="card bg-gradient-warning card-img-holder text-white">
        <div class="card-body">
          <img src="{{ asset('assets/images/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
          <h4 class="font-weight-normal mb-3">Total Completed Bookings <i class="mdi mdi-book-open-variant mdi-24px float-right"></i></h4>
          <h2 class="mb-0">741</h2>
          <h6 class="card-text">Application</h6>
          <div class="progress mt-3">
            <div class="progress-bar bg-gradient-primary" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8 stretch-card grid-margin">
      <div class="card bg-gradient-success card-img-holder text-white">
        <div class="card-body">
          <img src="{{ asset('assets/images/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
          <h4 class="font-weight-normal mb-3">Total Revenue <i class="mdi mdi-currency-usd mdi-24px float-right"></i></h4>
          <select class="mb-3 form-control">
            <option value="Today">Today</option>
            <option value="Tomorrow">Tomorrow</option>
            <option value="This Month">This Month</option>
            <option value="This Year">This Year</option>
          </select>
          <div class="mt-2">From Bookings <span class="float-right">2M</span></div>
          <div class="mt-2">From Washing Service <span class="float-right">2M</span></div>
          <div class="mt-2">From Subscriptions <span class="float-right">2M</span></div>
        </div>
      </div>
    </div>
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder text-dark">
        <div class="card-body">
          <img src="{{asset('assets/images/circle.svg') }}" class="card-img-absolute" alt="circle-image" />
          <h4 class="font-weight-normal mb-3">Revenue 
            <select class="form-control w-50 float-right">
              <option value="Today">Today</option>
              <option value="Tomorrow">Tomorrow</option>
              <option value="This Week">This Week</option>
              <option value="This Month">This Month</option>
              <option value="This Year">This Year</option>
            </select>
          </h4>
          <div class="clearfix mb-3"></div>
          <h6 class="mb-4">From Bookings <span class="float-right">2M</span></h6>
          <h6 class="mb-0">From Subscriptions <span class="float-right">2M</span></h6>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-sm-6 col-md-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="clearfix">
            <h4 class="card-title float-left"><i class="mdi mdi-reddit"></i> Appointments Occured</h4>
            <!-- <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div> -->
          </div>
          <select class="form-control mt-3">
            <option value="Week">Week</option>
            <option value="Month">Month</option>
            <option value="Year">Year</option>
          </select>
          <canvas id="visit-sale-chart" class="mt-4"></canvas>
          <div class="w-50 float-left mt-3">Overall : 70.65% <i class="mdi mdi-arrow-up text-success"></i></div>
          <div class="w-50 float-left mt-3">Yearly : 70.65% <i class="mdi mdi-arrow-down text-danger"></i></div>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title"><i class="mdi mdi-chart-line"></i> Traffic On User App</h4>
          <canvas id="traffic-chart"></canvas>
          <div id="traffic-chart-legend" class="rounded-legend legend-vertical legend-bottom-left pt-4"></div>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="clearfix">
            <h4 class="card-title float-left"><i class="mdi mdi-currency-usd"></i> Revenue</h4>
            <!-- <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div> -->
          </div>
          <select class="form-control mt-3">
            <option value="Week">Week</option>
            <option value="Month">Month</option>
            <option value="Year">Year</option>
          </select>
          <canvas id="revenue-chart" class="mt-4"></canvas>
          <div class="w-50 float-left mt-3">Tpday : 70.65% <i class="mdi mdi-arrow-up text-success"></i></div>
          <div class="w-50 float-left mt-3">Weekly : 70.65% <i class="mdi mdi-arrow-down text-danger"></i></div>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="clearfix">
            <h4 class="card-title float-left"><i class="mdi mdi-reddit"></i>  Appointment Complete</h4>
            <!-- <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div> -->
          </div>
          <select class="form-control mt-3">
            <option value="Week">Week</option>
            <option value="Month">Month</option>
            <option value="Year">Year</option>
          </select>
          <canvas id="appointment-chart" class="mt-4"></canvas>
          <div class="w-50 float-left mt-3">Tpday : 70.65% <i class="mdi mdi-arrow-up text-success"></i></div>
          <div class="w-50 float-left mt-3">Weekly : 70.65% <i class="mdi mdi-arrow-down text-danger"></i></div>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="clearfix">
            <h4 class="card-title float-left"><i class="mdi mdi-information-outline"></i> No Shows</h4>
            <!-- <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div> -->
          </div>
          <select class="form-control mt-3">
            <option value="Week">Week</option>
            <option value="Month">Month</option>
            <option value="Year">Year</option>
          </select>
          <canvas id="points-chart" class="mt-4"></canvas>
          <div class="w-50 float-left mt-3">Tpday : 70.65% <i class="mdi mdi-arrow-up text-success"></i></div>
          <div class="w-50 float-left mt-3">Weekly : 70.65% <i class="mdi mdi-arrow-down text-danger"></i></div>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="clearfix">
            <h4 class="card-title float-left"><i class="mdi mdi-currency-usd"></i> Performance </h4>
            <!-- <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div> -->
          </div>
          <select class="form-control mt-3">
            <option value="Week">Week</option>
            <option value="Month">Month</option>
            <option value="Year">Year</option>
          </select>
          <canvas id="performance-chart" class="mt-4"></canvas>
          <div class="w-50 float-left mt-3">Tpday : 70.65% <i class="mdi mdi-arrow-up text-success"></i></div>
          <div class="w-50 float-left mt-3">Weekly : 70.65% <i class="mdi mdi-arrow-down text-danger"></i></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
@stop

@section('js-content')
<script type="text/javascript">
  $(document).ready(function(){
    $("#start_date").datepicker({
      orientation: 'bottom',
      autoclose: true
    }).on('changeDate', function(selected) {
      var startDate = new Date(selected.date.valueOf());
      $('#end_date').datepicker('setStartDate', startDate);
    });
    
    $("#end_date").datepicker({
      orientation: 'bottom',
      autoclose: true
    }).on('changeDate', function(selected) {
      var startDate = new Date(selected.date.valueOf());
      $('#start_date').datepicker('setEndDate', startDate);
    });
  })
</script>

@stop