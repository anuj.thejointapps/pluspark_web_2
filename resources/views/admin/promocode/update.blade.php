<form action="{{ route('promocode.update', $promocode->id) }}" method="post" enctype="multipart/form-data" onsubmit="return Validate(this);">
  <input type="hidden" name="promocode_id" value="{{$promocode->id}}">
  @csrf
  {{ method_field('PUT') }}
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <form method="post" action="#">
            <h5>Update Promocode</h5>
            <div class="form-group">
              <label>Start Date</label><span class="required" style="color: red;">*</span>
              <input type="text" name="start_date" autocomplete="off" class="form-control start_date_formatted" id="start_date1" required="required" value="{{ $promocode->start_date_formatted ?? '' }}" />
            </div>
            <div class="form-group">
              <label>End Date</label><span class="required" style="color: red;">*</span>
              <input type="text" name="end_date" autocomplete="off" class="form-control end_date_formatted" id="end_date1" required="required" value="{{ $promocode->end_date_formatted ?? '' }}"/>
            </div>
            <div class="form-group">
              <label>Promocode ID</label><span class="required" style="color: red;">*</span>
              <input type="text" name="promocode_id" class="form-control" id="promocode_id" disabled="disabled"  value="{{ $promocode->unique_id ?? '' }}"/>
            </div>
            <div class="form-group">
              <label>Code</label><span class="required" style="color: red;">*</span>
              <input type="text" name="code" class="form-control" required="required" disabled="disabled"  value="{{ $promocode->code ?? '' }}"/>
            </div>
            <div class="form-group">
              <label>Discount Type</label><span class="required" style="color: red;">*</span>
              <select class="form-control" name="discount_type" id="discount_type" required="required">
                <option value="percent" {{ $promocode->offer_type == 'percent' ? 'selected' : '' }}>Percent Discount</option>
                <option value="flat" {{ $promocode->offer_type == 'flat' ? 'selected' : '' }}>Flat Discount</option>
              </select>
            </div>
            <div class="form-group">
              <label>Discount</label><span class="required" style="color: red;">*</span>
              <input type="text" name="discount" class="form-control"  onkeypress="return numbersonly(event);"  required="required"  value="{{ $promocode->discount ?? '' }}"/>
            </div>
            <div class="form-group">
              <label>Usage Allowed</label><span class="required" style="color: red;">*</span>
              <input type="text" name="usage_allowed" class="form-control"  onkeypress="return numbersonly(event);"  required="required" value="{{ $promocode->usage_allowed ?? '' }}"/>
            </div>

            <h6>Service</h6>
            <div class="row">
              <div class="form-group ml-5">
                <label class="form-check-label text-muted">
                  <input type="checkbox" name="booking" value="booking" class="form-check-input" {{ $promocode->on_booking ? 'checked' : '' }}>
                    <i class="input-helper"></i> Booking
                </label>
              </div>

              <div class="form-group ml-5">
                <label class="form-check-label text-muted">
                  <input type="checkbox" name="subscription"value="subscription" class="form-check-input" {{ $promocode->on_subscription ? 'checked' : '' }}>
                    <i class="input-helper"></i> Subscription
                </label>
              </div>

              <div class="form-group ml-5">
                <label class="form-check-label text-muted">
                  <input type="checkbox" name="valet"value="valet" class="form-check-input" {{ $promocode->on_wallet ? 'checked' : '' }}>
                    <i class="input-helper"></i> Valet
                </label>
              </div>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-gradient-success">Update</button>
               <button type="button" class="btn btn-gradient-danger" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</form>
<script>
  //Set the datepicker date Style to Y-m-D on Edit
  $( "input.start_date_formatted" ).datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: today,
  });

  
  //Set the datepicker date Style to Y-m-D
  $( "input.end_date_formatted" ).datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: today, 
  });

</script>
