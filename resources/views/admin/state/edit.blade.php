@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> City Edit 
    </h3>

    <nav aria-label="breadcrumb">
        <ul class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ route('states.index') }}">
               <i class="mdi mdi-arrow-left icon-sm text-primary align-middle mr-1"></i> Back
            </a>
          </li>
        </ul>
    </nav>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">

          <form class="cmxform" id="signupForm" method="post" action="{{ route('states.update', [$state->id ?? '']) }}">
            @csrf
            @method('patch')
            <fieldset>
              <div class="form-group">
                <label for="country_id">Country Name</label>
                <select name="country_id" class="form-control">
                    <option value="">Select Country</option>
                  @foreach($countries as $country)
                    <option {{ ($country->id==$state->country_id) ? 'selected="selected"' : '' }} value="{{ $country->id ?? '' }}">{{ $country->name ?? '' }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="username">City Name</label>
                <input id="username" class="form-control" placeholder="City Name" value="{{ $state->name ?? '' }}" name="name" type="text">
                <span class="text-danger">{{ $errors->first('name') }}</span>
              </div>
              <button type="submit" class="btn btn-primary">Update</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop