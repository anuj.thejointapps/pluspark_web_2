@extends('layouts.main-layout')
@section('content')

<!-- content-wrapper start -->
<div class="content-wrapper">
  <div class="page-header">
    <!-- <h3 class="page-title"> States </h3> -->

    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
          <i class="mdi mdi-account"></i>
        </span> {{__('city.cities')}} 
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('city.cities')}} </li>
        </ol>
    </nav>
  </div>

  <div class="row">
    <div class="col-md-12 stretch-card grid-margin">
      <div class="card card-img-holder">
        <div class="card-body">
          <div class="table-responsive">
            <a href="{{ route('states.create') }}" class="btn btn-sm float-left mr-1 btn-gradient-primary mb-3">{{__('basicbuttons.add')}}  {{__('city.city')}} </a>
            <table class="table table-bordered datatable-state">
              <thead>
                <tr>
                  <th>{{__('country.country_name')}}</th>
                  <th>{{__('country.name')}}</th>
                  <th class="wpx-90">{{__('basicbuttons.action')}}</th>
                </tr>
              </thead>
              <tbody>
                @foreach($states as $state)
                <tr>
                  <td>{{ $state->country ? $state->country->name : '' }}</td>
                  <td>{{ $state->name ?? '' }}</td>
                  <td>
                    <a href="{{ route('states.edit', [$state->id]) }}" class="btn btn-sm btn-gradient-primary">{{__('basicbuttons.edit')}}</a>
                    <label class="togglebox toggleStatus ml-1" data-state-id="{{ $state->id ?? ''}}">
                      <input type="checkbox" @if($state->status == 1) checked="checked" @endif name="block_unblock" hidden=""/>
                      <span class="label-text"></span>
                    </label>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('js-content')

  <script type="text/javascript">
    
      $('.datatable-state').DataTable({
        dom: 'Bfrtip',
        buttons: [
         {
            extend: 'copy',
            text:'{{ __('datatablebuttons.copy') }}',
            exportOptions: {
              columns: [0,1]
            }
         },
         {
            extend: 'csv',
            text:'{{ __('datatablebuttons.csv') }}',
            exportOptions: {
              columns: [0,1]
            }
         },
         {
            extend: 'excel',  
            text:'{{ __('datatablebuttons.excel') }}',
            exportOptions: {
              columns: [0,1]
            }
         },
         {
            extend: 'print',
            text:'{{ __('datatablebuttons.print') }}',
            exportOptions: {
              columns: [0,1]
            }
         }
        ]
      });
    $(document).on('change', 'label.toggleStatus', function() {
      var stateId = $(this).data('state-id')

      $.ajax({
        method: 'POST',
        url: "{{ route('states.status') }}",
        data: {state_id: stateId},
        dataType: 'json',
        success: function(response) {
          if (response.status == 200) {
            swal(response.success_message)
          }
        }
      })
    })
  </script>
@stop