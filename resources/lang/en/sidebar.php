<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'profile'                           => 'Profile',
    'sign_out'                          => 'Sign Out',
    'notifications'                     => 'Notifications',
    'dashboard'                         => 'Dashboard',
    'country_and_city_management'       => 'Country and City Management',
    'country'                           => 'Country',
    'city'                              => 'City',
    'currency'                          => 'Currency',
    'company_management'                => 'Company Management',
    'washer_management'                 => 'Washer Management',
    'washer_list'                       => 'Washer List',
    'company_with_washer_service'       => 'Company with Washer Service',
    'brand'                             => 'Brand',
    'color'                             => 'Color',
    'model'                             => 'Model',
    'popular_events_spaces_mgmt'        => 'Popular Events Spaces Mgmt',
    'popular_places'                    => 'Popular Places',
    'sub_admin'                         => 'Sub Admin (Country Admin)',
    'user_management'                   => 'User Management',
    'commission_management'             => 'Commission Management',
    'announcements_management'          => 'Announcements Management',
    'discount_management'               => 'Discount Management',
    'promocode_management'              => 'Promocode Management',
    'coins_management'                  => 'Coins Management',
    'payment_management'                => 'Payment Management',
    'user_transactions'                 => 'User Transactions',
    'company_transactions'              => 'Company Transactions',
    'commissions'                       => 'Commissions',
    'reports_management'                => 'Reports Management',
    'user_support_management'           => 'User Support Management',
    'user_support_request'              => 'User Support Request',
    'list_of_support_users'             => 'List Of Support Users',
    'notifications'                     => 'Notifications',
    'settings'                          => 'Settings',
];
