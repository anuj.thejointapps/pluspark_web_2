<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Country Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'countries'     => 'Countries',
    'country'       => 'Country',
    'add_country'   => 'Add Country',
    'name'          => 'Name',
    'country_code'  => 'Country Code',
    'cities'        => 'Cities',
    'action'        => 'Action',
    'edit'          => 'Edit',
    'back'          => 'Back',
    'country_name'  => 'Country Name',
    'country_code'  => 'Country Code',
    'update'        => 'Update',
    'create'        => 'Create',
];
