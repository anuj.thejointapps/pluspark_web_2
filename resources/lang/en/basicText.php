<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Basic Text Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'id'                        => 'Id',
    'mobile'                    => 'Mobile',
    'email'                     => 'Email',
    'date'                      => 'Date',
    'joining'                   => 'Joining',
    'parkings'                  => 'Parkings',
    'parking'                   => 'Parking',
    'owned'                     => 'Owned',
    'basic'                     => 'Basic',
    'details'                   => 'Details',
    'company'                   => 'Company',
    'name'                      => 'Name',
    'contact'                   => 'Contact',
    'person'                    => 'Person',
    'address'                   => 'Address',
    'd_o_j'                     => 'D.O.J.',
    'no_of_parking'             => 'Number of Parkings',
    'notes'                     => 'Notes',
    'please_enter'              => 'Please Enter',
    'code'                      => 'Code',
    'of'                        => 'of',
    'your'                      => 'your',
    'number'                    => 'number',
    'atleast_10_char_mobile'    => 'Your mobile must be at least 10 characters long',
    'atleast_5_char'            => 'should be at atleast 5 character',
    'is_required'               => 'is required',
    'already_in_record'         => 'already in record',
    'management'                => 'Management',
    'manage'                    => 'Manage',
    'washer'                    => 'Washer',
    'alloted_to'                => 'alloted to',
    'region'                    => 'Region',
    'delete_confirm'            => 'Are you sure you want to delete',
    'this'                      => 'this',
];
