<?php

return [
	'copy' 	=> 'Copy',
	'csv' 	=> 'Csv',
	'excel' => 'Excel',
	'pfd' 	=> 'PDF',
	'print' => 'Print'
];