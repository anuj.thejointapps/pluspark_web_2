<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Country Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'currencies'        => 'Currencies',
    'currency'          => 'Currency',
    'symbol'            => 'Symbol',
    'code'              => 'Code',
];
