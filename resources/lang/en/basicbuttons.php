<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Country Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'action'        => 'Action',
    'edit'          => 'Edit',
    'back'          => 'Back',
    'update'        => 'Update',
    'create'        => 'Create',
    'add'           => 'Add',
    'select'        => 'Select',
    'from_date'     => 'From Date',
    'to_date'       => 'To Date',
    'filter'        => 'Filter',
    'close'         => 'Close',
    'save'          => 'Save',
    'view'          => 'View',
    'yes'           => 'Yes',
    'no'            => 'No',
];
