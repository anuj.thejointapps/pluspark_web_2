<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Country Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'currencies'        => 'العملات',
    'currency'          => 'عملة',
    'symbol'            => 'رمز',
    'code'              => 'رمز',
];
