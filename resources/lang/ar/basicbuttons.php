<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Basic Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'action'        => 'عمل',
    'edit'          => 'تعديل',
    'back'          => 'عودة',
    'update'        => 'تحديث',
    'create'        => 'يخلق',
    'add'           => 'يضيف',
    'select'        => 'يختار',
    'from_date'     => 'من التاريخ',
    'to_date'       => 'حتي اليوم',
    'filter'        => 'منقي',
    'close'         => 'قريب',
    'save'          => 'يحفظ',
    'view'          => 'رأي',
    'yes'           => 'نعم',
    'no'            => 'رقم',
];
