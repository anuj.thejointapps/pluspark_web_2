<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Country Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'countries'     => 'بلدان',
    'country'       => 'دولة',
    'add_country'   => 'أضف الدولة',
    'name'          => 'اسم',
    'country_code'  => 'الرقم الدولي',
    'cities'        => 'مدن',
    'action'        => 'عمل',
    'edit'          => 'تعديل',
    'back'          => 'عودة',
    'country_name'  => 'اسم الدولة',
    'country_code'  => 'الرقم الدولي',
    'update'        => 'تحديث',
    'create'        => 'يخلق',
];
