<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Management Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'company'       => 'شركة',
    'management'    => 'إدارة',
];
