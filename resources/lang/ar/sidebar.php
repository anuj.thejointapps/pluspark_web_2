<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'profile'                           => 'الملف الشخصي',
    'sign_out'                          => 'اخروج',
    'notifications'                     => 'إشعارات',
    'dashboard'                         => 'لوحة القيادة',
    'country_and_city_management'       => 'إدارة البلد والمدينة',
    'country'                           => 'دولة',
    'city'                              => 'دينة',
    'currency'                          => 'عملة',
    'company_management'                => 'إدارة الشركة',
    'washer_management'                 => 'إإدارة الغسالة',
    'washer_list'                       => 'قائمة الغسالة',
    'company_with_washer_service'       => 'شركة مع خدمة الغسالة',
    'brand'                             => 'ماركة',
    'color'                             => 'اللون',
    'model'                             => 'نموذج',
    'popular_events_spaces_mgmt'        => 'الفعاليات الشعبية إدارة المساحات',
    'popular_places'                    => 'أماكن شعبية',
    'sub_admin'                         => 'االإدارة الفرعية (إدارة الدولة)',
    'user_management'                   => 'إدارةالمستخدم',
    'commission_management'             => 'إدارة العمولة',
    'announcements_management'          => 'إدارة الإعلانات',
    'discount_management'               => 'إدارة الخصم',
    'promocode_management'              => 'إدارة بروموكود',
    'coins_management'                  => 'إدارة العملات',
    'payment_management'                => 'إدارة الدفع',
    'user_transactions'                 => 'معاملات المستخدم',
    'company_transactions'              => 'معاملات الشركة',
    'commissions'                       => 'اللجان',
    'reports_management'                => 'إدارة التقارير',
    'user_support_management'           => 'إدارة دعم المستخدم',
    'user_support_request'              => 'طلب دعم المستخدم',
    'list_of_support_users'             => 'قائمة مستخدمي الدعم',
    'notifications'                     => 'إشعارات',
    'settings'                          => 'إعدادات',
];
