<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Basic Text Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by country.
    |
    */

    'id'                        => 'بطاقة تعريف',
    'mobile'                    => 'متحرك',
    'email'                     => 'بريد الالكتروني',
    'date'                      => 'تاريخ',
    'joining'                   => 'انضمام',
    'parkings'                  => 'مواقف سيارات',
    'parking'                   => 'موقف سيارات',
    'owned'                     => 'مملوكة',
    'basic'                     => 'أساسي',
    'details'                   => 'تفاصيل',
    'company'                   => 'شركة',
    'name'                      => 'اسم',
    'contact'                   => 'اتصل',
    'person'                    => 'شخص',
    'address'                   => 'تبوك',
    'd_o_j'                     => 'وزارة العدل',
    'no_of_parking'             => 'عدد المواقف',
    'notes'                     => 'ملحوظات',
    'please_enter'              => 'تفضل',
    'code'                      => 'رمز',
    'of'                        => 'من',
    'your'                      => 'لك',
    'number'                    => 'عدد',
    'atleast_10_char_mobile'    => 'يجب أن يتكون هاتفك المحمول من 10 أحرف على الأقل',
    'atleast_5_char'            => 'يجب ألا يقل عدد الأحرف عن 5 أحرف',
    'is_required'               => 'مطلوب',
    'already_in_record'         => 'بالفعل في التسجيل',
    'management'                => 'إدارة',
    'manage'                    => 'يدير',
    'washer'                    => 'غسالة',
    'alloted_to'                => 'مخصصة ل',
    'region'                    => 'منطقة',
    'delete_confirm'            => 'هل أنت متأكد أنك تريد حذف',
    'this'                      => 'هذه',
];
